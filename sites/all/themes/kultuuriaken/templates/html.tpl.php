<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $html_attributes:  String of attributes for the html element. It can be
 *   manipulated through the variable $html_attributes_array from preprocess
 *   functions.
 * - $html_attributes_array: An array of attribute values for the HTML element.
 *   It is flattened into a string within the variable $html_attributes.
 * - $body_attributes:  String of attributes for the BODY element. It can be
 *   manipulated through the variable $body_attributes_array from preprocess
 *   functions.
 * - $body_attributes_array: An array of attribute values for the BODY element.
 *   It is flattened into a string within the variable $body_attributes.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see bootstrap_preprocess_html()
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup templates
 */
?><!DOCTYPE html>
<html<?php print $html_attributes;?><?php print $rdf_namespaces;?>>
<head>
  <link rel="profile" href="<?php print $grddl_profile; ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="web_author" content="Trinidad Wiseman OÜ www.twn.ee">
  <meta name="viewport" content="width=device-width, user-scalable=no" />
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
  <!-- New google maps api key -->
  <script type="text/javascript" language="javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true&v=3&key=AIzaSyDkkYKEHizpkqQX3Ji4OLuO9nhtBp5fr0E&libraries=places&language=<?php print $language->language; ?>"></script>
  <?php print $scripts; ?>
  <script>
  var locale = {
     "default": {
          "direction": "ltr",
          "format": "DD.MM.YYYY",
          "separator": " - ",
          "applyLabel": "<?php print ka_t('Apply'); ?>",
          "cancelLabel": "<?php print ka_t('Cancel'); ?>",
          "fromLabel": "<?php print ka_t('From'); ?>",
          "toLabel": "<?php print ka_t('To'); ?>",
          "customRangeLabel": "Custom",
          "daysOfWeek": [
              "<?php print ka_t('Su'); ?>",
              "<?php print ka_t('Mn'); ?>",
              "<?php print ka_t('Tu'); ?>",
              "<?php print ka_t('We'); ?>",
              "<?php print ka_t('Th'); ?>",
              "<?php print ka_t('Fr'); ?>",
              "<?php print ka_t('Sa'); ?>"
          ],
          "monthNames": [
              "<?php print ka_t('January'); ?>",
              "<?php print ka_t('February'); ?>",
              "<?php print ka_t('March'); ?>",
              "<?php print ka_t('April'); ?>",
              "<?php print ka_t('May'); ?>",
              "<?php print ka_t('June'); ?>",
              "<?php print ka_t('July'); ?>",
              "<?php print ka_t('August'); ?>",
              "<?php print ka_t('September'); ?>",
              "<?php print ka_t('October'); ?>",
              "<?php print ka_t('November'); ?>",
              "<?php print ka_t('December'); ?>"
          ],
          "firstDay": 1
      }
  }
  </script>
</head>
<body<?php print $body_attributes; ?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
