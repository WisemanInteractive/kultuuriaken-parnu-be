<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
   <?php if (!empty($title) || !empty($caption)) : ?>
     <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <th></th>
        <th>
          <label>
             <span class="customCheckbox">
                <input name="checkAll" type="checkbox">
                <span class="indicator"></span>
             </span><!--/customRadio-->
          </label>
        </th>
        <?php foreach ($header as $field => $label): ?>
          <?php if($field === 'view') continue; ?>
          <?php if($field === 'field_event_date' || $field === 'views_conditional'): ?>
            <th class="header headerSortUp">
              <a data-organiser-sort="TRUE" ><?php print $label; ?>  <i></i></a>
            </th>
          <?php else: ?>
          <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?> scope="col">
            <?php print $label; ?>
          </th>
          <?php endif; ?>
        <?php endforeach; ?>
          <th></th>
      </tr>
    </thead>
  <?php endif; ?>

  <tbody class="organizer-events-rows">
    <?php foreach ($rows as $row_count => $row): 
    $row_id = $view->current_display . '_' . $row_count;
    $actions_printed = FALSE;
    ?>
      <?php $top_event = !empty($row['view']) ? 'status-ok' : ''; ?>
      <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
        <td></td>
        <td>
          <label>
            <span class="customCheckbox">
              <input name="row[<?php print $row_count; ?>]" type="checkbox">
              <span class="indicator"></span>
            </span><!--/customRadio-->
          </label>
        </td>
        <?php foreach ($row as $field => $content): ?>
          
          <?php if($field === 'view'): 
            $actions_printed = TRUE;
          ?>
            <td>
              <div class="link after-arrow_down" title="#<?php print $row_id; ?>" rel="interact" data-addonclass="linkList"><?php print ka_t('Actions'); ?></div>
              <div id="<?php print $row_id; ?>" class="title-content">
                 <a data-row="<?php print $row_id; ?>" href="" class="ka-add-same link secondary"><?php print ka_t('Add same'); ?></a>
                 <?php if($view->current_display === 'events'): ?>
                   <a data-row="<?php print $row_id; ?>" href="" class="ka-add-subevent link secondary"><?php print ka_t('Add sub-event'); ?></a>
                 <?php endif; ?>
                 <a data-row="<?php print $row_id; ?>" href="" class="ka-change link secondary"><?php print ka_t('Change'); ?></a>
                 <a data-row="<?php print $row_id; ?>" href="" class="ka-delete link secondary"><?php print ka_t('Delete'); ?></a>
              </div><!--/title-content-->
              <div class="ka-hidden" data-row-form="<?php print $row_id; ?>">
              <?php print render($item_menu[$row_count]); ?>
              </div>
            </td>
            <?php print $content; ?>
          <?php else: ?>
          <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
            <?php print $content; ?>
          </td>
          <?php endif; ?>
        <?php endforeach; ?>
        <?php if(!$actions_printed): ?>
          <td>
            <div class="link after-arrow_down" title="#<?php print $row_id; ?>" rel="interact" data-addonclass="linkList"><?php print ka_t('Actions'); ?></div>
            <div id="<?php print $row_id; ?>" class="title-content">
               <a data-row="<?php print $row_id; ?>" href="" class="ka-add-same link secondary"><?php print ka_t('Add same'); ?></a>
               <?php if($view->current_display === 'events'): ?>
                 <a data-row="<?php print $row_id; ?>" href="" class="ka-add-subevent link secondary"><?php print ka_t('Add sub-event'); ?></a>
               <?php endif; ?>
               <a data-row="<?php print $row_id; ?>" href="" class="ka-change link secondary"><?php print ka_t('Change'); ?></a>
               <a data-row="<?php print $row_id; ?>" href="" class="ka-delete link secondary"><?php print ka_t('Delete'); ?></a>
            </div><!--/title-content-->
            <div class="ka-hidden" data-row-form="<?php print $row_id; ?>">
            <?php print render($item_menu[$row_count]); ?>
            </div>
          </td>
        <?php endif; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
