<?php

/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
  
<?php endif; ?>

<div class="row customWidths-1">
  <div class="col-4">
    <div class="form-item rounded-corners">
      <i class="after-search"></i>
      <div class="form-item_title">&nbsp;</div>
      <?php print $widgets['filter-combine']->widget; ?>
    </div>
  </div>

  <div class="col-2">
    <div class="form-item rounded-corners">
      <i class="after-calendar"></i>
      <div class="form-item_title">&nbsp;</div>
      <?php print $widgets['filter-period']->widget; ?>
    </div>
  </div>
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title"><?php print ka_t('Filter'); ?></div>
      <?php print $widgets['filter-field_category_target_id']->widget; ?>
    </div>
  </div>
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title"><?php print ka_t('Filter'); ?></div>
      <?php if(isset($widgets['filter-field_ka_status_value_i18n'])): ?>
        <?php print $widgets['filter-field_ka_status_value_i18n']->widget; ?>
      <?php endif; ?>
      <?php if(isset($widgets['filter-field_ka_status_value'])): ?>
        <?php print $widgets['filter-field_ka_status_value']->widget; ?>
      <?php endif; ?>
    </div><!--/form-item-->
  </div>

  <?php if (!empty($sort_by)): ?>
    <div class="ka-hidden views-exposed-widget views-widget-sort-by">
      <?php print $sort_by; ?>
    </div>
    <div class="ka-hidden views-exposed-widget views-widget-sort-order">
      <?php print $sort_order; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($items_per_page)): ?>
    <div class="ka-hidden views-exposed-widget views-widget-per-page">
      <?php print $items_per_page; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($offset)): ?>
    <div class="views-exposed-widget views-widget-offset">
      <?php print $offset; ?>
    </div>
  <?php endif; ?>
  <div class="views-exposed-widget views-submit-button">
    <?php //print $button; ?>
    <?php print render($form['submit']); ?>
  </div>
  <?php if (!empty($reset_button)): ?>
    <div class="views-exposed-widget views-reset-button">
      <?php print $reset_button; ?>
    </div>
  <?php endif; ?>
</div>