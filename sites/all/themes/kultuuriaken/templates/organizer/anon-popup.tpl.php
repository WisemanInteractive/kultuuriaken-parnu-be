<?php if(!empty($anon_message_html)): ?>
  <?php print $anon_message_html['value']; ?>
<?php endif; ?>

<div class="row">
  <div class="col-12">
    <center>
        <a href="<?php if($obj_type === 'activity') {print url('add/activity');} else {print url('add/event');} ?>" class="link secondary"><?php print ka_t('Continue anonymously'); ?></a>
        <a href="<?php print url('organiser'); ?>" class="btn"><?php print ka_t('Log in'); ?></a>
    </center>
  </div><!--/col-12-->
</div><!--/row-->