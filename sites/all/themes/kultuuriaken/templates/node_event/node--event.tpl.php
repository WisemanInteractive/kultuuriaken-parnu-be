<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="inline <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row sm-hide">
          <div class="col-12">
            <ul class="breadcrumbs">
              <li><a href="<?php print url(); ?>"><?php print ka_t('Home page'); ?></a></li>
              <li><span class="before-arrow_right"></span></li>
              <li><a href="<?php print url('events'); ?>"><?php print ka_t('Events'); ?></a></li>
            </ul>
          </div>
          <!--/col-12-->
        </div>
  <!--/row-->

  <div class="row no-margin sm-overflow">

    <div class="col-6 stretch-content sm-hide">
            
            <?php if(!empty($content['field_main_image'])): ?>
            <div class="image" style="background-image:url(<?php print image_style_url('detail_main_image', $content['field_main_image']['#items'][0]['uri']); ?>)">
              <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" />
              
              <?php if(isset($main_categories)): ?>
              <ul class="tags filled">
              <?php foreach($main_categories as $main_category): 
              $tag_id = !empty($main_category->field_color_id[LANGUAGE_NONE][0]['value']) ? 
                $main_category->field_color_id[LANGUAGE_NONE][0]['value'] : tid_to_tag_id($main_category->tid);
              ?>
                <li class="tag-<?php print $tag_id; ?>"><a href=""><?php print strtolower($main_category->name); ?></a></li>
              <?php endforeach; ?>
              </ul>
              <?php endif; ?>
            </div>
            <!--/image-->
            <?php endif; ?>
            
          </div><!--/col-6-->
            
    <div class="sm-12 stretch-content sm-show">
      <div class="swiper-container swiper-inline" data-plugin="slideshow">
        <?php if(isset($main_category)): ?>
        <ul class="tags filled">
           <li class="tag-<?php print tid_to_tag_id($main_category->tid); ?>"><a href=""><?php print strtolower($main_category->name); ?></a></li>
        </ul>
        <?php endif; ?>
        <div class="swiper-holder">
          <div class="swiper-wrapper">
            <?php if(!empty($content['field_main_image'])): ?>
            <div class="swiper-slide">
              <div class="image" style="background-image:url(<?php print image_style_url('detail_main_image', $content['field_main_image']['#items'][0]['uri']); ?>)">
                <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" /> 
              </div><!--/image-->
            </div><!--/swiper-slide-->
            <?php endif; ?>
            <?php if(!empty($content['field_gallery']['#items'])): ?>
              <?php foreach($content['field_gallery']['#items'] as $item): ?>
                <div class="swiper-slide">
                  <div class="image" style="background-image:url(<?php print image_style_url('detail_main_image', $item['uri']); ?>)">
                    <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" /> 
                  </div><!--/image-->
                </div><!--/swiper-slide-->
              <?php endforeach; ?>
            <?php endif; ?>
          </div><!--/swiper-wrapper-->
        </div><!--/swiper-holder-->
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
	  		<div class="swiper-button-prev"></div>
      </div><!--/swiper-container-->
    </div><!--/col-12-->
    
    <div class="col-6 stretch-content sm-12">
      <div class="block-gray">
        <h1><?php print $title; ?></h1>
        <?php if(!empty($event_dates['current'])): ?>
        <?php if(!empty($event_dates['current']['event_location']['name'])): ?>
        <p class="highlight-1 size-xl sm-hide"><?php print check_plain($event_dates['current']['event_location']['name']); ?></p>
        <p class="size-xl sm-show"><?php print check_plain($event_dates['current']['event_location']['name']); ?> <a href="#map" class="before-location link pull-right"><?php print ka_t('view map'); ?></a></p>
        <?php endif; ?>
        <?php if(!empty($event_dates['current']['event_date'])): ?>
        <p class="size-xl">
          <b class="highlight-1"><?php print format_date($event_dates['current']['event_date'], 'weekday'); ?></b> 
          <?php print format_date($event_dates['current']['event_date'], 'detailed_date'); ?>

          <?php if(!empty($event_dates['current']['event_date_to']) && $event_dates['current']['event_date_to'] !== $event_dates['current']['event_date']): ?>
            <?php if(date('Y-m-d', $event_dates['current']['event_date']) === date('Y-m-d', $event_dates['current']['event_date_to'])): // Event ends on the same day ?>
              <?php if(!$event_dates['current']['end_date_time_unset']): ?>
              - <?php print format_date($event_dates['current']['event_date_to'], 'time'); ?>
              <?php endif; ?>
            <?php else: // Event ends on a different day ?>
            - <b class="highlight-1"><?php print format_date($event_dates['current']['event_date_to'], 'weekday'); ?></b>
              <?php if(format_date($event_dates['current']['event_date_to'], 'time') === '00:00' || $event_dates['current']['end_date_time_unset']): ?>
                <?php print format_date($event_dates['current']['event_date_to'], 'detailed_date_no_time'); ?>
              <?php else: ?>
                <?php print format_date($event_dates['current']['event_date_to'], 'detailed_date'); ?>
              <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>
          
          <?php if(!empty($event_dates['upcoming'])): ?>
          <a href="#other-dates"><?php print ka_t('+@count more', array('@count' => count($event_dates['upcoming']))); ?></a>
          <?php endif; ?>
          
        </p>
        <?php endif; ?>

          <?php if(!empty($event_dates['current']['price']) && ($ticket_availability && $ticket_availability['type'] !== 'link' || empty($ticket_availability['value']))): ?>
            <p class="size-xl"><?php print $event_dates['current']['price']; ?></p>
          <?php endif; ?>

          <?php if(isset($field_event_ticket_info_text)): ?>
            <p><?php print $field_event_ticket_info_text; ?></p>
          <?php endif; ?>
          
          <?php if(!empty($ticket_availability['type']) && $ticket_availability['type'] === 'other' && !empty($ticket_availability['value'])): ?>
            <p><?php print check_plain($ticket_availability['value']); ?></p>
          <?php endif; ?>

          <?php if(!empty($add_to_calendar)): ?>
            <?php print drupal_render($add_to_calendar); ?>
          <?php endif; ?>
        
        <?php endif; ?>

        <?php if(!empty($ticket_availability['type'])): ?>
        <?php switch($ticket_availability['type']):
          case 'link': ?>
            <?php if(!empty($ticket_availability['value'])): ?>
            <a target="_blank" href="<?php print check_url($event_dates['current']['ticket_link']); ?>" class="btn pull-right sm-hide"><?php print ka_t('Proceed to buy (@price)', array('@price' => $event_dates['current']['price'])); ?></a>
            <a target="_blank" href="<?php print check_url($event_dates['current']['ticket_link']); ?>" class="btn sm-show full-width"><?php print ka_t('Proceed to buy (@price)', array('@price' => $event_dates['current']['price'])); ?></a>
            <?php endif; ?>
            <?php break; ?>
          <?php case 'ticket_office': break; ?>
        <?php endswitch; ?>
        <?php endif; ?>
      </div><!--/block-gray-->

    </div><!--/col-6-->
  </div>
  <!--/row-->

  <div class="row sm-overflow">
    <div class="col-12">
      <div class="block">
        <div class="row">
          <div class="col-7 sm-12">

                  <article>
                    <?php if(!empty($content['field_gallery'])): ?>
                    <h5 class="sm-hide"><?php print ka_t('Gallery'); ?></h5>

                    <div class="row sm-hide" data-plugin="gallery">
                      <?php foreach($content['field_gallery']['#items'] as $item): ?>
                      <div class="col-2">
                        <a href="<?php print image_style_url('detail_image', $item['uri']); ?>" class="image" 
                        style="background-image:url(<?php print image_style_url('gallery_thumbnail', $item['uri']); ?>)">
                          <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-1.gif"; ?>">
                        </a>
                      </div>
                      <!--/col-2-->
                      <?php endforeach; ?>
                      <?php if(!empty($content['field_video']) && isset($video_thumbnail_url)): ?>
                      <div class="col-2">
                        <a href="<?php print check_url($video_url); ?>" class="image before-play" 
                        style="background-image:url(<?php print check_url($video_thumbnail_url); ?>)">
                          <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-1.gif"; ?>">
                        </a>
                      </div>
                      <?php endif; ?>
                    </div>
                    <!--/row-->
                    <?php endif; ?>
                    
                    <h5 class="sm-hide"><?php print ka_t('Description'); ?></h5>
                    
                    <div class="text-wrapper" data-class="'full-text' : moreText">
                      <?php if(!empty($content['field_summary'])): ?>
                      <p><b><?php print html_entity_decode(nl2br($content['field_summary'][0]['#markup'])); ?></b></p>
                      <?php endif; ?>
                      <?php if(!empty($content['field_body'])): ?>
                      <p><?php print html_entity_decode(nl2br($content['field_body'][0]['#markup'])); ?></p>
                      <?php endif; ?>
                      <?php if(!empty($content['field_additional_info_link'])): ?>
                      <p><?php print ka_t('Read more'); ?>: <a target="_blank" href="<?php print urldecode(check_url($content['field_additional_info_link']['#items'][0]['url'])); ?>"><?php print urldecode(check_url($content['field_additional_info_link']['#items'][0]['url'])); ?></a></p>
                      <?php endif; ?>
                    </div>
                    <div class="link after-arrow_down sm-show" data-toggle="moreText" data-class="'hidden' : moreText"><?php print ka_t('Read more'); ?></div>
                  </article>
          </div><!--/col-7-->
          <div class="col-5 sm-12">
                  
              <div class="addthis-holder sm-show">
                <h5 class="half-margin"><?php print ka_t('Share'); ?></h5>
                <a class="addthis_button_facebook btn circle before-facebook"></a>
                <a href="" class="addthis_button_twitter btn circle before-twitter"></a>
                <a href="" class="addthis_button_google_plusone_share btn circle before-googleplus"></a>
                <a href="" class="addthis_button_email btn circle before-mail"></a>
              </div><!--/sm-show-->
              
              <?php if(!empty($event_dates['current']['event_location']['latitude']) 
                    && !empty($event_dates['current']['event_location']['longitude'])): ?>
              <h5 class="half-margin"><?php print ka_t('Location on map'); ?></h5>

              <div id="map" class="map" data-plugin="googleMaps" data-lat="
              <?php print check_plain($event_dates['current']['event_location']['latitude']); ?>
              " data-lng="
              <?php print check_plain($event_dates['current']['event_location']['longitude']); ?>
              " data-icon="<?php print '/' . path_to_theme() . "/assets/imgs/location-default.png"; ?>">
                <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" class="placeholder">
              </div>
              <!--/image-->
              <?php endif; ?>
              
              <h5 class="half-margin"><?php print ka_t('Organiser'); ?></h5>

              <p>
                <?php if(!empty($content['field_organizer_name'][0]['#markup'])): ?>
                <b><?php print ka_t('Organisation'); ?></b>: <?php print check_plain($content['field_organizer_name'][0]['#markup']); ?><br />
                <?php endif; ?>
                <?php if(!empty($content['field_organizer_address'][0]['#markup'])): ?>
                <b><?php print ka_t('Address'); ?></b>: <?php print check_plain($content['field_organizer_address'][0]['#markup']); ?><br />
                <?php endif; ?>
                <?php if(!empty($content['field_organizer_contact'][0]['#markup'])): ?>
                <b><?php print ka_t('Contact'); ?></b>: <?php print check_plain($content['field_organizer_contact'][0]['#markup']); ?><br />
                <?php endif; ?>
                <?php if(!empty($content['field_organizer_phone'][0]['#markup'])): ?>
                <b><?php print ka_t('Phone'); ?></b>: <?php print check_plain($content['field_organizer_phone'][0]['#markup']); ?><br />
                <?php endif; ?>
                <?php if(!empty($content['field_organizer_email'])): ?>
                <b><?php print ka_t('E-Mail'); ?></b>: <?php print spamspan($content['field_organizer_email']['#items'][0]['email']); ?><br />
                <?php endif; ?>
                <?php if(!empty($content['field_organizer_www'])): ?>
                <a target="_blank" href="<?php print check_url($content['field_organizer_www']['#items'][0]['url']); ?>">
                  <?php print check_url($content['field_organizer_www']['#items'][0]['url']); ?>
                </a>
                <?php endif; ?>
              </p>
          </div><!--/col-5-->
        </div><!--/row-->
        <?php if(!empty($event_dates['upcoming'])): ?>
        <div class="row">
                <div class="col-12">
                  <?php if($roof_event): ?>
                  <h5 id="other-dates"><?php print ka_t('Sub-events') ?></h5>
                  <?php else: ?>
                  <h5 id="other-dates"><?php print ka_t('Other dates'); ?></h5>
                  <?php endif; ?>
                  
                  <table id="eventTable" class="main">
                    <thead>
                      <tr>
                        <th><?php print ka_t('Name') ?></th>
                        <th><?php print ka_t('Date') ?></th>
                        <th><?php print ka_t('Time') ?></th>
                        <th><?php print ka_t('Location') ?></th>
                        <th><?php print ka_t('Ticket') ?></th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php foreach($event_dates['upcoming'] as $id => $event): ?>
                        <tr class="eventItem">
                          <?php if(!empty($event['name'])): ?>
                          <td><b><a href="<?php print $event['url']; ?>"><?php print check_plain($event['name']); ?></a></b></td>
                          <?php else: ?>
                          <td><b><a href="<?php print $event['url']; ?>"><?php print $title; ?></a></b></td>
                          <?php endif; ?>
                          <td><?php print format_date($event['event_date'], 'table_date'); ?></td>
                          <td><?php print format_date($event['event_date'], 'time'); ?></td>
                          <td>
                            <?php if(!empty($event['event_location']['name'])): ?>
                            <?php print check_plain($event['event_location']['name']); ?>
                            <?php endif; ?>
                          </td>
                          <td><?php print check_plain($event['price']); ?></td>
                          <td>
                          <?php if(isset($event['availability_type'])): ?>
                          <?php switch($event['availability_type']):
                          case 'link': ?>
                            <?php if(!empty($event['availability_value'])): ?>
                            <a target="_blank" href="<?php print check_url($event['availability_value']); ?>" class="link after-arrow_right"><?php print ka_t('Proceed to buy'); ?></a>
                            <?php endif; ?>
                            <?php break; ?>
                          <?php case 'ticket_office': ?>
                            <?php print ka_t('Ticket booth (on location)'); 
                            break;
                            ?>
                          <?php case 'other': ?>
                            <?php if($event['availability_value']):
                            print check_plain($event['availability_value']);
                            endif;
                            break;
                            ?>
                          <?php endswitch; ?>
                          <?php endif;?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                  <?php if(count($event_dates['upcoming']) > 3): ?>
                  <center>
                    <a href="" id="loadMore" class="btn btn-inactive sm-stretch"><?php print ka_t('Load more') ?></a>
                  </center>
                  <?php endif; ?>
                </div>
                <!--/col-12-->
              </div>
        <!--/row-->
        <?php endif; ?>

      </div><!--/block-->
    </div><!--/col-12-->
  </div><!--/row-->

  <?php if(!empty($similar_events)): ?>
  <div class="row">
    <div class="col-12">
      <div class="row ">
        <div class="col-12">
          <h5><?php print ka_t('Similar events') ?></h5>
        </div><!--/col-12-->
      </div><!--/row-->
      <div class="row">
        <div class="col-12">
          <?php print $similar_events; ?>
        </div><!--/col-12-->
      </div><!--/row-->
    </div><!--/col-12-->
  </div>
  <?php endif; ?>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    ?>
</div>
<?php if($page && isset($content['field_ka_status']['#items'][0]['value']) && $content['field_ka_status']['#items'][0]['value'] === 'published'): ?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-596620478a40d533"></script>
<?php elseif(isset($content['field_ka_status']['#items'][0]['value']) && $content['field_ka_status']['#items'][0]['value'] === 'preview'): ?>
<div class="event_preview">
  <div class="event_preview-wrapper">
     <div class="inline">
        <div class="row">
           <div class="col-10">
              <h1><?php print $title; ?></h1> 
              <p><b><?php print t('Publish date', array(), array('langcode' => $_GET['lang'])); ?></b>: <?php echo date('d.m.y - H:i', $changed) ?></p>
              <p>
                <a data-target="preview-delete" class="link secondary half-margin"><?php print t('Close preview', array(), array('langcode' => $_GET['lang'])); ?></a>
              </p>
              <div id="preview-form">
                <?php print drupal_render($preview_form_submit); ?>
              </div>
           </div><!--/col-8-->
           
           <div class="col-2 text-right">
              <p>
                <?php print t('Check in entered languages', array(), array('langcode' => $_GET['lang'])); ?>: <br>
                <?php print $language_switcher_preview; ?>
                <!--<a href="" class="link dark active">EST</a>
                <a href="" class="link dark">ENG</a>
                <a href="" class="link dark">RUS</a>-->
              </p>
           </div><!--/col-4-->
        </div><!--/row-->
     </div><!--/inline-->
  </div><!--/event_preview-wrapper-->
</div>
<?php endif; ?>
