<?php 
	global $language;
	$lang = $language->language;
?>
<div class="full-banner" style="background-image:url(<?php print path_to_theme() . '/assets/imgs/front_pic.jpg'; ?>)">
	<div class="inline">
		<img alt="placeholder" src="<?php print path_to_theme() . '/assets/imgs/placeholder-31.gif'; ?>" class="placeholder">
		<div class="row">
			<div class="col-3 sm-hide"></div>
				<div class="col-6 sm-12">
					<div class="col-inline">
						<h3><?php print ka_t('Find quickly'); ?></h3>
						
							<form method="get" action="<?php print url('search'); ?>">
								
								<div class="quick-search">

									<div class="form-item rounded-corners">
										<input type="text" name="search" placeholder="<?php print ka_t('Search for events, activities, places'); ?>" data-plugin="autocomplete" data-url="/<?php print $lang; ?>/main/autocomplete">
									</div><!--/form-item-->
									
									<div class="form-item">
										<button class="btn"><?php print ka_t('Search'); ?></button>
									</div><!--/form-item-->
									
								</div><!--/quick-search-->
							<input type="hidden" name="node_type[event]"  value="event">
							<input type="hidden" name="node_type[activity]"  value="activity">
							</form>
							
							<center>
								<a href="<?php print url('events'); ?>" class="link shadow after-arrow_right"><?php print ka_t('See all events'); ?></a>

								<a href="<?php print url('activities'); ?>" class="link shadow after-arrow_right"><?php print ka_t('See all recreational activities'); ?></a>
							</center>
					</div>
				</div><!--/col-6-->
			<div class="col-3 sm-hide"></div>
		</div><!--/row-->
	</div><!--/inline-->
</div><!--/full-banner-->
		
<div class="inline">
		<?php if(isset($recommended_events) AND !empty($recommended_events) ): ?>
			<div class="row">
				<div class="col-12">
					<h3><?php print ka_t('Window of Culture recommends'); ?></h3>
				</div><!--/col-12-->
			</div><!--/row-->
			<div class="row">
				<div class="col-12">
					<?php print $recommended_events; ?>
				</div><!--/col-12-->
			</div><!--/row-->
			<?php endif; ?>
			<div class="row bottom-spacing">
				<div class="col-12">
					<div class="block">
						<h3><?php print ka_t('Event fields'); ?> <a href="<?php print url('events'); ?>" class="link pull-right after-arrow_right"><?php print ka_t('See all events'); ?></a></h3>
						<?php print $event_types; ?>
						<a href="<?php print url('events'); ?>" class="link sm-show"><?php print ka_t('+!count more', array('!count' => $event_types_count - 3)); ?></a>
					</div><!--/block-->
				</div><!--/col-12-->
			</div><!--/row-->
			<?php if(isset($upcoming_activities) AND !empty($upcoming_activities) ): ?>
				<div class="row">
					<div class="col-12">
						<h3><?php print ka_t('Upcoming recreational activities'); ?></h3>
					</div><!--/col-12-->
				</div><!--/row-->
				<div class="row">
					<div class="col-12">
						<?php print $upcoming_activities; ?>
					</div><!--/col-12-->
				</div>
			<?php endif; ?>
			<div class="row bottom-spacing">
				<div class="col-12">
					<div class="block">
						<h3><?php print ka_t('Recreational activity fields'); ?> <a href="<?php print url('activities'); ?>" class="link pull-right after-arrow_right"><?php print ka_t('See all recreational activities'); ?></a></h3>
						<?php print $activity_types; ?>
						<a href="<?php print url('activities'); ?>" class="link sm-show"><?php print ka_t('+!count more', array('!count' => $activity_types_count - 3)); ?></a>
					</div><!--/block-->
				</div><!--/col-12-->
			</div><!--/row-->
			
		</div><!--/inline-->
		
<?php print $image_banners; ?>
		
<div class="newslist sm-hide">
			<div class="inline">
				<div class="row">
					<div class="col-2"></div>
					<div class="col-8">
						<?php // print variable_get_value('join_newsletter_title')['value']; ?>
						<?php // print variable_get_value('join_newsletter_body')['value']; ?>
						<?php if($newsletter_subscribe_form): ?>
							<?php print $newsletter_subscribe_form['content']; ?>
						<?php endif; ?>
					</div><!--/col-8-->
					<div class="col-2"></div>
				</div><!--/row-->
			</div><!--/inline-->
		</div><!--/newslist-->

<div class="accent-background">
			<div class="inline">
				<div class="row">
					<div class="col-12">
						<h3><?php print ka_t('What is Window of Culture?'); ?></h3>
					</div><!--/col-12-->
				</div><!--/row-->
				<?php print $text_banners; ?>
			</div><!--/inline-->
		</div><!--/accent_background-->
		
<div class="inline">
			<div class="row">
				<div class="col-12">
					<?php print $sponsors; ?>
				</div><!--/col-12-->
			</div><!--/row-->
		</div><!--/inline-->