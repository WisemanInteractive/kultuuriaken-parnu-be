<div class="inline">
  <div class="image-404">
     <img src="/<?php print path_to_theme() . "/assets/imgs/404-image.svg"; ?>" />
     <span class="image-404-text">
        <h1><?php print ka_t("Sorry!"); ?> <br /><?php print ka_t('Page was not found'); ?>...</h1>

        <p><?php print ka_t("It seems the page you searched for no longer exists."); ?></p>
     </span><!--/image-404-text-->
  </div><!--/404-image-->
  
  <center>
     <a onclick="goBack()" href="<?php print referer_path(); ?>" class="btn btn-inactive min-width"><?php print ka_t('Back'); ?></a>
     <a href="<?php print url(); ?>" class="btn min-width"><?php print ka_t('Back to home page'); ?></a>
  </center>
</div>
<script>
function goBack() {
    window.history.back();
}
</script>