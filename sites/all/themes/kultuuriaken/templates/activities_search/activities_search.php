<?php 
	global $language;
	$lang = $language->language;
?>
<div class="inline">
  <div class="row sm-hide">
     <div class="col-12">
        <ul class="breadcrumbs">
           <li><a href="<?php print url(); ?>">Avaleht</a></li>
           <li><span class="before-arrow_right"></span></li>
           <li><a href="<?php print url('activities'); ?>"><?php print ka_t('Recreational activities'); ?></a></li>
        </ul>
     </div><!--/col-12-->
  </div><!--/row-->
  <div class="row">
     <div class="col-12">
        <h1><?php print ka_t('Recreational activities'); ?></h1>
     </div><!--/col-12-->
  </div><!--/row-->
         
  <div class="row" data-plugin="listFilter">
    <div class="col-3 sm-12 md-12">
      <div class="filter-tags block sm-show" data-plugin="stickyTags">
          <span class="toggler after-arrow_up"></span>
          <div class="link secondary after-close "><?php print ka_t('Cancel all filters'); ?></div>
          <ul class="tags" rel="tags"> </ul>
      </div><!--filter-tags-->
      <div class="block no-padding">
        <div class="filter-button sm-show before-filter after-arrow_down" id="filter-trigger">
          <b><?php print ka_t('Filter results'); ?></b>
        </div><!--/filter-button-->
        
        <div class="filter-wrapper" data-plugin="filterAccordion">
          
          <div class="filter-button sm-show before-filter after-close" rel="closeFilter">
            <b><?php print ka_t('Filter results'); ?></b>
          </div><!--/filter-button-->
          
          <div class="filter-scroller">
            <div class="row sm-show">
              <div class="col-12 text-right reset">
                  <div class="link secondary after-close "><?php print ka_t('Cancel all filters'); ?></div>
              </div><!--/col-12-->
            </div><!--/row-sm-show-->
                        
            <div class="row sm-show">
              <div class="col-10">
                <div class="form-item rounded-corners">
                  <input type="text" name="title" placeholder="<?php print ka_t('Search by keyword'); ?>" />
                </div><!--/form-item-->
              </div><!--/col-10-->
              <div class="col-2">
                <button class="btn stretch before-search"></button>
              </div><!--/col-2-->
            </div><!--/row--> <?php // Märksõna ?>
            
            <ul class="filter">
              
              <li class="reset sm-hide">
                <div class="link secondary after-close"><?php print ka_t('Cancel all filters'); ?></div>
              </li>	  	              
              <li class="sm-show active">
                <div class="filter-title before-calendar" rel="trigger"><?php print ka_t('Date'); ?></div>
                <ul>
                  <li class="range-date-wrapper">
                    <div class="form-item rounded-corners">
                      <i class="after-calendar"></i>
                      <div class="form-item_title"><?php print ka_t('Start date'); ?></div>
                      <input data-plugin="datepickerSingle" placeholder="<?php print ka_t('Select date'); ?>" type="text" name="date-1" rel="start" data-cancel="starting_time"/>
                    </div><!--/form-item-->
                    <div class="form-item rounded-corners">
                      <i class="after-calendar"></i>
                      <div class="form-item_title"><?php print ka_t('End date'); ?></div>
                      <input data-plugin="datepickerSingle" placeholder="<?php print ka_t('Select date'); ?>" type="text" name="date-2" rel="end" data-cancel="starting_time"/>
                    </div><!--/form-item-->
                  </li>
                </ul>
              </li> <?php // Kuupäev ?>
              
              <?php if(!empty($activity_categories)): ?>                          
	            <li class="active">
	              <div class="filter-title sm-hide" rel="trigger"><?php print ka_t('Select field'); ?></div>
	              <div class="filter-title sm-show before-tags" rel="trigger"><?php print ka_t('Field'); ?></div>
	              
	              <ul>
	                <?php foreach($activity_categories as $activity_category):
	                  if($activity_category->parents[0] !== '0') continue;
	                  $default_category = !empty($default_category_tid) && $default_category_tid === $activity_category->tid ? TRUE : FALSE;
	                  $count = isset($activity_category_summary[$activity_category->tid]) ? $activity_category_summary[$activity_category->tid]  : 0;
	                ?>
	                <li <?php //if($default_category) print 'class="active"' ?>>
	                <div class="form-item" <?php if(!empty($activity_subcategories[$activity_category->tid])) print 'rel="trigger"'; ?>>
	                   <label>
	                      <span class="customCheckbox">
	                         <input name="category_<?php print $activity_category->tid; ?>" value="all" type="checkbox" checked>
	                         <span class="indicator"></span>
	                      </span><!--/customRadio-->
	                      <span class="label-title"><?php print $activity_category->name; ?> (<?php print $count; ?>)</span>
	                   </label>
	                </div><!--/form-item-->
	                <?php if(!empty($activity_subcategories[$activity_category->tid])): 
	                  $subcategories = $activity_subcategories[$activity_category->tid];
	                ?>
	                <ul>
	                  <?php foreach($subcategories as $num => $subcategory): 
	                    $count = isset($activity_category_summary[$subcategory->tid]) ? $activity_category_summary[$subcategory->tid] : 0;
	                  ?>
	                  
	                  <li <?php if($num >= 3) print 'class="hidden"'; ?>>
	                    <div class="form-item">
	                       <label>
	                          <span class="customCheckbox">
	                             <input name="category_<?php print $activity_category->tid; ?>" value="<?php print $subcategory->tid; ?>" type="checkbox" checked>
	                             <span class="indicator"></span>
	                          </span><!--/customRadio-->
	                          <span class="label-title"><?php print $subcategory->name; ?> (<?php print $count; ?>)</span>
	                       </label>
	                    </div><!--/form-item-->
	                  </li>
	                  <?php endforeach; ?>
	                  <?php if(count($subcategories) > 3): ?>
	                  <li class="show-more">
	                    <div class="link after-plus" data-show_less="Näita vähem" data-show_more="Näita veel" ><?php print ka_t('Show more'); ?></div>
	                  </li>
	                  <?php endif; ?>
	                </ul>
	                <?php endif; ?>
	              </li>
	                <?php endforeach; ?>
	              </ul>
	            </li> <?php // Valdkond ?>
              <?php endif; ?>
              
              <li>
                <div class="filter-title sm-hide" rel="trigger"><?php print ka_t('Select price'); ?></div>
                <div class="filter-title sm-show before-money" rel="trigger"><?php print ka_t('Price'); ?></div>
                <ul>
                  <li class="slider-range-wrapper">
                    <div class="form-item">
                       <label>
                          <span class="customCheckbox">
                             <input name="cost" value="1" type="checkbox">
                             <span class="indicator"></span>
                          </span><!--/customRadio-->
                          <span class="label-title"><?php print ka_t('Free'); ?></span>
                       </label>
                    </div><!--/form-item-->
                    
                    <div class="form-item">
                       <div class="form-item_title"><?php print ka_t('Price range'); ?>:</div>
                       <input type="hidden" name="priceMin" rel="min" value="0"><input type="hidden" name="priceMax" value="40" rel="max">
                    
                       <div class="slider-range" data-plugin="sliderRange" data-min="0" data-max="50" data-unit="€"></div>
                    </div><!--/form-item-->
                                  
                  </li>
                </ul>
              </li> <?php // Hind ?>
              
              <li>
                <div class="filter-title sm-hide" rel="trigger"><?php print ka_t('Select target group'); ?></div>
                <div class="filter-title sm-show before-users" rel="trigger"><?php print ka_t('Target group'); ?></div>
                <ul>
                  <?php if(isset($target_groups)): ?>
                  <?php foreach($target_groups as $target_group):
                  if($target_group->tid != CHILDREN_FILTER_ID && $target_group->tid != ADULTS_FILTER_ID) continue;
                  ?>
                  <li>
                    <div class="form-item">
                       <label>
                          <span class="customCheckbox">
                             <input name="sihtryhm" value="<?php print $target_group->tid; ?>" type="checkbox">
                             <span class="indicator"></span>
                          </span><!--/customRadio-->
                          <span class="label-title"><?php print $target_group->name; ?></span>
                       </label>
                    </div><!--/form-item-->
                  </li>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </ul>
              </li> <?php // Sihtrühm ?>
                      
                     
            </ul>
          </div>
          
          <div class="sm-show select-filters">
            <center>
              <div class="btn" rel="closeFilter"><?php print ka_t('Choose filters'); ?></div>
            </center>
          </div><!--/sm-show-->
          
        </div>
        
      </div><!--/block-->
    </div><!--/col-3-->
    <div class="col-9 sm-12 md-12">
      <div class="row sm-hide">
        <div class="col-10">
          <div class="form-item rounded-corners">
            <input type="text" name="title" placeholder="<?php print ka_t('Find recreational activity: name/location . . .'); ?>" data-plugin="autocomplete" data-url="/<?php print $lang; ?>/activities/autocomplete">
          </div><!--/form-item-->
        </div><!--/col-10-->
        <div class="col-2">
          <button class="btn stretch"><?php print ka_t('Search'); ?></button>
        </div><!--/col-2-->
      </div><!--/row-->
      
      <div class="row" data-plugin="inputCancel" data-cancels="starting_time">
        <div class="col-12">
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="1">
            <span class="btn btn-plain circle"><?php print ka_t('Mo'); ?></span>
          </label>
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="2">
            <span class="btn btn-plain circle"><?php print ka_t('Tu'); ?></span>
          </label>
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="3">
            <span class="btn btn-plain circle"><?php print ka_t('We'); ?></span>
          </label>
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="4">
            <span class="btn btn-plain circle"><?php print ka_t('Th'); ?></span>
          </label>
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="5">
            <span class="btn btn-plain circle"><?php print ka_t('Fr'); ?></span>
          </label>
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="6">
            <span class="btn btn-plain circle"><?php print ka_t('Sa'); ?></span>
          </label>
          <label class="customInput half-margin">
            <input type="checkbox" name="day" value="7">
            <span class="btn btn-plain circle"><?php print ka_t('Su'); ?></span>
          </label>
        </div><!--/col-12-->
      </div>
      
      <div class="row sm-hide">
        <div class="col-9">
                     
          <div class="narrow-buttons">
            
            <b class="sm-hide"><?php print ka_t('Select time'); ?>:</b>
            
            <label class="customInput">
               <input type="radio" name="starting_time" value="1" data-date="<?php get_date_range('this_week');?>">
               <span class="btn btn-plain"><?php print ka_t('This week'); ?></span>
            </label>
            <label class="customInput">
               <input type="radio" name="starting_time" value="2" data-date="<?php get_date_range('next_week');?>">
               <span class="btn btn-plain"><?php print ka_t('Next week'); ?></span>
            </label>
            <label class="customInput">
               <input type="radio" name="starting_time" value="3" data-date="<?php get_date_range('30_days');?>">
               <span class="btn btn-plain"><?php print ka_t('30 days'); ?></span>
            </label>
        
            <span class="btn-spacing pull-right sm-hide"></span>
            
          </div><!--/narrow-buttons-->
                     
        </div><!--/col-9-->
        <div class="col-3">
          <div class="form-item rounded-corners">
            <i class="after-calendar"></i>
            <i class="before-close" data-plugin="resetField"></i>
            
            <input data-plugin="datepickerRange" data-relatives="starting_time" placeholder="<?php print ka_t('Select date'); ?>" type="text" name="date" data-cancel="starting_time">
          </div><!--/form-item rounded-corners-->
           
        </div><!--/col-3-->
      </div>

      <div class="row">
        <div class="col-6 sm-hide">
          <div class="form-item">
            <label>
              <span class="customCheckbox filled">
                <input name="unfilled" type="checkbox">
                <span class="indicator"></span>
              </span><!--/customRadio-->
              <span class="label-title"><?php print ka_t('Show recreational activities with vacancies'); ?></span>
            </label>
          </div><!--/form-item-->
        </div><!--/col-6-->
                  
        <div class="col-6 text-right sm-12">
           <a data-view="list-view" href="#" class="link view before-list"><?php print ka_t('List'); ?></a>
           <a data-view="map-view" href="#" class="link view before-location"><?php print ka_t('Map'); ?></a>
        </div><!--/col-12-->
      </div><!--/row-->
               
      <div class="filter-output list-view" data-url="<?php print url('activities/json'); ?>">
                  
        <div class="template-filter" style="display:none;">
          <div class="block block-map">
            <h6><?php print ka_t('Filter results:'); ?></h6>

            <div class="filter-buttons">
              
              <?php foreach($activity_categories as $activity_category):
                if($activity_category->parents[0] !== '0') continue; // Show only first level icons
                // Color id
                $color_field = field_get_items('taxonomy_term', $activity_category, 'field_color_id');
                $color_id = field_view_value('taxonomy_term', $activity_category, 'field_color_id', $color_field[0]);
                
                // Icon class
                $icon_field = field_get_items('taxonomy_term', $activity_category, 'field_icon_class');
                $icon_class = field_view_value('taxonomy_term', $activity_category, 'field_icon_class', $icon_field[0]);
              ?>
                {% if filters['theme-<?php print render($color_id); ?>'] %}
                <label class="theme-<?php print render($color_id); ?>">
                  <input name="filter-button" checked="checked" type="checkbox" value="<?php print render($color_id); ?>"><span class="before-<?php print render($icon_class); ?>"><?php print $activity_category->name; ?></span>
                </label><!--/filter-button-->
                {% endif %}
              <?php endforeach; ?>
                           
            </div><!--/filter-buttons-->

            <div class="map">
              <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-62.gif'; ?>" class="placeholder">
              <div class="map-obj"></div>
            </div><!--/image-->

          </div><!--/block block-map-->
                     
                     
          <div class="row vertical-half">
                        
                        {% if data.categories %}
                           {% for cat in data.categories %}
                              <div class="col-12">
                                 <h1>{{ cat.title }}</h1>
                              </div><!--/col-12-->

                              {% for item in data.list %}
                                 {% if item.category == cat.id %}
                                 <div class="col-4 sm-12">
                                    <div class="list-item">

                                       <a class="link more-link" href="{{ item.url }}">{{ item.additional }}</a>

                                       <div class="price">{{ item.price }}</div>

                                       <ul class="tags filled">
                                          {% for tag in item.tags %}
                                          <li class="tag-{{ tag.type }}"><a href="{{ item.url }}">{{ tag.label }}</a></li>
                                          {% endfor %}
                                       </ul>
                                       <a href="{{ item.url }}" class="image" style="background-image:url('{{ item.image }}')">
                                          <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-56.gif'; ?>" class="placeholder">
                                       </a><!--/image-->
                                       <div class="details">
                                          <a href="{{ item.url }}" class="title">{{ item.title }}</a>
                                          <span class="info">
                                             {% if item.location %}
                                             <p class="before-location" data-lng="{{ item.lng }}" data-lat="{{ item.lat }}">{{ item.location }}</p>
                                             {% endif %}

                                             {% if item.date %}
                                             <p class="before-calendar">{{ item.date }}</p>
                                             {% endif %}
                                          </span><!--/info-->

                                          <a href="{{ item.url }}">{{ item.content }}</a>
                                       </div><!--/details-->

                                    </div><!--/list-item-->
                                 </div><!--/col-4-->
                                 {% endif %}
                              {% endfor %}
                        
                           {% endfor %}
                        
                        {% else %}
                        
                           {% for item in data.list %}
                           <div class="col-4 sm-12">
                              <div class="list-item">

                                 <a class="link more-link" href="{{ item.url }}">{{ item.additional }}</a>

                                 <div class="price">{{ item.price }}</div>

                                 <ul class="tags filled">
                                    {% for tag in item.tags %}
                                    <li class="tag-{{ tag.type }}"><a href="{{ item.url }}">{{ tag.label }}</a></li>
                                    {% endfor %}
                                 </ul>
                                 <a href="{{ item.url }}" class="image" style="background-image:url('{{ item.image }}')">
                                    <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-56.gif'; ?>" class="placeholder">
                                 </a><!--/image-->
                                 <div class="details">
                                    <a href="{{ item.url }}" class="title">{{ item.title }}</a>
                                    <span class="info">
                                       {% if item.location %}
                                       <p class="before-location" data-lng="{{ item.lng }}" data-lat="{{ item.lat }}">{{ item.location }}</p>
                                       {% endif %}

                                       {% if item.date %}
                                       <p class="before-calendar">{{ item.date }}</p>
                                       {% endif %}
                                    </span><!--/info-->

                                    <a href="{{ item.url }}">{{ item.content }}</a>
                                 </div><!--/details-->

                              </div><!--/list-item-->
                           </div><!--/col-4-->
                           {% endfor %}
                        {% endif %}
                        
          </div><!--/row-->
        </div><!--/template-filter-->
                  
      </div><!--/filter-output-->
               
    </div><!--/col-9-->
  </div><!--/row-->
</div><!--/inline-->