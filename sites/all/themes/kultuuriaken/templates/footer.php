<div class="cookie-notification" data-plugin="cookiesNotification">
	<div class="inline">
		<div class="row">
	  	<div class="col-10">
	   	   <p><?php print ka_t('This page uses cookies. By continuing to use the website, you agree to the use of cookies.'); ?></p>

	   	   <a href="javascript:void(0);" class="btn" rel="accept"><?php print ka_t('Agree'); ?></a>
	   	   <a href="https://parnu.ee/index.php/linnakodanikule/dokumendid/isikuandmete-kaitse" class="btn" target="_blank" style="white-space: nowrap;"><?php print ka_t('Read more'); ?></a>
	   	</div><!--/col-8-->
	   	<div class="col-2">
	   	   <a href="javascript:void(0);" class="after-close pull-right" rel="close"><?php print ka_t('Close'); ?></a>
	   	</div><!--/col-4-->
		</div><!--/row-->
	</div><!--/inline-->
</div>

<div class="oldBrowser-popup">
	<div class="inline">
	   <h1><?php print ka_t('Your browser is out-of-date. Please download one of these up-to-date, free and excellent browsers'); ?>:</h1>
	   <a href="http://www.mozilla.com/firefox/" class="entry">
	      <img alt="Firefox" src="/<?php print path_to_theme() . "/assets/imgs/256-firefox.png"; ?>" />

	      <div class="text">
	         <b>Firefox</b>
	         Mozilla Foundation
	      </div><!--/text-->
	   </a><!--/entry-->
	   <a href="http://www.opera.com" class="entry">
	   		<img alt="Opera" src="/<?php print path_to_theme() . "/assets/imgs/256-opera.png"; ?>" />
	      <div class="text">
	         <b>Opera</b>
	         Opera Software
	      </div><!--/text-->
	   </a><!--/entry-->
	   <a href="https://www.google.com/chrome/browser/desktop/" class="entry">
	   		<img alt="Chrome" src="/<?php print path_to_theme() . "/assets/imgs/256-chrome.png"; ?>" />
	      <div class="text">
	         <b>Chrome</b>
	         Google
	      </div><!--/text-->
	   </a><!--/entry-->
	</div><!--/inline-->
</div><!--/oldBrowser-popup-->

<footer class="main">
	<div class="inline">
		<div class="row">
			<div class="col-4 sm-12">
				<?php print variable_get_value('footer_address')['value']; ?>
			</div><!--/col-4-->
			<div class="col-4 sm-12">
				<center>
					<?php print variable_get_value('footer_contact_us')['value']; ?>
			</div><!--/col-4-->
			<div class="col-4 sm-12">
				<?php if(variable_get('show_hide_newsletter') == 1): ?>
				<center>
					<p><b><?php print ka_t('Notification'); ?></b></p>
					<p><a href="" id="newsletter-modal" class="btn"><?php print ka_t('Order notification'); ?></a></p>
				</center>
				<div id="newsletter-form-modal" class="ka-hidden">
					<?php print $newsletter_subscribe_form_hidden['content']; ?>
				</div>
        <?php endif; ?>
			</div><!--/col-4-->
		</div><!--/row-->
    <div class="row">
        <div class="col-12">
          <center>
            <a href="https://kultuuriaken.tartu.ee" class="madeby" target="_blank">Powered by Tartu Kultuuriaken</a>
          </center>
          <center>
              <a href="https://twn.ee" class="madeby" target="_blank">Made by Trinidad Wiseman</a>
          </center>
        </div><!--/col-12-->
    </div><!--/row-->
	</div><!--/inline-->
</footer>
