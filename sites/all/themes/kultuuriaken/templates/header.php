<header class="main sm-hide">
	<!-- <div class="header-search" data-class="'visible' : headerSearch">
		<form method="get" action="<?php print url('search'); ?>">
	  	<div class="inline">
	  	   <div class="row">
	  	      <div class="col-6">
	  	         <div class="form-item">
	  	            <input type="text" name="search" placeholder="<?php print ka_t('Enter search keyword'); ?>">
	  	         </div> --><!--/form-item-->
	  	      <!-- </div> --><!--/col-6-->
	  	     <!--  <div class="col-6 text-right">
	  	         <div class="before-close" data-toggle="headerSearch"></div>
	  	      </div> --><!--/col-6-->
	  	   <!-- </div> --><!--/row-->
	  	<!-- </div> --><!--/inline-->
	  <!-- 	<input type="hidden" name="node_type[event]"  value="event">
			<input type="hidden" name="node_type[activity]"  value="activity">
	  </form>
	</div> -->
	<div class="inline">
		<div class="row toolbar">
			<div class="col-4">
				<div class="accessibility" data-toggle="accessibility" data-class="'active' : accessibility">
						<?php print ka_t('For visually impaired'); ?>
				</div>
				<div id="current-weather" class="weather"></div><!--/weather-->
			</div><!--/col-4-->
			<div class="col-4 center-links">
				<?php print render($header_menu); ?>
			</div><!--/col-4-->
			
			<div class="col-4 text-right">
				<?php print $language_switcher; ?>
				<!-- <a href="" class="before-search" data-toggle="headerSearch"></a> -->
			</div><!--/col-4-->
		</div><!--/row-->
	</div><!--/inline-->
	
	<div class="no-background">
		<div class="inline">
			<div class="row">
				<div class="col-3">
					<a href="<?php print url(); ?>" class="logo"><!--<img alt="Kultuuriaken" src="/<?php //print path_to_theme() . '/assets/imgs/logo.svg'; ?>" />--> <?php print ka_t(''); ?>kultuur.parnu.ee</a>
					
				</div><!--/col-3-->
				<div class="col-6 flex-center">
					<nav class="main">
						<?php  print render($main_menu); ?>
					</nav><!--/main-->
				</div><!--/col-6-->
				<div class="col-3 text-right">
					
					<a href="<?php print url('calendar'); ?>" class="account-link"><b><?php print ka_t('My calendar'); ?> <span class="before-calendar"></span></b></a>
					
					<?php if($logged_in): ?>
					<a href="<?php print url('user'); ?>" class="account-link alternative" title="#my_account" rel="interact" data-addonClass="dropdown-menu"><b><?php print ka_t('My account'); ?> <span class="before-user"></span></b></a>
					<?php endif; ?>
					
					<div class="title-content" id="my_account">
						<div class="user-info">
							<div class="user-info_icon">
								<span class="before-user"></span>
							</div><!--/user-info_icon-->
							<div class="user-info_details">
								<p>
									<?php if(!empty($user_institution)): ?>
									<b><?php print $user_institution; ?></b>
									<?php endif; ?>
									<?php if(!empty($user_email)): ?>
									<?php print $user_email; ?>
									<?php endif; ?>
								</p>
							</div><!--/user-info_details-->
						</div><!--/user-info-->
						<nav class="nav-dropdown">
							<ul>
								<li><a href="<?php print url('user'); ?>" class="after-settings"><?php print ka_t('Account settings'); ?></a></li>
								<li><a href="<?php print url('user/logout'); ?>" class="after-logout"><?php print ka_t('Log out'); ?></a></li>
							</ul>
						</nav><!--/nav-dropdown-->
					</div><!--/title_content-->
				</div><!--/col-3-->
			</div><!--/row-->
		</div><!--/inline-->
	</div><!--/no-background-->
</header><!--/main-->

<header class="mobile sm-show">
	<div class="header-search" data-class="'visible' : mobileSearch">
	   <form method="get" action="<?php print url('search'); ?>">
	   <div class="inline">
	      <div class="row">
	         <div class="sm-9">
	            <div class="form-item">
	               <input type="text" name="search" placeholder="<?php print ka_t('Enter search keyword'); ?>">
	            </div><!--/form-item-->
	         </div><!--/col-6-->
	         <div class="sm-3 text-right">
	            <button class="before-search"></button>
	         </div><!--/col-6-->
	      </div><!--/row-->
	   </div><!--/inline-->
	   <input type="hidden" name="node_type[event]"  value="event">
		 <input type="hidden" name="node_type[activity]"  value="activity">
	   </form>
	</div><!--/header-search-->
	
	<div class="upper">
	   <span class="before-menu" data-toggle="mobileMenu"></span>
	   <?php if(!empty($language_switcher_mobile)): ?>
	   	<?php print $language_switcher_mobile; ?>
	   <?php endif; ?>
	   <span class="before-search pull-right" data-toggle="mobileSearch" data-class="'active' : mobileSearch"></span>
	</div><!--/upper-->
	
	<div class="lower">
	   <a href="<?php print url(); ?>" class="logo"><img alt="Kultuuriaken" src="/<?php print path_to_theme() . '/assets/imgs/logo.svg'; ?>"> kultuur.parnu.ee</a>
	</div><!--/lower-->
</header>

<nav class="mobile sm-show" data-class="'active' : mobileMenu">
	   <div class="before-close" data-toggle="mobileMenu"></div>
	   <div class="holder">
	      <?php if(!empty($main_menu_mobile)): ?>
	      	<?php print render($main_menu_mobile); ?>
	      <?php endif; ?>
	      <?php if(!empty($header_menu_mobile)): ?>
	      	<?php print render($header_menu_mobile); ?>
	      <?php endif; ?>
	   </div><!--/holder-->
	</nav>
