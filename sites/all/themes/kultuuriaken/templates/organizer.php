<div class="inline">
         
  <div class="row">
     <div class="col-12">
        <ul class="breadcrumbs">
           <li><a href="<?php print url(); ?>"><?php print ka_t('Home page'); ?></a></li>
           <li><span class="before-arrow_right"></span></li>
           <li><a href=""><?php print ka_t('For organisers'); ?></a></li>
        </ul>
     </div><!--/col-12-->
  </div><!--/row-->
  
  <div class="row">
     <div class="col-12">
        <h1><?php print ka_t('For organisers'); ?></h1>
     </div><!--/col-12-->
  </div><!--/row-->

  <?php if(!$logged_in): ?>
  <div class="row">
    <div class="col-6"></div><!--/col-6-->
    <div class="col-6 text-right">
      <a href="<?php print url('new-event-anon/event'); ?>" class="<?php if($language->language === 'ru') print 'marginBottom '; ?>btn btn-disabled half-margin" data-plugin="overlay"><?php print ka_t('Add new event'); ?></a>
      <a href="<?php print url('new-event-anon/activity'); ?>" class="btn btn-disabled half-margin" data-plugin="overlay"><?php print ka_t('Add new recreational activity'); ?></a>
    </div><!--/col-6-->
  </div><!--/row-->
  
  
  <div class="row">
     <div class="col-12">
        <div class="block rounded">
           <div class="row separator">
              <div class="col-6">
                 <h3><?php print ka_t('Log in securely'); ?>:</h3>
                    <?php $elements = drupal_get_form("user_login"); $form = drupal_render($elements);
                        print $form;
                    ?>
                  <p><?php print t('Don\'t have account ?'); ?>
                    <a href="/user/register"><?php print t('Register here'); ?></a>
                  </p>
                  <p><?php print t('Forgot password ?'); ?>
                    <a href="/user/password"><?php print t('Request new password'); ?></a>
                  </p>
                <?php
                /*
                 <div class="login-buttons">
                    <a href="http://pass.raad.tartu.ee/ctrl/et/Auth/login/kultuuriaken"><img alt="ID-Kaart" src="/<?php print path_to_theme() . '/assets/imgs/id-kaart.svg'; ?>"> <?php print ka_t('Log in with ID-card'); ?></a>
                    <span class="divider"><span><?php print ka_t('or'); ?></span></span>
                    <a href="http://pass.raad.tartu.ee/ctrl/et/Auth/login/kultuuriaken"><img alt="Mobiil-ID" src="/<?php print path_to_theme() . '/assets/imgs/mobiil-id.svg'; ?>"> <?php print ka_t('Log in with Mobile ID'); ?></a>
                 </div><!--/login-buttons-->
                 */
                ?>
              </div><!--/col-6-->
              <div class="col-6">
                <?php if(!empty($hybridauth_login)): ?>
                  <?php print $hybridauth_login; ?>
                <?php endif; ?>
              </div><!--/col-6-->
           </div><!--/row-->
        </div><!--/block rounded-->
     </div><!--/col-12-->
  </div><!--/row-->
  <?php else: ?>
  <?php print $organizer_table_events; ?>
  <?php print $organizer_table_activities; ?>
  
  <?php endif; ?>
         
  <?php print $organizer_faq; ?>
         
</div>
