<?php if($provider_id === 'Facebook'): ?>
  <?php print ka_t('Log in with Facebook'); ?>
<?php elseif($provider_id === 'Google'): ?>
  <?php print ka_t('Log in with Google+'); ?>
<?php endif; ?>