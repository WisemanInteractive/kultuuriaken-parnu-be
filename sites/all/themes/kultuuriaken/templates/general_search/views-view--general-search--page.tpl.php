<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <div class="row sm-hide">
    <div class="col-12">
      <ul class="breadcrumbs">
        <li><a href="<?php print url(); ?>"><?php print ka_t('Home page'); ?></a></li>
        <li><span class="before-arrow_right"></span></li>
        <li><a href="<?php print url('search'); ?>"><?php print ka_t('Search'); ?></a></li>
      </ul>
    </div>
    <!--/col-12-->
  </div>
  
  <div class="row">
    <div class="col-12">
       <h1><?php print ka_t('Search'); ?></h1>
    </div><!--/col-12-->
  </div>
  
  <?php if ($exposed): ?>
    <?php print $exposed; ?>
  <?php endif; ?>
    
  <?php if(!empty($activity_search_result)): ?>
    <?php print $activity_search_result; ?>
  <?php elseif ($attachment_after): ?>
    <?php print $attachment_after; ?>
  <?php else: ?>
    <div class="row">
      <div class="col-12">
        <h5><?php print ka_t('No results matched your search.'); ?></h5>
      </div><!--/col-12-->
    </div>
  <?php endif; ?>
  </div>
<?php /* class view */ ?>