<div class="row">
  <div class="col-10">
    <div class="form-item rounded-corners">
      <?php print render($form['search']); ?>
    </div><!--/form-item-->
  </div>
  <div class="col-2">
    <?php print render($form['submit']); ?>
    <button class="btn stretch"><?php print ka_t('Search'); ?></button>
  </div><!--/col-2-->
</div>

<div class="row">
  <div class="col-12">
    <div class="block">
      <div class="search-filter">
        <b><?php print ka_t('Filter results'); ?>:</b>
        <div class="form-item">
          <label>
            <span class="customCheckbox">
              <?php print render($form['node_type']['event']); ?>
              <span class="indicator"></span>
            </span><!--/customRadio-->
            <span class="label-title"><?php print ka_t('Events'); ?> (<span id="eventCount">0</span>)</span>
          </label>
        </div><!--/form-item-->
         
        <div class="form-item">
          <label>
            <span class="customCheckbox">
              <?php print render($form['node_type']['activity']); ?>
              <span class="indicator"></span>
            </span><!--/customRadio-->
            <span class="label-title"><?php print ka_t('Recreational activities'); ?> (<span id="activityCount">0</span>)</span>
          </label>
        </div><!--/form-item-->
        <a id='resetSearch' href="" class="link secondary after-close pull-right"><?php print ka_t('Cancel search'); ?></a>
        <?php print render($form['reset']); ?>
      </div><!--/search-filter-->
    </div><!--/block-->
  </div><!--/col-12-->
</div>

<?php print drupal_render_children($form); ?>