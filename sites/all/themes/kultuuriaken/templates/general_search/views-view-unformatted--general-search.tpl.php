<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $id => $row): ?>
  <?php if(!empty($row)): ?>
  <div class="col-4">
    <div class="list-item">
        <?php print $row; ?>
    </div>
  </div>
  <?php endif; ?>
<?php endforeach; ?>
