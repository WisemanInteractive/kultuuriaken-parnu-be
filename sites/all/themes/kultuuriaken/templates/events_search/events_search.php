<?php 
	global $language;
	$lang = $language->language;
	
	$event_hour = (int) date('H', time());
?>
<div class="inline">
         <div class="row">
            <div class="col-12">
               <ul class="breadcrumbs">
                  <li><a href="<?php print url(); ?>"><?php print ka_t('Home page'); ?></a></li>
                  <li><span class="before-arrow_right"></span></li>
                  <li><a href="<?php print url('events'); ?>"><?php print ka_t('Events'); ?></a></li>
               </ul>
            </div><!--/col-12-->
         </div><!--/row-->
         <div class="row">
            <div class="col-12">
               <h1><?php print ka_t('Events'); ?></h1>
            </div><!--/col-12-->
         </div><!--/row-->

         <div class="row" data-plugin="listFilter">
            <div class="col-3 sm-12 md-12">
               <div class="filter-tags block sm-show" data-plugin="stickyTags">
                  <span class="toggler after-arrow_up"></span>
                  <div class="link secondary after-close "><?php print ka_t('Cancel all filters'); ?></div>
                  <ul class="tags" rel="tags"> </ul>
              </div><!--filter-tags-->
              <div class="block no-padding">
                  <div class="filter-button sm-show before-filter after-arrow_down" id="filter-trigger">
                     <b><?php print ka_t('Filter results'); ?></b>
                  </div><!--/filter-button-->

                  <div class="filter-wrapper" data-plugin="filterAccordion">
                     
                     <div class="filter-button sm-show before-filter after-close" rel="closeFilter">
                        <b><?php print ka_t('Filter results'); ?></b>
                     </div><!--/filter-button-->

                     <div class="filter-scroller">
                         <div class="row sm-show">
                           <div class="col-12 text-right reset">
                              <div class="link secondary after-close"><?php print ka_t('Cancel all filters') ?></div>
                           </div><!--/col-12-->
                        </div><!--/row-sm-show-->
                        
                        <div class="row sm-show">
                           <div class="col-10">
                              <div class="form-item rounded-corners">
                                 <input type="text" name="title" placeholder="<?php print ka_t('Search by keyword'); ?>" />
                              </div><!--/form-item-->
                           </div><!--/col-10-->
                           <div class="col-2">
                              <button class="btn stretch before-search"></button>
                           </div><!--/col-2-->
                        </div><!--/row--> <?php // Märksõna ?>
                        
                        <ul class="filter">
                          <li class="reset sm-hide">
                            <div class="link secondary after-close"><?php print ka_t('Cancel all filters'); ?></div>
                          </li>                          
                          <li class="sm-show active">
                              <div class="filter-title before-calendar" rel="trigger"><?php print ka_t('Date'); ?></div>
                              <ul>
                                <li class="range-date-wrapper">
                                  <div class="form-item rounded-corners">
                                    <i class="after-calendar"></i>
                                    <div class="form-item_title"><?php print ka_t('Start date'); ?></div>
                                    <input data-plugin="datepickerSingle" placeholder="<?php print ka_t('Select date'); ?>" type="text" name="date-1" rel="start" data-cancel="starting_time"/>
                                  </div><!--/form-item-->
                                  <div class="form-item rounded-corners">
                                    <i class="after-calendar"></i>
                                    <div class="form-item_title"><?php print ka_t('End date'); ?></div>
                                    <input data-plugin="datepickerSingle" placeholder="<?php print ka_t('Select date'); ?>" type="text" name="date-2" rel="end" data-cancel="starting_time"/>
                                  </div><!--/form-item-->
                                </li>
                              </ul>
                           </li> <?php // Kuupäeva ?>
                          
                          <?php if(!empty($event_categories)): ?>
                          <li class="active">
                            <div class="filter-title sm-hide" rel="trigger"><?php print ka_t('Select field'); ?></div>
                            <div class="filter-title sm-show before-tags" rel="trigger"><?php print ka_t('Field'); ?></div>
                            <ul>
                              
                              <?php foreach($event_categories as $event_category):
                                if($event_category->depth == '1') continue;
                                $default_category = !empty($default_category_tid) && $default_category_tid === $event_category->tid ? TRUE : FALSE;
                                $count = isset($event_category_summary[$event_category->tid]) ? $event_category_summary[$event_category->tid]  : 0;
                              ?>
                              <li <?php if($default_category) print 'class="active"' ?>>
                                <div class="form-item" <?php if(!empty($event_subcategories[$event_category->tid])) print 'rel="trigger"'; ?>>
                                   <label>
                                      <span class="customCheckbox">
                                         <input name="category_<?php print $event_category->tid; ?>" value="all" type="checkbox" checked>
                                         <span class="indicator"></span>
                                      </span><!--/customRadio-->
                                      <span class="label-title"><?php print $event_category->name; ?> (<?php print $count; ?>)</span>
                                   </label>
                                </div><!--/form-item-->
                                <?php if(!empty($event_subcategories[$event_category->tid])): 
                                  $subcategories = $event_subcategories[$event_category->tid];
                                ?>
                                <ul>
                                  <?php foreach($subcategories as $num => $subcategory): 
                                    $count = isset($event_category_summary[$subcategory->tid]) ? $event_category_summary[$subcategory->tid] : 0;
                                  ?>
                                  <li <?php if($num >= 3) print 'class="hidden"'; ?>>
                                    <div class="form-item">
                                       <label>
                                          <span class="customCheckbox">
                                             <input name="category_<?php print $event_category->tid; ?>" value="<?php print $subcategory->tid; ?>" type="checkbox" checked>
                                             <span class="indicator"></span>
                                          </span><!--/customRadio-->
                                          <span class="label-title"><?php print $subcategory->name; ?> (<?php print $count; ?>)</span>
                                       </label>
                                    </div><!--/form-item-->
                                  </li>
                                  <?php endforeach; ?>
                                  <?php if(count($subcategories) > 3): ?>
                                  <li class="show-more">
                                    <div class="link after-plus" data-show_less="Näita vähem" data-show_more="Näita veel" ><?php print ka_t('Show more'); ?></div>
                                  </li>
                                  <?php endif; ?>
                                </ul>
                                <?php endif; ?>
                              </li>
                              <?php endforeach; ?>
                            </ul>
                          </li> <?php // Valdkond ?>
                          <?php endif; ?>
                          
                          <li>
                              <div class="filter-title sm-hide" rel="trigger"><?php print ka_t('Select price'); ?></div>
                              <div class="filter-title sm-show before-money" rel="trigger"><?php print ka_t('Price'); ?></div>
                              <ul>
                                 <li class="slider-range-wrapper">
                                    <div class="form-item">
                                       <label>
                                          <span class="customCheckbox">
                                             <input name="cost" value="1" type="checkbox">
                                             <span class="indicator"></span>
                                          </span><!--/customRadio-->
                                          <span class="label-title"><?php print ka_t('Free'); ?></span>
                                       </label>
                                    </div><!--/form-item-->

                                    <div class="form-item">
                                       <div class="form-item_title"><?php print ka_t('Price range'); ?>:</div>
                                       <input type="hidden" name="priceMin" rel="min" value="0"><input type="hidden" name="priceMax" value="40" rel="max">

                                       <div class="slider-range" data-plugin="sliderRange" data-min="0" data-max="50" data-unit="€"></div>
                                    </div><!--/form-item-->

                                 </li>

                              </ul>
                           </li> <?php // Hind ?>
                          
                          <li>
                              <div class="filter-title sm-hide" rel="trigger"><?php print ka_t('Select target group'); ?></div>
                              <div class="filter-title sm-show before-users" rel="trigger"><?php print ka_t('Target group'); ?></div>
                              
                              <ul>
                                <?php foreach($target_groups as $target_group): ?>
                                <li>
                                    <div class="form-item">
                                       <label>
                                          <span class="customCheckbox">
                                             <input name="sihtryhm" value="<?php print $target_group->tid; ?>" type="checkbox">
                                             <span class="indicator"></span>
                                          </span><!--/customRadio-->
                                          <span class="label-title"><?php print $target_group->name; ?></span>
                                       </label>
                                    </div><!--/form-item-->
                                </li>
                                <?php endforeach; ?>
                              </ul>
                           </li> <?php // Sihtrühm ?>
                          
                        </ul>
                     </div><!--/filter-scroller-->
                     
                     <div class="sm-show select-filters">
                        <center>
                           <div class="btn" rel="closeFilter"><?php print ka_t('Choose filters'); ?></div>
                        </center>
                     </div><!--/sm-show-->
                  </div><!--/filter-wrapper-->
               </div><!--/block-->
            </div><!--/col-3-->
            
            <div class="col-9 sm-12 md-12">
               <div class="row sm-hide">
                  <div class="col-10">
                     <div class="form-item rounded-corners">
                       <input type="text" name="title" placeholder="<?php print ka_t('Find event: name / location . . .'); ?>" data-plugin="autocomplete" data-url="/<?php print $lang; ?>/events/autocomplete">
                     </div><!--/form-item-->
                  </div><!--/col-10-->
                  <div class="col-2">
                     <button class="btn stretch"><?php print ka_t('Search'); ?></button>
                  </div><!--/col-2-->
               </div><!--/row-->

               <div class="row">
                  <div class="col-9 sm-12">
                     <div class="narrow-buttons">
                        <label class="customInput">
                           <input type="radio" name="starting_time" value="1" <?php if(empty($_GET['date'])): ?>checked="checked"<?php endif; ?>>
                           <span class="btn btn-plain"><?php print ka_t('Today'); ?></span>
                        </label>
                        <label class="customInput">
                           <input type="radio" name="starting_time" value="2">
                           <span class="btn btn-plain"><?php print ka_t('Tomorrow'); ?></span>
                        </label>
                        <label class="customInput">
                           <input type="radio" name="starting_time" value="3">
                           <span class="btn btn-plain"><?php print ka_t('Weekend'); ?></span>
                        </label>

                        <span class="btn-spacing sm-hide"></span>
                        <?php if (variable_get('show_hide_events_time') == 1): ?>
                        <label class="customInput sm-hide">
                           <input type="checkbox" name="day_time" value="1">
                           <span class="btn btn-text-only"><?php print ka_t('Morning'); ?></span>
                        </label>
                        <label class="customInput sm-hide">
                           <input type="checkbox" name="day_time" value="2">
                           <span class="btn btn-text-only"><?php print ka_t('Lunch'); ?></span>
                        </label>
                        <label class="customInput sm-hide">
                           <input type="checkbox" name="day_time" value="3">
                           <span class="btn btn-text-only"><?php print ka_t('Evening'); ?></span>
                        </label>
                        <?php endif; ?>
                     </div><!--/narrow-buttons-->
                  </div><!--/col-9-->
                  <div class="col-3 sm-hide">
                     <div class="form-item rounded-corners">
                        <i class="after-calendar"></i>
                        <i class="before-close" data-plugin="resetField"></i>
                        <input data-plugin="datepickerRange" data-relatives="starting_time" placeholder="<?php print ka_t('Select date'); ?>" type="text" name="date" data-cancel="starting_time"/>
                     </div><!--/form-item rounded-corners-->
                  </div><!--/col-3-->
               </div><!--/row-->

               <div class="row">
                  <div class="col-12 text-right">
                     <a data-view="grid-view" href="" class="link view before-grid active"><?php print ka_t('Grid'); ?></a>
                     <a data-view="list-view" href="#" class="link view before-list sm-hide"><?php print ka_t('List'); ?></a>
                     <a data-view="map-view" href="#" class="link view before-location"><?php print ka_t('Map'); ?></a>
                  </div><!--/col-12-->
               </div><!--/row-->
               
               <div class="filter-output list-view" data-url="<?php print url('events/json'); ?>">
                  
                  <div class="template-filter" style="display:none;">
                     <div class="block block-map">
                        <h6><?php print ka_t('Filter results:'); ?></h6>


                        <div class="filter-buttons">
                          
                          <?php foreach($event_categories as $event_category):
                            // Color id
                            $color_field = field_get_items('taxonomy_term', $event_category, 'field_color_id');
                            $color_id = field_view_value('taxonomy_term', $event_category, 'field_color_id', $color_field[0]);
                            
                            // Icon class
                            $icon_field = field_get_items('taxonomy_term', $event_category, 'field_icon_class');
                            $icon_class = field_view_value('taxonomy_term', $event_category, 'field_icon_class', $icon_field[0]);
                          ?>
                          {% if filters['theme-<?php print render($color_id); ?>'] %}
                          <label class="theme-<?php print render($color_id); ?>">
                            <input name="filter-button" checked="checked" type="checkbox" value="<?php print render($color_id); ?>"><span class="before-<?php print render($icon_class); ?>"><?php print $event_category->name; ?></span>
                          </label><!--/filter-button-->
                          {% endif %}
                          <?php endforeach; ?>

                        </div><!--/filter-buttons-->

                        <div class="map">
                           <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-62.gif'; ?>" class="placeholder">
                           <div class="map-obj"></div>
                        </div><!--/image-->

                     </div><!--/block block-map-->
                     
                     
                     <div class="row vertical-half">
                        
                        {% if data.categories %}
                           {% for cat in data.categories %}

                            <?php if(variable_get('show_hide_event_time') == 1) : ?>

                              <div class="col-12">
                                 <h1>{{ cat.title }}</h1>
                              </div><!--/col-12-->
                              
                              {% if cat.id == "4" %}
                 					    <div class="col-12 grid-view-show">
              						      <div class="category-spacer"><?php print ka_t('Events in Progress'); ?></div>
            					        </div><!--/col-12-->	
                              {% endif %}

                            <?php endif; ?>

                              {% for item in data.list %}
                                 {% if item.category == cat.id %}
                                 <div class="col-4 sm-12">
                                    <div class="list-item">

                                       <a class="link more-link" href="{{ item.url }}">{{ item.additional }}</a>

                                       <div class="price">{{ item.price }}</div>

                                       <ul class="tags filled">
                                          {% for tag in item.tags %}
                                          <li class="tag-{{ tag.type }}"><a href="{{ item.url }}">{{ tag.label }}</a></li>
                                          {% endfor %}
                                       </ul>
                                       <a href="{{ item.url }}" class="image" style="background-image:url('{{ item.image }}')">
                                          <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-56.gif'; ?>" class="placeholder">
                                       </a><!--/image-->
                                       <div class="details">
                                          <a href="{{ item.url }}" class="title">{{ item.title }}</a>
                                          <span class="info">
                                             {% if item.location %}
                                             <p class="before-location" data-lng="{{ item.lng }}" data-lat="{{ item.lat }}">{{ item.location }}</p>
                                             {% endif %}

                                             {% if item.date %}
                                             <p class="before-calendar">{{ item.date }}</p>
                                             {% endif %}
                                          </span><!--/info-->

                                          <a href="{{ item.url }}">{{ item.content }}</a>
                                       </div><!--/details-->

                                    </div><!--/list-item-->
                                 </div><!--/col-4-->
                                 {% endif %}
                              {% endfor %}
                        
                           {% endfor %}
                        
                        {% else %}
                        
                           {% for item in data.list %}
                           <div class="col-4 sm-12">
                              <div class="list-item">

                                 <a class="link more-link" href="{{ item.url }}">{{ item.additional }}</a>

                                 <div class="price">{{ item.price }}</div>

                                 <ul class="tags filled">
                                    {% for tag in item.tags %}
                                    <li class="tag-{{ tag.type }}"><a href="{{ item.url }}">{{ tag.label }}</a></li>
                                    {% endfor %}
                                 </ul>
                                 <a href="{{ item.url }}" class="image" style="background-image:url('{{ item.image }}')">
                                    <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-56.gif'; ?>" class="placeholder">
                                 </a><!--/image-->
                                 <div class="details">
                                    <a href="{{ item.url }}" class="title">{{ item.title }}</a>
                                    <span class="info">
                                       {% if item.location %}
                                       <p class="before-location" data-lng="{{ item.lng }}" data-lat="{{ item.lat }}">{{ item.location }}</p>
                                       {% endif %}

                                       {% if item.date %}
                                       <p class="before-calendar">{{ item.date }}</p>
                                       {% endif %}
                                    </span><!--/info-->

                                    <a href="{{ item.url }}">{{ item.content }}</a>
                                 </div><!--/details-->

                              </div><!--/list-item-->
                           </div><!--/col-4-->
                           {% endfor %}
                        {% endif %}
                        
                     </div><!--/row-->
                  </div><!--/template-filter-->
                  
               </div><!--/filter-output-->
               
            </div><!--/col-9-->
         </div><!--/row-->
</div><!--/inline-->
