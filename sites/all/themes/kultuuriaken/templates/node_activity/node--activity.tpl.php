<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="inline <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row sm-hide">
          <div class="col-12">
            <ul class="breadcrumbs">
              <li><a href="<?php print url(); ?>"><?php print ka_t('Home page'); ?></a></li>
              <li><span class="before-arrow_right"></span></li>
              <li><a href="<?php print url('activities'); ?>"><?php print ka_t('Recreational activities'); ?></a></li>
            </ul>
          </div>
          <!--/col-12-->
        </div>
  <!--/row-->

  <div class="row no-margin sm-overflow">

    <div class="col-6 sm-12 stretch-content">
      
      <?php if(!empty($content['field_main_image'])): ?>
      <div class="image" style="background-image:url(<?php print image_style_url('detail_main_image', $content['field_main_image']['#items'][0]['uri']); ?>)">
        <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" />
        <?php if(isset($main_categories)): ?>
          <ul class="tags filled">
          <?php foreach($main_categories as $main_category): 
          $tag_id = !empty($main_category->field_color_id[LANGUAGE_NONE][0]['value']) ? 
            $main_category->field_color_id[LANGUAGE_NONE][0]['value'] : tid_to_tag_id($main_category->tid);
          ?>
            <li class="tag-<?php print $tag_id; ?>"><a href=""><?php print strtolower($main_category->name); ?></a></li>
          <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      </div>
      <!--/image-->
      <?php endif; ?>
      
    </div><!--/col-6-->

    <div class="col-6 sm-12 stretch-content">
      <div class="block-gray">
        <?php if(!empty($current_group)):?>
          <h1 style="margin: 0"><?php print $title; ?></h1>
          <span><?php print $current_group_summary['age_range'];?></span>
        <?php else: ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?> 
        <?php if(!empty($content['field_organizer_name'])): ?>
        <p class="highlight-1 size-xl"><?php print check_plain($content['field_organizer_name'][0]['#markup']); ?></p>
        <?php endif; ?>
        
        <?php if(!empty($current_group)): 
          $group_type = $current_group->field_activity_group_type->value();
        ?>
          <?php if(!empty($current_group->field_activity_date_period) && $group_type === 'regular'): 
          $group_period = $current_group->field_activity_date_period->value();
          ?>
            <p class="size-xl"><?php print sprintf('%s - %s', 
            format_date($group_period['value'], 'date_month'), 
            format_date($group_period['value2'], 'date_month')); ?></p>
          <?php endif; ?>
          <?php if(($current_group_summary) && !empty($current_group_summary['period']) && $group_type === 'fixed'): 
            $group_period = $current_group_summary['period'];
          ?>
            <p class="size-xl"><?php print sprintf('%s - %s', format_date($group_period['value'], 'date_month'), format_date($group_period['value2'], 'date_month')); ?></p>
          <?php endif; ?>
        <?php endif; ?>
        <?php if(!empty($group_price)): ?>
        <p class="size-xl"><?php print check_plain($group_price); ?></p>
        <?php endif; ?>

        <?php if(!empty($current_group_summary['additional_comment'])): // Additional comment ?>
            <p class="size-l"><?php print check_plain($current_group_summary['additional_comment']); ?></p>
        <?php endif; ?>

        <?php if(isset($current_group_date_location) && !empty($current_group_date_location['date_string'])): ?>
          <p><?php print $current_group_date_location['date_string']; ?></p>
        <?php endif; ?>
        <?php if(!empty($add_to_calendar)): ?>
          <?php print $add_to_calendar; ?>
        <?php endif; ?>
        <?php if($additional_link): ?>
          <?php if(!empty($additional_link['url'])):?>
            <a target="_blank" href="<?php print check_url($additional_link['info']); ?>" class="btn pull-right sm-hide"><?php print ka_t('Register'); ?></a>
            <a target="_blank" href="<?php print check_url($additional_link['info']); ?>" class="btn sm-show full-width"><?php print ka_t('Register'); ?></a>
          <?php else: ?>
            <span class="pull-right"><?php print sprintf('<b>%s</b> - %s', ka_t('Register on'), $additional_link['info']); ?> </span>
          <?php endif; ?>
        <?php endif; ?>
      </div><!--/block-gray-->

    </div><!--/col-6-->
  </div><!--/row-->

  <div class="row sm-overflow">
    <div class="col-12">
      <div class="block">
        <div class="row">
          <div class="col-7 sm-12">
            <article>
              <?php if(!empty($content['field_gallery'])): ?>
              <h5 class="sm-hide"><?php print ka_t('Gallery'); ?></h5>

              <div class="row sm-hide" data-plugin="gallery">
                <?php foreach($content['field_gallery']['#items'] as $item): ?>
                <div class="col-2">
                  <a href="<?php print image_style_url('detail_image', $item['uri']); ?>" class="image" 
                  style="background-image:url(<?php print image_style_url('gallery_thumbnail', $item['uri']); ?>)">
                    <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-1.gif"; ?>">
                  </a>
                </div>
                <!--/col-2-->
                <?php endforeach; ?>
                <?php if(!empty($content['field_video']) && !empty($video_url) && !empty($video_thumbnail_url)): ?>
                <div class="col-2">
                  <a href="<?php print check_url($video_url); ?>" class="image before-play" 
                  style="background-image:url(<?php print check_url($video_thumbnail_url); ?>)">
                    <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-1.gif"; ?>">
                  </a>
                </div>
                <?php endif; ?>
              </div>
              <!--/row-->
              <?php endif; ?>
              
              <h5 class="sm-hide"><?php print ka_t('Description'); ?></h5>
              <div class="text-wrapper" data-class="'full-text' : moreText">
                <?php if(!empty($content['field_summary'])): ?>
                <p><b><?php print nl2br($content['field_summary'][0]['#markup']); ?></b></p>
                <?php endif; ?>
                <?php if(!empty($content['field_body'])): ?>
                <p><?php print nl2br($content['field_body'][0]['#markup']); ?></p>
                <?php endif; ?>
                <?php if(!empty($content['field_additional_info_link'])): ?>
                <p><?php print ka_t('Read more'); ?>: <a target="_blank" href="<?php print urldecode(check_url($content['field_additional_info_link']['#items'][0]['url'])); ?>"><?php print urldecode(check_url($content['field_additional_info_link']['#items'][0]['url'])); ?></a></p>
                <?php endif; ?>
              </div><!--/text-wrapper-->
              <div class="link after-arrow_down sm-show" data-toggle="moreText" data-class="'hidden' : moreText" ><?php print ka_t('Read more'); ?></div>
            </article>
          </div><!--/col-7-->
          <div class="col-5 sm-12">
            
            <div class="addthis-holder sm-show">
              <h5 class="half-margin"><?php print ka_t('Share'); ?></h5>
              <a class="addthis_button_facebook btn circle before-facebook"></a>
              <a href="" class="addthis_button_twitter btn circle before-twitter"></a>
              <a href="" class="addthis_button_google_plusone_share btn circle before-googleplus"></a>
              <a href="" class="addthis_button_email btn circle before-mail"></a>
            </div><!--/sm-show-->
            
            <?php if(isset($current_group_date_location) && !empty($current_group_date_location['location']['lat']) && !empty($current_group_date_location['location']['lng'])): ?>

            <h5 class="half-margin"><?php print ka_t('Location on map'); ?></h5>

              <div class="map" data-plugin="googleMaps" 
              data-lat="<?php print check_plain($current_group_date_location['location']['lat']); ?>" 
              data-lng="<?php print check_plain($current_group_date_location['location']['lng']); ?>
              " data-icon="<?php print '/' . path_to_theme() . "/assets/imgs/location-default.png"; ?>">
                <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" class="placeholder">
              </div>
              <!--/image-->
              <?php endif; ?>
              
              <h5 class="half-margin"><?php print ka_t('Organiser'); ?></h5>

            <p>
              <?php if(!empty($content['field_organizer_name'])): ?>
              <b><?php print ka_t('Organisation'); ?></b>: <?php print check_plain($content['field_organizer_name'][0]['#markup']); ?><br />
              <?php endif; ?>
              <?php if(!empty($content['field_organizer_address'])): ?>
              <b><?php print ka_t('Address'); ?></b>: <?php print check_plain($content['field_organizer_address'][0]['#markup']); ?><br />
              <?php endif; ?>
              <?php if(!empty($content['field_organizer_contact'])): ?>
              <b><?php print ka_t('Contact'); ?></b>: <?php print check_plain($content['field_organizer_contact'][0]['#markup']); ?><br />
              <?php endif; ?>
              <?php if(!empty($content['field_organizer_phone'])): ?>
              <b><?php print ka_t('Phone'); ?></b>: <?php print check_plain($content['field_organizer_phone'][0]['#markup']); ?><br />
              <?php endif; ?>
              <?php if(!empty($content['field_organizer_email'])): ?>
              <b><?php print ka_t('E-Mail'); ?></b>: <?php print spamspan($content['field_organizer_email']['#items'][0]['email']); ?><br />
              <?php endif; ?>
              <?php if(!empty($content['field_organizer_www'])): ?>
              <a target="_blank" href="<?php print check_url($content['field_organizer_www']['#items'][0]['url']); ?>">
                <?php print check_url($content['field_organizer_www']['#items'][0]['url']); ?>
              </a>
              <?php endif; ?>
            </p>
          </div><!--/col-5-->
        </div><!--/row-->

        <?php if(!empty($groups_summary)): ?>
        <div class="row">
          <div class="col-12">
            <h5><?php print ka_t('Information about other groups'); ?></h5>
            
            <?php if(!empty($groups_summary_mobile)): ?>
            <?php print $groups_summary_mobile; ?>
            <?php endif; ?>
            
            <?php if(!empty($groups_summary)): ?>
            <table class="main sm-hide">
              <thead>
                <tr>
                  <th><?php print ka_t('Name'); ?></th>
                  <th><?php print ka_t('Schedule'); ?></th>
                  <th><?php print ka_t('Time'); ?></th>
                  <th><?php print ka_t('Location'); ?></th>
                  <th><?php print ka_t('Price'); ?></th>
                </tr>
              </thead>
               <tbody>
                <?php print $groups_summary; ?>
               </tbody>
            </table>
            <?php endif; ?>
            <!--
            <center>
               <a href="" class="btn btn-inactive"><?php print ka_t('Load more'); ?></a>
            </center>
            -->
          </div><!--/col-12-->
        </div><!--/row-->
        <?php endif; ?>

      </div><!--/block-->
    </div><!--/col-12-->
  </div><!--/row-->
  <?php if(!empty($similar_activities)): ?>
  <div class="row">
    <div class="col-12">
      <div class="row">
        <div class="col-12">
          <h5><?php print ka_t('Similar recreational activities') ?></h5>
        </div><!--/col-12-->
      </div><!--/row-->
      
      
      <div class="row">
        <div class="col-12">
          <?php print $similar_activities; ?>
        </div><!--/col-12-->
      </div><!--/row-->
      
    </div><!--/col-12-->
  </div>
  <?php endif; ?>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    ?>
</div>
<?php if($page && isset($content['field_ka_status']['#items'][0]['value']) && $content['field_ka_status']['#items'][0]['value'] === 'published'): ?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-596620478a40d533"></script>
<?php elseif(isset($content['field_ka_status']['#items'][0]['value']) && $content['field_ka_status']['#items'][0]['value'] === 'preview'): ?>
<div class="event_preview">
  <div class="event_preview-wrapper">
     <div class="inline">
        <div class="row">
           <div class="col-10">
              <h1><?php print $title; ?></h1> 
              <p><b><?php print t('Publish date', array(), array('langcode' => $_GET['lang'])); ?></b>: <?php echo date('d.m.y - H:i', $changed) ?></p>
              <p>
                <a data-target="preview-delete" class="link secondary half-margin"><?php print t('Close preview', array(), array('langcode' => $_GET['lang'])); ?></a>
              </p>
              <div id="preview-form">
                <?php print drupal_render($preview_form_submit); ?>
              </div>
           </div><!--/col-8-->
           
           <div class="col-2 text-right">
              <p>
                <?php print t('Check in entered languages', array(), array('langcode' => $_GET['lang'])); ?>: <br>
                <?php print $language_switcher_preview; ?>
              </p>
           </div><!--/col-4-->
        </div><!--/row-->
     </div><!--/inline-->
  </div><!--/event_preview-wrapper-->
</div>
<?php endif; ?>