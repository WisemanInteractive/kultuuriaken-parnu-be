<div class="row">
  <div class="col-12">
    <h3>
    <?php if($node_type === 'event'): ?>
      2. <?php print ka_t('Event type'); ?>
    <?php else: ?>
      1. <?php print ka_t('Recreational activity type'); ?>
    <?php endif; ?>
    <?php if(!empty($data['errors'])): ?>
      <span class="circle-icon before-exclamation danger"></span>
    <?php else: ?>
      <span class="circle-icon before-tick"></span>
    <?php endif; ?>
     <a data-change-id="change-category-summary" href="" class="link secondary before-edit pull-right"><?php print ka_t('Change'); ?></a>
    </h3>
  </div><!--/col-12-->
</div>
<div class="row">
    <div class="col-12">
       <div class="block dashed">
          <div class="row">
             <div class="col-6">
                <p>
                  <b><?php print ka_t('Field'); ?></b>: 
                  <?php if(!empty($data['category']['field_category'])) {
                    $categories_string = array();
                    foreach($data['category']['field_category'] as $category) {
                      if(!empty($category['term']->name)) {
                        $categories_string[] = $category['term']->name;
                      }
                    }
                    print implode(', ', $categories_string);
                  } ?> 
                  <?php if(!empty($data['errors']['field_category'])) {print '<span class="red-text">' . implode('; ', $data['errors']['field_category']) . '</span>';} ?><br>
                  <b><?php print ka_t('Category'); ?></b>: 
                  <?php if(!empty($data['category']['field_subcategory'])) {
                    $categories_string = array();
                    foreach($data['category']['field_subcategory'] as $category) {
                      if(!empty($category['term']->name)) {
                        $categories_string[] = $category['term']->name;
                      }
                    }
                    print implode(', ', $categories_string);
                  } ?> 
                  <?php if(!empty($data['errors']['field_subcategory'])) {print '<span class="red-text">' . implode('; ', $data['errors']['field_subcategory']) . '</span>';} ?><br>
                </p>
             </div><!--/col-12-->
             <?php if(!empty($data['field_target_groups'])): ?>
             <div class="col-6">
                <p>
                  <b><?php print ka_t('Target groups'); ?></b>: <?php if(!empty($data['field_target_groups']['rendered'])) {print $data['field_target_groups']['rendered'];} ?> 
                  <?php if(!empty($data['errors']['field_target_groups'])) {print '<span class="red-text">' . implode('; ', $data['errors']['field_target_groups']) . '</span>';} ?>
                  <br>
                </p>
             </div><!--/col-12-->
             <?php endif; ?>
          </div><!--/row-->
       </div><!--/block dashed-->
    </div><!--/col-12-->
  </div>