<?php if($container['type']['#access']): // Multigroup ?>
  <div class="row">
    <div class="col-12">
      <div class="form-item">
        <span class="form-item_label_title"><?php print ka_t('Choose schedule type'); ?>:</span>
        <label>
          <span class="customRadio">
            <?php print render($container['type']['regular']); ?>
            <span class="indicator"></span>
          </span><!--/customRadio-->
          <span class="label-title"><?php print ka_t('Regular schedule'); ?></span>
        </label>
        <label>
          <span class="customRadio">
            <?php print render($container['type']['fixed']); ?>
            <span class="indicator"></span>
          </span><!--/customRadio-->
          <span class="label-title"><?php print ka_t('Specific dates'); ?></span>
        </label>
         
      </div><!--/form-item-->
    </div><!--/col-12-->
  </div><!--/row-->
  
  <?php if(!empty($container['regular'])): ?>
  <div class="row">
      
    <div class="col-2">
      <div class="form-item rounded-corners">
          <div class="form-item_title"><?php print ka_t('Beginning date'); ?></div>
            <i class="after-calendar"></i>
            <?php print render($container['regular']['period_from']); ?>
      </div>
    </div><!--/col-2-->
     
    <div class="col-2">
      <div class="form-item rounded-corners">
        <div class="form-item_title"><?php print ka_t('Ending date'); ?></div>
        <i class="after-calendar"></i>
        <?php print render($container['regular']['period_to']); ?>
      </div>
    </div><!--/col-2-->
               
  </div><!--/row-->
  <?php endif; ?>
                
  <table class="table-form">
  <thead>
    <tr>
       <th class="size-m"><?php print ka_t('Day'); ?>:</th>
       <th class="size-xs"><?php print ka_t('Time'); ?>:</th>
       <th class="minimal"></th>
       <th class="size-xs"></th>
       <th class="size-l"><?php print ka_t('Location'); ?>:</th>
       <th class="size-l"><?php print ka_t('Address'); ?>:*</th>
       <th class="minimal"></th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($container['regular'])): ?>
      <?php foreach($container['regular']['day'] as $day_num => $day): 
      if(!is_numeric($day_num)) continue; ?>
      <tr>
      <td class="size-m">
        <div class="form-item">
          <?php print render($container['regular']['weekdays'][$day_num]); ?>
        </div><!--/form-item-->
      </td>
      <td class="size-xs">
        <div class="form-item">
           <?php print render($container['regular']['day'][$day_num]['time_from']); ?>
        </div><!--/form-item-->
      </td>
      <td class="minimal">
          -
      </td>
      <td class="size-xs">
          <div class="form-item">
            <?php print render($container['regular']['day'][$day_num]['time_to']); ?>
          </div><!--/form-item-->
       </td>
       <td class="size-l">
          <?php print render($container['regular']['day'][$day_num]['location']); ?>
       </td>
       <td class="size-l">
        <div class="form-item">
          <?php print render($container['regular']['day'][$day_num]['address']); ?>
          <?php print render($container['regular']['day'][$day_num]['street_number']); ?>
          <?php print render($container['regular']['day'][$day_num]['route']); ?>
          <?php print render($container['regular']['day'][$day_num]['city']); ?>
          <?php print render($container['regular']['day'][$day_num]['state']); ?>
          <?php print render($container['regular']['day'][$day_num]['postal_code']); ?>
          <?php print render($container['regular']['day'][$day_num]['lng']); ?>
          <?php print render($container['regular']['day'][$day_num]['lat']); ?>
        </div>
       </td>
       <td class="minimal">
        <a data-action="cancel-regular-date"  href="" class="link secondary after-close"><?php print ka_t('Cancel'); ?></a>
       </td>
    </tr>
      <?php endforeach; ?>
    <?php elseif(!empty($container['fixed'])): ?>
      <?php foreach($container['fixed'] as $key => $fixed_date): 
      if(!is_numeric($key)) continue; ?>
      <tr>
        <td class="size-m">
          <div class="form-item rounded-corners">
            <i class="after-calendar"></i>
            <?php print render($fixed_date['date']); ?>
          </div><!--/form-item-->
        </td>
        <td class="size-xs">
          <div class="form-item">
            <?php print render($fixed_date['time_from']); ?>
          </div><!--/form-item-->
        </td>
        <td class="minimal">
            -
        </td>
        <td class="size-xs">
          <div class="form-item">
            <?php print render($fixed_date['time_to']); ?>
          </div><!--/form-item-->
         </td>
         <td class="size-l">
          <div class="form-item">
            <?php print render($fixed_date['location']); ?>
          </div><!--/form-item-->
         </td>
         <td class="size-l">
            <div class="form-item">
              <?php print render($fixed_date['address']); ?>
              <?php print render($fixed_date['street_number']); ?>
              <?php print render($fixed_date['route']); ?>
              <?php print render($fixed_date['city']); ?>
              <?php print render($fixed_date['state']); ?>
              <?php print render($fixed_date['postal_code']); ?>
              <?php print render($fixed_date['lng']); ?>
              <?php print render($fixed_date['lat']); ?>
            </div><!--/form-item-->
         </td>
         <td class="minimal">
          <?php if(!empty($fixed_date['remove'])): ?>
            <a href="" class="date-remove link secondary after-close"><?php print ka_t('Cancel'); ?></a>
            <?php print render($fixed_date['remove']); ?>
          <?php endif; ?>
         </td>
      </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
  
  <?php if(!empty($container['fixed']['add_more_group_dates'])): ?>
  <div class="row">
    <div class="col-12">
       <a data-target-next="add-more-dates" href="" class="link before-plus"><?php print ka_t('Add new dates'); ?></a>
       <?php print render($container['fixed']['add_more_group_dates']); ?>
    </div><!--/col-12-->
  </div>
  <?php endif; ?>

<?php else: // Single group ?>

<?php endif; ?>