<?php if($container['activity_type']['#access']): ?>
<div class="row">
    <div class="col-6">
      <div class="form-item">
        <label>
          <span class="customRadio">
            <?php print render($container['activity_type']['multigroup']); ?>
            <span class="indicator"></span>
          </span><!--/customRadio-->
          <span class="label-title"><?php print ka_t('Enter schedule information by groups'); ?></span>
        </label>
      </div>
    </div>
    <div class="col-6">
      <div class="form-item">
        <label>
          <span class="customRadio">
            <?php print render($container['activity_type']['single_group']); ?>
            <span class="indicator"></span>
          </span><!--/customRadio-->
          <span class="label-title"><?php print ka_t('Enter main schedule information'); ?></span>
        </label>
      </div>
    </div>
</div>
<?php endif; ?>

<?php if(!empty($container['add_group']) && $container['add_group']['#access']): ?>
<div class="row">
  <div class="col-6">
     <p><?php print ka_t('Enter the recreational activity schedule information by groups'); ?></p>
  </div><!--/col-6-->
  <div class="col-6 text-right">
    <a href="" data-target="add-group" class="link after-plus"><?php print ka_t('Add group'); ?></a>
    <?php print render($container['add_group']); ?>
  </div><!--/col-6-->
</div>
<?php endif; ?>

<?php foreach($container['groups'] as $key => $group):
  if(!is_int($key) || $group['remove_group_confirmed']['#value']) continue;
?>
  <?php print render($group); ?>
<?php endforeach; ?>