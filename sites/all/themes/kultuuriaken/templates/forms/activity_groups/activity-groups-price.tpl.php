<div class="row pull-up">
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title input-height"><?php print ka_t('Set price'); ?>:*</div>
    </div><!--/form-item-->
  </div><!--/col-2-->
  
  <div class="col-10">
    
    <div class="form-item multiple">
      <label class="input-height">
        <span class="customRadio">
          <?php print render($container['type']['single_payment']); ?>
          <span class="indicator"></span>
        </span>
        <span class="label-title"><?php print ka_t('Single payment'); ?></span>
      </label>
      <?php print render($container['single_payment']); ?>
      <span class="label-title">€</span>
    </div><!--/form-item-->
    
    <div class="form-item multiple">
      <label class="input-height">
        <span class="customRadio">
          <?php print render($container['type']['monthly_payment']); ?>
          <span class="indicator"></span>
        </span><!--/customRadio-->
        <span class="label-title"><?php print ka_t('Monthly payment'); ?></span>
      </label>
      <?php print render($container['monthly_payment']); ?>
      <span class="label-title"><?php print ka_t('€/month'); ?></span>
    </div><!--/form-item-->
    <?php //dpm($container); ?>
    <div class="form-item multiple">
      <label class="input-height">
        <span class="customRadio">
          <?php print render($container['type']['free']); ?>
          <span class="indicator"></span>
        </span><!--/customRadio-->
        <span class="label-title"><?php print ka_t('Free'); ?></span>
      </label>
    </div><!--/form-item-->
        
  </div><!--/col-10-->
</div><!--/row-->

<div class="row pull-up">
    <div class="col-2">
        <div class="form-item">
            <div class="form-item_title input-height"><?php print ka_t('Additional comment:'); ?></div>
        </div><!--/form-item-->
    </div><!--/col-2-->

    <div class="col-8">
        <?php print render($container['additional_comment']); ?>
    </div><!--/col-8-->
</div><!--/row-->