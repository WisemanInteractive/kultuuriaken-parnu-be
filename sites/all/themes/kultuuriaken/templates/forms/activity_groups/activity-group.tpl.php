<?php if(!empty($container['#active_group']) && $container['#active_group']): ?>
  <div class="row">
    <div class="col-12">
      <div class="block half-dashed">
        <div class="row">
          <div class="col-12">
            
            <?php if($container['#group_type'] === 'multigroup'): ?>
              <h5><?php print ka_t('Enter the new group\'s information'); ?></h5>
              <h6>1. <?php print ka_t('Group name'); ?>: <?php print $group_name_tooltip; ?></h6>
            <?php else: ?>
              <h5><?php print ka_t('Enter the activity information'); ?></h5>
              <h6>1. <?php print ka_t('Activity name'); ?>: <?php print $group_name_tooltip; ?></h6>
            <?php endif; ?>
            
            <div class="row">
              <div class="col-8">
                 <?php print render($container['name']); ?>
                 <?php print render($container['name_t']); ?>
                 <?php print render($container['add_name_other_languages_link']); ?>
              </div><!--/col-8-->

              <?php if(!empty($container['is_full']) && $container['activity_type']['#value'] !== 'single_group'): ?>
              <div class="col-4">
                 <div class="form-item">
                    <div class="form-item_title">&nbsp;</div>
                    <label class="input-height">
                    <?php print render($container['is_full']); ?>
                    <span class="label-title"><?php print ka_t('Group is full'); ?></span>
                    </label>
                 </div><!--/form-item-->
              </div><!--/col-4-->
              <?php endif; ?>
                   
            </div><!--/row-->
            
            <h6>2. <?php print ka_t('Target group'); ?>: 
            
            <?php print $group_age_tooltip; ?></h6>
                
            <div class="row">
               <div class="col-3">
                  <div class="form-item">
                    <label class="input-height">
                      <span class="customCheckbox">
                         <?php print render($container['age_groups']['children']); ?>
                         <span class="indicator"></span>
                      </span><!--/customRadio-->
                      <span class="label-title"><?php print ka_t('Children aged'); ?></span>
                    </label>
                    
                  </div><!--/form-item-->
               </div><!--/col-3-->
               <div class="col-9">
                  <div class="form-item size-xs">
                    <?php print render($container['age_range']['from']); ?>
                    <div class="form-item_title"><?php print ka_t('to'); ?></div>
                  </div>
                  <div class="form-item size-xs">
                    <?php print render($container['age_range']['to']); ?>
                    <div class="form-item_title"><?php print ka_t('years'); ?></div>
                  </div>
               </div><!--/col-9-->
            </div><!--/row-->
                
            <div class="row pull-up">
               <div class="col-3">
                  <div class="form-item">
                    <label class="input-height">
                      <span class="customCheckbox">
                        <?php print render($container['age_groups']['adults']); ?>
                        <span class="indicator"></span>
                      </span><!--/customRadio-->
                      <span class="label-title"><?php print ka_t('Adults'); ?></span>
                    </label>
                    
                  </div><!--/form-item-->
               </div><!--/col-3-->
            </div><!--/row-->
                
            <h6>3. <?php print ka_t('Time and place'); ?>: <?php print $time_place_tooltip; ?></h6>
            
            <?php if(!empty($container['schedule'])): ?>
              <?php print render($container['schedule']); ?>
            <?php endif; ?>
            
          </div><!--/col-12-->
        </div><!--/row-->
          
        <div class="row">
          <div class="col-12">
             
             <h6>4. <?php print ka_t('Price'); ?>: <?php print $price_tooltip; ?></h6>
             
          </div><!--/col-12-->
        </div><!--/row-->
          
        <?php print render($container['price']); ?>
        
      </div><!--/block-->
       
    </div><!--/col-12-->
  </div>
<?php else: 
  $group_data = !empty($container['#group_summary']) ? $container['#group_summary'] : $group;
  $errors = NULL;

  if(!isset($group_id)) {
    $group_id = $container['#group_nr'];
  }

  if(!empty($data['errors']['groups'][$group_id])) {
    $errors = $data['errors']['groups'][$group_id];
  } else if(!empty($group_data['errors']['groups'][$group_id])) {
    // For existing node
    $errors = $group_data['errors']['groups'][$group_id];
  } else if(!empty($summary['#data']['errors']['groups'][$group_id])) {
    // Summary view
    $errors = $summary['#data']['errors']['groups'][$group_id];
  }
?>
  <div class="row">
    <div class="col-12">
      <div class="block dashed">
        <div class="row">
          <div class="<?php print empty($container['#group_summary']) ? 'col-5' : 'col-4'; ?>">
            <h5 class="no-margin">
              <?php print sprintf('%s: %s', 
                $group_data['activity_type'] === 'multigroup' ? ka_t('Group') : ka_t('Name'), 
              check_plain($group_data['name'])); ?>
              <?php if(isset($errors['name'])) print ka_error_string($errors['name']); ?>
              <?php if(!empty($errors)): ?>
                <span class="circle-icon before-exclamation danger"></span>
              <?php else: ?>
                <span class="circle-icon before-tick"></span>
              <?php endif; ?>
            </h5>
            <p>
            <?php if(!empty($group_data['name_t']['en'])): ?>
              <?php print sprintf('<b>%s</b>: %s', 'ENG', check_plain($group_data['name_t']['en'])); ?>
              <?php if(isset($errors['name_t']['en'])) print ka_error_string($errors['name_t']['en']); ?>
              <br>
            <?php endif; ?>
            <?php if(!empty($group_data['name_t']['ru'])): ?>
              <?php print sprintf('<b>%s</b>: %s', 'RUS', check_plain($group_data['name_t']['ru'])); ?>
              <?php if(isset($errors['name_t']['ru'])) print ka_error_string($errors['name_t']['ru']); ?>
              <br>
            <?php endif; ?>
            </p>
            <p>
              <?php if(!empty($group_data['age_groups']['children']) || !empty($group_data['age_groups']['adults'])): ?>
              <?php 
              $demographics = array();
              foreach($group_data['age_groups'] as $key => $age_group) {
                $demographics[$key] = t($age_group, array('context' => 'Age group information'));
              } ?>
              <b><?php print ka_t('Group age'); ?></b>: <?php 
                print render_age_range(
                  check_plain($group_data['age_range']['from']), 
                  check_plain($group_data['age_range']['to']), 
                  $demographics);
                ?>
                <?php if(isset($errors['age'])) print ka_error_string($errors['age']); ?>
                <br>
              <?php endif; ?>
              <b><?php print ka_t('Price'); ?></b>: 
              <?php if($group_data['price']['type'] === 'single_payment' && !empty($group_data['price']['single_payment'])): ?>
                <?php print sprintf('%s %s', check_plain($group_data['price']['single_payment']), '€'); ?>
              <?php elseif($group_data['price']['type'] === 'monthly_payment' && !empty($group_data['price']['monthly_payment'])): ?>
                <?php print sprintf('%s %s', check_plain($group_data['price']['monthly_payment']), ka_t('€/month')); ?>
              <?php elseif($group_data['price']['type'] === 'free'): ?>
                <?php print ka_t('Free'); ?>
              <?php endif; ?>
              <?php if(isset($errors['price'])) print ka_error_string($errors['price']); ?>
              <br>

              <?php if(isset($group_data['price']['additional_comment'])): // Additional comment preview ?>
                <b><?php print ka_t('Additional comment:'); ?> </b>
                <?php print check_plain($group_data['price']['additional_comment']); ?>
              <?php endif; ?>
              <br>

              <?php if($group_data['activity_type'] === 'multigroup' 
                && $group_data['schedule']['type'] === 'regular'): ?>
                <b><?php print ka_t('Activity period'); ?></b>:
                <?php if(empty($errors['schedule']['regular']['period'])): ?>
                  <?php print sprintf('%s - %s', check_plain($group_data['schedule']['regular']['period_from']), 
                    check_plain($group_data['schedule']['regular']['period_to'])); ?>
                <?php else: ?>
                  <?php if(isset($errors['schedule']['regular']['period'])) print ka_error_string($errors['schedule']['regular']['period']); ?>
                <?php endif; ?>
                <br>
              <?php endif; ?>
            </p>
          </div><!--/col-5-->
          
          <div class="<?php print empty($container['#group_summary']) ? 'col-7' : 'col-8'; ?>">
            <?php if(!empty($container['#group_summary'])): ?>
              <a href="" class="link secondary before-edit pull-right change-group-button"><?php print ka_t('Change'); ?></a>
              <?php print render($container['change_active']); ?>
            <?php endif; ?>
            <div class="form-item <?php if(empty($container['#group_summary'])) print 'text-right'; ?>">
              <?php if(!empty($container) && $container['is_full']['#access']): ?>
              <label>
                  <?php if(!empty($container['is_full'])): ?>
                    <?php print render($container['is_full']); ?>
                  <?php elseif(!empty($summary['is_full'][$group_id])): ?>
                    <?php print render($summary['is_full'][$group_id]); ?>
                  <?php endif; ?>
                <span class="label-title"><?php print ka_t('Group is full'); ?></span>
              </label>
              <?php endif; ?>
            </div><!--/form-item-->
            <p>
              <b><?php print ka_t('Date & location'); ?>:</b><br>
              <?php if(!empty($group_data['schedule']['type']) && $group_data['schedule']['type'] === 'regular'): 
                $days = $group_data['schedule']['regular']['day'];
              ?>
                
                <?php foreach($days as $weekday => $day): 
                  if($group_data['schedule']['regular']['weekdays'][$weekday] === 0) continue;
                  // Build group date string, show errors if applicable.
                  $day_summary = array();
                  $day_summary[] = empty($errors['schedule']['regular']['day'][$weekday]['time']) ?
                    check_plain($day['time_from']) . ' - ' . check_plain($day['time_to']) :
                      ka_error_string($errors['schedule']['regular']['day'][$weekday]['time']);
                  if(!empty($day['location'])) {
                    $day_summary[] = check_plain($day['location']);
                  }
                  $day_summary[] = empty($errors['schedule']['regular']['day'][$weekday]['address']) ?
                    check_plain($day['address']) :
                    ka_error_string($errors['schedule']['regular']['day'][$weekday]['address']);
                ?>
                  <?php print sprintf('<b>%s</b> %s<br>', weekday_array($weekday), implode(" | ", $day_summary)); ?>
                <?php endforeach; ?>
                
                <?php if(!empty($errors['schedule']['regular']['weekdays'])): ?>
                  <?php print ka_error_string($errors['schedule']['regular']['weekdays']); ?>
                <?php endif; ?>
              <?php elseif($group_data['activity_type'] !== 'single_group' && !empty($group_data['schedule']['type']) && $group_data['schedule']['type'] === 'fixed'): 
                $days = $group_data['schedule']['fixed'];
              ?>
                <?php foreach($days as $key => $day): 
                  if(!is_int($key)) continue;
                  // Build group date string, show errors if applicable.
                  $day_summary = array();
                  $day_summary[] = empty($errors['schedule']['fixed'][$key]['time']) ?
                    check_plain($day['time_from']) . ' - ' . check_plain($day['time_to']) :
                      ka_error_string($errors['schedule']['fixed'][$key]['time']);
                  if(!empty($day['location'])) {
                    $day_summary[] = check_plain($day['location']);
                  }
                  $day_summary[] = empty($errors['schedule']['fixed'][$key]['address']) ?
                    check_plain($day['address']) :
                    ka_error_string($errors['schedule']['fixed'][$key]['address']);
                  $schedule_date = empty($errors['schedule']['fixed'][$key]['date']) ?
                    check_plain($day['date']) : ka_error_string($errors['schedule']['fixed'][$key]['date']);
                ?>
                  
                  <?php print sprintf('<b>%s</b> %s<br>', $schedule_date, implode(" | ", $day_summary)); ?>
                <?php endforeach; ?>
              <?php elseif($group_data['activity_type'] === 'single_group'): ?>
                <?php if(!empty($group_data['schedule']['date']['start']['field_event_date_from'])): ?>
                  <?php print sprintf('<b>%s:</b> %s', t('Start'), 
                    check_plain($group_data['schedule']['date']['start']['field_event_date_from'])); ?>
                  <?php if(!empty($errors['schedule']['date']['start']['field_event_date_from'])): ?>
                    <?php print ka_error_string($errors['schedule']['date']['start']['field_event_date_from']); ?>
                  <?php endif; ?>
                <?php endif; ?>
                <?php if(!empty($group_data['schedule']['date']['end']['field_event_date_to'])): ?>
                  <?php print sprintf('<br><b>%s:</b> %s ', t('End'), 
                    check_plain($group_data['schedule']['date']['end']['field_event_date_to'])); ?>
                  <?php if(!empty($errors['schedule']['date']['end']['field_event_date_to'])): ?>
                    <?php print ka_error_string($errors['schedule']['date']['end']['field_event_date_to']); ?>
                  <?php endif; ?>
                <?php endif; ?>
                <?php if(!empty($group_data['schedule']['location'])): ?>
                <?php 
                  $location = array();
                  if(!empty($group_data['schedule']['location']['field_event_location'])) {
                    $location[] = $group_data['schedule']['location']['field_event_location'];
                  }
                  if(!empty($group_data['schedule']['location']['field_event_address'])) {
                    $location[] = $group_data['schedule']['location']['field_event_address'];
                  }
                  $location = implode(', ', $location);
                  print sprintf('<br><b>%s:</b> %s ', t('Location'), check_plain($location)); ?>
                  <?php if(!empty($errors['schedule']['location']['field_event_address'])): ?>
                    <?php print ka_error_string($errors['schedule']['location']['field_event_address']); ?>
                  <?php endif; ?>
                <?php endif; ?>
              <?php endif; ?>
            </p>
          </div><!--/col-7-->
        </div><!--/row-->
        <?php if(!empty($container['remove_group'])): ?>
          <div class="row">
            <div class="col-12">
              
             <a data-remove-group="<?php print $container['remove_group']['#group_id']; ?>" href="" class="link secondary after-close pull-right"><?php print ka_t('Remove'); ?></a>
             <div style="display:none;"><?php print render($container['remove_group']); ?></div>
            </div><!--/col-12-->
          </div>
        <?php endif; ?>
      </div><!--/block dashed-->
    </div><!--/col-12-->
  </div>
  
<?php endif; ?>