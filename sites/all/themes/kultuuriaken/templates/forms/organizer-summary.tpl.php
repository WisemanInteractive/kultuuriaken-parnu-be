<div class="row">
  <div class="col-12">
    <h3>5. <?php print ka_t('Organiser data'); ?> 
    <a data-change-id="change-organizer-summary" href="" class="link secondary before-edit pull-right"><?php print ka_t('Change'); ?></a>
    <?php if(!empty($data['errors'])): ?>
    <span class="circle-icon before-exclamation danger"></span>
    <?php else: ?>
    <span class="circle-icon before-tick"></span>
    <?php endif; ?>
    </h3>
  </div>
</div>

<div class="row">
  <div class="col-6">
     <p class="lineHeight-2x">
        <b><?php print ka_t('Organiser'); ?></b>: 
        <?php if(!empty($data['left']['field_organizer_name'])): ?>
          <?php print check_plain($data['left']['field_organizer_name']); ?>
        <?php endif; ?>
        <?php if(!empty($data['errors']['left']['field_organizer_name'])) {print ' ' . ka_form_error($data['errors']['left']['field_organizer_name']);} ?>
        <br>
        <b><?php print ka_t('Address'); ?></b>: 
        <?php if(!empty($data['left']['field_organizer_address'])): ?>
          <?php print check_plain($data['left']['field_organizer_address']); ?>
        <?php endif; ?>
        <?php if(!empty($data['errors']['left']['field_organizer_address'])) {print ' ' . ka_form_error($data['errors']['left']['field_organizer_address']);} ?>
        <br>
        <b><?php print t('Home page'); ?></b>: <a href="<?php print check_url($data['left']['field_organizer_www']); ?>"><?php print check_url($data['left']['field_organizer_www']); ?></a>
        <?php if(!empty($data['errors']['left']['field_organizer_www'])) {print ' ' . ka_form_error($data['errors']['left']['field_organizer_www']);} ?>
        <br>
        <?php if(!empty($data['registration'])): ?>
          <b><?php print ka_t('Registration'); ?></b>:<br>
          <?php if(isset($data['registration']['field_registration_types']['local']) &&
            $data['registration']['field_registration_types']['local'] === 'local'): ?>
            <?php print check_plain($data['registration']['field_registration_local']); ?>
            <?php if(isset($data['errors']['registration']['field_registration_local']))
              print ka_error_string($data['errors']['registration']['field_registration_local']); ?>
            <br>
          <?php endif; ?>
          <?php if(isset($data['registration']['field_registration_types']['internet']) &&
          $data['registration']['field_registration_types']['internet'] === 'internet'): ?>
            <?php print check_url($data['registration']['field_registration_internet']); ?>
            <?php if(isset($data['errors']['registration']['field_registration_internet'])) 
              print ka_error_string($data['errors']['registration']['field_registration_internet']); ?>
            <br>
          <?php endif; ?>
          <?php if(isset($data['registration']['field_registration_types']['email']) &&
          $data['registration']['field_registration_types']['email'] === 'email'): ?>
            <?php print filter_var($data['registration']['field_registration_email'], FILTER_SANITIZE_EMAIL); ?>
            <?php if(isset($data['errors']['registration']['field_registration_email'])) 
              print ka_error_string($data['errors']['registration']['field_registration_email']); ?>
            <br>
          <?php endif; ?>
          <?php if(isset($data['registration']['field_registration_types']['phone']) &&
          $data['registration']['field_registration_types']['phone'] === 'phone'): ?>
            <?php print check_plain($data['registration']['field_registration_phone']); ?>
            <?php if(isset($data['errors']['registration']['field_registration_phone']))
              print ka_error_string($data['errors']['registration']['field_registration_phone']); ?>
          <?php endif; ?>
        <?php endif; ?>
     </p>
  </div><!--/col-6-->
  <div class="col-6">
     <p class="lineHeight-2x">
        <b><?php print ka_t('Contact'); ?></b>: 
        <?php if(!empty($data['right']['field_organizer_contact'])): ?>
          <?php print check_plain($data['right']['field_organizer_contact']); ?>
        <?php endif; ?>
        <?php if(!empty($data['errors']['right']['field_organizer_contact'])) {print ' ' . ka_form_error($data['errors']['right']['field_organizer_contact']);} ?>
        <br>
        <b><?php print ka_t('Phone'); ?></b>: 
        <?php if(!empty($data['right']['field_organizer_phone'])): ?>
          <?php print check_plain($data['right']['field_organizer_phone']); ?>
        <?php endif; ?>
        <?php if(!empty($data['errors']['right']['field_organizer_phone'])) {print ' ' . ka_form_error($data['errors']['right']['field_organizer_email']);} ?>
        <br>
        <b><?php print ka_t('E-Mail'); ?></b>: 
        <?php if(!empty($data['right']['field_organizer_email'])): ?>
          <?php print check_plain($data['right']['field_organizer_email']); ?>
        <?php endif; ?>
        <?php if(!empty($data['errors']['right']['field_organizer_email'])) {print ' ' . ka_form_error($data['errors']['right']['field_organizer_phone']);} ?>
        <br>
     </p>
  </div><!--/col-6-->
</div>