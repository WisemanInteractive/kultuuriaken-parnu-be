<div class="row">
  <div class="col-12">
    <h3>
    <?php if($node_type === 'event'): ?>
     1. <?php print ka_t('Event description'); ?>
    <?php else: ?>
     2. <?php print ka_t('Recreational activity description'); ?>
    <?php endif; ?>
    <?php if(!empty($data['errors']['introduction_languages'])): ?>
    <span class="circle-icon before-exclamation danger"><?php print ka_t('Incomplete'); ?></span>
    <?php endif; ?>
     <a data-change-id="change-introduction-summary" href="" class="link secondary before-edit pull-right"><?php print ka_t('Change'); ?></a>
    </h3>
  </div><!--/col-12-->
</div>

<?php $language_list = language_list();

foreach($data['introduction_languages'] as $lang => $language): 
?>
  <?php if(!empty($language)): ?>
  <div class="row">
    <div class="col-12">
       <div class="block dashed">
          <div class="row">
             <div class="col-6">
                <?php if(empty($data['errors'][$lang])) {$status = '<span class="circle-icon before-tick"></span>';} else {$status = '<span class="circle-icon before-exclamation danger">';}?>
                <h5 class="no-margin"><?php print ka_t('@lang description', array('@lang' => t($language_list[$lang]->name, array(), array('context' => 'KA description field')))); ?> <?php print $status; ?></h5>
                <p>
                  <?php if($language === 'et' || $node_type !== 'activity'): // Show name only for ET. ?>
                  <b><?php print ka_t('Name'); ?></b>: <?php print check_plain($data[$language]['field_title']); ?> <?php if(!empty($data['errors'][$lang]['field_title'])) {print '<span class="red-text">' . implode('; ', $data['errors'][$lang]['field_title']) . '</span>';} ?><br>
                  <?php endif; ?>
                  <b><?php print ka_t('Short description'); ?></b>:<br>
                  <?php print nl2br(check_plain($data[$language]['field_summary'])); ?> <?php if(!empty($data['errors'][$lang]['field_summary'])) {print '<span class="red-text">' . implode('; ', $data['errors'][$lang]['field_summary']) . '</span>';} ?><br>
                  <br>
                  <b><?php print ka_t('Additional information'); ?></b>: <?php print check_url($data[$language]['field_additional_info_link']); ?> <?php if(!empty($data['errors'][$lang]['field_additional_info_link'])) {print '<span class="red-text">' . implode('; ', $data['errors'][$lang]['field_additional_info_link']) . '</span>';} ?><br>
                </p>
             </div><!--/col-6-->
             
             <div class="col-6">
                <p>
                  <b><?php print ka_t('Main text'); ?></b>:<br>
                   <?php print nl2br(check_plain($data[$language]['field_body'])); ?> <?php if(!empty($data['errors'][$lang]['field_body'])) {print '<span class="red-text">' . implode('; ', $data['errors'][$lang]['field_body']) . '</span>';} ?><br>
                </p>
             </div><!--/col-6-->
          </div><!--/row-->
       </div><!--/block dashed-->
    </div><!--/col-12-->
  </div>
  <?php endif; ?>
<?php endforeach; ?>  