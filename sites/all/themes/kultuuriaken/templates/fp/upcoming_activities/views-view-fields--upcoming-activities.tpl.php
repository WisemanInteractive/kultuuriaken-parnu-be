<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
  $dates = calculate_activity_dates($fields['item_id']->content, FALSE, 'next');
  $group_type = $fields['field_activity_group_type']->content;
  $location = !empty($dates[0]['location']['name']) ? check_plain($dates[0]['location']['name']) : check_plain($dates[0]['location']['street']);
  $tags = explode(',',$fields['field_category_activity']->content);
  
?>

<div class="list-item">
   <span class="image" style="background-image:url(<?php print $fields['field_main_image']->content; ?>)">
      <?php if(!empty($fields['field_category_activity'])): ?>
        <ul class="tags filled">
        <?php foreach($tags as $tag_id): 
            $tag = i18n_taxonomy_localize_terms(taxonomy_term_load(trim($tag_id))); 
            
            if(isset($tag->field_color_id[LANGUAGE_NONE][0]['value'])): ?>
      		   <li class="tag-<?php print $tag->field_color_id[LANGUAGE_NONE][0]['value']; ?>"><a href="<?php print $fields['path']->content; ?>"><?php print $tag->name; ?></a></li>
      	  <?php endif; ?>	 
      	<?php endforeach; ?>
      	</ul>
      <?php endif; ?>
      <a href="<?php print $fields['path']->content; ?>">
        <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-56.gif'; ?>" class="placeholder">
      </a>
   </span><!--/image-->
   <a href="<?php print $fields['path']->content; ?>" class="details">
      <span class="title"><?php print $fields['field_activity_group_name']->content; ?></span>
      <span class="info">
        <?php if(!empty($location)): ?>
        <p class="before-location"><?php print $location; ?></p>
        <?php endif; ?>
        <?php if(!empty($fields['field_event_date']->content)): ?>
          <p class="before-calendar"><?php print $fields['field_event_date']->content; ?></p>
        <?php elseif(!empty($fields['field_activity_date_period']->content)): ?>
          <p class="before-calendar"><?php print $fields['field_activity_date_period']->content; ?></p>
        <?php endif; ?>
        
      </span><!--/info-->
   </a><!--/details-->
</div><!--/list-item-->