
var $listFilterJSON;

$.fn.listFilter = function(){
   var main = $(this);
   var inputs = main.find("select,input");
   var sliderInputs = main.find(".slider-range-wrapper input");
   var dateInputs = main.find(".date-wrapper input");
   var delay = 300;
   var outputObj = main.find(".filter-output:first");
   var debounce;
   var url = outputObj.attr("data-url");
   var page = 0;

   var template = main.find(".template-filter:first").html();
   var viewBtns = main.find("[data-view]");
   var mapContainer;

   var tagsObj = main.find("[rel='tags']:first");

   var vatValue = parseInt(inputs.filter("[name='km']:checked").val());
   var currentView = 'list-view';

   var activeLocation = false;
	var firstLoad = false;

   viewBtns.bind("click", function(e){
      e.preventDefault();

      viewBtns.each(function(){
         outputObj.removeClass( $(this).attr("data-view") );
      }).removeClass("active");

      $(this).addClass("active");
      outputObj.addClass( $(this).attr("data-view") );

      currentView = $(this).attr("data-view");
      
		createCookie("view", currentView );
      
		if( firstLoad ){
			//To change template on view-type change
      	if($listFilterJSON) { generateHTML(); }
			initializeMap();
		}
   });

	var view = '';

	if( readCookie("view") ){
		view = readCookie("view");
		
		if( viewBtns.filter("[data-view='"+view+"']").length > 0 ){
			viewBtns.filter("[data-view='"+view+"']").trigger("click");
		}else{
			viewBtns.eq(0).trigger("click");
		}
	}else{
		viewBtns.filter(":visible").filter(".active").eq(0).trigger("click");
	}
   
	
   $(window).bind("resize:listFilter", function(){
		view = readCookie("view");
		
      if( $(window).width() <= 640 ){
         if( viewBtns.filter("[data-view='grid-view']").size() > 0 ){
            if( viewBtns.filter("[data-view='list-view']").is(".active") ){
               viewBtns.filter("[data-view='grid-view']").trigger("click");
            }
         }
      }else{
         if( viewBtns.filter("[data-view='grid-view']").is(".sm-show") ){
            viewBtns.filter("[data-view='list-view']").trigger("click");
         }
      }
   });
   
   if( device.mobile() || $(window).width() <= 640 ){
      if( viewBtns.filter("[data-view='"+view+"']").is(":visible") ){
         viewBtns.filter("[data-view='"+view+"']").trigger("click");
      }else{
         viewBtns.eq(0).trigger("click");
      }
   }
   
   function setInputValue(options){
      
      var input = inputs.filter("[name='"+options.key+"']");

      if( input.is("[type='checkbox']") || input.is("[type='checkbox']") ){
         input = input.filter("[value='"+options.value+"']");
         input.addClass("has-changed").prop("checked", true); 
      }
      else if( input.is("[type='text']") ){
			if( input.attr("name").match("date") ){
				var tmpDate = options.value.replace(/.2[0-9]{3}/g, "");
				input.attr("urlVal",  options.value ).val(tmpDate).addClass("has-changed");
			}else{
				input.val( options.value ).addClass("has-changed");
			}
      }

   }
   
   function initializeInputs(){
      var search = window.location.search;
      

      search = getParameters(search);
      
      for( var i in search ){
         var key = i;
         var value = search[i];
         
         
         if( value.indexOf("|") !== -1 ){
            value = value.split("|");
            
            for( var ii in value ){
               setInputValue({
                  key: key,
                  value: value[ii]
               }); 
            }
            
         }else{
            setInputValue({
               key: key,
               value: value
            });
         }
         
         
         
      }
   };
   
   initializeInputs();
   
   inputs.filter(":checked").not("[name='filter-button']").addClass("has-changed");
   
   var pageEnter = true;
   
   inputs.bind("change getValues", function(e){
      
      var obj = $(this);
      
      var name = obj.attr("name");
      var allInput = inputs.filter("[type='checkbox'][name='"+name+"'][value='all']");
      var siblings = inputs.filter("[type='checkbox'][name='"+name+"']").not("[value='all']");
      var total = siblings.length;
      var totalChecked = siblings.filter(":checked").length;

      if( obj.val() !== "all" ){

        if( total == totalChecked ){
          allInput.prop("checked", true);
          allInput.parents(".form-item:first").removeClass("partial-checked");
        }else if( totalChecked !== 0){
          allInput.prop("checked", false);
          allInput.parents(".form-item:first").addClass("partial-checked");
        }else{
          allInput.parents(".form-item:first").removeClass("partial-checked");
        }
        
      }
      
      
      if( e.type == "change" ){

        if( pageEnter && obj.attr("data-cancel") == "starting_time" || obj.attr("name") == "title"){
          inputs.filter("[name='starting_time']").prop("checked", false);
        }
         
         pageEnter = false;

        if( obj.val() == "all"){
          if( obj.prop("checked") && total > totalChecked && totalChecked !== 0 ){
            obj.parents(".form-item:first").removeClass("partial-checked");
            obj.prop("checked", false);
            siblings.prop("checked", false);
          }else{
            obj.parents(".form-item:first").removeClass("partial-checked");
            siblings.prop("checked", obj.prop("checked"));
          }
        }
      }
         
      clearTimeout(debounce);
      debounce = setTimeout(function(){
         if( obj.is("[type='text'],[type='hidden']") && obj.val() == ""){
            obj.removeClass("has-changed");
         }else{
            obj.addClass("has-changed");   
         }
         
         gatherValues();
      }, delay);
   }).trigger("getValues");
   
   

   function gatherValues( returnValue ){
      var tmpArray = [];

      inputs.filter(".has-changed").each(function(){

         var obj = $(this);
         var name = obj.attr("name");
         var value = obj.val();
			var urlValue = obj.attr("urlVal");

         if( obj.is("[type='checkbox']") || obj.is("[type='radio']") ){
            if( obj.is(":checked") ){

               if( !tmpArray[name] ){
                  tmpArray[name] = new Array();
               }

               if( value ){
                  tmpArray[name].push(value);	
               }

            }

         }else{
            
            if( value !== "" ){
               tmpArray[name] = urlValue || value;  
            }
         }
      });

      if( !returnValue ){
         generateTags();
         pushState(tmpArray);	
      }else{
         return tmpArray;
      }


   };
   
   function generateTags(){
      var maxTags = 4;
      var tagCount = 0;
      var totalTags = 0;

      var moreLabel = outputObj.attr("data-more_label") || "";

      var tags = gatherValues(true);
      
      var tagArray = {};
      
      //console.log(tags);
      
      for( var i in tags ){
                  
         if( tags[i] instanceof Array ){

            for( var ii in tags[i] ){
               var input = inputs.filter("[name='"+i+"'][value='"+tags[i][ii]+"']");
               
               if( input.size() == 0 ){ continue; }
               
               var title = input.parents("label").find(".label-title").text();
               title = title.replace(/\(+.*?\)+/g, "");
               
               if( !tagArray[i] ){
                  tagArray[i] = [];
               }
               tagArray[i].push({
                  "obj": input,
                  "type": "check",
                  "name": i,
                  "value": tags[i][ii],
                  "title": title
               });
            }
         }else{

            if( tags[i] == "" ){ continue; }
            
            var type = inputs.filter("[name='"+i+"']").is("input") ? "input" : "select";
            
            if( type == "input" ){
               
               var parentLi = inputs.filter("[name='"+i+"']").parents("li:first");
               
               if( i == "title" ){
                  
                  tagArray[i] = {
                     "obj": input,
                     "type": "input",
                     "name": i,
                     "value": tags[i]
                  };
               }
               else if (parentLi.is(".slider-range-wrapper")) {
                  
                  var both = inputs.filter("[name='"+i+"']").parent().find("input");
                  var title = "";

                  tagArray[title] = {
                     obj: both,
                     name: both.eq(0).attr("name"),
                     type: "range",
                     title:  title,
                     min: both.filter("[rel='min']").val(),
                     max: both.filter("[rel='max']").val()
                  }
               } 
               else if (parentLi.is(".range-date-wrapper")){
                  var both = inputs.filter("[name='"+i+"']").parents(".range-date-wrapper").find("input");
                  var title = "";

                  tagArray[name] = {
                     obj: both,
                     name: both.eq(0).attr("name"),
                     type: "dateRange",
                     title:  title,
                     min: both.filter("[rel='start']").val(),
                     max: both.filter("[rel='end']").val()
                  }
               };
               
               
            }
            else if( type == "select" ){
               var input = inputs.filter("[name='"+i+"']");
               var name = input.attr("name");
               var value = input.find("option[value='"+input.val()+"']").text();
               var title = input.attr("data-title");
               
               if( name == "sort"){ continue; }
               tagArray[name] = {
                  name: name,
                  type: "select",
                  title:  title,
                  value: value
                  
               }
               
            }
            
         }
         
      }
      
      //console.log(tagArray);

      var output = '';
      
      for( var i in tagArray ){
         //console.log(tagArray);
         var current = tagArray[i];
         
         if( current instanceof Array ){
            //checkboxes
            
            var all = false;

            for( var ii in current ){
               if( current[ii].value == "all" ){
                  all = current[ii];
               }
            }

            if( all ){
               if (all.name == "starting_time" || all.name == "day" || all.name == "filter-button") {

               } else {
                  totalTags++;
                  if( tagCount == maxTags ){ continue; }
                  output+= '<li><a href="#" class="after-close check" data-name="'+all.name+'" data-value="'+all.value+'">'+all.title+'</a></li>';
                  tagCount++;
               }
            }
         }
         
      }

      
      for( var i in tagArray ){
         //console.log(tagArray);
         var current = tagArray[i];
         
         if( current instanceof Array ){
            //checkboxes
            
            var all = false;

            for( var ii in current ){
               if( current[ii].value == "all" ){
                  all = current[ii];
               }
            }

            if( !all ){
               for(var ii in current){
                  var entry = current[ii];
                  
                  if (entry.name == "starting_time" || entry.name == "day" || entry.name == "filter-button") {
   //                  output="";
                  } else {
                     totalTags++;
                     if( tagCount == maxTags ){ continue; }
                     output+= '<li><a href="#" class="after-close check" data-name="'+entry.name+'" data-value="'+entry.value+'">'+entry.title+'</a></li>';
                     tagCount++;
                  }
                     
               }
            }
            
         }else{
            //range
            if( current.type == "range"){
               totalTags++;
               if( tagCount == maxTags ){ continue; }
               output+= '<li><a href="#" class="after-close input" data-name="'+current.name+'">'+current.min+" - "+current.max+'</a></li>';   
               tagCount++;
            }
            else if( current.type == "dateRange"){
               totalTags++;
               if( tagCount == maxTags ){ continue; }
               output+= '<li><a href="#" class="after-close dateinput" data-name="'+current.name+'">'+current.min+" - "+current.max+'</a></li>';   
               tagCount++;
            }
            else if( current.type == "select" ){
               totalTags++;
               if( tagCount == maxTags ){ continue; }
               output+= '<li><a href="#" class="after-close select" data-name="'+current.name+'">'+current.title+' '+ current.value +' </a></li>';   
               tagCount++;
            }else if( current.type == "input" ){
               totalTags++;
               if( tagCount == maxTags ){ continue; }
               output+= '<li><a href="#" class="after-close inputField" data-name="'+current.name+'">'+ current.value +' </a></li>';   
               tagCount++;
            }
         }
         
      }
      

      if( output == '' ){
         tagsObj.parent(".filter-tags").addClass("empty");
      }else{
         tagsObj.parent(".filter-tags").removeClass("empty");
      }
      
      tagsObj.html( output );

      var tagsHidden = (totalTags - tagCount);

      if( tagsHidden > 0 ){
         var moreText = moreLabel.replace("{num}", (totalTags - tagCount));
         tagsObj.append('<li class="leftover-tags">'+moreText+'</li>');
      }
      
      var tagsObjParent = tagsObj.parent(".filter-tags");
      
      tagsObjParent.find(".link").bind("click", function(e){
         e.preventDefault();   
         $(".filter .reset:first .link:first").trigger("click");
      });
      
//      tagsObjParent.find(".toggler").bind("click", function(e){
//         e.preventDefault();   
//         if (tagsObjParent.is(".collapsed")){
//            tagsObjParent.removeClass("collapsed");
//         } else {
//            tagsObjParent.addClass("collapsed");
//         };
//      });
      
      tagsObj.find(".check").each(function(){
         var obj = $(this);
         var name = obj.attr("data-name");
         var value = obj.attr("data-value");
         
         obj.bind("click", function(e){
            e.preventDefault();
            inputs.filter("[name='"+name+"'][value='"+value+"']").prop("checked", false).trigger("change");
         });
      });
      
      tagsObj.find(".input").each(function(){
         var obj = $(this);
         var name = obj.attr("data-name");
         
         obj.bind("click", function(e){
            e.preventDefault();

            inputs.filter("[name='"+name+"']").parent(".form-item").find(".slider-range").trigger("reset");
            inputs.trigger("change");
         });
      });
      
      tagsObj.find(".inputField").each(function(){
         var obj = $(this);
         var name = obj.attr("data-name");
         
         obj.bind("click", function(e){
            e.preventDefault();

            inputs.filter("[name='"+name+"']").val("").trigger("change");
            inputs.trigger("change");
         });
      });
      
      tagsObj.find(".dateinput").each(function(){
         var obj = $(this);
         var name = obj.attr("data-name");
         
         obj.bind("click", function(e){
            e.preventDefault();

            inputs.filter("[name='"+name+"']").parents(".range-date-wrapper").find("input").val("").trigger("change");
         });
      });
      
   };

   function pushState(stateArray){

      var url = "?";

      for( var i in stateArray ){
         var key = i;
         var values = stateArray[i];


         if( url !== "?" ){
            url+="&";
         }

         url+= key+"=";

         if( values instanceof Array ){
            var valueCounter = 0;
            for( var ii in values ){
               if( valueCounter !== 0 ){ url+="|"; }
               valueCounter++;
               url+= values[ii];
            }
         }else{
            url+=values;
         }
      }

      history.replaceState(null, null, url);

      var data = url.replace("?", "");

      if( stateArray['km'] && vatValue !== parseInt(stateArray['km']) ){
         window.location.reload();
      }else{
         getData(data);	
      }

   }
   
   function getData(data){
      
      var loader;
      outputObj.html( loader = $('<div class="ajax-loader"></div>') );
      loader.fadeIn(250);
      setTimeout(function(){
			
         $.ajax({
            url: url,
            method: "get",
            data: data,
            dataType: "json",
           cache: true,
            success: function(response){

					firstLoad = true;
					
               $listFilterJSON = response;
               
               var events = false;
               
               if( $listFilterJSON.list && $listFilterJSON.list[0] && $listFilterJSON.list[0].start_date ){
                  events = true;
               }
               
               //Chronological order
               var sortedList = [];
               var sortedSequence = Object.keys(response.list).sort(function(a, b) {
                  if( events ){
                     return response.list[a].start_date - response.list[b].start_date;
                  }else{
                     return response.list[a].date_unix - response.list[b].date_unix;
                  }
               });
               sortedSequence.forEach(function(key) {
                  sortedList.push(response.list[key]); 
               });
               $listFilterJSON.list = Array.from(sortedList)

               generateHTML();
               
               //updateCounts( response );
               //bindEvents();
            }
         })

      }, 0);

   }

   function arrayToString(stateArray){
      var url = '';
      for( var i in stateArray ){
         var key = i;
         var values = stateArray[i];


         if( url !== "" ){
            url+="&";
         }

         url+= key+"=";

         if( values instanceof Array ){
            var valueCounter = 0;
            for( var ii in values ){
               if( valueCounter !== 0 ){ url+="|"; }
               valueCounter++;
               url+= values[ii];
            }
         }else{
            url+=values;
         }
      }
      return url;
   }


   function bindEvents(){
      var loadMore = outputObj.find("[rel='load-more']");
      var loadAll = outputObj.find("[rel='load-all']");

      loadMore.bind("click", function(e){
         e.preventDefault();
         var data = arrayToString(gatherValues(true));

         page++;

         if( data == "" ){
            data+= "page="+page;
         }else{
            data+= "&page="+page;
         }

         $.ajax({
            url: url,
            method: "get",
            data: data,
            dataType: "json",
           cache:false,
            success: function(response){
               outputObj.find(".section-button").remove();
               outputObj.append( response.content );
               bindEvents();
            }
         });

      });

      loadAll.bind("click", function(e){
         e.preventDefault();

         var data = arrayToString(gatherValues(true));

         if( data == "" ){
            data+= "page=all";
         }else{
            data+= "&page=all";
         }

         $.ajax({
            url: url,
            method: "get",
            data: data,
            dataType: "json",
           cache: true,
            success: function(response){
               outputObj.find(".section-button").remove();
               outputObj.html( response.content );
               bindEvents();
            }
         });

      });

      outputObj.find(".list-item").each(function(){
         var obj = $(this);
         var location = obj.find(".before-location:first");

         location.bind("click", function(e){
            e.preventDefault();

            activeLocation = {
               lng: parseFloat(location.attr("data-lng")),
               lat: parseFloat(location.attr("data-lat"))
            }

            viewBtns.filter("[data-view='map-view']").trigger("click");
         });
      });

      initializeMap();
   }

   function generateHTML(){

      var filters = getMapFilters();
      if(currentView == 'list-view') { var enableCat = true } 
      else { enableCat = false }
      
//      $listFilterJSON.list = sortByKey($listFilterJSON.list, 'date_unix');
      
		var list = JSON.stringify($listFilterJSON);
         list = JSON.parse(list);
		
		if( !enableCat ){
         
			list.list = sortByKey(list.list, 'date_unix');
			for( var i in list.list ){
				if( list.list[i].category !== 4 ){list.list[i].category = 1;}
			}
		}

      swig.setDefaults({ cache: false, autoescape: false });
      var myhtml = swig.render(template, {locals:{
         data: list,
         filters: filters,
         current: enableCat
      }});
      outputObj.html( myhtml );
      bindEvents();
   }

   function getMapFilters(){
      var tmpArray = new Array();
      var eventList = $listFilterJSON.list;

      for( var i in eventList ){
         for( var p in eventList[i].theme ){
            tmpArray["theme-"+eventList[i].theme[p]+""] = true;
         }
      }

      return tmpArray;
   }

   function updateCounts( response ){
      return false;

      for( var i in response.counts ){
         var entry = response.counts[i];
         var name = entry.name;
         var value = entry.value;
         var count = entry.count;

         inputs.filter("[name='"+name+"'][value='"+value+"']:first").parents("label:first").find(".count").text( count );
      }
   }

   function initializeMap(){

      if( currentView !== "map-view" ){ return false; }

      mapContainer = main.find(".map:first");

      var zoomIn, zoomOut, buttonsCont;
      var markerList = [];
      
      mapContainer.append(buttonsCont = $('<div class="map-buttons"></div>') );

      buttonsCont.append( zoomIn = $("<a href='#' class='btn btn-filled circle before-plus'></a>") );
      buttonsCont.append( zoomOut = $("<a href='#' class='btn btn-filled circle before-minus'></a>") );

      zoomIn.bind("click", function(e){
         e.preventDefault();
         map.setZoom(map.getZoom()+1);
      });

      zoomOut.bind("click", function(e){
         e.preventDefault();
         map.setZoom(map.getZoom()-1);

      });
      
      var mapObj = mapContainer.find(".map-obj:first");
      mapObj = mapObj[0];
      var filterContainer = main.find(".filter-buttons:first");
      var filterInputs = filterContainer.find("input");
      var filteredList;
      
      var mapOptions = {
         zoom: 16,
         styles: $mapStyles,
         maxZoom: 18,
         disableDefaultUI: true
      };
      
      var map = new google.maps.Map(mapObj, mapOptions);

      var bounds = new google.maps.LatLngBounds();
      
//      var iconPrefix = "/sites/all/themes/kultuuriaken/assets/imgs/location-";
//
//      if( location.protocol == "file:" || location.hostname == "localhost" ){
//         iconPrefix = "assets/imgs/location-"
//      }
      
      
      filterInputs.bind("change", function(){
         filterMapObjects();
      });
      
      function filterMapObjects(){
         
         var checkedFilters = filterInputs.filter(":checked");
         var totalCheckedFilters = checkedFilters.size();
         var totalFilters = filterInputs.size();

         if( totalCheckedFilters == totalFilters ){
            filteredList = $listFilterJSON.list;
         }else{
            filteredList = [];
            
            var activeFilters = new Array();
            
            checkedFilters.each(function(){
               activeFilters.push( $(this).val() );   
            });
            
            for( var i in $listFilterJSON.list ){
               var current = $listFilterJSON.list[i];
               
               var inList = false;
               for( var ii in activeFilters ){
                  for( var p in current.theme ){
                     if( activeFilters[ii] == current.theme[p] ){
                        inList = true;
                     }
                  }
                  
               }
               
               if( inList ){
                  filteredList.push(current);
               }
            }
         }
         
         for( var i in markerList ){
            if( markerList[i].infobubble ){
               if( markerList[i].infobubble.isOpen() ){
                  markerList[i].infobubble.close();
               };
            };
            markerList[i].marker.setMap(null);   
         }
         
         markerList = [];
         
         for( var i in filteredList ){
            var current = filteredList[i];
            generateMarker(current);
         }

         for( var i in markerList ){
            bindMarker( markerList[i], i );
         }
      }
      
      filterMapObjects();
      
      function generateMarker(current){
         
         var latlng = {
            lat: parseFloat(current.lat),
            lng: parseFloat(current.lng)
         }

         var theme = current.theme;
         var icon = current.icon;
         

         var marker = new google.maps.Marker({
            position: {lat: latlng.lat, lng: latlng.lng},
            map: map,
            title: 'PEPSized Coffee',
            icon: {
               url: icon,
               scaledSize: new google.maps.Size(33, 46)
            }
         });
         

         var tmp = {
            marker: marker,
            data: current
         }
         markerList.push(tmp);

         bounds.extend(new google.maps.LatLng(latlng.lat, latlng.lng));

         marker.setMap(map);

      }
      
      function bindMarker( arr, i){
         
         var marker = arr.marker;
         
         var infobubble;
         
         marker.addListener('click', function() {

            
            if( !markerList[i].infobubble ){
               infobubble = new InfoBubble({
                  content: "<div class='infobubble-content'><h3><a href='"+arr.data.url+"'>"+arr.data.title+"</a></h3><p class='before-location'>"+arr.data.location+"</p><p class='before-calendar'>"+arr.data.date+"</p></div>",
                  maxWidth: 240,
                  shadowStyle: 0,
                  borderRadius: 5,
                  borderWidth: 2,
                  borderColor: "#E6E6E6",
                  padding: 0,
                  minWidth: 300,
                  maxWidth: 300,
                  maxHeight: 500,
                  minHeight: 110,
                  boxStyle: { 
                     background: "#fff",
                     minHeight: 0,
                     maxHeight: 500,
                     opacity: 1,
                     width: "300px"
                  }
               });  
            }else{
               if( markerList[i].infobubble.isOpen() ){
                  markerList[i].infobubble.close();
                  return false;
               }
            }

            for( var ii in markerList ){
               if( markerList[ii].infobubble ){
                  markerList[ii].infobubble.close();
               }
            }
            
            google.maps.event.addListener(map, "click", function () { 
               infobubble.close();
            });
            
            infobubble.open(map, marker);
            
            markerList[i].infobubble = infobubble;

         });

      }

      if( activeLocation ){
         map.setZoom(15);
         map.panTo(new google.maps.LatLng(activeLocation.lat, activeLocation.lng));
         activeLocation = false;
      }else{  
         map.fitBounds(bounds);
      }



   }

}

function getParameters(hash, strip){
    var params = {};

    var keyValuePairs = (hash || '').substr(1).split('&');
    for (var x in keyValuePairs){
    	if( keyValuePairs[x] == "" ){
    		continue;
    	}
		var split = keyValuePairs[x].split('=', 2);
		if( !strip && split[0].substring(0,1) !== "_" ){
			params[split[0]] = (split[1]) ? decodeURI(split[1]) : "";
		}
		else if( strip && split[0].substring(0,1) == "_" ){
			params[split[0]] = (split[1]) ? decodeURI(split[1]) : "";
		}
        
    }
    return params;
}
