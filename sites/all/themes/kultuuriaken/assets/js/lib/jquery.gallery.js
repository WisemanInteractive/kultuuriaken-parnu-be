
$.fn.gallery = function(){

   var main = $(this);
   var anchors = main.find("a").not("[data-plugin]");
   if( main.is("a") ){
      anchors = main;
   }
   var gallery, galleryWrapper, thumbnailsContainer, thumbnails, visibleThumbs, activeIndex, imageContainer, previousButton, nextButton, closeButton;
   var thumbWidth = 138;
   var totalThumbs = anchors.size()-1;
   var direction = 1;
   var flag = false;


   anchors.bind("click", function(e){
      e.preventDefault();
      var obj = $(this);
      var index = anchors.index(obj);
      openGallery(index);
   });

   function openGallery( index ){
      activeIndex = index;
      $("html").addClass("gallery-open");
      $("body").append(gallery = $('<div class="gallery-popup"><div class="gallery-wrapper"></div></div>') );
      galleryWrapper = gallery.find(".gallery-wrapper:first");


      galleryWrapper.append(imageContainer = $('<div class="gallery-image"></div>') );

      var swiperSlides = '';

      for( var i = 0; i < totalThumbs+1; i++ ){
         var title = anchors.eq(i).attr("title") || anchors.eq(i).find("img:first").attr("title") || "";
         var author = anchors.eq(i).attr("data-author") || anchors.eq(i).find("img:first").attr("alt") || "";
         swiperSlides+= '<div class="swiper-slide"><div class="image"><div class="loader"></div><span class="state">'+(i+1)+' / '+(totalThumbs+1)+'</span></div></div>';
      }

      imageContainer.append(
         '<div class="swiper-container">'+
            '<div class="swiper-holder">'+
               '<div class="swiper-wrapper">'+
                  swiperSlides+
               '</div>'+
            '</div>'+
         '</div>'
      );


      if( totalThumbs > 0 ){
         imageContainer.append(previousButton = $('<div class="gallery-previous"></div>') );
         imageContainer.append(nextButton = $('<div class="gallery-next"></div>') );
      }

      imageContainer.append(closeButton = $('<div class="gallery-close"></div>') );

      if( thumbnailsContainer ){
         thumbnails = thumbnailsContainer.find("li");
      }

      var mySwiper;

      $(window).bind("resize:gallery", function(){
         var contWidth = galleryWrapper.width();


         if( mySwiper ){
            mySwiper.update()
         }

      }).trigger("resize:gallery");

      mySwiper = new Swiper(imageContainer.find(".swiper-container"), {
         onSlideChangeStart: function(swiper){

            activeIndex = swiper.activeIndex;

            if( activeIndex == 0 && totalThumbs >  0){
               previousButton.hide();
            }
            else if( activeIndex !== 0 && totalThumbs > 0 ) {
               previousButton.show();
            }

            if( activeIndex == totalThumbs && totalThumbs > 0 ){
               nextButton.hide();
            }
            else if( activeIndex !== totalThumbs && totalThumbs > 0 ) {
               nextButton.show();
            }

            imageContainer.find("iframe").remove();

            loadImage(activeIndex);

         },
         setWrapperSize: true,
         initialSlide: index
      });   

      loadImage(index);

      if( index == 0 && totalThumbs > 0 ){
         previousButton.hide();
      }
      else if( index == totalThumbs && totalThumbs > 0 ){
         nextButton.hide();
      }

      if( thumbnailsContainer ){
         thumbnails.bind("click", function(e){
            e.preventDefault();
            var obj = $(this);
            var i = parseInt(obj.find("a:first").attr("href").split("#")[1]);
            mySwiper.slideTo(i);

         }).eq(index).trigger("click");
      };

      $(window).bind("keydown", function(e){
         var key = e.keyCode ? e.keyCode : e.which;
         var tmpIndex = activeIndex;

         if( key == 37 ){
            mySwiper.slidePrev();
         }else if( key == 39 ){
            mySwiper.slideNext();
         }else if( key == 27 ){
            closePopup(mySwiper);
            return false;
         }
      });

      if( totalThumbs > 0 ){
         previousButton.bind("click", function(e){
            e.preventDefault();
            mySwiper.slidePrev();
         });

         nextButton.bind("click", function(e){
            e.preventDefault();
            mySwiper.slideNext();
         });
      };

      closeButton.bind("click", function(e){
         e.preventDefault();
         closePopup(mySwiper);
      });


   }

   function closePopup(mySwiper){
      mySwiper.destroy();
      gallery.remove();
      $(window).unbind("resize:gallery");
      $(window).unbind("keydown");
      $("html").removeClass("gallery-open");
   }
   function moveThumbnails(index){

      thumbnails.removeClass("active");
      thumbnails.eq(index).addClass("active");

      var half = visibleThumbs / 2;
      var toIndex = index - Math.floor(half);
      if( toIndex < 0 ){
         toIndex = 0;
      }
      else if( toIndex + visibleThumbs -1 > totalThumbs ){
         toIndex = totalThumbs - visibleThumbs + 1;
      }

      var left = toIndex * thumbWidth * (-1);

      thumbnailsContainer.find("ul:first").css({
         left: left
      });
   }

   function loadImage(index){

      flag = true;

      var current = anchors.eq(index);
      var thumb = current.find("img").attr("src");
      var href = current.attr("href");
      var title = current.attr("title");
      var author = current.attr("data-author") || '';

      if( current.attr("data-iframe") ){
         appendIframe(href, thumb, title, author, index);	
      }else{
         appendImage( href, thumb, title, author, index);	
      }

   }

   function appendImage( href, thumb, title, author, index){

      var tmp = new Image();

      //Youtube video
      var isYoutube = false;

      if( href.match("watch?") ){
         isYoutube = true;
         var youtubeHash = href.split("watch?v=")[1];
         href = "https://img.youtube.com/vi/"+youtubeHash+"/maxresdefault.jpg";
      };

      tmp.onload = function(){
         var imageCont = imageContainer.find(".swiper-slide").eq(index).find(".image");

         imageCont.css({
            backgroundImage: "url("+href+")"
         }).find(".loader").remove();

         if( isYoutube ){
            imageCont.find(".youtube-video").remove();
            imageCont.append("<div class='youtube-video' data-youtubeHash='"+youtubeHash+"'></div>");

            youtubeControls( imageCont.find(".youtube-video:first") );
         }
      }

      tmp.src = href;

   }

   function youtubeControls( obj ){
      var embedTemplate = '<iframe src="https://www.youtube.com/embed/'+obj.attr("data-youtubeHash")+'?autoplay=1" frameborder="0" allowfullscreen></iframe>';
      obj.bind("click", function(e){
         e.preventDefault();
         obj.html(embedTemplate);
         obj.unbind("click");
      });
   }

   function appendIframe( href, thumb, title, author, index){
      if( imageContainer.find(".swiper-slide").eq(index).find(".image").find("iframe").size() > 0 ){
         return false;
      }
      imageContainer.find(".swiper-slide").eq(index).find(".image").append("<iframe src='"+href+"' allowfullscreen />").find(".loader").remove();
   }

   function generateThumbnails( index ){
      var output = '<ul>';

      anchors.each(function(e, val){
         var obj = $(this);
         var src = obj.find("img").attr("src");
         var className = '';
         if( obj.attr("data-iframe") ){
            src = obj.css("backgroundImage").replace('url(','').replace(')','').replace(/\"/gi, "");
         }
         output+= '<li><a href="#'+e+'"><img src="'+src+'" /></a></li>';
      });

      output+= '</ul>';
      return output;
   }
}
