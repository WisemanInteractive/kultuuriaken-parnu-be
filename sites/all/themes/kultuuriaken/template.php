<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

require "template-form.php";
require "template-views.php";

function kultuuriaken_preprocess_html(&$vars) {
  // Weather library
  drupal_add_js(drupal_get_path('theme', 'kultuuriaken') .'/js/libraries/jquery.simpleWeather.min.js', array('group' => JS_LIBRARY));
  
  if(current_path() === 'new-event-anon') {
    $vars['theme_hook_suggestions'][] = 'html__blank';
  }
  
  // Event/activity form page
  if((!empty($vars['page']['content']['system_main']['#form_id'])
  && $vars['page']['content']['system_main']['#form_id'] === 'ka_event_form') || current_path() === 'organiser') {
    // Make form pages non-responsive
    $vars['body_attributes_array']['class'][] = 'no-responsive';
  }
  
  // Prevent browser caching on events and activity page
  if(current_path() === 'events' || current_path() === 'activities' || drupal_is_front_page()) {
    $cache_control = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'http-equiv' =>  'Cache-Control',
        'content' => 'no-cache, no-store, must-revalidate',
      )
    );
    drupal_add_html_head($cache_control, 'filter_cache_control');
    $pragma = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'http-equiv' =>  'Pragma',
        'content' => 'no-cache',
      )
    );
    drupal_add_html_head($pragma, 'pragma_filter_cache_control');
    $expires = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'http-equiv' =>  'Expires',
        'content' => '0',
      )
    );
    drupal_add_html_head($expires, 'expires_filter_cache_control');
  }
}

function kultuuriaken_preprocess_node(&$vars) {
  global $language;
  
  
  $vars['node_ka_status'] = $vars['field_ka_status'][0]['value'];
  if($vars['node_ka_status'] === 'preview'){
    $vars['preview_form_submit'] = drupal_get_form('ka_event_preview_form', $vars['node']);
    $vars['language_switcher_preview'] = module_invoke('ka_views', 'block_view_locale_preview', 'language')['content'];
  }
  
  // Use the top-level category for labeling.
  if($vars['node']->type === 'activity') {
    $category_field = $vars['field_category_activity'];
  } else if($vars['node']->type === 'event') {
    $category_field = $vars['field_category'];
  }
  
  if(!empty($category_field) && $vars['page']) {
    
    foreach($category_field as $category) {
      $sub_category_tid = $category['target_id'];
      $vars['main_categories'][] = i18n_taxonomy_localize_terms(taxonomy_term_load($sub_category_tid));
    }
  }
  
  // Get the activity group info
  if(!empty($vars['field_activity_groups']) && $vars['page']) {
    $vars['activity_groups'] = get_activity_groups($vars['field_activity_groups']);
  }
  
  // Event node specific
  if($vars['node']->type === 'event') {
    if($vars['page']) {
      if (isset($vars['node']->field_event_ticket_info[$language->language])) {
        $vars['field_event_ticket_info_text'] = $vars['node']->field_event_ticket_info[$language->language][0]['value'];
      }
      // Get the latest event date
      $event_date_ids = array();
      foreach($vars['field_event_dates'] as $event_date) {
        $event_date_ids[] = $event_date['value'];
      }
      
      $vars['event_dates'] = get_upcoming_events($vars['node']);

      // Check if there are any sub-events
      if(!empty($vars['event_dates']['current'])) {
        if($vars['nid'] !== $vars['event_dates']['current']['host_nid']) {
          $roof_event = TRUE;
        }
      }
      if(!empty($vars['event_dates']['upcoming']) && !isset($roof_event)) {
        $upcoming_dates = $vars['event_dates']['upcoming'];
        foreach($upcoming_dates as $upcoming_date) {
          if($vars['nid'] !== $upcoming_date['host_nid']) {
            $roof_event = TRUE;
            break;
          }
        }
      }
      $vars['roof_event'] = isset($roof_event) ? $roof_event : FALSE;
      
      $similar_events_items = views_get_view_result('fp_list', 'similar_events', $vars['node']->uid);

      if($vars['node']->uid === '0' || $vars['node']->uid === '1' || count($similar_events_items) < 2) {
        $event_tids = array();
        if(!empty($vars['node']->field_category[LANGUAGE_NONE])) {
          foreach($vars['node']->field_category[LANGUAGE_NONE] as $category) {
            $event_tids[] = $category['target_id'];
          }
        }
        $event_tids = !empty($event_tids) ? implode('+', $event_tids) : '';
        
        $vars['similar_events'] = views_embed_view('fp_list', 'similar_events_anon', $event_tids, $vars['node']->nid);
      } else {
        $vars['similar_events'] = views_embed_view('fp_list', 'similar_events', $vars['node']->uid);
      }
    }
    
    // Ticket type + price
    if(!empty($vars['field_event_ticket_type'])) {
      if($vars['page']) {
        $ticket_type = $vars['field_event_ticket_type'][0]['value'];
        $price = !empty($vars['field_event_ticket_price']) ? $vars['field_event_ticket_price'][0]['value'] : NULL;
        $price_from = !empty($vars['field_event_ticket_price_from']) ? $vars['field_event_ticket_price_from'][0]['value'] : NULL;
        $price_to = !empty($vars['field_event_ticket_price_to']) ? $vars['field_event_ticket_price_to'][0]['value'] : NULL;
        $vars['ticket_price'] = render_ticket_price($ticket_type, $price, $price_from, $price_to);
      } else if($vars['view_mode'] === 'search_result') {
        $ticket_type = $vars['field_event_ticket_type'][LANGUAGE_NONE][0]['value'];
        $price = !empty($vars['field_event_ticket_price']) ? $vars['field_event_ticket_price'][LANGUAGE_NONE][0]['value'] : NULL;
        $price_from = !empty($vars['field_event_ticket_price_from']) ? $vars['field_event_ticket_price_from'][LANGUAGE_NONE][0]['value'] : NULL;
        $price_to = !empty($vars['field_event_ticket_price_to']) ? $vars['field_event_ticket_price_to'][LANGUAGE_NONE][0]['value'] : NULL;
        $vars['ticket_price'] = render_ticket_price($ticket_type, $price, $price_from, $price_to);
      }
    }
    
    // Ticket purchase
    if(!empty($vars['field_event_ticket_availability']) && $vars['page']) {
      $ticket_availability = $vars['field_event_ticket_availability'][0]['value'];

      switch($ticket_availability) {
        case 'link':
            $ticket_avail = !empty($vars['field_event_ticket_link'][0]['url']) ? $vars['field_event_ticket_link'][0]['url'] : NULL;
            $vars['ticket_availability'] = array('type' => $ticket_availability, 'value' => $ticket_avail);
          break;
        case 'ticket_office':
          $vars['ticket_availability'] = array('type' => $ticket_availability);
          break;
        case 'other':
          if(!empty($vars['field_event_ticket_other'])) {
            $ticket_avail = !empty($vars['field_event_ticket_other'][0]['value']) ? $vars['field_event_ticket_other'][0]['value'] : NULL;
            $vars['ticket_availability'] = array('type' => $ticket_availability, 'value' => $ticket_avail);
          }
          break;
      }
    }
  }
  
  // Activity node specific
  if($vars['node']->type === 'activity') {
    $emw_node = entity_metadata_wrapper('node', $vars['node']);
    // $vars['additional_link'] = $emw_node->field_additional_info_link->value();
    $vars['additional_link'] = format_registration_options($vars['node'], TRUE);
    // Detailed page specific
    if($vars['page']) {
      // Set group specific page variables.
      $parameters = drupal_get_query_parameters();
      if(isset($parameters['group']) && is_numeric($parameters['group'])) {
        $group_id = intval($parameters['group']);
      }

      $activity_groups = $emw_node->field_activity_groups->getIterator();
      
      $activity_groups_summary = array();
      foreach($activity_groups as $activity_group) {
        $id = $activity_group->getIdentifier();
        $group_summary = get_group_summary($activity_group, TRUE); // Get group summary.
        if(
          (!empty($group_summary['period']['value2']) && $group_summary['period']['value2'] < time()) ||
          (isset($group_summary['next_date']) && !$group_summary['next_date'] && !$group_summary['single_group']) 
        ) continue; // Skip groups with passed periods.
        
        $activity_groups_summary[$id] = $group_summary;

        if(empty($group_id)) $group_id = $id;
        if($activity_group->getIdentifier() == $group_id) {
          $vars['current_group'] = $activity_group;
          $vars['current_group_summary'] = $activity_groups_summary[$id];
        
          // Get the next group event time.
          $group_type = $activity_group->field_activity_group_type->value();
          switch($group_type) {
            case 'regular':
              $activity_group_times = $activity_group->field_activity_groups_regular->getIterator();
              break;
            case 'fixed':
              $activity_group_times = $activity_group->field_activity_groups_fixed->getIterator();
              break;
          }
          $date_period = $activity_group->field_activity_date_period->value();
          
          $vars['next_event'] = calculate_next_event_date($group_type, $activity_group_times, $date_period);
          
          $vars['current_group_date_location'] = get_event_location($activity_group->{"field_activity_groups_$group_type"}->getIterator(), $group_type);
          
          // Get the group price
          if(!empty($activity_group->field_activity_group_price_type)) {
            $ticket_type = $activity_group->field_activity_group_price_type->value();
            $price = !empty($activity_group->field_activity_group_price) ? $activity_group->field_activity_group_price->value() : NULL;
             // Group price
            $vars['group_price'] = render_ticket_price($ticket_type, $price);
          }
        }
      }
      $activity_groups_rendered = array_map(function($value) { return $value['rendered'];}, $activity_groups_summary);
      $vars['activity_groups_summary'] = implode('<br>', $activity_groups_rendered);
      $vars['title'] = $vars['current_group_summary']['name'];
      
      $similar_activities_items = views_get_view_result('fp_list', 'similar_activities', $vars['node']->uid);
      if($vars['node']->uid === '0' || $vars['node']->uid === '1' || empty($similar_activities_items)) {
        $activity_tids = array();
        if(!empty($vars['node']->field_category_activity[LANGUAGE_NONE])) {
          foreach($vars['node']->field_category_activity[LANGUAGE_NONE] as $category) {
            $activity_tids[] = $category['target_id'];
          }
        }
        $activity_tids = !empty($activity_tids) ? implode('+', $activity_tids) : '';
        $vars['similar_activities'] = views_embed_view('fp_list', 'similar_activities_anon', $activity_tids, $vars['node']->uid);
      } else {
        $vars['similar_activities'] = views_embed_view('fp_list', 'similar_activities', $vars['node']->uid);
      }
      // Generate activity group summary
      $groups_summary = array();
      $activity_groups = $emw_node->field_activity_groups->getIterator();
      foreach($activity_groups as $group) {
        $id = $group->getIdentifier();
        $groups_summary[$id] = get_group_summary($group, TRUE);
      }
      // kpr($groups_summary);
      $vars['groups_summary_array'] = $groups_summary;

      $vars['groups_summary'] = render_activity_group_information($groups_summary, $vars['node'], drupal_get_query_parameters());
      $vars['groups_summary_mobile'] = render_activity_group_information_mobile($groups_summary, $vars['node'], drupal_get_query_parameters());
    }
  }
  
  
  // Activity & event node specific
  if($vars['node']->type === 'activity' || $vars['node']->type === 'event') {
    if($vars['node']->type === 'event'){
      $event_id = !empty($vars['event_dates']['current']['id']) ? $vars['event_dates']['current']['id'] : NULL;
      $vars['add_to_calendar'] = drupal_get_form('add_to_calendar_form', $vars['node']->nid, $event_id);
      $vars['add_to_calendar']['add_to_calendar']['#prefix'] = '<a data-target-id="add-to-calendar" href="" class="link after-calendar">' . ka_t('Add to calendar') . '</a>';
    }elseif($vars['node']->type === 'activity' && $parameters){
      $event_id = $vars['current_group_summary']['id'];
      $vars['add_to_calendar'] = get_activity_add_to_calendar_form($vars['current_group_summary']);
    }
    
    if($vars['view_mode'] === 'search_result') {
      // Node summary
      $node_summary = ka_node_to_array($vars['node']);
      if(!empty($node_summary)) $vars['node_summary'] = reset($node_summary);
      
      // Text intro summary
      if(!empty($vars['field_summary'])) {
        $vars['summary'] = $vars['field_summary'][0]['safe_value'];
      }
      
    }
  }
  
  // Get video thumbnail and URL
  if(!empty($vars['field_video']) && $vars['page'] && $vars['field_video'][0]['type'] === 'video') {
    $vars['video_thumbnail_url'] = video_thumbnail_url($vars['field_video'][0]['uri']);
    if($vars['field_video'][0]['filemime'] === 'video/youtube') {
      $yt_code = str_replace('youtube://v/', '', $vars['field_video'][0]['uri']);
      $vars['video_url'] = sprintf('https://www.youtube.com/watch?v=%s', $yt_code);
    } else if($vars['field_video'][0]['filemime'] === 'video/vimeo') {
      $vimeo_code = str_replace('vimeo://v/', '', $vars['field_video'][0]['uri']);
      $vars['video_url'] = sprintf('https://player.vimeo.com/video/%s', $vimeo_code);
    }
  }
  
}

function kultuuriaken_preprocess_page(&$vars) {
  global $language;
  global $user;

  $vars['show_title'] = TRUE;
  // Add main menu
  $main_menu = menu_tree_all_data('main-menu');
  $vars['main_menu'] = menu_tree_output($main_menu);
  // Mobile main menu
  $vars['main_menu_mobile'] = menu_tree_output($main_menu);
  $vars['main_menu_mobile']['#theme_wrappers'] = array('menu_tree__main_menu_mobile');

  // Add header menu
  $header_menu = menu_tree_all_data('menu-header-menu');
  $vars['header_menu'] = menu_tree_output($header_menu);
  
  // Mobile header menu
  $vars['header_menu_mobile'] = menu_tree_output($header_menu);
  $vars['header_menu_mobile']['#theme_wrappers'] = array('menu_tree__menu_header_menu_mobile');
  
  // Add language switcher
  if(current_path() === 'activities') {
    $vars['language_switcher'] = '<a href="/et" class="lang active" xml:lang="et" lang="et">EST</a>';
    $vars['language_switcher_mobile'] = '<ul><li class="active"><a href="/et" class="lang active" xml:lang="et" lang="et">EST</a></li></ul>';
    //$vars['language_switcher'] = module_invoke('locale', 'block_view', 'language')['content'];
    //$vars['language_switcher_mobile'] = module_invoke('ka_views', 'block_view_locale_mobile', 'language')['content'];
  }
  else{
    $vars['language_switcher'] = module_invoke('locale', 'block_view', 'language')['content'];
    $vars['language_switcher_mobile'] = module_invoke('ka_views', 'block_view_locale_mobile', 'language')['content'];
  }
  // Front page variables
  if(drupal_is_front_page()) {
    $vars['text_banners'] = views_embed_view('fp_banners', 'fp_text_banners');
    $vars['recommended_events'] = views_embed_view('fp_list', 'recommended');
    
    // Upcoming activites
    $activities = ka_get_nodes(array('starting_time' => UPCOMING_ACTIVITY), 'activities');
    $nids = array_keys($activities);
    //dpm($nids);
    $vars['upcoming_activities'] = views_embed_view('fp_list', 'upcoming_activities', implode('+', $nids));
    
    // Categories lists
    // Event types
    $vars['event_types'] = views_embed_view('fp_categories', 'fp_event_types');
    $event_types = views_get_view_result('fp_categories', 'fp_event_types');
    $vars['event_types_count'] = count($event_types);
    
    // Activity types
    $vars['activity_types'] = views_embed_view('fp_categories', 'fp_activity_types');
    $activity_types = views_get_view_result('fp_categories', 'fp_activity_types');
    $vars['activity_types_count'] = count($activity_types);
    
    $vars['sponsors'] = views_embed_view('fp_banners', 'fp_sponsor_banners');
    $vars['image_banners'] = views_embed_view('fp_banners', 'fp_image_banners');
    
    // Subscription form
    $vars['newsletter_subscribe_form'] = module_invoke('webform','block_view','client-block-94');
  }
  
  $vars['newsletter_subscribe_form_hidden'] = module_invoke('ka_newsletter','block_view','client-block-94', 'modal');

  // Event filter page specific
  if(current_path() === 'events' || current_path() === 'activities' || drupal_is_front_page()) {
    // Target groups filter
    $tg_vocabulary = taxonomy_vocabulary_machine_name_load('target_groups');
    $vars['target_groups'] = i18n_taxonomy_localize_terms(taxonomy_get_tree($tg_vocabulary->vid));
    
    // Set category filters
    $params = drupal_get_query_parameters();
    $set_categories = array();
    foreach($params as $key => $p) {
      if(strpos($key, 'category_') !== FALSE) {
        $category_tid = strtok($key, 'category_');
        $set_categories[$category_tid] = TRUE;
        if($p !== 'all') {
          $sub_tids = explode(',', $p);
          foreach($sub_tids as $st) {
            $set_categories[$st] = TRUE;
          }
        }
      }
    }
    $vars['set_categories'] = $set_categories;
  }

  if(current_path() === 'add/event'
  || (!empty($vars['page']['content']['system_main']['#form_id'])
  && $vars['page']['content']['system_main']['#form_id'] === 'ka_event_form')
  ) { // Add separate template for forms
    $vars['theme_hook_suggestions'][] = 'page__form';
    $vars['show_messages'] = FALSE;
    $vars['form_messages'] = theme('status_messages');
  }
  
  // User profile page specific
  if(arg(0) === 'user' && user_is_logged_in()) {
    $vars['user_profile'] = $vars['page']['content']['system_main'];
    $vars['is_profile_page'] = TRUE;
    
    // Delete account form
    $uid = is_numeric(arg(1)) ? arg(1) : NULL;
    $delete_account_form = drupal_get_form('ka_organizer_delete_form', $uid);
    $vars['delete_account_form'] = drupal_render($delete_account_form);
    
    // Edit account form
    $edit_user = user_load($uid);
    $edit_account_form = drupal_get_form('ka_organizer_edit_form', $edit_user->uid);
    $vars['edit_account_form'] = drupal_render($edit_account_form);
  }
  
  // Data for logged in user.
  if(user_is_logged_in()) {
    $user_full = user_load($user->uid);
    $vars['user_institution'] = !empty($user_full->field_institution) ? $user_full->field_institution[LANGUAGE_NONE][0]['safe_value'] : NULL;
    if(empty($vars['user_institution'])) {
      $vars['user_institution'] = !empty($user_full->field_full_name) ? $user_full->field_full_name[LANGUAGE_NONE][0]['safe_value'] : NULL;
    }
    $vars['user_email'] = $user_full->mail;
  }
  
  // 404 page specific
  if(current_path() === 'page-not-found' || current_path() === 'access-denied') {
    $vars['container_class'] .= ' center-align';
  }
  
  if(current_path() === 'organiser') {
    $vars['organizer_faq'] = views_embed_view('organizers', 'faq');
    $vars['organizer_table_events'] = views_embed_view('organizer_tables', 'events');
    $vars['organizer_table_activities'] = views_embed_view('organizer_tables', 'activities');
    
    // Hybridauth block
    if (module_exists('hybridauth') /*&& !user_is_logged_in()*/) {
      $element['#type'] = 'hybridauth_widget';
      $vars['hybridauth_login'] = drupal_render($element);
    }
    
  }

  // Anonymous new event popup
  if(arg(0) === 'new-event-anon' && (arg(1) === 'event' || arg(1) === 'activity')) {
    $vars['theme_hook_suggestions'][] = 'page__blank';
  }
  
  // Search page view
  $views_page = views_get_page_view();
  if (is_object($views_page) && $views_page->name === 'general_search') {
    $vars['show_title'] = FALSE;
  }
}

function kultuuriaken_preprocess_block(&$vars) {
  //krumo($vars);
}

function kultuuriaken_preprocess_hybridauth_widget(&$vars, $hook) {
  $element = $vars['element'];
  $vars['providers'] = array();
  foreach ($element['providers'] as $provider_id => $provider) {
    if($provider_id === 'Facebook') {
      $provider['options']['attributes']['class'][] = 'btn before-facebook';
    } else if($provider_id === 'Google') {
      $provider['options']['attributes']['class'][] = 'btn before-googleplus';
    }
    $vars['providers'][] = l(render($provider['text']), $provider['path'], $provider['options']);
  }
}

function kultuuriaken_preprocess_views_view__general_search__page(&$vars) {
  $search = !empty($_GET['search']) ? check_plain($_GET['search']) : NULL;
  
  if(!empty($_GET['node_type']['activity']) || !empty($_GET['node_type']['event'])) {
    $vars['activity_search_result'] = ka_filters_global_search($search);
    $GLOBALS['acitivity_count'] = 0;
  }
}

function kultuuriaken_css_alter(&$css) {
  //if(current_path() === 'calendar') { // Remove conflicting Date module CSS.
    $css_path = 'sites/all/modules/date/date_api/date.css';
    if(isset($css[$css_path])) {
      unset($css[$css_path]);
    }
  //}
}

function kultuuriaken_js_alter(&$js) {
  //unset($js['sites/all/themes/kultuuriaken/assets/js/main.js']);
  //$js['sites/all/themes/kultuuriaken/assets/js/main.js']['group'] = JS_THEME;
  //$js['sites/all/themes/kultuuriaken/assets/js/main.js']['weight'] = 100;
}

function kultuuriaken_menu_tree__main_menu($vars) {
  $output = '<ul>';
  foreach($vars['#tree'] as &$item) {
    if(isset($item['#href'])) {
      unset($item['#attributes']);
      $output .= render($item);
    }
  }
  $output .= '</ul>';
  return $output;
}

function kultuuriaken_menu_tree__main_menu_mobile($vars) {
  $output = '<ul class="main">';
  foreach($vars['#tree'] as &$item) {
    if(isset($item['#href'])) {
      unset($item['#attributes']);
      $output .= render($item);
    }
  }
  // Calendar link
  $output .= '<li><a href="' . url('calendar') . '">' . ka_t('My calendar') . '</a></li>';
  $output .= '</ul>';
  return $output;
}

function kultuuriaken_menu_tree__menu_header_menu($vars) {
  $output = '';
  foreach($vars['#tree'] as &$item) {
    if(isset($item['#href'])) {
      $item['#attributes']['target'] = "_blank";
      $output .= render($item);
    }
  }
  // Add FB Link to the end.
  $fb_link = variable_get_value('facebook_link');
  //$output .= '<a target="_blank" href="' . $fb_link . '" class="link before-facebook">' . ka_t('Window of Culture') . '</a>';
  $output .= '<a target="_blank" href="' . $fb_link . '" class="link before-facebook">' . 'Facebook' . '</a>';

  return $output;
}

function kultuuriaken_menu_tree__menu_header_menu_mobile($vars) {
  $output = '<ul class="sub">';
  foreach($vars['#tree'] as &$item) {
    if(isset($item['#href'])) {

      $output .= '<li>' . render($item) . '</li>';
    }
  }
  // Add FB Link to the end.
  $fb_link = variable_get_value('facebook_link');
  //$output .= '<li><a href="' . $fb_link . '" class="link before-facebook">' . ka_t('Window of Culture') . '</a></li>';
  $output .= '<li><a href="' . $fb_link . '" class="link before-facebook">'.'Facebook'.'</a></li>';

  $output .= '</ul>';
  return $output;
}

function kultuuriaken_menu_link__menu_header_menu($variables) {

  $element = $variables['element'];
  $element['#localized_options']['attributes']['class'][] = 'link before-arrow';
  $element['#localized_options']['attributes']['target'] = "_blank";

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return $output;
}

function kultuuriaken_links__locale_block(&$vars) {
  foreach($vars['links'] as $lang => $langInfo) {
    $name = $langInfo['language']->native;
    $vars['links'][$lang]['html'] = TRUE;
    $vars['links'][$lang]['attributes']['class'][0] = 'lang';
    
    switch($lang) {
      case 'et':
        $vars['links'][$lang]['title'] = 'EST';
        break;
      case 'en':
        $vars['links'][$lang]['title'] = 'ENG';
        break;
      case 'ru':
        $vars['links'][$lang]['title'] = 'RUS';
        break;
    }
    if(isset($vars['type']) && $vars['type'] == 'preview'){
      $vars['links'][$lang]['attributes']['class'][] = 'link dark';
      if(!isset($vars['translations'][$lang])){
        unset($vars['links'][$lang]);
      }
    }
  }
  $vars['attributes']['class'][] = 'more';
  
  $content = ka_language_links($vars);
  
  return $content;
}

// Theme KA language switcher block.
function ka_language_links($variables) {
  if(isset($_GET['lang'])) $preview_lang = $_GET['lang'];
  
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';
  $mobile = isset($variables['type']) && $variables['type'] === 'mobile' ? TRUE : FALSE;

  if (count($links) > 0) {
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $num_links = count($links);
    $i = 1;
    
    if($mobile) $output .= '<ul>';
    
    foreach ($links as $key => $link) {
      $class = array($key);
      if(isset($preview_lang)) $link['lang'] = $preview_lang;
      // Add first, last and active classes to the list of links to help out
      // themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      $is_active = FALSE;
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
         && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
        $is_active = TRUE;
      }

      if($mobile) $output .= '<li' . ($is_active ? ' class="active" ' : '') . '>';
      
      // For W3C conformity
      if(!empty($link['attributes']['xml:lang'])) $link['attributes']['lang'] = $link['attributes']['xml:lang'];
      
      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        if(!isset($preview_lang)){
          $output .= l($link['title'], $link['href'], $link);
        }else{
          $output .= l($link['title'], $link['href'], $link + array('query' => array('lang' => $preview_lang)));
        }
        
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for
        // adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }
      
      if($mobile) $output .= '</li>';

      $i++;
    }
    
    if($mobile) $output .= '</ul>';
  }
  return $output;
}

function kultuuriaken_block_view_alter(&$data, $block) {
  /*
  static $second_newsletter_form;
  
  if(isset($data['theme_hook_original']) && $data['theme_hook_original'] === 'webform_form_94') {
    $second_newsletter_form = TRUE;
    dpm($data);
  }
  */
}

function get_upcoming_events($node) {
  $parameters = drupal_get_query_parameters();
  $event_id = isset($parameters['event']) ? intval($parameters['event']) : NULL;

  $emw_node = entity_metadata_wrapper('node', $node);
  
  $event_date_items = $emw_node->field_event_dates->value();
  
  if(empty($event_id)) { // Set event ID if it is not set as query parameter
    foreach($event_date_items as $event_date) {
      $emw_date = entity_metadata_wrapper('paragraphs_item', $event_date);
      $event_date_value = $emw_date->field_event_date->value();
      if(!empty($event_date_value) && $event_date_value['value2'] > time()) {
        $event_id = intval($emw_date->getIdentifier());
        break;
      }
    }
  }

  // Add any sub-events
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'event')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('field_ka_status', 'value', 'published')
    ->fieldCondition('field_event_roof_event', 'target_id', $node->nid)
    ->fieldCondition('field_publish_date', 'value', time(), '<');
  
  $result = $query->execute();
  if(!empty($result['node'])) {
    $node_list = array_keys($result['node']);
    $nids = $node_list;
    if(!is_array($nids)) $nids = array($nids);
    $nodes = node_load_multiple($nids);

    foreach($nodes as $node) {
      $emw_subnode = entity_metadata_wrapper('node', $node);

      $availability_type = $emw_subnode->field_event_ticket_availability->value();
      if($availability_type && $availability_type != 'ticket_office') $availability_info = $emw_subnode->{"field_event_ticket_$availability_type"}->value();
      
      $event_dates = $emw_subnode->field_event_dates->getIterator();
      foreach($event_dates as $event_date) {
        $item = $event_date->value();
        $item->name = $emw_subnode->label();
        $item->host_nid = $emw_subnode->getIdentifier();
        // Price
        $ticket_type = $emw_subnode->field_event_ticket_type->value();
        $price = $emw_subnode->field_event_ticket_price->value();
        $price_from = $emw_subnode->field_event_ticket_price_from->value();
        $price_to = $emw_subnode->field_event_ticket_price_to->value();
        $item->price = render_ticket_price($ticket_type, $price, $price_from, $price_to);
        // Ticket URL
        $item->ticket_link = $emw_subnode->field_event_ticket_link->value();
        $item->availability_type = !empty($availability_type) ? $availability_type : '';
        $item->availability_info = !empty($availability_info) ? $availability_info : '';
        $event_date_items[] = $item;
      }
    }
  }
  
  // Sort the event dates
  if(count($event_date_items) > 1) {
    usort($event_date_items, 
    function($a, $b) {
      $a_time = !empty($a->field_event_date[LANGUAGE_NONE][0]['value']) ? 
        $a->field_event_date[LANGUAGE_NONE][0]['value'] : time();
      $b_time = !empty($b->field_event_date[LANGUAGE_NONE][0]['value']) ? 
        $b->field_event_date[LANGUAGE_NONE][0]['value'] : time();
      return $a_time - $b_time;
    });
  }
  
  $now = time();
  $event_dates = array();

  foreach($event_date_items as $item) {
    $id = intval($item->item_id);
    if(!empty($item->host_nid)) {
      $host_nid = $item->host_nid; // Fix for sub-events
    } else {
      $host_nid = $item->hostEntityId();
    }
    $url = url('node/' . $host_nid, array('query' => array('event' => $id)));
    $event_date = !empty($item->field_event_date) ? (int) $item->field_event_date[LANGUAGE_NONE][0]['value'] : NULL;
    $event_date_to = !empty($item->field_event_date[LANGUAGE_NONE][0]['value2']) ? (int) $item->field_event_date[LANGUAGE_NONE][0]['value2'] : NULL;
    $event_location = !empty($item->field_location) ? $item->field_location[LANGUAGE_NONE][0] : NULL;
    $sold_out = !empty($item->field_sold_out) ? boolval($item->field_sold_out[LANGUAGE_NONE][0]['value']) : FALSE;
    $name = !empty($item->name) ? $item->name : NULL;
    // Price
    if(!empty($item->price)) {
      $price = $item->price;
    } else {
      $ticket_type = $emw_node->field_event_ticket_type->value();
      $price = $emw_node->field_event_ticket_price->value();
      $price_from = $emw_node->field_event_ticket_price_from->value();
      $price_to = $emw_node->field_event_ticket_price_to->value();
      $price = render_ticket_price($ticket_type, $price, $price_from, $price_to);
    }
    // Ticket link
    if(!empty($item->ticket_link['url'])) {
      $ticket_link = $item->ticket_link['url'];
    } else {
      $ticket_link = !empty($emw_node->field_event_ticket_link->value()['url']) ? $emw_node->field_event_ticket_link->value()['url'] : NULL;
    }
    if(isset($item->availability_type)){
      $event['availability_type'] = $item->availability_type;
    }
    if(isset($item->availability_info)){
      $event['availability_value'] = $item->availability_info;
    }
    if(!empty($item->field_end_date_unset)) {
      $end_date_time_unset = $item->field_end_date_unset[LANGUAGE_NONE][0]['value'];
    } else {
      $end_date_time_unset = FALSE;
    }
    
    $event = array('id' => $id, 'host_nid' => $host_nid, 'name' => $name, 'event_date' => $event_date, 
      'event_date_to' => $event_date_to, 'event_location' => $event_location, 'url' => $url, 'price' => $price,
      'ticket_link' => $ticket_link, 'end_date_time_unset' => $end_date_time_unset,
      );
    

    if($now > $event_date_to) { // Past event
      $event_dates['past'][$id] = $event;
    } else {
      if((empty($event_dates['current']) && empty($event_id)) || (!empty($event_id) && $event_id === $id)) { // Current event
        $event_dates['current'] = $event;
      } else { // Upcoming event
        $event_dates['upcoming'][$id] = $event;
      }
    }
  }
  
  if(empty($event_dates['current'])) { 
    if(!empty($event_dates['upcoming'])) {
      // Go to main event page if the id is outdated
      drupal_goto('node/' . $node->nid);
    } else if(!user_access('administer nodes')) {
      // If there are no upcoming events, go to front page
      drupal_goto('<front>');
    }
  }
  
  return $event_dates;
}

function get_activity_groups($group_array) {
  $now = time();

  foreach($group_array as $group_item) { // Iterate over activity groups
    $group = paragraphs_item_load($group_item['value']); // Load group paragraph item
    $group_emw = entity_metadata_wrapper('paragraphs_item', $group);
    
    $period = $group_emw->field_activity_date_period->value();
    if($period['value2'] < $now) continue; // Period end is past the current date, skip group.
    
    // Different methods depending on event date type.
    $group_type = $group_emw->field_activity_group_type->value();
    if($group_type === 'fixed' && !empty($group_emw->field_activity_groups_fixed->value())) {
      $fixed_dates = $group_emw->field_activity_groups_fixed->value();
      foreach($fixed_dates as $date) {
        $date_emw = entity_metadata_wrapper('paragraphs_item', $date);
      }
    } elseif ($group_type === 'regular' && !empty($group_emw->field_activity_groups_regular->value())) {
      
    }
  }
}

function get_video_thumbnail($fid) {
  // Make sure the correct include file is loaded
  module_load_include('inc', 'media_youtube', '/includes/media_youtube.formatters.inc');

  // Load the file
  $file = file_load($fid);
  
  // Set up the settings array with your image style
  $display['settings'] = array('image_style' => 'unlinked_thumbnail');
  
  // Get the render array for the thumbnail image
  $image_render_array = media_youtube_file_formatter_image_view($file, $display, LANGUAGE_NONE);
  
  return $image_render_array;
}

function weekday_num_to_string($num) {
  $weekdays = array(
    '1' => t('Monday'),
    '2' => t('Tuesday'),
    '3' => t('Wednesday'),
    '4' => t('Thursday'),
    '5' => t('Friday'),
    '6' => t('Saturday'),
    '7' => t('Sunday'),
  );
  
  return $weekdays[$num];
}

function render_activity_group_information($groups, $node, $params = array()) {
  $output = '';
  $registration_options = format_registration_options($node);
  if(isset($registration_options['internet'])){
    $registration_options_link = $registration_options['internet'];
    unset($registration_options['internet']);
  }
  foreach($groups as $key => $group) {
    //if($node->nid == $group->nid) continue;
    if(isset($params['group']) && ($key == $params['group'])) continue;
    if(!empty($group['period']['value2']) && $group['period']['value2'] < time()) continue;
    if(isset($group['next_date']) && !$group['next_date']) continue;

    $type = $group['group_type'];
    $group_dates = array_values($group['group_dates']);
    
    for($i = 0; $i < count($group_dates); $i++) {
      if($i === count($group_dates) - 1) {
        $output .= '<tr class="opaque-line">';
      } else {
        $output .= '<tr class="no-line">';
      }
      
      $age_group = render_age_range($group['age_from'], $group['age_to'], $group['demographics']);
      if($i === 0) {
        $output .= sprintf('<td %s><b><a href="%s">%s</a></b>%s</td>', 
          $group['is_full'] ? 'class="disabled"' : '', $group['url'], $group['name'],
          count($group_dates) === 1 && !empty($age_group) ? "<br><b class='disabled'>($age_group)</b>" : ''); // Group name
      } else if($i === 1) {
        if(!empty($age_group)) {
          $output .= sprintf('<td %s><b class="disabled">(%s)</b></td>', $group['is_full'] ? 'class="disabled"' : '', $age_group); // Age group
        } else {
          $output .= sprintf('<td></td>');
        }
        
      } else if($i > 1) {
        $output .= '<td></td>';
      }
      $date = $group_dates[$i];
      
      if($type === 'regular') {
        $output .= sprintf('<td>%s</td>', weekday_num_to_string($date['day'])); // Weekday
      } else if($type === 'fixed' && !empty($date['time']['value'])) {
        $output .= sprintf('<td>%s</td>', format_date($date['time']['value'], 'table_date')); // Weekday
      }
      
      $output .= sprintf('<td>%s - %s</td>', 
      format_date($date['time']['value'], 'time'), 
      format_date($date['time']['value2'], 'time')); // Time

      // Location
      $location = $date['location'];
      if(!empty($location['name']) && !empty($location['street'])) {
        $output .= sprintf('<td>%s | %s</td>', check_plain($location['name']), check_plain($location['street']));
      } else if(!empty($location['name'])) {
        $output .= sprintf('<td>%s</td>', check_plain($location['name']));
      } else if(!empty($location['street'])) {
        $output .= sprintf('<td>%s</td>', check_plain($location['street']));
      } else {
        $output .= '<td></td>';
      }
      
      
      if($i === 0) {
        // Price
        $price = $group['price'];
        $output .= sprintf('<td>%s</td>', render_ticket_price($price['type'], $price['value']));
      } else {
        $output .= '<td></td>';
      }
      
      $output .= '</tr>';
      
    }

    $output .= '<tr class="register">';
    $add_to_calendar = drupal_get_form("add_to_calendar_form_" . $group['id'], $group['nid'], $group['id']);

    $output .= sprintf('<td><a data-target-id="add-to-calendar" data-target-group-id="%s" href="" class="link after-calendar">%s</a><div class="">%s</div></td>',
    $group['id'], ka_t('Add to calendar'), drupal_render($add_to_calendar)); // Add to calendar

    $output .= '<td colspan="3">';
    if(!is_array_empty($registration_options)) {
      if(isset($registration_options['internet']) && !empty($node->field_registration_internet));
      $output .= sprintf('<b>%s:</b> %s', ka_t('Registration'), implode(' | ', $registration_options));
    }
    $output .= '</td>';
    // Register button / full
    if($group['is_full']) {
      $output .= sprintf('<td><span class="link danger">%s</span></td>', ka_t('Group is full!'));
    } else if(isset($registration_options_link) && !empty($node->field_registration_internet)) {
      $url = $node->field_registration_internet[LANGUAGE_NONE][0]['url'];
      $output .= sprintf('<td><a target="_blank" href="%s" class="link after-arrow_right">%s</a></td>', check_url($url), ka_t('Register'));
    } else {
      $output .= sprintf('<td></td>');
    }
    $output .= '</tr>';
  }

  return $output;
}

function render_activity_group_information_mobile($groups, $node, $params = array()) {
  $output = '<table class="interests sm-show">';
  $registration_options = format_registration_options($node);
  
  foreach($groups as $key => $group) {
    if(isset($params['group']) && ($key == $params['group'])) continue;
    if(!empty($group['period']['value2']) && $group['period']['value2'] < time()) continue;
    if(isset($group['next_date']) && !$group['next_date']) continue;
  	$output .= '<tbody>';
  	
  	$age_group = render_age_range($group['age_from'], $group['age_to'], $group['demographics']);
    
    // General information
  	$output .= sprintf('<tr><td %s><b><a href="%s">%s</a></b> %s%s</td></tr>', 
  	  $group['is_full'] ? 'class="disabled"' : '', check_url($group['url']), check_plain($group['name']), 
  	  !empty($age_group) ? '<b>(' . $age_group . ')</b>' : '', 
  	  !empty($group['period']) ? '<br />' . sprintf('%s - %s', date('d.m.Y', $group['period']['value']), date('d.m.Y', $group['period']['value2'])) : '');

  	$group_dates = array_values($group['group_dates']);

  	// Schedule
  	foreach($group_dates as $group_date) {
  	  // Build location string
  	  $location = array();
  	  if(!empty($group_date['location']['name'])) $location[] = $group_date['location']['name'];
  	  if(!empty($group_date['location']['street'])) $location[] = $group_date['location']['street'];
  	  
      $output .= sprintf('<tr><td>%s</td><td>%s</td></tr>', 
      sprintf('%s %s - %s', weekday_num_to_string($group_date['day']), format_date($group_date['time']['value'], 'time'), format_date($group_date['time']['value2'], 'time')), 
      sprintf('%s', implode(' | ', $location)));
  	}
  	
  	// Add to calendar
  	$add_to_calendar = drupal_get_form("add_to_calendar_form_" . $group['id'], $group['nid'], $group['id']);
  	$output .= sprintf('<tr><td colspan="2"><a data-target-id="add-to-calendar" data-target-group-id="%s" href="" 
  	  class="link before-calendar">%s</a><div class="">%s</div></td></tr>',
  	$group['id'], ka_t('Add to calendar'), drupal_render($add_to_calendar)
  	);
  	
  	// Registration
  	$registration_options = format_registration_options($node);
  	if(!empty($registration_options)) {
  	  $output .= '<tr><td colspan="2">';
      $output .= sprintf('<b>%s:</b> %s', ka_t('Registration'), implode(' | ', $registration_options));
      $output .= '</td></tr>';
    }
    
    // Price
    $price = $group['price'];
    $output .= '<tr><td colspan="2">';
    $output .= sprintf('<b>%s:</b> %s', ka_t('Price'), render_ticket_price($price['type'], $price['value']));
    $output .= '</td></tr>';
    
    // Registration
    $output .= '<tr><td colspan="2">';
    if($group['is_full']) {
      $output .= sprintf('<span class="link danger">%s</span>', ka_t('Group is full!'));
    } else if(isset($registration_options['internet']) && !empty($node->field_registration_internet)) {
      $url = $node->field_registration_internet[LANGUAGE_NONE][0]['url'];
      $output .= sprintf('<a target="_blank" href="%s" class="btn stretch">%s</a>', check_url($url), ka_t('Register'));
    }
    $output .= '</td></tr>';
    
  	$output .= '</tbody>';
  }

  $output .= '</table>';

  return $output;
}

function format_registration_options($node, $get_one = FALSE) {
  $emw_node = entity_metadata_wrapper('node', $node);
  $options = array();
  
  $registration_types = $emw_node->field_registration_types->value();
  
  foreach($registration_types as $registration_type) {
    if($registration_type === 'internet') {
      $link_array = $emw_node->{"field_registration_$registration_type"}->value();
      if(!empty($link_array['url'])) {
        $url = check_url($link_array['url']);
        if(!$get_one){
          $options[$registration_type] = sprintf('<a href="%s" class="link">%s</a>', $url, str_replace('http://', '', $url));
        }else{
          $options[$registration_type] = $url;
        }
      }
    } else if(!empty($emw_node->{"field_registration_$registration_type"}->value())) {
      $options[$registration_type] = check_plain($emw_node->{"field_registration_$registration_type"}->value());
    }
  }
  if($get_one){
    ksort($options);
    if(!empty($options['internet'])){
      $options_one['url'] = TRUE;
      $options_one['info'] = $options['internet'];
    }else{
      $options_one['info'] = array_shift($options);
    }
    return $options_one;
  }else{
    return $options;
  }
}

function video_thumbnail_url($uri) {
  $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
  $local_path = $wrapper->getLocalThumbnailPath();
  $video_thumbnail_url = image_style_url('gallery_thumbnail', $local_path);
  
  return $video_thumbnail_url;
}

function kultuuriaken_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_class = array(
    'status' => '',
    'error' => ' information',
    'warning' => ' danger',
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= '<div class="notification' . $status_class[$type] . '" data-plugin="containerClose">';
    $output .= '<div class="inline"><div class="indicator"></div><div class="message">';
    
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= reset($messages);
    }
    $output .= '</div><div class="close"><i class="before-close" rel="close"></i></div></div></div>';
  }
  return $output;
}

function is_array_empty($InputVariable)
{
   $Result = true;

   if (is_array($InputVariable) && count($InputVariable) > 0)
   {
      foreach ($InputVariable as $Value)
      {
         $Result = $Result && is_array_empty($Value);
      }
   }
   else
   {
      $Result = empty($InputVariable);
   }

   return $Result;
}

/*
* Checks the cookies for accessibility settings and returns the appropriate class array.
*/
/*
function get_accessibility_settings() {
  $acc_settings_array = array();
  
  if(isset($_COOKIE['contrast'])) {
    switch($_COOKIE['contrast']) {
      case '0':
        $acc_settings_array[] = 'contrast-0';
        break;
      case '1':
        $acc_settings_array[] = 'contrast-1';
        break;
    }
  }
  
  if(isset($_COOKIE['lineHeight'])) {
    switch($_COOKIE['lineHeight']) {
      case '2x':
        $acc_settings_array[] = 'lineHeight-2x';
        break;
      case '4x':
        $acc_settings_array[] = 'lineHeight-4x';
        break;
      case '6x':
        $acc_settings_array[] = 'lineHeight-6x';
        break;
    }
  }
  
  if(isset($_COOKIE['textSize'])) {
    switch($_COOKIE['textSize']) {
      case '0':
        $acc_settings_array[] = 'textSize-0';
        break;
      case '1':
        $acc_settings_array[] = 'textSize-1';
        break;
      case '2':
        $acc_settings_array[] = 'textSize-2';
        break;
    }
  }
  
  return $acc_settings_array;
}
*/

function get_activity_add_to_calendar_form($group){
  $output = '';
  $add_to_calendar = drupal_get_form("add_to_calendar_form_" . $group['id'], $group['nid'], $group['id']);

  $output .= sprintf('<a data-target-id="add-to-calendar" data-target-group-id="%s" href="" class="link after-calendar">%s</a><div class="">%s</div>',
  $group['id'], ka_t('Add to calendar'), drupal_render($add_to_calendar)); // Add to calendar
  
  return $output;
}

function kultuuriaken_date_display_range($variables) {
  $date1 = $variables['date1'];
  $date2 = $variables['date2'];
  $timezone = $variables['timezone'];
  $attributes_start = $variables['attributes_start'];
  $attributes_end = $variables['attributes_end'];

  // Wrap the result with the attributes.
  return t('!start-date - !end-date', array(
    '!start-date' => '<span class="date-display-start"' . drupal_attributes($attributes_start) . '>' . $date1 . '</span>', 
    '!end-date' => '<span class="date-display-end"' . drupal_attributes($attributes_end) . '>' . $date2 . $timezone . '</span>',
  ));
}
