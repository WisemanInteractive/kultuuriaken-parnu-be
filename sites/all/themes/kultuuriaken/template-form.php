<?php

function kultuuriaken_theme($existing, $type, $theme, $path) {
  $items['ka_event_form'] = array(
    'render element' => 'form',
    'template' => 'add-event',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
  );
  
  $item['ka_activity_form'] = array(
    'render element' => 'form',
    'template' => 'add-activity',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
  );
  
  // Summaries
  $items['introduction_summary'] = array(
    'render element' => 'summary',
    'template' => 'introduction-summary',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
    'variables' => array('data' => NULL, 'node_type' => 'event'),
  );
  
  $items['category_summary'] = array(
    'render element' => 'summary',
    'template' => 'category-summary',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
    'variables' => array('data' => NULL, 'node_type' => 'event'),
  );
  
  $items['ticket_summary'] = array(
    'render element' => 'summary',
    'template' => 'ticket-summary',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
    'variables' => array('data' => NULL),
  );
  
  $items['gallery_summary'] = array(
    'render element' => 'summary',
    'template' => 'gallery-summary',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
    'variables' => array('data' => NULL),
  );
  
  $items['organizer_summary'] = array(
    'render element' => 'summary',
    'template' => 'organizer-summary',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
    'variables' => array('data' => NULL),
  );
  
  $items['activity_groups_summary'] = array(
    'render element' => 'summary',
    'template' => 'activity-groups-summary',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/activity_groups',
    
  );
  
  $items['activity_group'] = array(
    'render element' => 'container',
    'template' => 'activity-group',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/activity_groups',
  );
  
  $items['ka_activity_form'] = array(
    'render element' => 'form',
    'template' => 'add-activity',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
  );
  
  $items['activity_registration_type_container'] = array(
    'render element' => 'container',
    'template' => 'activity-groups-container',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/activity_groups'
  );
  
  $items['registration_types_container'] = array(
    'render element' => 'container',
    'template' => 'activity-registration-types-container',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/activity_groups'
  );
  
  $items['group_price_container'] = array(
    'render element' => 'container',
    'template' => 'activity-groups-price',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/activity_groups'
  );
  
  $items['activity_groups_container'] = array(
    'render element' => 'container',
  );
  
  $items['ticket_availability_container'] = array(
    'render element' => 'container',
  );
  
  $items['ticket_price_container'] = array(
    'render element' => 'container',
  );
  
  $items['event_category_container'] = array(
    'render element' => 'container',
  );
  
  $items['event_video_container'] = array(
    'render element' => 'container',
  );
  
  $items['event_gallery_container'] = array(
    'render element' => 'container',
  );
  
  $items['event_gallery_file_upload'] = array(
    'render element' => 'button',
  );
  
  $items['gallery_image_preview'] = array(
    'render element' => 'container',
  );
  
  $items['gallery_images_preview_container'] = array(
    'render element' => 'container',
  );
  
  $items['next_step_continue'] = array(
    'render element' => 'container',
  );
  
  $items['publish_options'] = array(
    'render element' => 'container',
    'template' => 'publish-options',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
  );
  
  $items['organizer_select'] = array(
    'render element' => 'select',
  );
  
  $items['group_schedule'] = array(
    'render element' => 'container',
    'template' => 'activity-groups-schedule',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/activity_groups',
  );
  
  $items['event_tickets_additional'] = array(
    'render element' => 'container',
    'template' => 'event-tickets-additional',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms/events',
  );
  
  // Organizer tables
  $items['pager_show_all'] = array(
    'render element' => 'items',
  );
  
  $items['ka_organizer_edit_form'] = array(
    'render element' => 'form',
    'template' => 'edit-account',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/forms',
  );
  
  // Add event popup for anonymous.
  $items['ka_anon_popup'] = array(
    'render element' => 'container',
    'template' => 'anon-popup',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/organizer',
    'variables' => array('anon_message_html' => NULL, 'type' => NULL),
  );
  
  // General Search form
  $items['views_exposed_form__general_search'] = array(
    'render element' => 'form',
    'template' => 'general-search',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/general_search',
  );
  
  // Newsletter modal form
  $items['ka_newsletter_form_modal'] = array(
    'render element' => 'form',
    'template' => 'webform-form-modal',
    'path' => drupal_get_path('theme', 'kultuuriaken') . '/templates/newsletter',
  );
  
  return $items;
}

function kultuuriaken_preprocess(&$vars, $hook) {
  if($hook === 'activity_group') {
    // Get activity group tooltips
    /* Group name */
    $group_name_tooltip = variable_get_value('group_name_tooltip');
    $vars['group_name_tooltip'] = !empty($group_name_tooltip) ? '<span class="tooltip-mark" title="' . check_plain($group_name_tooltip) . '"></span>' : '';
    /* Group age */
    $group_age_tooltip = variable_get_value('group_age_tooltip');
    $vars['group_age_tooltip'] = !empty($group_age_tooltip) ? '<span class="tooltip-mark" title="' . check_plain($group_age_tooltip) . '"></span>' : '';
    /* Time & place */
    $time_place_tooltip = variable_get_value('time_place_tooltip');
    $vars['time_place_tooltip'] = !empty($time_place_tooltip) ? '<span class="tooltip-mark" title="' . check_plain($time_place_tooltip) . '"></span>' : '';
    /* Price */
    $price_tooltip = variable_get_value('price_tooltip');
    $vars['price_tooltip'] = !empty($price_tooltip) ? '<span class="tooltip-mark" title="' . check_plain($price_tooltip) . '"></span>' : '';
  }
  if($hook === 'category_summary') {
    $category_tids = $vars['data']['category']['field_category'];
    if(!empty($category_tids)) {
      foreach($category_tids as $category_tid) {
        $category = taxonomy_term_load($category_tid);
        $vars['data']['category']['field_category'][$category_tid] = array('tid' => $category_tid, 'term' => i18n_taxonomy_localize_terms($category));
      }
    }
    $subcategory_tids = !empty($vars['data']['category']['field_subcategory']) ? $vars['data']['category']['field_subcategory'] : NULL;
    if(!empty($subcategory_tids)) {
        foreach($subcategory_tids as $subcategory_tid) {
          $category = taxonomy_term_load($subcategory_tid);
          $vars['data']['category']['field_subcategory'][$subcategory_tid] = array('tid' => $subcategory_tid, 'term' => i18n_taxonomy_localize_terms($category));
        }
      }
    
    if(!empty($vars['data']['field_target_groups'])) { // Only for events
      $target_groups = $vars['data']['field_target_groups'];
      $selected_target_group_names = array();
      foreach($target_groups as $tid => $target_group) {
        if(!$target_group) continue;
        $target_group_term = i18n_taxonomy_localize_terms(taxonomy_term_load($tid));
        $vars['data']['field_target_groups'][$tid] = array('tid' => $tid, 'term' => $target_group_term);
        $selected_target_group_names[] = $target_group_term->name;
      }
      $vars['data']['field_target_groups']['rendered'] = implode(', ', $selected_target_group_names);
    }
  }
  if($hook === 'ticket_summary') {
    // Price string
    $type = $vars['data']['event_tickets']['price']['field_event_ticket_type'];
    $price = !empty($vars['data']['event_tickets']['price']['field_event_ticket_price']) ?
      floatval(str_replace(',', '.', $vars['data']['event_tickets']['price']['field_event_ticket_price'])) : NULL;
    $price_from = !empty($vars['data']['event_tickets']['price']['field_event_ticket_price_from']) ? 
      floatval(str_replace(',', '.', $vars['data']['event_tickets']['price']['field_event_ticket_price_from'])) : NULL;
    $price_to = !empty($vars['data']['event_tickets']['price']['field_event_ticket_price_to']) ?
      floatval(str_replace(',', '.', $vars['data']['event_tickets']['price']['field_event_ticket_price_to'])) : NULL;
    $vars['ticket_price'] = render_ticket_price($type, $price, $price_from, $price_to);
    
    // Ticket availability string
    $field_event_ticket_availability = field_info_field('field_event_ticket_availability');
    $event_ticket_availability = list_allowed_values($field_event_ticket_availability);
    $vars['ticket_availability'] = !empty($event_ticket_availability[$vars['data']['event_tickets']['availability']['field_event_ticket_availability']]) ?
      $event_ticket_availability[$vars['data']['event_tickets']['availability']['field_event_ticket_availability']] : '';
  }
  if($hook === 'ka_organizer_edit_form') {

    if(!empty($vars['form']['edit_uid']['#value'])) {
      $uid = $vars['form']['edit_uid']['#value'];
      $user = user_load($uid);
      $emw_user = entity_metadata_wrapper('user', $user);
      
      $vars['user_data']['name'] = $emw_user->field_full_name->value();
      $vars['user_data']['institution'] = $emw_user->field_institution->value();
      $vars['user_data']['mail'] = $emw_user->mail->value();
      $vars['user_data']['phone'] = $emw_user->field_user_phone->value();
    }
  }
  if($hook === 'ka_event_form') {
  }
  if($hook === 'views_exposed_form__general_search') {
    global $language;
	  $lang = $language->language;
    $vars['form']['search']['#theme_wrappers'] = array();
    $vars['form']['submit']['#theme_wrappers'] = array();
    //$vars['form']['reset']['#theme_wrappers'] = array();
    $vars['form']['reset']['#attributes']['style'] = 'display:none;';
    $vars['form']['search']['#attributes']['placeholder'] = ka_t('Search for events, actvities, places');
    $vars['form']['search']['#attributes']['data-url'] = '/'.$lang.'/main/autocomplete';
    $vars['form']['search']['#attributes']['data-plugin'] = 'autocomplete';
    
    $vars['form']['reset']['#process'] = array('bootstrap_form_process');
  }
  if($hook === 'views_view__general_search__page') {
    if(empty($vars['view']->exposed_input['node_type']['event'])) {
      $vars['show_event_search'] = FALSE;
    } else {
      $vars['show_event_search'] = TRUE;
    }
  }
  if($hook === 'views_view__general_search__attachment') {
    if(empty($vars['view']->exposed_input['node_type']['activity'])) {
      $vars['show_activity_search'] = FALSE;
    } else {
      $vars['show_activity_search'] = TRUE;
    }
  }
  if($hook === 'webform_form') {
    //dpm($vars);
  }
}

/* FORM FUNCTIONS */
function kultuuriaken_form_ka_event_form_alter(&$form, &$form_state, $form_id) {
   //krumo($form);
}

function kultuuriaken_checkbox($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array('id', 'name', '#return_value' => 'value'));

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));
  
  // Custom theme classes
  $prefix = '<span class="customCheckbox">';
  $suffix = '<span class="indicator"></span></span>';
  
  return $prefix . '<input' . drupal_attributes($element['#attributes']) . ' />' . $suffix;
}

function kultuuriaken_checkboxes($variables) {
  $element = $variables['element'];
  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'][] = 'form-checkboxes';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = array_merge($attributes['class'], $element['#attributes']['class']);
  }
  if (isset($element['#attributes']['title'])) {
    $attributes['title'] = $element['#attributes']['title'];
  }
  $prefix = '<div class="row"><div class="col-12">';
  $suffix = '</div></div>';

  return $prefix . '<div' . drupal_attributes($attributes) . '>' . (!empty($element['#children']) ? $element['#children'] : '') . '</div>' . $suffix;
}

function kultuuriaken_container($variables) {
  $element = $variables['element'];

  // Ensure #attributes is set.
  $element += array('#attributes' => array());

  // Special handling for form elements.
  if (isset($element['#array_parents'])) {
    // Assign an html ID.
    if (!isset($element['#attributes']['id'])) {
      $element['#attributes']['id'] = $element['#id'];
    }

    // Core's "form-wrapper" class is required for states.js to function.
    $element['#attributes']['class'][] = 'form-wrapper';

    // Add Bootstrap "form-group" class.
    $element['#attributes']['class'][] = 'form-group';
  }
  // Don't wrap checkboxes in DIVs.
  if(!empty($element['#attributes']['class'][3]) && $element['#attributes']['class'][3] === 'checkbox') {
    return $element['#children'];
  }
  if(!empty($element['#id']) && $element['#id'] === 'edit-event-info-event-tickets-price') {
    $prefix = '<div class="row"><div class="col-2"><div class="form-item"><div class="form-item_title input-height">' . ka_t('Ticket price') 
    . ':*</div></div></div><div class="col-10">';
    $suffix = '';
    return '<div' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</div>';
  }
  return '<div' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</div>';
}

function kultuuriaken_form_element_label(&$variables) {
  $element = $variables['element'];
  // Extract variables.
  $output = '';

  $title = !empty($element['#title']) ? filter_xss_admin($element['#title']) : '';

  // Only show the required marker if there is an actual title to display.
  if ($title && $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '') {
    $title .= $required;
  }

  $display = isset($element['#title_display']) ? $element['#title_display'] : 'before';
  $type = !empty($element['#type']) ? $element['#type'] : FALSE;
  $checkbox = $type && $type === 'checkbox';
  $radio = $type && $type === 'radio';

  // Immediately return if the element is not a checkbox or radio and there is
  // no label to be rendered.
  if (!$checkbox && !$radio && ($display === 'none' || !$title)) {
    return '';
  }

  // Retrieve the label attributes array.
  $attributes = &_bootstrap_get_attributes($element, 'label_attributes');

  // Add Bootstrap label class.
  $attributes['class'][] = 'control-label';
  
  if(strpos($element['#id'], 'edit-step-5-registration-field-registration-types') === 0) {
    $attributes['class'][] = 'input-height';
  }

  // Add the necessary 'for' attribute if the element ID exists.
  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // Checkboxes and radios must construct the label differently.

  if ($checkbox || $radio) {
    if ($display === 'before') {
      $output .= '<span class="label-title">' . $title . '</span>';
    }
    elseif ($display === 'none' || $display === 'invisible') {
      $output .= '<span class="element-invisible">' . $title . '</span>';
    }
    // Inject the rendered checkbox or radio element inside the label.
    if (!empty($element['#children'])) {
      $output .= $element['#children'];
    }
    if ($display === 'after') {
      $output .= '<span class="label-title">' . $title . '</span>';
    }
  }
  // Otherwise, just render the title as the label.
  else {
    // Show label only to screen readers to avoid disruption in visual flows.
    if ($display === 'invisible') {
      $attributes['class'][] = 'element-invisible';
    }
    $output .= $title;
  }

  // Kultuuriaken specific changes
  if($element['#type'] === 'checkboxes') {
    $attributes['class'][] = 'form-item_label_title';
    return ' <span' . drupal_attributes($attributes) . '>' . $output . "</span>\n";
  }

  if($element['#type'] !== 'checkbox' && $element['#type'] !== 'radio' && $element['#title_display'] !== 'invisible') {
    
    $output = '<div class="form-item_title">' . $output;
    if((strpos($element['#id'], 'edit-step-1-') !== FALSE || strpos($element['#id'], 'edit-step-2-') !== FALSE)
    && ($element['#type'] === 'textfield' || $element['#type'] === 'textarea') && empty($element['#no_counter'])) {
      $output .= '<span class="form-item_info">' . ka_t('Characters') . ': <span rel="counter"></span></span>'; // Add letter counter
    }
    $output .= '</div>';
    
    if(strpos($element['#id'], 'field-event-date') !== FALSE) {
      $output .= '<i class="after-calendar"></i>';
    }

    return  $output;
  }
  
  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $output . "</label>\n";
}

function kultuuriaken_button($variables) {
  $element = $variables['element'];
  $text = $element['#value'];

  // Allow button text to be appear hidden.
  // @see https://www.drupal.org/node/2327437
  if (!empty($element['#hide_text']) || $element['#icon_position'] === 'icon_only') {
    $text = '<span class="sr-only">' . $text . '</span>';
  }

  // Add icons before or after the value.
  // @see https://www.drupal.org/node/2219965
  if (!empty($element['#icon']) && ($icon = render($element['#icon']))) {
    // Add icon position class.
    _bootstrap_add_class('icon-' . drupal_html_class($element['#icon_position'] === 'icon_only' ? 'only' : $element['#icon_position']), $element);

    if ($element['#icon_position'] === 'after') {
      $text .= ' ' . $icon;
    }
    else {
      $text = $icon . ' ' . $text;
    }
  }
  /*
  if(strpos($element['#id'], 'add-more-event-dates') !== FALSE) {
    return '<a' . drupal_attributes($element['#attributes']) . '>' . filter_xss_admin($text) . "</a>\n";
  }*/
  // This line break adds inherent margin between multiple buttons.
  return '<button' . drupal_attributes($element['#attributes']) . '>' . filter_xss_admin($text) . "</button>\n";
}

function kultuuriaken_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array('id', 'name', '#return_value' => 'value'));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {

    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

function kultuuriaken_ticket_availability_container($variables) {
  $output = '';
  $output .= '<div class="form-item multiple">';
  
  $output .= '<label class="input-height"><span class="customRadio">';
  $output .= render($variables['container']['field_event_ticket_availability']['link']);
  $output .= '<span class="indicator"></span></span><span class="label-title">' . ka_t('Link to ticket purchase') . '</span></label>';
  
  $output .= render($variables['container']['field_event_ticket_link']);
  
  $output .= '</div><div class="form-item multiple">';
  
  $output .= '<label class="input-height"><span class="customRadio">';
  $output .= render($variables['container']['field_event_ticket_availability']['ticket_office']);
  $output .= '<span class="indicator"></span></span><span class="label-title">' . ka_t('Ticket booth (on location)') . '</span></label>';
  
  $output .= '</div><div class="form-item multiple">';
  
  $output .= '<label class="input-height"><span class="customRadio">';
  $output .= render($variables['container']['field_event_ticket_availability']['other']);
  $output .= '<span class="indicator"></span></span><span class="label-title">' . ka_t('Other') . '</span></label>';
  
  $output .= render($variables['container']['field_event_ticket_other']);
  
  $output .= '</div>';
  
  /*
  //krumo($variables);
  $element = $variables['element'];
  
  // Ensure #attributes is set.
  $element += array('#attributes' => array());

  // Special handling for form elements.
  if (isset($element['#array_parents'])) {
    // Assign an html ID.
    if (!isset($element['#attributes']['id'])) {
      $element['#attributes']['id'] = $element['#id'];
    }

    // Core's "form-wrapper" class is required for states.js to function.
    $element['#attributes']['class'][] = 'form-wrapper';

    // Add Bootstrap "form-group" class.
    $element['#attributes']['class'][] = 'form-group';
  }
  
  return '<div' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</div>';
  */
  return $output;
}

function kultuuriaken_ticket_price_container($variables) {
  $element = $variables['container'];
  
  $output = '';
  $output .= '<div class="form-item multiple">';
  
  $output .= '<label class="input-height auto-width"><span class="customRadio">';
  $output .= render($element['field_event_ticket_type']['fixed']);
  $output .= '<span class="indicator"></span></span></label>';
  
  $output .= render($element['field_event_ticket_price']);
  $output .= '<span class="label-title">€</span></div>';
  
  $output .= '<div class="form-item multiple">';
  
  $output .= '<label class="input-height auto-width"><span class="customRadio">';
  $output .= render($element['field_event_ticket_type']['between']);
  $output .= '<span class="indicator"></span></span></label>';
  
  $output .= '<span class="label-title">' . ka_t('Starting from') . '</span>';
  $output .= render($element['field_event_ticket_price_from']);
  $output .= '<span class="label-title">€ ' . ka_t('to') . '</span>';
  $output .= render($element['field_event_ticket_price_to']);
  $output .= '<span class="label-title">€</span></div>';
  
  $output .= '<div class="form-item multiple">';
  $output .= '<label class="input-height auto-width"><span class="customRadio">';
  $output .= render($element['field_event_ticket_type']['free']);
  $output .= '<span class="indicator"></span></span><span class="label-title">' . ka_t('Free') . '</span></label></div>';
  
  return $output;
}

function kultuuriaken_event_category_container($variables) {
  $element = $variables['container'];
  
  $output = '<div class="row"><div class="col-4">';
  $output .= render($element['field_category']);
  $output .= '</div>';
  
  $output .= '<div class="col-4">';
  $output .= render($element['field_subcategory']);
  $output .= '</div></div>';
  
  return $output;
}

function kultuuriaken_event_video_container($variables) {
  $element = $variables['container'];
  
  $output = '<div class="row"><div class="col-12">';
  
  $add_video_tooltip_text = variable_get_value('enter_video_url_tooltip');
  $add_video_tooltip = !empty($add_video_tooltip_text) ? '<span class="tooltip-mark" title="' . check_plain($add_video_tooltip_text) . '"></span>' : '';
  $output .= '<h6>' . ka_t('Add video URL') . $add_video_tooltip . '</h6>';
  
  if(!empty($element['et'])) {
    $output .= render($element['et']);
  }
  if(!empty($element['ru'])) {
    $output .= render($element['ru']);
  }
  if(!empty($element['en'])) {
    $output .= render($element['en']);
  }
  $output .= '</div></div>';
  
  //$output .= '<div class="row"><div class="col-12">';
  $output .= render($element['add_video_other_languages_link']);
  $output .= render($element['add_video_other_languages']);
  //$output .= '</div></div>';
  
  return $output;
}

function kultuuriaken_event_gallery_container($variables) {
  $element = $variables['container'];

  $output = '<div class="row"><div class="col-12">';
  $output .= '<h6>' . ka_t('Select how to add photos');
  
  $tooltip_text = variable_get_value('add_picture_tooltip');
  $tooltip = !empty($tooltip_text) ? '<span class="tooltip-mark" title="' . check_plain($tooltip_text) . '"></span>' : '';
  $output .= $tooltip . '</h6>';
  
  $output .= '<div class="form-item"><label><span class="customRadio">';
  $output .= render($element['upload_type']['upload']);
  $output .='<span class="indicator"></span></span><span class="label-title">' . ka_t('From computer') . ' </span></label>';
  
  $output .= '<label><span class="customRadio">';
  $output .= render($element['upload_type']['link']);
  $output .= '<span class="indicator"></span></span><span class="label-title">' . ka_t('Enter URL link') . '</span></label>';
  
  $output .= '</div></div></div>';
  
  $output .= '<div class="row"><div class="col-12">';
  $output .= '<table class="files"><tbody>';
  
  if(!empty($element['image_preview'])) {
    $output .= render($element['image_preview']);
  }
  
  if(!empty($element['gallery_upload'])) {
    $output .= '<tr><td class="no-padding"><div class="file-upload">';
    $output .= render($element['gallery_upload']);
    $output .= render($element['gallery_upload_submit']);
    $output .= '<img src="/' . path_to_theme() . '/assets/imgs/placeholder-1.gif"></div></td>';
    $output .= '<td>' . ka_t('Add photo') . '</td></tr>';
  }
  
  $output .= '</tbody></table>';
  
  $output .= '</div></div>';
  
  if(!empty($element['gallery_url'])) {
    $output .= '<div class="row"><div class="col-10">';
    $output .= render($element['gallery_url']);
    $output .= '</div><div class="col-2">';
    $output .= render($element['gallery_url_submit']);
    $output .= '</div></div>';
  }
  
  //krumo($element);
  return $output;
}

function kultuuriaken_gallery_images_preview_container($variables) {
  $element = $variables['container'];
  $images = !empty($element['images']) ? $element['images'] : array();
  $output = '';

  foreach($images as $fid => $image) {
    if(!is_numeric($fid)) continue;
    $file = $image['file']['#value'];
    $output .= render($image);
    $output .= '<td><label><span class="customRadio">';
    $output .= render($element['main_image'][$file->fid]);
    $output .= '<span class="indicator"></span></span>';
    $output .= '<span class="label-title">' . ka_t('select as main photo') . '</span></label></td>';
    
    $output .= '<td class="line"></td>';
    
    $output .= '<td><a href="" class="preview-image-remove link after-close secondary">' . ka_t('Remove') .'</a>' . render($element['remove_image'][$fid]) . '</td>';
    $output .= '</tr>';
  }
  return $output;
}

function kultuuriaken_gallery_image_preview($variables) {
  $element = $variables['container'];
  $file = $element['file']['#value'];
  
  $output = '<tr><td class="no-padding">';
  $output .= '<div class="file-image" style="background-image:url(' . file_create_url($file->uri) . ')">';
  $output .= '<img src="/' . path_to_theme() . '/assets/imgs/placeholder-1.gif' . '"></div></td>';
  $output .= '<td>' . $file->filename . sprintf(' (%s)', format_size_units($file->filesize)) . '</td>';
  $output .= '<td class="line"></td>';
  
  return $output;
}

function kultuuriaken_next_step_continue($variables) {
  $element = $variables['container'];
  
  $output = '<div class="row"><div class="col-12">';
  $output .= render($element['save_and_continue']);
  $output .= '<a data-target="cancel-button" href="" class="link secondary">' . ka_t('Cancel') . '</a>';
  $output .= render($element['cancel']);
  $output .= '</div></div>';

  return $output;
}

/*
function kultuuriaken_publish_options($variables) {
  $element = $variables['container'];
  
  $output = '<div class="row"><div class="col-12">';
  $output .= '<a href="" class="link secondary after-modul big-icon">' . ka_t('Preview') . '</a>';
  $output .= '</div></div><div class="row"><div class="col-12">';

  $output .= render($element['publish_now']);
  $output .= render($element['publish_later']);
  $output .= render($element['save_draft']);
  $output .= render($element['delete']);
  $output .= render($element['preview']);
  $output .= '<a data-id="delete-node" href="" class="link secondary after-trash pull-right btn-height">' . ka_t('Delete event') .'</a>';
  $output .= '</div></div>';
  
  return $output;
}
*/

function kultuuriaken_event_gallery_file_upload($variables) {
  $element = $variables['button'];
  
  return $element;
}

function ka_radios_after_build($element, &$form_state) {
  foreach (element_children($element) as $key) {
    unset($element[$key]['#theme_wrappers']);
  }
  // Always return the element to render in after_build callbacks.
  return $element;
}

function format_size_units($bytes) {
  if ($bytes >= 1073741824)
  {
      $bytes = number_format($bytes / 1073741824, 2) . ' GB';
  }
  elseif ($bytes >= 1048576)
  {
      $bytes = number_format($bytes / 1048576, 2) . ' MB';
  }
  elseif ($bytes >= 1024)
  {
      $bytes = number_format($bytes / 1024, 2) . ' KB';
  }
  elseif ($bytes > 1)
  {
      $bytes = $bytes . ' bytes';
  }
  elseif ($bytes == 1)
  {
      $bytes = $bytes . ' byte';
  }
  else
  {
      $bytes = '0 bytes';
  }
  return $bytes;
}

function kultuuriaken_activity_registration_type_container($variables) {
  $element = $variables['container'];
  
  $output = '<div class="row"><div class="col-12">';
  $output .= '<h6>' . ka_t('Registration information') . ':</h6>';
  $output .= '</div></div>';

  $output .= '<div class="row"><div class="col-2"><div class="form-item">';
  $output .= '<div class="form-item_title">&nbsp;</div>';
  $output .= render($element['registration_types']['local']);
  $output .= '</div></div>';
  
  $output .= '<div class="col-4"><div class="form-item padding-right">';
  $output .= '<div class="form-item_title">' . ka_t('Address') . ':</div>';
  $output .= render($element['local']);
  $output .= '</div></div>';
  
  $output .= '<div class="col-2"><div class="form-item">';
  $output .= '<div class="form-item_title">&nbsp;</div>';
  $output .= render($element['registration_types']['phone']);
  $output .= '</div></div>';
  
  $output .= '<div class="col-4"><div class="form-item">';
  $output .= '<div class="form-item_title">' . ka_t('Telephone number') . ':</div>';
  $output .= render($element['phone']);
  $output .= '</div></div></div>';
  
  $output .= '<div class="row pull-up"><div class="col-2"><div class="form-item">';
  $output .= '<div class="form-item_title">&nbsp;</div>';
  $output .= render($element['registration_types']['email']);
  $output .= '</div></div>';
  
  $output .= '<div class="col-4"><div class="form-item padding-right">';
  $output .= '<div class="form-item_title">' . ka_t('E-Mail') . ':</div>';
  $output .= render($element['email']);
  $output .= '</div></div>';
  
  $output .= '<div class="col-2"><div class="form-item">';
  $output .= '<div class="form-item_title">&nbsp;</div>';
  $output .= render($element['registration_types']['internet']);
  $output .= '</div></div>';
  
  $output .= '<div class="col-4"><div class="form-item">';
  $output .= '<div class="form-item_title">' . ka_t('URL') . ':</div>';
  $output .= render($element['internet']);
  $output .= '</div></div>';
  
  $output .= '</div>';
  
  return $output;
}

function ka_form_error($error_array) {
  if(isset($error_array) && !empty($error_array)) {
    return '<span class="red-text">' . implode('; ', $error_array) . '</span>';
  } else {
    return '';
  }
}

function kultuuriaken_organizer_select($variables) {
  $element = $variables['select'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select'));
  
  $prefix = '<div class="form-item"><div class="form-item_title">' . ka_t('Filter') . '</div>';
  $suffix = '</div>';
  
  return $prefix . '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>' . $suffix;
}

/**
 * Override node preview to remove trimmed teaser version.
 */
function kultuuriaken_node_preview($variables) {
  $node = $variables['node'];

  $output = '<div class="preview">';

  $preview_trimmed_version = FALSE;

  $elements = node_view(clone $node, 'teaser');
  $trimmed = drupal_render($elements);
  $elements = node_view($node, 'full');
  $full = drupal_render($elements);

  // Do we need to preview trimmed version of post as well as full version?
  if ($trimmed != $full) {
   // drupal_set_message(t('The trimmed version of your post shows what your post looks like when promoted to the main page or when exported for syndication.<span class="no-js"> You can insert the delimiter "&lt;!--break--&gt;" (without the quotes) to fine-tune where your post gets split.</span>'));
    //$output .= '<h3>' . t('Preview trimmed version') . '</h3>';
    //$output .= $trimmed;
    //$output .= '<h3>' . t('Preview full version') . '</h3>';
    $output .= $full;
  }
  else {
    $output .= $full;
  }
  $output .= "</div>\n";

  return $output;
}