<?php

function ka_event_export($node_type){

  header("Access-Control-Allow-Origin: *");

  $node_array = ka_event_export_nodes(NULL, $node_type);
  //kpr($node_array);
  ka_node_to_json_output($node_array);
}

function ka_event_export_nodes($parameters = NULL, $node_type) {
  $tids = array();
  
  $vocabulary = ($node_type == 'events') ? 'event_categories' : 'activity_categories' ;
  $argument_strings = !empty($parameters) ? $parameters : $_GET;
  $argument_categories = array();
  
  if(isset($argument_strings['categories'])){
    $categories_params = explode('|', $argument_strings['categories']);
    foreach ($categories_params as $category) {
      
      $tid = taxonomy_get_term_by_name($category, $vocabulary);
      //kpr($tid);
      $parent_tids = array_keys($tid);
      foreach($parent_tids as $parent){
        if(empty(taxonomy_get_parents($parent))){
          $argument_categories[] += $parent;
          $childrens[] = array_keys(taxonomy_get_children($parent));
          foreach (array_keys(taxonomy_get_children($parent)) as $parent_children) {
            $argument_categories[] += $parent_children;
          }
        }
      }
    }
  }
  //kpr($argument_categories);
  
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  if($node_type === 'events') {
    $query->entityCondition('bundle', 'event');
    if(!empty($argument_categories)) $query->fieldCondition('field_category', 'target_id', $argument_categories);
    
  } else if($node_type === 'activities') {
    $query->entityCondition('bundle', 'activity');
    if(!empty($argument_categories)) $query->fieldCondition('field_category_activity', 'target_id', $argument_categories);
    
    // Query only nodes with present activity groups.
    $query->fieldCondition('field_activity_groups', 'value', 'NULL', '!=');
  }

  $query->propertyCondition('status', NODE_PUBLISHED);
  
  // Only query nodes past the publish date
  $now = time();
  $query->fieldCondition('field_publish_date', 'value', $now, '<');
  
  // Only query nodes with approved status
  $query->fieldCondition('field_ka_status', 'value', 'published');
  
  $result = $query->execute();
  
  if (isset($result['node'])) {
    $nids = array_keys($result['node']);
    return node_load_multiple($nids);
  } else {
    return array();
  }
}

function ka_node_to_json_output($nodes, $array = NULL) {
  $output = new stdClass();
  $output->list = array();

  foreach($nodes as $node) {
    $json_array = export_ka_node_to_array($node);
    
    if(!is_array($json_array)) continue;
    foreach($json_array as $item) {
      $output->list[] = $item;
    }
  }
  
  //kpr($output);
  if(!$array) {
    drupal_json_output($output);
  } else {
    return $output;
  }
}

function export_ka_node_to_array($node) {
  global $language;
  global $arguments;
  global $base_url;
  $emw_node = entity_metadata_wrapper('node', $node)->language('et');
  
  global $argument_categories;
  
  $translation_handler = entity_translation_get_handler('node', $emw_node->value());
  $existing_translations = $translation_handler->getTranslations('title_field', 'en');
  //kpr($emw_node->value());
  $json_node = array();
  $json_node['id'] = $emw_node->nid->value();
  $json_node['title'] = check_plain($emw_node->title_field->value());
  $json_node['summary'] = check_plain($emw_node->field_summary->value());
  $json_node['content'] = check_plain($emw_node->field_body->value());
  
  if(isset($existing_translations->data['en'])){
    $emw_node_en = entity_metadata_wrapper('node', $node)->language('en');
    $json_node['title_eng'] = check_plain($emw_node_en->title_field->value());
    $json_node['summary_eng'] = check_plain($emw_node_en->field_summary->value());
    $json_node['content_eng'] = check_plain($emw_node_en->field_body->value());
  }
  $existing_translations = $translation_handler->getTranslations('title_field', 'ru');
  if(isset($existing_translations->data['ru'])){
    $emw_node_en = entity_metadata_wrapper('node', $node)->language('ru');
    $json_node['title_rus'] = check_plain($emw_node_en->title_field->value());
    $json_node['summary_rus'] = check_plain($emw_node_en->field_summary->value());
    $json_node['content_rus'] = check_plain($emw_node_en->field_body->value());
  }
  
  
  foreach ($emw_node->field_gallery->value() as $img) {
    $img_uri = $img['uri'];
    $json_node['images'][] = image_style_url('detail_image', $img_uri);
  }
  
  
  // Tags (Valdkond)
  if($emw_node->getBundle() === 'event') {
    $c_tid = i18n_taxonomy_localize_terms($emw_node->field_category->value());

  } else if($emw_node->getBundle() === 'activity') {
    $c_tid = i18n_taxonomy_localize_terms($emw_node->field_category_activity->value());
  }
  
  if(!empty($c_tid)) {
    //return FALSE;
    //kpr($c_tid);
    //if(in_array('muu', $c_tid)) return FALSE;
    foreach($c_tid as $c_tid_single) {
      if(mb_stripos(mb_strtolower(check_plain($c_tid_single->name)), 'põnevus'))  print_r($c_tid_single->name);//print_r('sama');
      $json_node['categories'][] = mb_strtolower(check_plain($c_tid_single->name));
    }
  }
  
  $json_node['organizer_info'] = array(
    'organizer_name' => $emw_node->field_organizer_name->value(),
    'organizer_link' => $emw_node->field_organizer_www->value(),
    'organizer_contact' => $emw_node->field_organizer_contact->value(),
    'organizer_phone' => $emw_node->field_organizer_phone->value(),
    'organizer_email' => $emw_node->field_organizer_email->value(),
  );
  
  $json_nodes = array();
  // Event node type specific
  if($emw_node->getBundle() === 'event') {
    $nid = $emw_node->getIdentifier();
    $event_dates = $emw_node->field_event_dates->getIterator();
    foreach($event_dates as $key => $event_date) {
      if($event_date->field_event_date->value()['value'] < time()) continue;
      
      $start = !empty($event_date->field_event_date->value()['value']) ? (int) $event_date->field_event_date->value()['value'] : 0;
      $end = !empty($event_date->field_event_date->value()['value2']) ? (int) $event_date->field_event_date->value()['value2'] : 0;
      
      if(isset($_GET['date_start']) && !empty($_GET['date_start'])) {
        $date_from = strtotime($_GET['date_start'] . ' 00:00');
        if($date_from && $date_from > $start) {
          continue;
        }
      }
      
      $json_node['events'][$key] = array(
        'start' => $event_date->field_event_date->value()['value'],
        'end' => $event_date->field_event_date->value()['value2'],
        'start_string' => date('Y-m-dTH:m:s', $event_date->field_event_date->value()['value']),
        'end_string' => date('Y-m-dTH:m:s', $event_date->field_event_date->value()['value2']),
        'location_string' => $event_date->field_location->value()['street'],
        'coordinates' => array(
          'lat' => $event_date->field_location->value()['latitude'],
          'lng' => $event_date->field_location->value()['longitude'],
        ),
        'link' => $base_url . '/et/' . drupal_get_path_alias('node/' . $nid, 'et') . '?event=' . $event_date->getIdentifier(),
        //'link_eng' => isset($emw_node_eng) ? $base_url . '/en/' . drupal_get_path_alias('node/' . $nid, 'en') . '?event=' . $event_date->getIdentifier() : NULL ;
      );
      if(isset($emw_node_en)){
        $json_node['events'][$key]['link_eng'] = $base_url . '/en/' . drupal_get_path_alias('node/' . $nid, 'en') . '?event=' . $event_date->getIdentifier();
      }
    }
    if(empty($json_node['events'])) return FALSE;
    //if(empty($json_node['events'])) break;
    // Price
    $ticket_type = $emw_node->field_event_ticket_type->value();
    switch($ticket_type) {
      case 'fixed':
        $price = $emw_node->field_event_ticket_price->value();
        $json_node['price']['fixed'] = sprintf('%s', check_plain($price));
        break;
      case 'free':
        $json_node['price'] = ka_t('Free');
        break;
      case 'between':
        $price_from = $emw_node->field_event_ticket_price_from->value();
        $price_to = $emw_node->field_event_ticket_price_to->value();
        $json_node['price'] = array(
          'from' => sprintf('%s', check_plain($price_from)),
          'to' => sprintf('%s', check_plain($price_to)),
        );
        break;
    }
    
    $json_nodes[] = $json_node;
  }
  
  // Activity node type specific
  if($emw_node->getBundle() === 'activity') {
    $nid = $emw_node->getIdentifier();
    $activity_groups = $emw_node->field_activity_groups->getIterator();
    $i = 0;
    foreach ($activity_groups as $groups) {
      //kpr($groups->value());
      $target_groups = array();
      foreach ($groups->field_activity_group_demographic as $target_group) {
        $target_groups[] = $target_group->value();
      }
      $d = array();
      $period_type = !empty($groups->field_activity_groups_regular->value()) ? 'regular' : 'fixed' ;
      if($period_type === 'fixed'){
        $activity_dates = $groups->field_activity_groups_fixed->getIterator();
        foreach ($activity_dates as $dates) {
          
          $start = $dates->field_event_date->value()['value'];
          
          if(isset($_GET['date_start']) && !empty($_GET['date_start'])) {
            $date_from = strtotime($_GET['date_start'] . ' 00:00');
            if($date_from && $date_from > $start) {
              continue;
            }
          }
          
          if($dates->field_event_date->value()['value'] <= time()) continue;
          $d = array(
            'date' => array(
              'from' => $dates->field_event_date->value()['value'],
              'to' => $dates->field_event_date->value()['value2'],
            ),
            'start_string' => date('Y-m-dTH:m:s', $dates->field_event_date->value()['value']),
            'end_string' => date('Y-m-dTH:m:s', $dates->field_event_date->value()['value2']),
            'location' => $dates->field_location->value()['street'],
          );
        }
      }else{
        $activity_dates = $groups->field_activity_groups_regular->getIterator();
        foreach ($activity_dates as $dates) {
          
          $start = $dates->field_activity_date_time->value()['value'];
          
          if(isset($_GET['date_start']) && !empty($_GET['date_start'])) {
            $date_from = strtotime($_GET['date_start'] . ' 00:00');
            if($date_from && $date_from > $start) {
              continue;
            }
          }
          
          $d = array(
            'days' => $dates->field_activity_date_days->value(),
            'date' => array(
              'from' => $dates->field_activity_date_time->value()['value'],
              'to' => $dates->field_activity_date_time->value()['value2'],
            ),
            'start_string' => format_date($dates->field_activity_date_time->value()['value'], 'time'),
            'end_string' => format_date($dates->field_activity_date_time->value()['value2'], 'time'),
            'location' => $dates->field_location->value()['street'],
          );
        }
      }
      if(empty($d)) break;
      $json_node['activity'][] = array(
        'group_name' => $groups->field_activity_group_name->value(),
        'group_full' => $groups->field_activity_group_full->value(),
        'group_occurrence_type' => $groups->field_activity_group_type->value(),
        'group_age_range' => array(
          'from' => $groups->field_activity_group_age_from->value(),
          'to' => $groups->field_activity_group_age_to->value(),
        ),
        'target_groups' => $target_groups,
        'price_type' => $groups->field_activity_group_price_type->value(),
        'price' => $groups->field_activity_group_price->value(),
      );
      if($period_type == 'regular'){
        if($groups->field_activity_date_period->value()['value'] <= time()) continue;
        $json_node['activity'][$i] += array(
          'activity_period' => array(
            'from' => $groups->field_activity_date_period->value()['value'],
            'to' => $groups->field_activity_date_period->value()['value2'],
          ),
        );
      }
      $json_node['activity'][$i] += array(
        'link' => $base_url . '/' . $language->language . '/' . drupal_get_path_alias('node/' . $nid, $language->language) . '?group=' . $groups->getIdentifier(),
        'activity_period_type' => $period_type,
        'activity_dates' => $d,
      );
    }
    if(!empty($json_node['activity'])){
      $json_nodes[] = $json_node;
    }
    
  }

  if(!empty($json_nodes)) {
    return $json_nodes;
  }
}
