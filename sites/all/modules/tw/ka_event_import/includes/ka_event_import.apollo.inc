<?php

function ka_event_import_apollo(){
  
  $existing_api_events = get_nodes_by_api('apollo', 'event');
  
	$full_event_info = array();
	$response_data_string = drupal_http_request('https://www.apollokino.ee/xml/Events/?area=1002&includeGallery=true')->data;
	//kpr($response_data_string);
	$response_array = response_data_string_to_array($response_data_string);
  //kpr($response_array);
  //die();
  
  $target_groups = taxonomy_get_tree(TARGET_GROUPS_VID);
	$target_groups_assoc = get_taxonomy_assoc($target_groups);
	$movie_event_terms = taxonomy_get_children(2);
	
	$full_event_info = array();
	$i = 0;
	foreach ($response_array['Event'] as $show) {
		
		//kpr($show);
  	$step = array();
		$event_dates_xml = drupal_http_request('https://www.apollokino.ee/xml/Schedule/?area=1002&nrOfDays=31&eventId=' . $show['ID'])->data;
		$event_dates = response_data_string_to_array($event_dates_xml);
		if(empty($event_dates['Shows'])) continue;
	  	$step['step_1'] = array(
				'introduction_languages' => array(
					'et' => 'et',
					'ru' => 0,
					'en' => 0
				),
				'et' => array(
					'field_title' => $show['Title'],
					'field_summary' => $show['ShortSynopsis'],
					'field_body' => $show['Synopsis'],
					'field_additional_info_link' => $show['EventURL'],
				),
			);
	
	  	$step['step_2'] = array(
				'category' => array(
					'field_category' => array('2' => '2'),
					'field_subcategory' => 	get_event_category($show['Genres'], $movie_event_terms),
				),
				'field_target_groups' => get_target_group($show['RatingLabel'], $target_groups_assoc),
	  	);
	  	$step['step_3'] = array(
				'event_date_location' => parse_event_date_location_apollo($event_dates),
				'event_tickets' => array(
					'price' => array(
						'field_event_ticket_type' => 'between',
	  				'field_event_ticket_price' => '',
	  				'field_event_ticket_price_from' => '4,69',
	  				'field_event_ticket_price_to' => '8,29',
					),
					'availability' => array(
						'field_event_ticket_availability' => 'link',
						'field_event_ticket_link' => $show['EventURL'],
						'field_event_ticket_other' => '',
					),
					'additional' => array(
						'et' => 'https://www.apollokino.ee/cinemas/akl-prices',
						'ru' => '',
						'en' => '',
					),
				),
	  	);
	  	$excisting_gallery = (isset($existing_api_events[$show['ID']]) && !empty($existing_api_events[$show['ID']]->field_gallery)) ? $existing_api_events[$show['ID']]->field_gallery[LANGUAGE_NONE] : array();
	  	$images = ($show['Gallery']) ? $show['Gallery'] : $show['Images'] ;
	  	$gallery = get_event_images_apollo($images, $excisting_gallery);
	  	
	  	$step['step_4'] = array(
	  		'gallery' => $gallery
	  	);
	  	
	  	$step['step_5'] = array(
	  		'left' => array(
					'field_organizer_name' => 'Apollo Kino',
					'field_organizer_www' => 'https://www.apollokino.ee',
					'field_organizer_address' => 'Pärnu Keskus, Lai 5, Pärnu, 80011',
				),
				'right' => array(
					'field_organizer_contact' => 'Apollo Kino',
					'field_organizer_email' => 'info@apollokino.ee',
					'field_organizer_phone' => '+372 633 6020',
	 			),
	  	);
	  	
	  	$step['node_type'] = 'event';
	  	$step['field_api_provider'] = 'apollo';
	  	$step['field_api_id'] = $show['ID'];
	  	if(isset($existing_api_events[$show['ID']]) && $existing_api_events[$show['ID']]){
	  		$step['existing_node'] = $existing_api_events[$show['ID']];
	  	}
	  	
	  	$full_event_info[] = $step;
	  	
	}
	#kpr($full_event_info);
	#die();
	return $full_event_info;
	
}

function parse_event_date_location_apollo($info){
	$event_dates = array();
	//kpr($info);
	if(!empty($info['Shows'])){
		if(key($info['Shows']['Show']) === 0){
			foreach ($info['Shows']['Show'] as $value) {
				$ts = strtotime($value['dttmShowStart']);
				$te = strtotime($value['dttmShowEnd']);
				
				list($start_date, $start_time) = explode(" ", date("d.m.Y H:i", $ts));
				list($end_date, $end_time) = explode(" ", date("d.m.Y H:i", $te));
				
				$event_dates[] = array(
					'date' => array(
						'start' => array(
							'field_event_date_from' => $start_date,
							'field_event_time_from' => $start_time,
						),
						'end' => array(
							'field_event_date_to' => $end_date,
							'field_event_time_to' => $end_time,
						),
					),
					'location' => array(
						'field_event_location' => 'Apollo Kino',
						'field_event_address' => 'Pärnu Keskus, Lai 5, Pärnu, 80011',
					),
				);
			}
		}else{
			$ts = strtotime($info['Shows']['Show']['dttmShowStart']);
			$te = strtotime($info['Shows']['Show']['dttmShowEnd']);
			
			list($start_date, $start_time) = explode(" ", date("d.m.Y H:i", $ts));
			list($end_date, $end_time) = explode(" ", date("d.m.Y H:i", $te));
			
			$event_dates[] = array(
				'date' => array(
					'start' => array(
						'field_event_date_from' => $start_date,
						'field_event_time_from' => $start_time,
					),
					'end' => array(
						'field_event_date_to' => $end_date,
						'field_event_time_to' => $end_time,
					),
				),
				'location' => array(
					'field_event_location' => 'Apollo Kino',
					'field_event_address' => 'Pärnu Keskus, Lai 5, Pärnu, 80011',
				),
			);
		}
	}
	
	return $event_dates;
}

function get_event_images_apollo($gallery, $excisting_gallery = array()){
	//kpr($gallery);
	//kpr($excisting_gallery);
	$ex_imgs = array();
	$images_array = array();
	$first = TRUE;
	
	if(!empty($excisting_gallery)){
		foreach($excisting_gallery as $imgs){
			$ex_imgs[] = $imgs['field_source_url'][LANGUAGE_NONE][0]['url'];
		}
	}
	if(!empty($gallery)){
		if(isset($gallery['GalleryImage']) && key($gallery['GalleryImage']) === 0){
			for ($i = 0; $i < 3; $i++) {
				if(!isset($gallery['GalleryImage'][$i])) break;
				if(!in_array($gallery['GalleryImage'][$i]['Location'], $ex_imgs)){
					//kpr($gallery['GalleryImage'][$i]);
					if($img = ka_download_image_link($gallery['GalleryImage'][$i]['Location'])) $images_array['image_preview']['images'][$img->fid] = array('file' => $img);
				}else{
					foreach($excisting_gallery as $excisting_image){
						$images_array['image_preview']['images'][$excisting_image['fid']] = $excisting_image;
					}
				}
			}
		}else{
			if(isset($gallery['GalleryImage']['Location'])) {
				$gallery_location = $gallery['GalleryImage']['Location'];
			} else if(isset($gallery['Location'])) {
				$gallery_location = $gallery['Location'];
			} else if(isset($gallery['EventLargeImagePortrait'])){
				$gallery_location = $gallery['EventLargeImagePortrait'];
			}
			
			if(!in_array($gallery_location, $ex_imgs)){
				
				if($img = ka_download_image_link($gallery_location)) $images_array['image_preview']['images'][$img->fid] = array('file' => $img);
			}elseif(!empty($existing_image)){
				$images_array['image_preview']['images'][$excisting_image[0]['fid']] = $excisting_image;
			}
		}
		if(isset($images_array['image_preview']['images']) && !empty($images_array['image_preview']['images'])){
			$images_array['image_preview']['main_image'] = (string) key($images_array['image_preview']['images']);
		}
	}
	
	return $images_array;
}
