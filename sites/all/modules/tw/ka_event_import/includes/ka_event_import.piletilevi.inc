<?php

function ka_event_import_piletilevi() {

  $response_data = drupal_http_request('http://www.piletilevi.ee/api/?preset=kultuurparnu&language=est');
  $response_array = drupal_json_decode($response_data->data);

  if (empty($response_array['responseStatus']) || $response_array['responseStatus'] !== 'success') {
    return array();
  }

  $existing_api_events = get_nodes_by_api('piletilevi', 'event');

  $target_groups = taxonomy_get_tree(TARGET_GROUPS_VID);
  $target_groups_assoc = get_taxonomy_assoc($target_groups);
  $voc = taxonomy_vocabulary_machine_name_load('event_categories');
  $tree = taxonomy_get_tree($voc->vid, 0, NULL);

  $shows = $response_array['responseData']['show'];
  $sub_shows = $response_array['responseData']['concert'];
  $categorys = $response_array['responseData']['category'];

  $cats = array();
  foreach($categorys as $category) {
    //kpr($category);
    if ($category['parentCategoryId'] == 57394) {
      $cats[$category['id']] = $category;
    }
    else {
      $cats[$category['parentCategoryId']]['child'][$category['id']] = $category;
    }
  }

  $full_event_info = array();
  $i = 0;

  // Group shows
  $group_shows = array();
  foreach ($shows as $show) {
    foreach ($sub_shows as $sub_show) {
      if($show['id'] == $sub_show['showId']) {
        $show['childEvent'][] = $sub_show;
      }
    }
    $group_shows[] = $show;
  }

  foreach ($group_shows as $show){

    $step = array();

    if ($show['venueId']) {
      $venue = ka_event_import_piletilevi_get_venue($show['venueId'], $response_array['responseData']['venue']);
    } else {
      $venue = [];
    }

    $step['step_1'] = array(
      'introduction_languages' => array(
        'et' => 'et',
        'ru' => 0,
        'en' => 0
      ),
      'et' => array(
        'field_title' => $show['decoratedTitle'],
        'field_summary' => ' ',
        'field_body' => $show['descriptionPlain'],
        'field_additional_info_link' => $show['link'],
      ),
    );

    $step['step_2'] = [];
    $step['step_2'] = [
      'category' => ka_event_import_piletilevi_set_category($cats, $show),
    ];
    if ($show['childEvent'][0]['ageRestriction'] == NULL) {
      $step['step_2']['field_target_groups'] = [
        'lapsed',
        'noored',
        'täiskasvanud',
        'pered'
      ];
    }
    else {
      $step['step_2']['field_target_groups'] = ['täiskasvanud'];
    }

    $step['step_3'] = array();
    $step['step_3'] = array(
      'event_date_location' => ka_event_import_piletilevi_format_date($show, $venue),
    );

    $prices = explode(' - ', $show['prices']);
    if (count($prices) > 1) {
      $price_type = 'between';
      $price = '';
      $price_from = $prices[0];
      $price_to = $prices[1];
    }
    else {
      $price_type = 'fixed';
      $price = $show['prices'];
      $price_from = '';
      $price_to = '';
    }
    $step['step_3']['event_tickets'] = array(
      'price' => array(
        'field_event_ticket_type' => $price_type,
        'field_event_ticket_price' => $price,
        'field_event_ticket_price_from' => $price_from,
        'field_event_ticket_price_to' => $price_to,
      ),
      'availability' => array(
        'field_event_ticket_availability' => 'link',
        'field_event_ticket_link' => $show['link'],
        'field_event_ticket_other' => '',
      ),
      'additional' => array(
        'et' => $show['purchaseDescription'],
        'ru' => '',
        'en' => '',
      ),
    );

    $existing_gallery = (isset($existing_api_events[$show['id']]) && !empty($existing_api_events[$show['id']]->field_gallery)) ? $existing_api_events[$show['ID']]->field_gallery[LANGUAGE_NONE] : array();
    $gallery = ka_event_import_piletilevi_get_gallery($show['originalImageUrl'], $existing_gallery);

    $step['step_4'] = array(
      'gallery' => $gallery
    );

    if (empty($show['promoterId'])) {
      if (empty($venue)) {
        $step['step_5'] = array(
          'left' => array(
            'field_organizer_name' => ka_t('Look description'),
            'field_organizer_www' => '',
            'field_organizer_address' => '',
          ),
          'right' => array(
            'field_organizer_contact' => '',
            'field_organizer_email' => '',
            'field_organizer_phone' => '',
          ),
        );
      } else {
        $step['step_5'] = array(
          'left' => array(
            'field_organizer_name' => $venue['decoratedTitle'],
            'field_organizer_www' => $venue['externalUrl'],
            'field_organizer_address' => $venue['address'],
          ),
          'right' => array(
            'field_organizer_contact' => $venue['externalUrl'],
            'field_organizer_email' => '',
            'field_organizer_phone' => '',
          ),
        );
      }
    } else {
      $promoter = ka_event_import_piletilevi_get_promoter($show['promoterId'], $response_array['responseData']['promoter']);
      $step['step_5'] = array(
        'left' => array(
          'field_organizer_name' => $promoter['decoratedTitle'],
          'field_organizer_www' => $promoter['link'],
          'field_organizer_address' => $promoter['address'],
        ),
        'right' => array(
          'field_organizer_contact' => $promoter['link'],
          'field_organizer_email' => $promoter['email'],
          'field_organizer_phone' => $promoter['phone'],
        ),
      );
    }

    $step['node_type'] = 'event';
    $step['field_api_provider'] = 'piletilevi';
    $step['field_api_id'] = $show['id'];
    if (isset($existing_api_events[$show['id']]) && $existing_api_events[$show['id']]) {
      $step['existing_node'] = $existing_api_events[$show['id']];
    }

    $full_event_info[] = $step;

    $i++;

  }
  return $full_event_info;
}

function ka_event_import_piletilevi_format_date($show, $venue) {

  //kpr($venue);
  //Location
  if(!empty($show)) {
    foreach ($show['childEvent'] as $date) {
      //kpr($date);
      if (!$venue || empty($venue['address'])) {
        $event_address = 'Pärnu';
      }
      else {
        $venu_co = ka_event_import_piletilevi_normalize_venue_address($venue);
        $event_address = $venu_co;
      }

      $ts = $date['startTime']['stamp'];
      $te = $date['endTime']['stamp'];

      list($start_date, $start_time) = explode(" ", date("d.m.Y H:i", $ts));
      list($end_date, $end_time) = explode(" ", date("d.m.Y H:i", $te));

      $event_dates[] = [
        'date' => [
          'start' => [
            'field_event_date_from' => $start_date,
            'field_event_time_from' => $start_time,
          ],
          'end' => [
            'field_event_date_to' => $end_date,
            'field_event_time_to' => $end_time,
          ],
        ],
        'location' => [
          'field_event_location' => ucwords(mb_strtolower($date['eventPlace'])),
          'field_event_address' => $event_address,
        ],
      ];
    }
  }
  return $event_dates;
}

function ka_event_import_piletilevi_set_category($categorys ,$shows){

  // TODO: Multiple parent categories.
  $category = ka_event_import_piletilevi_get_category($categorys ,$shows);
  $step = array();
  if(!empty($category)) {
    foreach ($category as $cats) {
      $step = [
        'field_category' => [$cats['category'] => $cats['category']],
        'field_subcategory' => NULL,
      ];
    }
  }
  else{
    $step = [
      'field_category' => ['1' => '1'],
      'field_subcategory' => NULL,
    ];
  }
  return $step;
}

function ka_event_import_piletilevi_get_category($categorys ,$shows) {

  // Child kategooriat ei teinud (polnud hetkel vajadust);
  $taxonomy_event = array(
    'EV101' => 54,
    'Teater' => 1,
    'Film' => 2,
    'Sport' => 7,
    'Kontsert' => 3,
    'Kirjandus' => 8,
    'Toit' => 55,
    'Näitus' => 4,
    'Mess/laat' => 5,
    'Pidu/klubi' => 6,
    'Muu' => 9,
  );

  $categories_translation = array(
    'Varia' => 'Teater', //voi "muu"

    'Teater' => 'Teater',
    'Tants' => 'Teater',
    'Suvelavastus' => 'Teater',
    'Stand-up komöödia' => 'Teater',
    'Performance' => 'Teater',
    'Ooper ja Operett' => 'Teater',
    'Muusikal' => 'Teater',
    'Monolavastus' => 'Teater',
    'Mitmesugust' => 'Teater',
    'Komöödia' => 'Teater',
    'Draama' => 'Teater',
    'Tragikomöödia' => 'Teater',

    'Muusika' => 'Kontsert',
    'Mitmesugust' => 'Kontsert',
    'Klassika' => 'Kontsert',
    'Kergemuusika' => 'Kontsert',
    'Kammermuurika' => 'Kontsert',
    'Jazz ja Bluus' => 'Kontsert',
    'Hip Hop' => 'Kontsert',
    'Eesti muusika' => 'Kontsert',
    'Rock ja Pop' => 'Kontsert',
    'Pärimusmuusika' => 'Kontsert',
    'Rahvamuusika' => 'Kontsert',

    //1008 puudus parent info
    'Tähtpäevaks' => 'Muu',
    'Sport' => 'Sport',
    'Mitmesugust' => 'Muu',
    //1004 puudus parent info
    'Show' => 'Kontsert',
    'Muusika ja Teater' => 'Teater',
    'Mitmesugust' => 'Kontsert',
    'Jaanituled' => 'Pidu/klubi',

    'Söök ja jook' => 'Toit',

    'Festival' => 'Kontsert',

    'Jazzkaar' => 'Kontsert',
  );

  $event_category = array();

  //$event_category['category'] = NULL;
  //$event_category['subCategory'] = NULL;

  foreach ($shows['categories'] as $show){
    foreach ($categorys as $category){
      if($show == $category['id']){
        $key_value = $category['decoratedTitle'];
        foreach ($categories_translation as $key => $value){
          if($key == $key_value){
            foreach ($taxonomy_event as $key => $tax){
              if($value == $key){
                $event_category[]['category'] = $tax;
              }
            }
          }
        }
      }
      else {
        foreach ($category['child'] as $sub) {
          if ($show == $sub['id']) {
            $key_value = $sub['decoratedTitle'];
            foreach ($categories_translation as $key => $value) {
              if ($key == $key_value) {
                foreach ($taxonomy_event as $key => $tax){
                  if($value == $key){
                    $event_category[]['category'] = $tax;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return $event_category;
}

function ka_event_import_piletilevi_download_image($link) {
  $url_parsed = parse_url($link);

  if(!empty($link) && isset($url_parsed['path'])) {
    try {
      $file = system_retrieve_file($link, NULL, TRUE, FILE_EXISTS_RENAME);
    } catch (Exception $e) {}
  }

  if (!empty($file)) { // File has been downloaded
    $file->field_source_url[LANGUAGE_NONE][0] = array('url' => $link);
    // $file->filemime = 'image/jpeg'; // Not working
    $file = file_save($file);
    return $file;
  } else {
    // File download failed
    return FALSE;
  }
}

function ka_event_import_piletilevi_get_gallery($image_url, $excisting_gallery = array()) {
  $images_array = array();
  if(!empty($excisting_gallery)){
    foreach($excisting_gallery as $imgs){
      if ($imgs['field_source_url'][LANGUAGE_NONE][0]['url'] == $image_url) {
        return $excisting_gallery;
      }
    }
  }
  if($img = ka_event_import_piletilevi_download_image($image_url)) {
    $images_array['image_preview']['images'][$img->fid] = array('file' => $img);
  }
  if(isset($images_array['image_preview']['images']) && !empty($images_array['image_preview']['images'])){
    $images_array['image_preview']['main_image'] = (string) key($images_array['image_preview']['images']);
  }
  return $images_array;
}

function ka_event_import_piletilevi_get_promoter($id, $promoters) {
  foreach ($promoters as $promoter) {
    if ($promoter['id'] == $id) return $promoter;
  }
}

function ka_event_import_piletilevi_get_venue($venue_id, $venues) {
  foreach ($venues as $venue) {
    if ($venue['id'] == $venue_id) return $venue;
  }
}

function ka_event_import_piletilevi_normalize_venue_address($venue){

  if($venue['address'] == 'Keskväljak 1, Pärnu' || $venue['address'] == 'Keskväljak 1'){
    return 'Keskväljak 1, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Silluste 1'){
    return 'Sillutise 1, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Linnamehe, Kuiaru küla'){
    return 'Tammiste, Kuiaru, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Aida 4'){
    return 'Aida 4, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Jannseni 37'){
    return 'J. V. Jannseni 37, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Ranna puiestee 3'){
    return 'Ranna puiestee 3, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'A. H. Tammsaare puiestee 4A'){
    return 'A. H. Tammsaare puiestee 4a, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Õhtu tn 1 A'){
    return 'Tallinna värav, Vana-Tallinna 1, Pärnu, Pärnu maakond, Eesti';
  }
  #elseif($venue['city'] == 'Tahkuranna'){
  #  return 'Reiu 86508, Pärnu maakond, Eesti';
  #}
  elseif($venue['address'] == 'Kalli mnt 13'){
    return 'Kalli maantee 13, Tõstamaa, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Uus-Sauga 62'){
    return 'Uus-Sauga 62, Pärnu, Pärnu maakond, Eesti';
  }
  elseif($venue['address'] == 'Nikolai 22'){
    return 'Nikolai 22, Pärnu, Pärnu maakond, Eesti';
  }
  else{
    return 'Pärnu';
  }
}
