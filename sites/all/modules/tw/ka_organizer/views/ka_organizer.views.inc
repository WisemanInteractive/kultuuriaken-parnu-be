<?php

function ka_organizer_views_data_alter(&$data) {
   if ( isset($data['node']) && !isset($data['node']['period']) ) {
    $data['node']['period'] = array(
      'real field' => 'nid', // name of entity field to which filter applies
      'title' => t('Date string'),
      'help' => t('Date String Filter'),
      'filter' => array(
        'handler' => 'ka_organizer_handler_filter_period',
      ),
    );
  }
}