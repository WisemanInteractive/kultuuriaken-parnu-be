<?php

function ka_event_cronapi() {
  $items = array();
  
  $items['delete_preview_nodes'] = array(
    'title' => t('Delete preview nodes'),
    'scheduler' => array(
      'name' => 'crontab',
      'crontab' => array(
        'rules' => array('30 * * * *'),
      ),
    ),
  );
  
  $items['ka_cache_general_event_page'] = array(
    'title' => t('Cache general event'),
    'scheduler' => array(
      'name' => 'crontab',
      'crontab' => array(
        'rules' => array('0-59 * * * *'),
      ),
    ),
  );
  
  $items['ka_cache_general_event_page_vt'] = array(
    'title' => t('Cache general event VT'),
    'scheduler' => array(
      'name' => 'crontab',
      'crontab' => array(
        'rules' => array('0-59 * * * *'),
      ),
    ),
  );
  
  return $items;
}

function ka_cache_general_event_page() {
  global $language;
  global $base_url;
  $language_list = language_list();

  foreach($language_list as $l => $lang) {
    $ka_event_output = 'ka_event_output_' . $lang->language;

    $cache_output = cache_get($ka_event_output);
    if(!$cache_output || $cache_output->expire < (time() - 1800)) {
      $language = $lang;
      $expiration = time() + 3600;
      $output = ka_filters_json_callback('events', TRUE, TRUE);
      cache_set($ka_event_output, $output, 'cache', $expiration);
      return;
    }
  }
}

function ka_cache_general_event_page_vt() {
  global $language;
  
  $language_list = language_list();
  foreach($language_list as $l => $lang) {
    // Generate VT API output
    $ka_event_output_vt = 'ka_event_output_' . $lang->language . '_vt';
    
    $cache_output = cache_get($ka_event_output_vt);
    
    if(!$cache_output || $cache_output->expire < time()) {
      $language = $lang;
      $_GET['type'] = 'visittartu';
      $expiration = time() + 10800;
      $output = ka_filters_json_callback('events', TRUE, $language);
      cache_set($ka_event_output_vt, $output, 'cache', $expiration);
      return;
    }
  }
}

/**
 * Deletes old preview event/activity nodes which are older than 20 minutes.
 */
function delete_preview_nodes() {
  $query = new EntityFieldQuery();
  
  $preview_expired_date = strtotime('now -20 minutes');
  
  $query->entityCondition('entity_type', 'node')
    ->fieldCondition('field_ka_status', 'value', 'preview', '=')
    ->propertyCondition('created', $preview_expired_date, '<');
  
  $result = $query->execute();

  if(!empty($result['node'])) {
    $nodes = $result['node'];
    $nids = array_keys($nodes);
    watchdog('ka_event', 'Deleting temporary preview nodes. %count nodes deleted.', array('%count' => count($nids)));
    node_delete_multiple($nids);
  }
}
