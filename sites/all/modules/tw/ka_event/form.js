(function ($) {
  Drupal.behaviors.example = {
    attach: function (context, settings) {
      // When the hyperlink is clicked...
      $('#trigger-add-more-event-dates', context).click(function(event){
        // Stop the link's default behavior.
        event.preventDefault();
        // Trigger a click even on the hidden button to initate our AJAX.
        // I prefer using a class to find the element because sometimes Drupal appends the id with the delta.
        // e.g. edit-ajax-submit--2
        $('.add-more-button', context).trigger('mousedown');
      });
      
      $('body', context).on('click', '#save-as-draft-popup',function(event){
        event.preventDefault();
        $('#publish').css('display', 'block');
      });
      
      $('body', context).on('click', '.overlay .close-x, .overlay .close-x-link',function(event){
        event.preventDefault();
        $('#publish').css('display', 'none');
      });
      
      $('#add-videos-other-languages', context).click(function(event){
        // Stop the link's default behavior.
        event.preventDefault();
        // Trigger a click even on the hidden button to initate our AJAX.
        // I prefer using a class to fm$ind the element because sometimes Drupal appends the id with the delta.
        // e.g. edit-ajax-submit--2
        $('.add-videos-other-lang-button', context).trigger('mousedown');
      });

      $('body', context).each(function() {
        
      });
        
        /*
        var previewLinkAdded = typeof $(document, context).previewLinkAdded === undefined ? false : $(document, context).previewLinkAdded ;
        if(!previewLinkAdded) {
          $(document, context).previewLinkAdded = true;
          $('[data-target="preview-button"]', context).click(function(e) {
            e.preventDefault();
            var url = $('#item-preview', context).text();
            window.open(url, '_blank');
          });
        }
        */
      
      // Reapply JS wrappers after ajax.
      var SumoSelect;
      $(document, context).ajaxStop(function() {
        jQuery(document, context).bindWPM();
        SumoSelect = $('select').SumoSelect();
        
        // Show preview
        /*
        if($.trim($('#item-preview').html())) {
          var preview = $('#item-preview').html();
          $('#item-preview').html('');
          var modal = new tingle.modal({
            footer: false,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Close",
            onOpen: function() {
            },
            onClose: function() {
            },
            beforeClose: function() {
                // here's goes some logic
                // e.g. save content before closing the modal
              return true; // close the modal
            	return false; // nothing happens
            }
        });

        var url = preview.replace('<div></div>', '');
        
        // open modal
        //modal.open();
        
        var w = window.open(url, '_blank', 'location=yes,scrollbars=yes,status=yes');
        //$(w.document.body).html(preview);
        }*/
        
        // Other group names
        var defaultLang = $('[name^="step_3[groups]["][name$="][name]"]');
        if(!defaultLang.attr('data-other-lang')) {
          var langHidden = 0;
            $('[name^="step_3[groups][0"][name*="][name_t]["]').each(function() {
              if(!$(this).val()) {
        	   	  langHidden++;
                $(this).parent().hide();
              }
            });
            if(langHidden == 0) {
        	   	$('#add-name-other-languages').hide();
        	   }
            defaultLang.attr('data-other-lang', 1);
        }
        
        $('#add-name-other-languages').click(function(e) {
        	e.preventDefault();
            $(this).hide();
            $('[name^="step_3[groups][0"][name*="][name_t]["]').each(function() {
                $(this).parent().show();
            });
        });
        
      });
      
      // Activity group cancel button
      $('[data-action="cancel-regular-date"]', context).click(function(e) {
      	e.preventDefault();
      	var row = $(this, context).parents('tr');
      	row.find('input').val('').removeAttr('checked');
      	var selectedRows = row.find('select').val('00:00');
      	SumoSelect.filter(selectedRows).each(function(key, val) {
      	  val.sumo.reload();
      	});
      });
      
      $('body', context).on('change', "[id^='edit-step-4-gallery-gallery-upload']", function(){
        $("[id^='edit-step-4-gallery-gallery-upload-submit']", context).trigger('mousedown');
      });
      
      $('body', context).on('click', ".preview-image-remove, .date-remove, .change-group-button", function(event){
        event.preventDefault();
        $(this, context).next('button').trigger('mousedown');
      });
      
      $('body', context).on('click', "[data-change-id]", function(event){
        event.preventDefault();
        var changeId = $(this, context).attr('data-change-id');
        $('[data-id="' + changeId + '"]', context).trigger('mousedown');
      });
      
      $('body', context).on('click', "[data-add-id]", function(event){
        event.preventDefault();
        var changeId = $(this, context).attr('data-add-id');
        if($("[id^='" + changeId + "']", context).length > 0) {
          $("[id^='" + changeId + "']", context).trigger('mousedown');
        } else {
          $(this, context).next('button').trigger('click');
        }
        
      });
      
      $('body', context).on('click', "[data-target-next]", function(event){
        event.preventDefault();
        var targetId = $(this, context).attr('data-target-next');
        $(this, context).next('[data-id=' + targetId + ']').trigger('mousedown');
      });
      
      $('body', context).on('click', "[data-target]", function(event){
        event.preventDefault();
        var targetId = $(this, context).attr('data-target');
        $('[data-id=' + targetId + ']', context).trigger('mousedown');
      });
      
      $('body', context).on('click', "[data-remove-group]", function(event){
        event.preventDefault();
        
        var groupId = $(this, context).attr('data-remove-group');
        $('button[name="remove_group_' + groupId, context).trigger('mousedown');
      });
      
      /*
      $('body', context).on('click', "[data-target='delete-node']", function(event){
        event.preventDefault();
        $("[data-id='delete-node']", context).trigger('click');
      });
      */
      // Hide missing buttons
      $('[data-target]', context).each(function() {
         var target = $(this).attr('data-target');
         if($('[data-id=' + target + ']', context).length < 1) {
            $(this, context).addClass('ka-hidden');
         }
      });
      
      // Prevent submission on pressing enter, add a new line in case of textarea
      $(window, context).on('keydown', ':input:not(textarea)', function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }
  }
}(jQuery));