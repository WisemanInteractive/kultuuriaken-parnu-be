<?php

define('EVENT_CATEGORIES_VID', 2);
define('TARGET_GROUPS_VID', 4);

module_load_include('inc', 'ka_event', 'ka_event.form');
module_load_include('inc', 'ka_event', 'ka_event.edit_form');
module_load_include('inc', 'ka_event', 'ka_event.cron');

function ka_event_menu() {
  $items['add/event'] = array(
    'title' => 'New event',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ka_event_form', NULL, 'event'),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
  );
  
  $items['add/activity'] = array(
    'title' => 'New recreational activity',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ka_event_form', NULL, 'activity'),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
  );
  
  $items['edit/%node/%'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ka_event_form', 1),
    'access callback' => 'ka_edit_form_access',
    'access arguments' => array('update', 1, 2),
  );
  
  $items['new-event-anon/%'] = array(
    'title' => 'New event',
    'page callback' => 'new_event_anon_page_view',
    'page arguments' => array(1),
    'access callback' => 'anon_popup_page_access',
    'access arguments' => array(1),
  );
  
  return $items;
}

function ka_event_node_access($node, $op, $account) {
  // Deny access to nodes without upcoming event dates.
  if($op = 'view' && !user_access('administer nodes') && // Trying to view and is not admin
    !empty($node->type) && ($node->type === 'event' || $node->type === 'activity') && // Is event or activity
    ($node->uid === 0 || $node->uid !== $account->uid) && // Is not created by the user
    empty(ka_node_to_array($node))) {
    return NODE_ACCESS_DENY;
  }
  
}

function ka_event_entity_presave($entity, $type) {
  // Sort the regular day schedule based on weekday.
  if($type === 'paragraphs_item' && $entity->bundle === 'activity_group') {
    $group = entity_metadata_wrapper('paragraphs_item', $entity);
    if($group->field_activity_group_type->value() === 'regular') { // Regular schedule group
      $schedule_days = array();
      foreach($group->field_activity_groups_regular->getIterator() as $schedule_day) {
        $delta = $schedule_day->field_activity_date_days->value();
        $schedule_days[$schedule_day->field_activity_date_days->value()] = $schedule_day->value();
      }
      
      $entity->field_activity_groups_regular[LANGUAGE_NONE] = array();
      
      ksort($schedule_days);
      foreach($schedule_days as $schedule_day) {
        $entity->field_activity_groups_regular[LANGUAGE_NONE][] = array('value' => $schedule_day->item_id, 'revision_id' => $schedule_day->revision_id);
      }
    }
  }
}

function ka_event_entity_property_info_alter(&$info) {
  $info['node']['properties']['next_event_date'] = array(
    'type' => 'date',
    'label' => t('Next event date'),
    'sanitized' => TRUE,
    'getter callback' => 'ka_event_search_api_property_next_event_date_getter_callback',
  );
}

function ka_event_search_api_property_next_event_date_getter_callback($item) {
  if($item->type === 'event') {
    $emw_node = entity_metadata_wrapper('node', $item);
    $event_dates = $emw_node->field_event_dates->getIterator();
    foreach($event_dates as $delta => $event) {
      $now = time();
      $event_date = !empty($event->field_event_date->value()['value']) ? (int) $event->field_event_date->value()['value'] : 0;
      $event_date2 = !empty($event->field_event_date->value()['value2']) ? (int) $event->field_event_date->value()['value2'] : 0;
      if($now < $event_date2) return $event_date;
    }
  }
  if($item->type === 'activity') {
    $summary = ka_node_to_array($item);
    $now = time();
    if(!empty($summary)) {
      foreach($summary as $s) {
        if($now < $s['end_date']) {
          return $s['date_unix'];
        }
      }
    }
    
  }
  return 0;
}

function sort_by_weekday($a, $b) {
  if ($a->field_activity_date_days->value() == $b->field_activity_date_days->value()) {
        return 0;
    }
  return ($a->field_activity_date_days->value() < $b->field_activity_date_days->value()) ? 1 : -1;
}

function new_event_anon_page_view($obj_type) {
  if($obj_type === 'activity') {
    $anon_message_html = variable_get('anon_add_activity_note');
  } else {
    $anon_message_html = variable_get('anon_add_event_note');
  }
  
  return theme('ka_anon_popup', array('anon_message_html' => $anon_message_html, 'obj_type' => $obj_type));
}

function anon_popup_page_access($arg) {
  return ($arg === 'event' || $arg === 'activity');
}

function ka_edit_form_access($method, $node, $bundle) {
  $access = FALSE;
  
  if(module_exists('ka_user') && ka_user_shared_account($method, $node, $bundle)) {
    $access = TRUE;
  }
  
  if(node_access($method, $node)) {
    $access = TRUE;
  }
  
  if($node->type !== $bundle) {
    $access = FALSE;
  }
  
  if(!in_array($node->type, array('event', 'activity'))) {
    $access = FALSE;
  }
  
  return $access;
}

function ka_set_event_form_values($node, &$form_state) {
  $emw_node = entity_metadata_wrapper('node', $node);
  $translations = $node->translations->data;
  $node_type = $emw_node->getBundle();

  // Step 1
  if($node_type === 'event') {
    $step_1 = &$form_state['storage']['step_1'];
  } else {
    $step_1 = &$form_state['storage']['step_2'];
  }
  foreach($translations as $lang => $translation) {
    //$step_1['introduction_languages'][$lang] = $lang;
    
    $step_1[$lang]['field_title'] = $emw_node->language($lang)->title_field->value();
    $step_1[$lang]['field_summary'] = $emw_node->language($lang)->field_summary->value();
    $step_1[$lang]['field_body'] = $emw_node->language($lang)->field_body->value();
    $step_1[$lang]['field_additional_info_link'] = $emw_node->language($lang)->field_additional_info_link->value()['url'];
    
    if(!empty($step_1[$lang]['field_title']) || !empty($step_1[$lang]['field_summary']) ||
      !empty($step_1[$lang]['field_body']) || !empty($step_1[$lang]['field_additional_info_link'])
    ) { // Enable the language if any fields are not empty
      $step_1['introduction_languages'][$lang] = $lang;
    }
  }
  
  // Step 2
  if($node_type === 'event') {
    $step_2 = &$form_state['storage']['step_2'];
  } else {
    $step_2 = &$form_state['storage']['step_1'];
  }
  // -- Category & subcategory 
  if($node_type === 'event') {
    $categories = $emw_node->field_category->value();
  } else {
    $categories = $emw_node->field_category_activity->value();
  }
  
  $parent_categories = array();
  $child_categories = array();

  foreach($categories as $category) {
    if(!empty($category)) {
      $p = taxonomy_get_parents($category->tid);
      if(!empty($p) && $parent_category = reset($p)) {
        $parent_categories[$parent_category->tid] = $parent_category->tid;
        $child_categories[$category->tid] = $category->tid;
      } else {
        $parent_categories[$category->tid] = $category->tid;
      }
    }
  }
  
  $step_2['category']['field_category'] = $parent_categories;
  $step_2['category']['field_subcategory'] = $child_categories;

  // -- Target groups
  if($node_type === 'event') {
    $target_groups_objs = $emw_node->field_target_groups->value();
    $target_groups = array();
    foreach($target_groups_objs as $target_group) {
      $target_groups[$target_group->tid] = $target_group->tid;
    }
    $step_2['field_target_groups'] = $target_groups;
  }
  
  // Step 3
  $step_3 = &$form_state['storage']['step_3'];
  
  // -- Event dates
  if($node_type === 'event') {
    $event_dates = $emw_node->field_event_dates->getIterator();
    $qty = 0;
    foreach($event_dates as $event_date) {
      $date = $event_date->field_event_date->value();
      $location = $event_date->field_location->value();
      
      $step_3['event_date_location'][] = array(
          'date' => array(
              'start' => array(
                  'field_event_date_from' => format_date($date['value'], 'date_short'),
                  'field_event_time_from' => format_date($date['value'], 'time'),
                ),
              'end' => array(
                  'field_event_date_to' => format_date($date['value2'], 'date_short'),
                  'field_event_time_to' => format_date($date['value2'], 'time'),
                ),
            ),
          'location' => array(
              'field_event_location' => !empty($location['name']) ? $location['name'] : '',
              'field_event_address' => !empty($location['street']) ? $location['street'] : '',
            ),
          'pid' => $event_date->getIdentifier(),
          'end_date_unset' => $event_date->field_end_date_unset->value(),
        );
      $qty++;
    }
    $form_state['event_qty'] = $qty;
    
    // -- Ticket type
    $type = $emw_node->field_event_ticket_type->value();
    $step_3['event_tickets']['price']['field_event_ticket_type'] = $type;
    
    // -- Ticket price
    switch($type) {
      case 'fixed':
        $price = str_replace('.', ',', $emw_node->field_event_ticket_price->value());
        $step_3['event_tickets']['price']['field_event_ticket_price'] = $price;
        break;
      case 'between':
        $price_from = str_replace('.', ',', $emw_node->field_event_ticket_price_from->value());
        $step_3['event_tickets']['price']['field_event_ticket_price_from'] = $price_from;
        $price_to = str_replace('.', ',', $emw_node->field_event_ticket_price_to->value());
        $step_3['event_tickets']['price']['field_event_ticket_price_to'] = $price_to;
        break;
    }
    
    // -- Ticket availability
    $availability = $emw_node->field_event_ticket_availability->value();
    $step_3['event_tickets']['availability']['field_event_ticket_availability'] = $availability;
    
    switch($availability) {
      case 'link':
        $link = $emw_node->field_event_ticket_link->value();
        $step_3['event_tickets']['availability']['field_event_ticket_link'] = !empty($link['url']) ? $link['url'] : '';
        break;
      case 'other':
        $step_3['event_tickets']['availability']['field_event_ticket_other'] = $emw_node->field_event_ticket_other->value();
        break;
    }
    
    // -- Ticket additional info
    foreach($node->field_event_ticket_info as $lang => $additional_info) {
      $step_3['event_tickets']['additional']['languages'][$lang] = $emw_node->language($lang)->field_event_ticket_info->value();
    }
  } else {
    $groups = $emw_node->field_activity_groups->getIterator();
    foreach($groups as $group) {
      // NB! We have to keep existing groups to preserve link and cookie functionality
      $group_data = array();
      
      // Group ID
      $group_data['group_id'] = $group->getIdentifier();
      
      $group_data['name'] = $group->field_activity_group_name->value();
      $group_data['name_t']['en'] = $group->field_activity_group_name_en->value();
      $group_data['name_t']['ru'] = $group->field_activity_group_name_ru->value();
      
      $group_data['is_full'] = intval($group->field_activity_group_full->value());
      // Age groups
      $age_groups = $group->field_activity_group_demographic->value();
      $group_data['age_groups'] = array_combine($age_groups, $age_groups);
      // Age range
      $age_from = $group->field_activity_group_age_from->value();
      $group_data['age_range']['from'] = !empty($age_from) ? $age_from : '';
      $age_to = $group->field_activity_group_age_to->value();
      $group_data['age_range']['to'] = !empty($age_to) ? $age_to : '';
      // Price
      $group_data['price']['type'] = $group->field_activity_group_price_type->value();
      
      $price = str_replace('.', ',', $group->field_activity_group_price->value());
      $group_data['price']['single_payment'] = $group_data['price']['type'] === 'single_payment' ? $price : '';
      $group_data['price']['monthly_payment'] = $group_data['price']['type'] === 'monthly_payment' ? $price : '';

      $group_data['price']['additional_comment'] = $group->field_additional_comment->value();

      $group_type = $group->field_activity_group_type->value();

      if($group_type === 'regular') {
        $group_data['schedule']['type'] = 'regular';
        $step_3['activity_type'] = 'multigroup';
        
        $group_period = $group->field_activity_date_period->value();
        $group_data['schedule']['regular']['period_from'] = format_date($group_period['value'], 'date_short');
        $group_data['schedule']['regular']['period_to'] = format_date($group_period['value2'], 'date_short');
        
        // Schedule
        $schedule_days = $group->field_activity_groups_regular->getIterator();
        foreach($schedule_days as $schedule_day) {
          // Weekdays
          $weekday = $schedule_day->field_activity_date_days->value();
          $group_data['schedule']['regular']['weekdays'][$weekday] = $weekday;
          
          // Schedule ID
          $group_data['schedule']['regular']['day'][$weekday]['schedule_id'] = $schedule_day->getIdentifier();
          
          // Time
          $time = $schedule_day->field_activity_date_time->value();
          $group_data['schedule']['regular']['day'][$weekday]['time_from'] = format_date($time['value'], 'time');
          $group_data['schedule']['regular']['day'][$weekday]['time_to'] = format_date($time['value2'], 'time');
          
          // Location
          $location = $schedule_day->field_location->value();
          $group_data['schedule']['regular']['day'][$weekday]['location'] = $location['name'];
          $group_data['schedule']['regular']['day'][$weekday]['address'] = $location['street'];
        }
        
      } else if($group_type === 'fixed') {
        $group_data['schedule']['type'] = 'fixed';
        $step_3['activity_type'] = 'multigroup';
        $group_data['activity_type'] = 'multigroup';
        $single_group_activity = $group->field_single_group_activity->value();
        
        $group_dates = $group->field_activity_groups_fixed->getIterator();
        foreach($group_dates as $group_date) {
          $group_date_data = array();
          
          // Schedule ID
          $group_date_data['schedule_id'] = $group_date->getIdentifier();
          
          // Date
          $date = $group_date->field_event_date->value();
          $group_date_data['date'] = format_date($date['value'], 'date_short');
          $group_date_data['time_from'] = format_date($date['value'], 'time');
          $group_date_data['time_to'] = format_date($date['value2'], 'time');
          
          // Location
          $location = $group_date->field_location->value();
          $group_date_data['location'] = $location['name'];
          $group_date_data['address'] = $location['street'];
          
          if(!$single_group_activity) {
            $group_data['schedule']['fixed'][] = $group_date_data;
            $group_data['activity_type'] = 'multigroup';
          } else {
            $group_data['schedule']['date']['start']['field_event_date_from'] = format_date($date['value'], 'date_short');
            $group_data['schedule']['date']['end']['field_event_date_to'] = format_date($date['value2'], 'date_short');
            $group_data['schedule']['location']['field_event_location'] = $group_date_data['location'];
            $group_data['schedule']['location']['field_event_address'] = $group_date_data['address'];
            $group_data['activity_type'] = 'single_group';
            $step_3['activity_type'] = 'single_group';
          }
          
        }
      }
      
      $step_3['groups'][] = $group_data;
    }
  }
  
  // Step 4
  $step_4 = &$form_state['storage']['step_4'];
  $step_4 = array();
  // -- Video
  foreach($node->field_video as $lang => $video) {
    $uri = $emw_node->language($lang)->field_video->value()['uri'];
    if(strpos($uri, 'youtube://v/') === 0) {
      $url = str_replace('youtube://v/', 'https://www.youtube.com/watch?v=', $uri);
    } else if(strpos($uri, 'youtube://v/') === 0) {
      $url = str_replace('vimeo://v/', 'https://vimeo.com/', $uri);
    } else {
      continue;
    }
    
    $step_4['field_video'][$lang] = $url;
    $step_4['video_parsed'][$lang] = (object) $emw_node->language($lang)->field_video->value();
    
    if($lang !== 'et') $form_state['video_other_languages'] = TRUE;
  }
  
  // -- Gallery
  $main_image = (object) $emw_node->field_main_image->value();

  if(!empty($main_image->fid)) {
    $step_4['gallery']['image_preview']['images'][$main_image->fid]['file'] = $main_image;
    $step_4['gallery']['image_preview']['remove_image'][$main_image->fid]['fid'] = $main_image->fid;
    $step_4['gallery']['image_preview']['main_image'] = $main_image->fid;
  }
  
  $gallery = $emw_node->field_gallery->getIterator();
  foreach($gallery as $picture) {
    $file = (object) $picture->value();
    $step_4['gallery']['image_preview']['images'][$file->fid]['file'] = $file;
    $step_4['gallery']['image_preview']['remove_image'][$file->fid]['fid'] = $file->fid;
  }
  
  // Step 5
  $step_5 = &$form_state['storage']['step_5'];
  $step_5['left']['field_organizer_name'] = $emw_node->field_organizer_name->value();
  $step_5['left']['field_organizer_address'] = $emw_node->field_organizer_address->value();
  
  $organizer_link = $emw_node->field_organizer_www->value();
  $step_5['left']['field_organizer_www'] = !empty($organizer_link['url']) ? $organizer_link['url'] : '';
  
  $step_5['right']['field_organizer_contact'] = $emw_node->field_organizer_contact->value();
  $step_5['right']['field_organizer_email'] = $emw_node->field_organizer_email->value();
  $step_5['right']['field_organizer_phone'] = $emw_node->field_organizer_phone->value();
  
  
  if($node_type === 'activity') {
    // Registration options
    $reg_options_array = $emw_node->field_registration_types->value();
    $reg_options = array_combine($reg_options_array, $reg_options_array);
    
    $step_5['registration']['field_registration_types'] = $reg_options;
    foreach($reg_options as $option) {
      $value = $emw_node->{"field_registration_$option"}->value();
      if($option === 'internet') {
        $step_5['registration']["field_registration_$option"] = !empty($value['url']) ? $value['url'] : '';
      } else {
        $step_5['registration']["field_registration_$option"] = $value;
      }
    }
  }

  $form['#node_type'] = $node_type;
  ka_event_form_validate($form, $form_state, TRUE); // Validate the fields
}

function weekday_array($day = NULL) {
  $weekdays = array(
    1 => t('Monday'),
    2 => t('Tuesday'),
    3 => t('Wednesday'),
    4 => t('Thursday'),
    5 => t('Friday'),
    6 => t('Saturday'),
    7 => t('Sunday'),
  );
  
  if(empty($day)) {
    return $weekdays;
  } else {
    return $weekdays[$day];
  }
  
}

function ka_error_string($error_array = NULL) {
  if(is_array($error_array) && !empty($error_array)) {
  return '<span class="red-text">' . implode('; ', $error_array) . '</span>';
  } else if(is_string($error_array)) {
    return '<span class="red-text">' . $error_array . '</span>';
  } else {
    return '';
  }
}

function current_quarter_hour() {
    $minutes = date('i');
    $minutes_rounded = $minutes - ($minutes % 15);
    $minutes_rounded = $minutes_rounded === 0 ? $minutes_rounded . '0' : $minutes_rounded;
    $hour = date('H');
    return sprintf('%s:%s', $hour, $minutes_rounded);
}

function ka_download_image_link($link) {
  $url_parsed = parse_url($link);
  
  if(!empty($link) && isset($url_parsed['path'])) {
    try {
      $file = system_retrieve_file($link, NULL, TRUE, FILE_EXISTS_RENAME);
    } catch (Exception $e) {}
  }

  if (!empty($file)) { // File has been downloaded
    if(strpos($file->filemime, 'image') !== FALSE && validate_image($file)) { // File is valid        
      $file->field_source_url[LANGUAGE_NONE][0] = array('url' => $link);
      return file_save($file);
    } else {
      // File is invalid
      file_delete($file);
      return FALSE;
    }
  } else {
    // File download failed
    return FALSE;
  }
}

function is_valid_timestamp($timestamp)
{
    return ((string) (int) $timestamp === $timestamp) 
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
}
function ka_event_node_presave($node){
  if(!empty($node->field_category)){
    $cats = array();
    foreach ($node->field_category['und'] as $tid) {
      $cats[] = $tid['target_id'];
    }
    $unique_tid = array_unique($cats);
    unset($node->field_category);
    $i=0;
    foreach ($unique_tid as $new_tid) {
      $node->field_category['und'][$i]['target_id'] = $new_tid;
      $i++;
    }
  }
  if(!empty($node->field_category_activity)){
    $cats = array();
    foreach ($node->field_category_activity['und'] as $tid) {
      $cats[] = $tid['target_id'];
    }
    $unique_tid = array_unique($cats);
    unset($node->field_category_activity);
    $i=0;
    foreach ($unique_tid as $new_tid) {
      $node->field_category_activity['und'][$i]['target_id'] = $new_tid;
      $i++;
    }
  }
}
function ka_event_node_save($node){
  if(!empty($node->field_category)){
    $cats = array();
    foreach ($node->field_category['und'] as $tid) {
      $cats[] = $tid['target_id'];
    }
    $unique_tid = array_unique($cats);
    unset($node->field_category);
    $i=0;
    foreach ($unique_tid as $new_tid) {
      $node->field_category['und'][$i]['target_id'] = $new_tid;
      $i++;
    }
  }
  if(!empty($node->field_category_activity)){
    $cats = array();
    foreach ($node->field_category_activity['und'] as $tid) {
      $cats[] = $tid['target_id'];
    }
    $unique_tid = array_unique($cats);
    unset($node->field_category_activity);
    $i=0;
    foreach ($unique_tid as $new_tid) {
      $node->field_category_activity['und'][$i]['target_id'] = $new_tid;
      $i++;
    }
  }
}
function ka_event_user_presave(&$edit, $account, $category) {
  if (isset($account->is_new) && !empty($account->is_new)) {
    $role = user_role_load_by_name('organizer');
    $edit['roles'] = array($role->rid => 1);
  }
}
