<?php
/*
* NB! Replace FILE_EXISTS_REPLACE with rename after going live.
*/
module_load_include('inc', 'ka_event', 'includes/ka_event.time_select');
module_load_include('inc', 'ka_event', 'includes/ka_event.create_node');
module_load_include('inc', 'ka_event', 'includes/ka_event.elements');
module_load_include('inc', 'ka_event', 'includes/ka_event.summary');
module_load_include('inc', 'ka_event', 'includes/ka_event.validate');

function ka_event_forms($form_id, $args) {
  $forms = array();
  if (strpos($form_id, 'ka_event_edit_form_') === 0) {
    $forms[$form_id] = array('callback' => 'ka_event_edit_form',
                             'callback arguments' => array_values($args));
  }
  
  return $forms;
}

function ka_event_form($form, &$form_state, $node = NULL, $type = NULL) {
  global $user;
  $language_list = language_list();

  $form['#tree'] = TRUE;
  
  $form['#prefix'] = '<div id="add-form">';
  $form['#suffix'] = '</div>';
  
  // Adding sub-event to parent event node.
  if(!empty($_GET['parent_id'])) {
    if(is_numeric($_GET['parent_id']) && is_int((int) $_GET['parent_id'])) {
      $parent_id = $_GET['parent_id'];
      $parent_node = node_load($parent_id);
      
      // Only allow adding sub-events to your own non-anon events.
      if($parent_node && user_is_logged_in() 
      && ($user->uid === $parent_node->uid) // User & event owner are same
      || in_array($user->uid, ka_user_get_shared_accounts_list())) { // Event is shared to the user
        if(!isset($node) || (isset($node) && $node->nid !== $parent_node->nid)) { // Make sure the current node is not the parent.
          if($parent_node->type === 'event' && $type === 'event') { // Both msut be event nodes
            $form_state['storage']['parent_node'] = $parent_node->nid;
          }
        }
      }
    }
  }

  // Initialize the node if one was created during preview.
  if(!isset($node) && !empty($form_state['storage']['existing_node'])) {
    $node = $form_state['storage']['existing_node'];
  }
  
  if(!empty($type)) {
    $form['#node_type'] = $type;
  } else if(!empty($node)) {
    $form['#node_type'] = $node->type;
  }
  
  if($node && !isset($form_state['step'])) { // Run only once
    ka_set_event_form_values($node, $form_state);
    $form_state['step'] = 6;
    $form_state['storage']['existing_node'] = (array) $node;
  }
  // Event specific
  if(empty($form_state['event_qty'])) {
    $form_state['event_qty'] = 1;
  }
  
  // Activity specific
  if($form['#node_type'] === 'activity') {
    if(isset($form_state['triggering_element']['#action']) && $form_state['triggering_element']['#action'] === 'add_group') {
      
    } else if($node && !empty($form_state['storage']['step_3']['groups'])) {
      $form_state['groups_qty'] = count($form_state['storage']['step_3']['groups']);
    } else if(empty($form_state['groups_qty'])) {
      $form_state['groups_qty'] = 1;
    }

    for($i = 0; $i < $form_state['groups_qty']; $i++) {
      // If editing existing node, count the number of dates, use default otherwise.
      //dpm($form_state['storage']);
      if(isset($form_state['storage']['step_3']['groups'][$i]) 
        && !empty($form_state['storage']['step_3']['groups'][$i]['schedule']['type'])
        && $form_state['storage']['step_3']['groups'][$i]['schedule']['type'] === 'fixed'
        && !isset($form_state['triggering_element']['#action'])
        ) {
          // Remove text labels from array before counting
          if(isset($form_state['storage']['step_3']['groups'][$i]['schedule']['fixed'])) {
            $fixed_group_dates = array_filter($form_state['storage']['step_3']['groups'][$i]['schedule']['fixed'], 'only_array_elements');
            $form_state['activity_date_qty'][$i] = count($fixed_group_dates);
          } else {
            $form_state['activity_date_qty'][$i] = 1;
          }
      } else if(empty($form_state['activity_date_qty'][$i])) {
        $form_state['activity_date_qty'][$i] = 1;
      }
    }

    // Set the first group as active if this is not the summary view.
    if(!$node && !isset($form_state['active_group'])) {
      $form_state['active_group'] = 0;
    }
  }
  
  $form_state['step'] = isset($form_state['step']) ? $form_state['step'] : 1;
  $step = $form_state['step'];

  if($step < 6) {
    $form['continue'] = array(
      '#theme' => 'next_step_continue',
      '#type' => 'container',
    );
    
    $form['continue']['save_and_continue'] = array(
      '#type' => 'submit',
      '#value' => ka_t('Continue'),
      '#submit' => array('ka_change_step'),
      '#ajax' => array(
        'callback' => 'next_step_callback',
        'wrapper' => 'add-form',
        'method' => 'replace',
      ),
      '#name' => 'save_and_continue',
      '#attributes' => array(
        'class' => array('btn-empty'),
      ),
    );
    
    $form['continue']['cancel'] = array(
      '#value' => ka_t('Cancel'),
      '#type' => 'submit',
      '#submit' => array('ka_empty_fields'),
      '#attributes' => array(
        'style' => array('display:none;'),      
        'class' => array(
          'cancel-button',
        ),
        'data-id' => 'cancel-button',
      ),
      '#ajax' => array(
        'callback' => 'cancel_callback',
        'wrapper' => 'add-form',
        'method' => 'replace',
      ),
    );
  
  }
  
  // Form elements
  switch($step) {
    case 1:
      if($form['#node_type'] === 'event') {
        introduction_elements($form, $form_state);
      } else if($form['#node_type'] === 'activity') {
        event_type_elements($form, $form_state, $step);
      }
      break;
    case 2:
      if($form['#node_type'] === 'event') {
        event_type_elements($form, $form_state);
      } else if($form['#node_type']=== 'activity') {
        introduction_elements($form, $form_state);
      }
      break;
    case 3:
      if($form['#node_type'] === 'event') {
        event_date_location_elements($form, $form_state);
      } else if($form['#node_type']=== 'activity') {
        activity_groups_elements($form, $form_state);
      }
      break;
    case 4:
      pictures_videos_elements($form, $form_state);
      break;
    case 5:
      event_organizer_elements($form, $form_state);
      break;
    case 6:
      publish_options_elements($form, $form_state, $node);
      break;
  }
  
  // Introduction summary
  if($step !== 1 && !empty($form_state['storage']['step_1'])) {
    if($form['#node_type'] === 'event') {
      introduction_summary($form, $form_state, 1);
    } else {
      category_summary($form, $form_state, 1);
    }
  }
  
  // Category summary
  if($step !== 2 && !empty($form_state['storage']['step_2'])) {
    if($form['#node_type'] === 'event') {
      category_summary($form, $form_state, 2);
    } else {
      introduction_summary($form, $form_state, 2);
    }
  }
  
  if($step !== 3 && !empty($form_state['storage']['step_3'])) {
    if($form['#node_type'] === 'event') {
      event_ticket_summary($form, $form_state, 3);
    } else  {
      activity_groups_summary($form, $form_state, 3);
    }
  }

  if($step !== 4 && isset($form_state['storage']['step_4'])) {
    
    gallery_summary($form, $form_state, 4);
  }

  if($step !== 5 && !empty($form_state['storage']['step_5'])) {
    organizer_summary($form, $form_state, 5);
  }
  
  // Helper JS file
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'ka_event') . '/form.js',
    //drupal_get_path('theme', 'bootstrap') . '/form.js',
  );
  
  $form['#attached']['js'][] = array(
    'data' => array('ka_event_form' => ka_get_js_settings($form)),
    'type' => 'setting',
  );
  
  return $form;
}

function ka_event_form_validate($form, &$form_state, $edit = NULL) {
  $triggering_element = isset($form_state['triggering_element']['#name']) ? $form_state['triggering_element']['#name'] : NULL;
  if($triggering_element === 'save_and_continue' || $edit) {
    $step = $edit ? 'all' : $form_state['step'];
    $source = $edit ? 'storage' : 'values';
    switch($step) {
      case 1:
      case 'all':
        $validation_step = 1;
        if($form['#node_type'] === 'event') {
          introduction_validaton($form_state, $source, $validation_step);
        } else {
          category_validation($form_state, $source, $validation_step);
        }
        if($step !== 'all') break;
      case 2:
        $validation_step = 2;
        if($form['#node_type'] === 'event') {
          category_validation($form_state, $source, $validation_step);
        } else {
          introduction_validaton($form_state, $source, $validation_step);
        }
        if($step !== 'all') break;
      case 3:
        if($form['#node_type'] === 'event') {
          event_validation($form_state, $source);
        } else {
          activity_validation($form, $form_state, $source);
        }
        if($step !== 'all') break;
      case 4:
        gallery_validation($form_state, $source);
        if($step !== 'all') break;
      case 5:
        organizer_validation($form_state, $source);
        if($step !== 'all') break;
    }
  }
}

function ka_event_form_submit($form, &$form_state) {
  global $user;
  $form_data = array_merge($form_state['storage'], array('node_type' => $form['#node_type']));
  
  if($form_state['triggering_element']['#name'] === 'publish_later') {
    $publish_type = 'publish_later';
    
    $form_data['publish_later'] = array(
      'date' => $form_state['values']['publish_options']['publish_date'],
      'time' => $form_state['values']['publish_options']['publish_time']
    );
  } else {
    $publish_type = 'publish';
  }
  
  ka_create_node($form_data, $user, $publish_type);
  
  if(isset($form_state['preview_generated'])){
    $node = node_load($form_state['preview_generated']);
    node_delete($node->nid);
  }
  
  if(user_is_logged_in()) {
    $form_state['redirect'] = 'organiser';
  } else {
    $form_state['redirect'] = '<front>';
  }
  
}

function ka_change_step($form, &$form_state) {
  $step = $form_state['step'];
  // Save step input
  if(isset($form_state['values']["step_$step"])) {
    $form_state['storage']["step_$step"] = $form_state['values']["step_$step"];
  }
  
  // Either "Save and continue" or "Change" action.
  $triggering_element = $form_state['triggering_element']['#name'];
  if($triggering_element === 'save_and_continue') {
    $form_state['step']++;
  } else if($triggering_element === 'change') {
    $change_step = $form_state['triggering_element']['#value'];
    $form_state['step'] = $change_step;
  }
  $form_state['rebuild'] = TRUE;
} 

function ka_empty_fields($form, &$form_state) { 
  $step = $form_state['step'];
  
  if(isset($form_state['values']["step_$step"])) {
    $form_state['values']["step_$step"] = NULL;
  }
  
  if(isset($form_state['storage']["step_$step"])) {
    $form_state['storage']["step_$step"] = NULL;
  }
  if(isset($form_state['input']["step_$step"])) {
    $form_state['input']["step_$step"] = NULL;
  }
  
  // Reset event quantity
  if(!empty($form_state['event_qty'])) {
    $form_state['event_qty'] = 1;
  }
  
  // Reset group quantity
  if(!empty($form_state['groups_qty'])) {
    $form_state['groups_qty'] = 1;
    $form_state['active_group'] = NULL;
  }
  
  $form_state['rebuild'] = TRUE;
}

function save_draft($form, &$form_state) {
  global $user;
  if(user_is_logged_in()) {
    $form_state['redirect'] = 'organiser';
  } else {
    $form_state['redirect'] = '<front>';
  }
  $form_data = array_merge($form_state['storage'], array('node_type' => $form['#node_type']));
  ka_create_node($form_data, $user, 'draft');
}

function ka_delete_node($form, &$form_state) {
  $nid = $form_state['triggering_element']['#value'];
  $node = node_load($nid);
  $node_type = node_type_get_name($node);
  node_delete($nid);
  drupal_set_message(ka_t('@content_type deleted!', array('@content_type' => $node_type)));
  if(user_is_logged_in()) {
    $form_state['redirect'] = 'organiser';
  } else {
    $form_state['redirect'] = '<front>';
  }
}

function ka_generate_preview($form, &$form_state) {
  global $user;
  global $base_url;
  global $language;
    
  form_load_include($form_state, 'inc', 'node', 'node.pages');
  
  $form_data = array_merge($form_state['storage'], array('node_type' => $form['#node_type']));
  
  if(!empty($form_state['storage']['preview_generated']['node'])) {
    $form_data['preview_generated']['node'] = $form_state['storage']['preview_generated']['node'];
  }
  
  $node = ka_create_node($form_data, $user, 'preview');
  
  $path = $base_url . url('node/' . $node->nid) . '?lang=' . $language->language;
  
  $form_state['preview'] = $path;
  //$form_state['rebuild'] = TRUE;
  
  $form_state['storage']['preview_generated'] = array('nid' => $node->nid, 'path' => $path, 'node' => $node);
}

function add_remove_language_fields_introduction($form, &$form_state) {
  $step = $form['#node_type'] === 'event' ? 'step_1' : 'step_2';
  return $form[$step];
}

// Multigroup activities
function add_activity_group($form, &$form_state) {
  $form_state['active_group'] = $form_state['groups_qty'];
  $form_state['groups_qty']++;
  $form_state['storage']['step_3'] = $form_state['values']['step_3'];
  
  $form_state['storage']['step_3']['groups'][$form_state['groups_qty'] - 1] = array(); // Fix for 

  // Validate each group
  activity_validation($form, $form_state, 'storage');
  $form_state['rebuild'] = TRUE;
}

function set_active_group($form, &$form_state) {
  $group_nr = $form_state['triggering_element']['#group_nr'];
  $form_state['active_group'] = $group_nr;
  $form_state['rebuild'] = TRUE;
  
  $form_state['storage']['step_3'] = $form_state['values']['step_3'];
  // Workaround for showing errors on the lowest level.
  activity_validation($form, $form_state, 'storage');
  $GLOBALS['errors']['step_3'] = isset($form_state['storage']['step_3']['errors']) ? $form_state['storage']['step_3']['errors'] : 'test';
}

function update_group_count($form, &$form_state) {
  return $form['step_3'];
}

function add_more_group_dates($form, &$form_state) {
  $group_nr = !empty($form_state['triggering_element']['#parents'][2]) ? 
    $form_state['triggering_element']['#parents'][2] : 0;
  
  $form_state['activity_date_qty'][$group_nr]++;

  $form_state['values'] = $form_state['input']; // Important!
  
  $form_state['rebuild'] = TRUE;
}

function remove_group($form, &$form_state) {
  $group_id = $form_state['triggering_element']['#group_id'];
  $form_state['storage']['step_3']['groups'][$group_id]['remove_group_confirmed'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function remove_group_date($form, &$form_state) {
  $group_nr = !empty($form_state['triggering_element']['#parents'][2]) ? 
    $form_state['triggering_element']['#parents'][2] : 0;

  if($form_state['triggering_element']['#action'] === 'remove_group_date') {
    $group_nr = $form_state['triggering_element']['#parents'][2];
    
    $remove_id = $form_state['triggering_element']['row_id']['#value'];
    
    unset($form_state['values']['step_3']['groups'][$group_nr]['schedule']['fixed'][$remove_id]);
    $form_state['values']['step_3']['groups'][$group_nr]['schedule']['fixed'] = 
      array_values($form_state['values']['step_3']['groups'][$group_nr]['schedule']['fixed']);
    unset($form_state['storage']['step_3']['groups'][$group_nr]['schedule']['fixed'][$remove_id]);
    $form_state['storage']['step_3']['groups'][$group_nr]['schedule']['fixed'] = 
      array_values($form_state['storage']['step_3']['groups'][$group_nr]['schedule']['fixed']);
    unset($form_state['input']['step_3']['groups'][$group_nr]['schedule']['fixed'][$remove_id]);
    $form_state['input']['step_3']['groups'][$group_nr]['schedule']['fixed'] = 
      array_values($form_state['storage']['step_3']['groups'][$group_nr]['schedule']['fixed']);
      
    $form_state['activity_date_qty'][$group_nr]--;
  }
  $form_state['rebuild'] = TRUE;
}

function update_event_type_fields($form, &$form_state) {
  return $form['step_2'];
}

function update_category_fields($form, &$form_state) {
  if($form['#node_type'] === 'event') {
    return $form['step_2']['category'];
  } else {
    return $form['step_1']['category'];
  }
}

function update_event_ticket_availability($form, &$form_state) {
  return $form['step_3']['event_tickets']['availability'];
}

function update_event_date_fields($form, &$form_state) {
  $form_state['event_qty']++;
  $form_state['rebuild'] = TRUE;
}

function remove_event_date_field($form, &$form_state) {
  $form_state['event_qty']--;
  //$form_state['input'] = array();
  $form_state['rebuild'] = TRUE;
}

function update_event_date_fields_callback($form, &$form_state) {
  return $form['step_3']['event_date_location'];
}

// For add video fields in other languages
function update_event_video_fields($form, &$form_state) {
  $form_state['video_other_languages'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function update_event_video_fields_callback($form, &$form_state) {
  return $form['step_4']['field_video'];
}

function update_event_ticket_fields($form, &$form_state) {
  return $form['step_3']['event_tickets']['price'];
}

function set_ticket_fields($form, &$form_state) {
  if(isset($form_state['storage']['step_3']['event_tickets'])) {
    $form_state['storage']['step_3']['event_tickets'] = $form_state['input']['step_3']['event_tickets'];
  }
  $form_state['rebuild'] = TRUE;
}

function add_multilungual_additional_information($form, &$form_state) {
  $form_state['additional_multilang'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function update_event_ticket_fields_additional($form, &$form_state) {
  return $form['step_3']['event_tickets']['additional'];
}

function update_groups($form, &$form_state) {
  // Check which group to update
  $group_nr = !empty($form_state['triggering_element']['#parents'][2]) ? 
  $form_state['triggering_element']['#parents'][2] : 0;
  // Save step input
  if(isset($form_state['values']["step_3"])) {
    $form_state['storage']["step_3"] = $form_state['values']["step_3"];
  }
  
  return $form['step_3']['groups'][$group_nr]['schedule'];
}

function update_group_price_options($form, &$form_state) {
  $group_nr = !empty($form_state['triggering_element']['#parents'][2]) ? 
  $form_state['triggering_element']['#parents'][2] : 0;

  return $form['step_3']['groups'][$group_nr]['price'];
}

function upload_image_callback($form, &$form_state) {
  return $form['step_4']['gallery'];
}

function registration_types_callback($form, &$form_state) {
  return $form['step_5']['registration'];
}

function generate_preview_callback($form, &$form_state) {
  return $form['preview'];
}

function cancel_callback($form, &$form_state) {
  return $form;
}

function validate_image($file) {
  // Check file extension.
  $validate_extensions = file_validate_extensions($file, 'png jpg jpeg');
  if(!empty($validate_extensions)) {
    drupal_set_message(implode($validate_extensions), 'warning');
    return FALSE;
  }
  
  // Check file is image
  $validate_size = file_validate_is_image($file);
  if(!empty($validate_size)) {
    drupal_set_message(implode($validate_size), 'warning');
    return FALSE;
  }
  
  // Check file name length
  $validate_length = file_validate_name_length($file);
  if(!empty($validate_length)) {
    drupal_set_message(implode($validate_length), 'warning');
    return FALSE;
  }
  
  // Check file size (2 Megabytes)
  $validate_size = file_validate_size($file, 1024*1024*20);
  if(!empty($validate_size)) {
    drupal_set_message(implode($validate_size), 'warning');
    return FALSE;
  }
  return TRUE; // All validations passed, file is valid.
}

function upload_image($form, &$form_state) {
    $upload_type = !empty($form_state['values']['step_4']['gallery']['upload_type']) ?
    $form_state['values']['step_4']['gallery']['upload_type'] : NULL;
    
    switch($upload_type) {
      case 'upload':
        try {
          $file = file_save_upload('step_4', array('file_validate_extensions' => array('png jpg jpeg')), "public://", FILE_EXISTS_RENAME);
        } catch (Exception $e) {
          
        }
        
        break;
      case 'link':
        $link = $form_state['values']['step_4']['gallery']['gallery_url'];
        $url_parsed = parse_url($link);
        
        if(!empty($link) && isset($url_parsed['path'])) {
          try {
            $file = system_retrieve_file($link, NULL, TRUE, FILE_EXISTS_RENAME);
          } catch (Exception $e) {

          }
        }
        break;
    }

    if (!empty($file)) { // File has been download
      if(strpos($file->filemime, 'image') !== FALSE && validate_image($file)) { // File is valid
        $file->status = 0; // Set file to temporary, we should set it to permament on whole form submission.
        file_save($file);
        drupal_set_message(ka_t('File uploaded.'));
      
        $form_state['values']['step_4']['gallery']['image_preview']['images'][$file->fid] = $file;
        $form_state['rebuild'] = TRUE;
      } else {
        file_delete($file);
        drupal_set_message(ka_t('No file uploaded.'), 'warning');
      }
    } else {
      drupal_set_message(ka_t('No file uploaded.'), 'warning');
    }
}

function set_is_full($form, &$form_state) {
  $group_id = $form_state['triggering_element']['#group_id'];
  $value = $form_state['triggering_element']['#value'];
  $form_state['storage']['step_3']['groups'][$group_id]['is_full'] = $value;
  $form_state['rebuild'] = TRUE;
}

function is_full_callback($form, &$form_state) {
  return $form['summary'][3];
}

function close_preview_callback($form, &$form_state) {
  return $form;
}

function remove_image($form, &$form_state) {
  $element = $form_state['triggering_element'];
  $fid = $element['fid']['#value'];

  // Delete file
  if(isset($form_state['values']['step_4']['gallery']['image_preview']['images'][$fid])) {
    $file = $form_state['values']['step_4']['gallery']['image_preview']['images'][$fid];
    $file = is_array($file) ? reset($file) : $file;
    file_delete($file);
    unset($form_state['values']['step_4']['gallery']['image_preview']['images'][$fid]);
    // Deleted image was set as the main image
    if($form_state['values']['step_4']['gallery']['image_preview']['main_image'] === $fid) {
      $form_state['values']['step_4']['gallery']['image_preview']['main_image'] = NULL;
    }
  }
  
  $form_state['rebuild'] = TRUE;
}

function next_step_callback($form, &$form_state) {
  return $form;
}

function update_activity_type($form, &$form_state) {
  return $form['step_3'];
}

function ka_get_js_settings($form) {
  $settings = array();
  
  $settings['charcount_label'] = ka_t('Characters');
  
  $settings['title_char_limit'] = variable_get_value('title_char_limit');
  $settings['short_description_char_limit'] = variable_get_value('short_description_char_limit');
  $settings['main_text_char_limit'] = variable_get_value('main_text_char_limit');
  
  return $settings;
}

/**
 * Check the form state for errors, return FALSE if any errors are found, TRUE otherwise.
 */
function ka_valid_form($form_array) {
  foreach($form_array as $step) {
    if(!empty($step['errors'])) {
      return FALSE;
    }
  }
  return TRUE;
}

function only_array_elements($a) {
  return is_array($a);
}

function ka_event_preview_form($form, &$form_state, $node = NULL){
  $form['#node'] = $node;
  $form['close_preview'] = array(
    '#type' => 'submit',
    '#submit_type' => 'close',
    '#attributes' => array(
      'data-id' => 'preview-delete',
      'style' => 'display:none',
    ),
    '#value' => 'close',
    '#ajax' => array(
      'callback' => 'close_preview_callback',
      'wrapper' => 'preview-form',
      'method' => 'replace',
    )
  );
  
  /*
  $form['update_status'] = array(
    '#type' => 'submit',
    '#submit_type' => 'publish',
    '#attributes' => array(
      'data-id' => 'preview-change-status',
      'style' => 'display:none',
    ),
    '#value' => 'publish',
    '#ajax' => array(
      'callback' => 'close_preview_callback',
      'wrapper' => 'preview-form',
      'method' => 'replace',
    ),
  );
  */
  
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'ka_event') . '/form.js',
  );
  
  //opener.document.getElementById('edit-publish-options-publish-now').click();
  if(isset($form_state['close_preview'])) {
    $form['close_preview']['#prefix'] = "<script>window.close()</script>";
  }
  if(isset($form_state['publish_now'])){
    $form['update_status']['#prefix'] = "<script>opener.document.getElementById('edit-publish-options-publish-now').click();</script>";
  }
  
  return $form; 
}
function ka_event_preview_form_submit($form, &$form_state){
  $submit_type = $form_state['triggering_element']['#submit_type'];


  switch($submit_type) {
    case 'publish':
      $form_state['close_preview'] = TRUE;
      $form_state['publish_now'] = TRUE;
      $form_state['rebuild'] = TRUE;

      break;
    case 'close':
      $form_state['close_preview'] = TRUE;
      $form_state['rebuild'] = TRUE;
      break;
  }
  
}
