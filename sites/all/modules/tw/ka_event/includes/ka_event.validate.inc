<?php

/* VALIDATION FUNCTIONS */

function introduction_validaton(&$form_state, $source, $step) {
  /* Language select */
  // At least one language must be selected.
  $languages_selected = FALSE;
  $node_type = !empty($form_state['complete form']['#node_type']) ? $form_state['complete form']['#node_type'] : 'event';
  foreach($form_state[$source]["step_$step"]['introduction_languages'] as $language_selection) {
    if(!empty($language_selection)) $languages_selected = TRUE;
  }
  if(!$languages_selected) {
    $form_state[$source]["step_$step"]['errors']['introduction_languages'] = ka_t('At least one language must be selected.');
  }
  foreach($form_state[$source]["step_$step"] as $lang => $introduction) {
    if($lang === 'introduction_languages') continue;
    
    /* Name */
    if(empty($introduction['field_title']) && ($lang === 'et' || $node_type !== 'activity')) { // Validate multilang only for events.
      $form_state[$source]["step_$step"]['errors'][$lang]['field_title'][] = ka_t('Blank!');
    }
    
    $title_field = field_info_field('title_field');
    if(!empty($title_field['settings']['max_length'])) {
      $title_field_char_limit = 100;
    }
    
    if(!empty($introduction['field_title']) && ($lang === 'et' || $node_type !== 'activity')) { // Validate multilang only for events.
      if(strlen(utf8_decode($introduction['field_title'])) > (!empty($title_field_char_limit) ? $title_field_char_limit : 100)) { // Title
        $form_state[$source]["step_$step"]['errors'][$lang]['field_title'][] = ka_t('Exceeds the character limit.');
      }
    }
    
    /* Short description */
    // Check if filled
    if(empty($introduction['field_summary'])) {
      $form_state[$source]["step_$step"]['errors'][$lang]['field_summary'][] = ka_t('Blank!');
    }
    
    // Check textarea length.
    $short_description_char_limit = variable_get_value('short_description_char_limit');
    
    if(!empty($introduction['field_summary'])) {
      if(strlen(utf8_decode($introduction['field_summary'])) > (!empty($short_description_char_limit) ? $short_description_char_limit : 400)) { // Summary
        $form_state[$source]["step_$step"]['errors'][$lang]['field_summary'][] = ka_t('Exceeds the character limit.');
      }
    }
    
    /* Main description */
    // Check if filled
    if(empty($introduction['field_body'])) {
      $form_state[$source]["step_$step"]['errors'][$lang]['field_body'][] = ka_t('Blank!');
    }
    
    // Check textarea length.
    $main_text_char_limit = variable_get_value('main_text_char_limit');
    
    if(!empty($introduction['field_body'])) {
      if(strlen(utf8_decode($introduction['field_body'])) > (!empty($main_text_char_limit) ? $main_text_char_limit : 1500)) { // Body
        $form_state[$source]["step_$step"]['errors'][$lang]['field_body'][] = ka_t('Exceeds the character limit.');
      }
    }
    
    /* Additional information */
    // Check if filled
    if(empty($introduction['field_additional_info_link'])) {
      $form_state[$source]["step_$step"]['errors'][$lang]['field_additional_info_link'][] = ka_t('Blank!');
    }
    // Validate additional info link as proper URL.
    if(!empty($introduction['field_additional_info_link']) && !link_validate_url($introduction['field_additional_info_link'])) { // URL
      $form_state[$source]["step_$step"]['errors'][$lang]['field_additional_info_link'][] = ka_t('Invalid URL address.');
    }
  }
}

function category_validation(&$form_state, $source, $step) {
  // Category
  $category = $form_state[$source]["step_$step"]['category']['field_category'];
  // Category must be selected
  if(empty($category)) {
    $form_state[$source]["step_$step"]['errors']['field_category'][] = ka_t('Not selected!');
  }
  
  // Sub-category
  if(!empty($category)) {
    $sub_category_options = taxonomy_get_children($category);
    
    $sub_category = $form_state[$source]["step_$step"]['category']['field_subcategory'];
    // If there are options for sub-category, it must be selected.
    if(!empty($category) && empty($sub_category) && count($sub_category_options) > 1) {
      $form_state[$source]["step_$step"]['errors']['field_subcategory'][] = ka_t('Not selected!');
    }
  }
  
  
}

function event_validation(&$form_state, $source) {
  // Date & time
  if(!empty($form_state[$source]['step_3']['event_date_location'])) {
    $date_locations = $form_state[$source]['step_3']['event_date_location'];
    foreach($date_locations as $delta => $date_location) {
    if(!is_numeric($delta)) continue;
    $date_start = explode(".", $date_location['date']['start']['field_event_date_from']);
    $date_end = explode(".", $date_location['date']['end']['field_event_date_to']);
    /* From date */
    // Check from date fields format
    $valid_start_date = checkdate((int) $date_start[1], (int) $date_start[0], (int) $date_start[2]);

    if(count($date_start) !== 3 || !$valid_start_date) {
      $form_state[$source]['step_3']['errors']['event_date_location'][$delta]['date']['start']['field_event_date_from'][] = ka_t('Invalid value.');
    }
    /* To date */
    // Check to date fields format
    $valid_end_date = checkdate((int) $date_end[1], (int) $date_end[0], (int) $date_end[2]);
    if(count($date_end) !== 3 || !$valid_end_date) {
      $form_state[$source]['step_3']['errors']['event_date_location'][$delta]['date']['end']['field_event_date_to'][] = ka_t('Invalid value.');
    }
    $date_time_from = $date_location['date']['start']['field_event_date_from'] . ' ' . $date_location['date']['start']['field_event_time_from'];
    
    $time_part = empty($date_location['date']['end']['field_event_time_to']) 
    || $date_location['date']['end']['field_event_time_to'] === 'unset' ? '23:55' : $date_location['date']['end']['field_event_time_to'];

    $date_time_to = $date_location['date']['end']['field_event_date_to'] . ' ' . $time_part;
    // Make sure the to date is higher than from date.
    if($valid_start_date && $valid_end_date && strtotime($date_time_from) >= strtotime($date_time_to)) {
      $form_state[$source]['step_3']['errors']['event_date_location'][$delta]['date']['end']['field_event_date_to'][] = ka_t('End date must be after the start date.');
    }
    /* Address */
    // Check if empty
    if(empty($form_state[$source]['step_3']['event_date_location'][$delta]['location']['field_event_address'])) {
      $form_state[$source]['step_3']['errors']['event_date_location'][$delta]['location']['field_event_address'][] = ka_t('Blank!');
    }
    // Check length
    if(strlen($form_state[$source]['step_3']['event_date_location'][$delta]['location']['field_event_address']) > 128) { // Summary
      $form_state[$source]['step_3']['errors']['event_date_location'][$delta]['location']['field_event_address'][] = ka_t('Exceeds the character limit.');
    }
    /* Location */
    // Check length
    if(strlen($form_state[$source]['step_3']['event_date_location'][$delta]['location']['field_event_location']) > 128) { // Summary
      $form_state[$source]['step_3']['errors']['event_date_location'][$delta]['location']['field_event_location'][] = ka_t('Exceeds the character limit.');
    }
  } 
    
  }
  // Ticket type & price
  $ticket = $form_state[$source]['step_3']['event_tickets']['price'];

  if($ticket['field_event_ticket_type'] === 'fixed') {
    /* Price */
    // Check if empty
    if(empty($ticket['field_event_ticket_price'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price'][] = ka_t('Blank!');
    }
    // Check if number
    if(!validate_is_number($ticket['field_event_ticket_price'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price'][] = ka_t('Must be a number.');
    }
  }
  if($ticket['field_event_ticket_type'] === 'between') {
    $price_from = $ticket['field_event_ticket_price_from'] !== "" ? floatval($ticket['field_event_ticket_price_from']) : FALSE;
    $price_to = $ticket['field_event_ticket_price_to'] !== "" ? floatval($ticket['field_event_ticket_price_to']) : FALSE;

    /* Price from */
    // Check if number
    if(!validate_is_number($ticket['field_event_ticket_price_from'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price_from'][] = ka_t('Must be a number.');
    }
    // Check if empty
    if($ticket['field_event_ticket_price_from'] === "") {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price_from'][] = ka_t('Blank!');
    }
    /* Price to */
    // Check if number
    if(!validate_is_number($ticket['field_event_ticket_price_to'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price_to'][] = ka_t('Must be a number.');
    }
    // Check if empty
    if($ticket['field_event_ticket_price_to'] === "") {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price_to'][] = ka_t('Blank!');
    }
    // Price to must be higher than price from.
    if(validate_is_number($price_from) 
      && validate_is_number($price_to)
      && $price_from >= $price_to) {
      $form_state[$source]['step_3']['errors']['event_tickets']['price']['field_event_ticket_price_to'][] = ka_t('Maximum price must be higher than minimum price!');
    }
  }
  // Ticket availability
  $availability = $form_state[$source]['step_3']['event_tickets']['availability'];
  if($availability['field_event_ticket_availability'] === 'link' && !empty($availability['field_event_ticket_link'])) { // Validate only if set
    // Check length
    if(strlen($availability['field_event_ticket_link']) > 255) {
      $form_state[$source]['step_3']['errors']['event_tickets']['availability']['field_event_ticket_link'][] = ka_t('Exceeds the character limit.');
    }
    // Check if empty
    if(empty($availability['field_event_ticket_link'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['availability']['field_event_ticket_link'][] = ka_t('Blank!');
    }
    // Validate URL
    if(!empty($availability['field_event_ticket_link']) && !link_validate_url($availability['field_event_ticket_link'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['availability']['field_event_ticket_link'][] = ka_t('Invalid URL address.');
    }
  }
  if($availability['field_event_ticket_availability'] === 'other' && !empty($availability['field_event_ticket_other'])) { // Validate only if set
    // Check length
    if(strlen($availability['field_event_ticket_other']) > 255) {
      $form_state[$source]['step_3']['errors']['event_tickets']['availability']['field_event_ticket_other'][] = ka_t('Exceeds the character limit.');
    }
    // Check if empty
    if(empty($availability['field_event_ticket_other'])) {
      $form_state[$source]['step_3']['errors']['event_tickets']['availability']['field_event_ticket_other'][] = ka_t('Blank!');
    }
  }
  // Ticket additional information
  if(!empty($form_state[$source]['step_3']['event_tickets']['additional']['languages'])) {
    $additional = $form_state[$source]['step_3']['event_tickets']['additional']['languages'];
    foreach($additional as $lang => $additional_text) {
      // Check length
      if(strlen($additional_text) > 128) {
        $form_state[$source]['step_3']['errors']['event_tickets']['additional']['languages'][$lang] = ka_t('Exceeds the character limit.');
      }
    }
  }
  
}

function activity_validation($form, &$form_state, $source = 'storage') {
  foreach($form_state[$source]['step_3']['groups'] as $key => $group) {
    if(!is_array($group) || 
      (!empty($group['remove_group_confirmed']) && $group['remove_group_confirmed'])) continue;
    /* Group name */
    if(empty($group['name'])) {
      $form_state[$source]['step_3']['errors']['groups'][$key]['name'][] = ka_t('Blank!');
    }
    if(strlen($group['name']) > 255) {
      $form_state[$source]['step_3']['errors']['groups'][$key]['name'][] = ka_t('Exceeds the character limit.');
    }
    if(!empty($group['name_t']['en'])) {
      if(strlen($group['name_t']['en']) > 255) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['name_t']['en'][] = ka_t('Exceeds the character limit.');
      }
    }
    if(!empty($group['name_t']['ru'])) {
      if(strlen($group['name_t']['ru']) > 255) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['name_t']['ru'][] = ka_t('Exceeds the character limit.');
      }
    }
    /* Age range */
    // One of the target groups must be set to require validation.
    if((!empty($group['age_groups']['children']) || !empty($group['age_groups']['adults'])) 
      && $group['age_range']['from'] != $group['age_range']['to']) { // Has been set
      // Must be int and between 0 and 100.
      if(!empty($group['age_range']['to']) &&
        (!filter_var($group['age_range']['to'], FILTER_VALIDATE_INT) || $group['age_range']['to'] < 0 || $group['age_range']['to'] > 100))
      {
        $form_state[$source]['step_3']['errors']['groups'][$key]['age'][] = ka_t('Invalid value.');
      }
      if(!empty($group['age_range']['from']) &&
        !filter_var($group['age_range']['from'], FILTER_VALIDATE_INT) || $group['age_range']['from'] < 0 || $group['age_range']['from'] > 100) 
      {
        $form_state[$source]['step_3']['errors']['groups'][$key]['age'][] = ka_t('Invalid value.');
      }
      // "To" must be higher than "from"
      if(is_numeric($group['age_range']['to']) && is_numeric($group['age_range']['from']) && $group['age_range']['to'] < $group['age_range']['from']) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['age'][] = ka_t('Maximum age must be higher than minimum age!');
      }
    }
    /* Price */
    // Check if not set to "free".
    if($group['price']['type'] === 'single_payment' || $group['price']['type'] === 'monthly_payment') {
      $price_type = $group['price']['type'];
      $price = $group['price'][$price_type];
      // Must be a number above 0
      if(empty($price) || !validate_is_number($price) || floatval($price) <= 0) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['price'][] = ka_t('Invalid value.');
      }
    }
    /* Schedule */
    if(!empty($group['activity_type']) && $group['activity_type'] === 'single_group') {
      if(!empty($group['schedule'])) {
        // Period
        $date_start = explode(".", $group['schedule']['date']['start']['field_event_date_from']);
        $date_end = explode(".", $group['schedule']['date']['end']['field_event_date_to']);
        
        $valid_start_date = count($date_start) === 3 ?
          checkdate((int) $date_start[1], (int) $date_start[0], (int) $date_start[2]) : FALSE;
        $valid_end_date = count($date_end) === 3 ?
          checkdate((int) $date_end[1], (int) $date_end[0], (int) $date_end[2]) : FALSE;
        // Check period fields format
        if(!$valid_start_date) { // Invalid start date
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']
          ['date']['start']['field_event_date_from'][] = ka_t('Invalid date.');
        }
        if(!$valid_end_date) { // Invalid end date
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']
          ['date']['end']['field_event_date_to'][] = ka_t('Invalid date.');
        }
        // End date before start date
        if(strtotime($group['schedule']['date']['start']['field_event_date_from']) > strtotime($group['schedule']['date']['end']['field_event_date_to'])) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']
            ['date']['end']['field_event_date_to'][] = ka_t('End date must be after the start date.');
        }
        // Address must be set
        if(empty($group['schedule']['location']['field_event_address'])) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['location']['field_event_address'][] = ka_t('Address not set!');
        }
      }
    } else {
      if($group['schedule']['type'] === 'regular') {
      /* Regular */
      // Period
      $date_start = explode(".", $group['schedule']['regular']['period_from']);
      $date_end = explode(".", $group['schedule']['regular']['period_to']);
      
      $valid_start_date = count($date_start) === 3 ?
        checkdate((int) $date_start[1], (int) $date_start[0], (int) $date_start[2]) : FALSE;
      $valid_end_date = count($date_end) === 3 ?
        checkdate((int) $date_end[1], (int) $date_end[0], (int) $date_end[2]) : FALSE;
      // Check period fields format
      if(!$valid_start_date || !$valid_end_date) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['regular']['period'][] = ka_t('Invalid date.');
      } else if(strtotime($group['schedule']['regular']['period_from']) > strtotime($group['schedule']['regular']['period_to'])) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['regular']['period'][] = ka_t('End date must be after the start date.');
      }
      // Weekdays
      $weekdays_selected = FALSE;
      foreach($group['schedule']['regular']['weekdays'] as $day => $active) {
        if(empty($active)) continue; // Check only set dates
        $weekdays_selected = TRUE;
        $time_from = strtotime($group['schedule']['regular']['day'][$day]['time_from']);
        $time_to = strtotime($group['schedule']['regular']['day'][$day]['time_to']);

        // Compare the set times
        /*if($time_from >= $time_to) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['regular']['day'][$day]['time'][] = ka_t('End time must be after the start time.');
        }*/
        // Address must be set
        if(empty($group['schedule']['regular']['day'][$day]['address'])) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['regular']['day'][$day]['address'][] = ka_t('Address not set!');
        }
      }
      
      if(!$weekdays_selected) {
        $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['regular']['weekdays'] = ka_t('No weekdays selected!');
      }
      
      } else if($group['schedule']['type'] === 'fixed') {
      /* Fixed */
      foreach($group['schedule']['fixed'] as $s_key => $schedule) {
        if(!is_array($schedule)) continue;
        $date = explode(".", $schedule['date']);
        $valid_date = count($date) === 3 ? checkdate((int) $date[1], (int) $date[0], (int) $date[2]) : FALSE;
        // Check if valid date
        if(!$valid_date) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['fixed'][$s_key]['date'][] = ka_t('Invalid date.');
        }
        
        // Compare the set times
        $time_from = strtotime($schedule['time_from']);
        $time_to = strtotime($schedule['time_to']);
        /*if($time_from >= $time_to) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['fixed'][$s_key]['time'][] = ka_t('End time must be after the start time.');
        }*/
        // Address must be set
        if(empty($schedule['address'])) {
          $form_state[$source]['step_3']['errors']['groups'][$key]['schedule']['fixed'][$s_key]['address'][] = ka_t('Address not set!');
        }
      }
    }
    }
  }
}

function gallery_validation(&$form_state, $source) {
  // Video URL
  $videos = $form_state[$source]['step_4']['field_video'];
  foreach($videos as $lang => $video) {
    if($lang === 'add_video_other_languages') continue;
    if(!empty($video)) {
      $url = parse_url($video);
      $url = isset($url['host']) ? $url['host'] : $url['path'];

      if(strpos($url, 'youtube') !== FALSE || strpos($url, 'youtu.be') !== FALSE) { // Youtube video
        $handler = new MediaInternetYouTubeHandler($video);
      } else if(strpos('vimeo', $url) !== FALSE) { // Vimeo video
        $handler = new MediaInternetVimeoHandler($video);
      }
      if(isset($handler)) { // Handler matched
        try {
          $file = $handler->getFileObject(); // Try to fetch the video.
          if($file) {
            $form_state[$source]['step_4']['video_parsed'][$lang] = $file;
          }
        } catch (Exception $e) {
          $field_name = $form['step_4']['field_video'][$lang]['#name'];
          $field_title = $form['step_4']['field_video'][$lang]['#title'];
          form_set_error($field_name, ka_t('!title field video could not be parsed. 
          Make sure the link leads to a valid video.', array('!title' => $field_title)));
        }
      } else { // Unknown URL.
        $field_name = $form['step_4']['field_video'][$lang]['#name'];
        $field_title = $form['step_4']['field_video'][$lang]['#title'];
        form_set_error($field_name, ka_t('!title field video could not be parsed. 
        Make sure the link leads to a valid video.', array('!title' => $field_title)));
      }
    }
  }
  // Images
  if(empty($form_state[$source]['step_4']['gallery']['image_preview']['images'])) {
    $form_state[$source]['step_4']['errors']['gallery'][] = ka_t('No photos added!');
  }
}

function organizer_validation(&$form_state, $source) {
  // Organizer
  ka_validate_string(
    $form_state[$source]['step_5']['left']['field_organizer_name'],
    $form_state[$source]['step_5']['errors']['left']['field_organizer_name']
  );
  
  // Homepage
  ka_validate_url(
    $form_state[$source]['step_5']['left']['field_organizer_www'],
    $form_state[$source]['step_5']['errors']['left']['field_organizer_www'],
    FALSE
  );
  
  // E-mail
  /*ka_validate_email(
    $form_state[$source]['step_5']['right']['field_organizer_email'],
    $form_state[$source]['step_5']['errors']['right']['field_organizer_email']
  );*/
  
  // Phone
  /*ka_validate_string(
    $form_state[$source]['step_5']['right']['field_organizer_phone'],
    $form_state[$source]['step_5']['errors']['right']['field_organizer_phone']
  );*/

  // Activity registration information
  if(!empty($form_state[$source]['step_5']['registration'])) {
    if(isset($form_state[$source]['step_5']['registration']['field_registration_types']['local']) &&
      $form_state[$source]['step_5']['registration']['field_registration_types']['local'] === 'local') {
      ka_validate_string(
        $form_state[$source]['step_5']['registration']['field_registration_local'],
        $form_state[$source]['step_5']['errors']['registration']['field_registration_local']
      );
    }
    
    if(isset($form_state[$source]['step_5']['registration']['field_registration_types']['email']) &&
      $form_state[$source]['step_5']['registration']['field_registration_types']['email'] === 'email') {
      ka_validate_email(
        $form_state[$source]['step_5']['registration']['field_registration_email'],
        $form_state[$source]['step_5']['errors']['registration']['field_registration_email']
      );
    }
    if(isset($form_state[$source]['step_5']['registration']['field_registration_types']['internet']) &&
      $form_state[$source]['step_5']['registration']['field_registration_types']['internet'] === 'internet') {
      ka_validate_url(
        $form_state[$source]['step_5']['registration']['field_registration_internet'],
        $form_state[$source]['step_5']['errors']['registration']['field_registration_internet']
      );
    }
    if(isset($form_state[$source]['step_5']['registration']['field_registration_types']['phone']) &&
      $form_state[$source]['step_5']['registration']['field_registration_types']['phone'] === 'phone') {
      ka_validate_string(
        $form_state[$source]['step_5']['registration']['field_registration_phone'],
        $form_state[$source]['step_5']['errors']['registration']['field_registration_phone']
      );
    }
  }
  // Remove blank fields
  $form_state[$source]['step_5'] = array_remove_empty($form_state[$source]['step_5']);
}

function ka_validate_url($target, &$error_array, $required = TRUE) {
  if(empty($target) && $required) { // Check if empty
    $error_array[] = ka_t('Blank!');
  }
  if(!empty($target) && !link_validate_url($target)) { // Validate URL structure
    $error_array[] = ka_t('Invalid URL address.');
  }
}

function ka_validate_string($target, &$error_array) {
  if(empty($target)) { // Check if empty
    $error_array[] = ka_t('Blank!');
  }
}

function ka_validate_email($target, &$error_array) {
  if(empty($target)) { // Check if empty
    $error_array[] = ka_t('Blank!');
  }
  if(!empty($target) && !valid_email_address(trim($target))) { // Validate e-mail structure
    $error_array[] = ka_t('Invalid e-mail address.');
  }
}