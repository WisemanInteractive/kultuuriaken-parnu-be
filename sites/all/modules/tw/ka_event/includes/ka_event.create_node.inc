<?php

function ka_create_node($form_data, $user = NULL, $type = NULL) {
  global $language;
  if(!$user) $user = user_load(1);
  $node_type = $form_data['node_type'];
  
  if(isset($form_data['existing_node']) && $type !== 'preview') {
    $node = (object) $form_data['existing_node'];
  } else if(!empty($form_data['preview_generated']) && $type === 'preview') {
    $node = (object) $form_data['preview_generated']['node'];
  } else {
    $node = new stdClass();
    $node->uid = $user->uid;
    $node->type = $node_type;
  }

  $emw_node = entity_metadata_wrapper('node', $node);
  
  // The form data comes from an API
  if(!empty($form_data['field_api_provider']) && !empty($form_data['field_api_provider'])) {
    $type = 'api';
    $emw_node->field_api_provider = $form_data['field_api_provider'];
    $emw_node->field_api_id = $form_data['field_api_id'];
  }
  
  // Initialize the publish date if it hasn't been set before
  if(empty($emw_node->field_publish_date->value())) {
    $emw_node->field_publish_date = time();
  }
  
  if($type === 'draft') {
    
    foreach($form_data as $step => $data) {
      if(!empty($data['errors'])) {
        //unset($form_data[$step]); // Remove steps with errors in draft save.
      }
    }
  }
  
  // Set parent node
  if(!empty($form_data['parent_node'])) {
    $emw_node->field_event_roof_event = $form_data['parent_node'];
  }
  
  // Step 1 data
  // Data stored on different steps for activity & event.
  $step_1 = $node_type === 'event' ? $form_data['step_1'] : $form_data['step_2'];
  if(!empty($step_1)) {
    $translation_handler = entity_translation_get_handler('node', $emw_node->value());
    $existing_translations = $translation_handler->getTranslations();
    foreach($step_1['introduction_languages'] as $lang => $val) {
      //krumo($step_1);
      if($val !== 0 && !empty($step_1[$lang])) {
        // Set introduction texts.
        $introduction = $step_1[$lang];
        if($node_type === 'activity') {
          $emw_node->language($lang)->title_field = !empty($step_1['et']['field_title']) ? (string) $step_1['et']['field_title'] : variable_get_value('activity_name_placeholder'); // Title (Set to EST for all)
        } else {
          $emw_node->language($lang)->title_field = !empty($introduction['field_title']) ? (string) $introduction['field_title'] : NULL; // Title
          //$emw_node->language($lang)->title = !empty($introduction['field_title']) ? (string) $introduction['field_title'] : NULL; // Title
        }
        
        $emw_node->language($lang)->field_summary = (string) $introduction['field_summary']; // Summary
        $emw_node->language($lang)->field_body = (string) $introduction['field_body']; // Main body
        $emw_node->language($lang)->field_additional_info_link = array('url' => $introduction['field_additional_info_link']); // Additional info link
        
        // Source language has not been set or source language fields are empty.
        if(empty($emw_node->value()->language) || empty($step_1[$emw_node->value()->language])) {
          // Set base language
          $emw_node->language->set($lang);
          $emw_node->title = !empty($introduction['field_title']) ? $introduction['field_title'] : 'Huvitegevus';
        } else if($lang === $emw_node->value()->language) {
          // Update default title
          $emw_node->title = !empty($introduction['field_title']) ? $introduction['field_title'] : 'Huvitegevus';
        }
      } else { // Delete translations if they existed before
        $emw_node->language($lang)->title_field = ''; // Title
        $emw_node->language($lang)->field_summary = ''; // Summary
        $emw_node->language($lang)->field_body = ''; // Main body
        $emw_node->language($lang)->field_additional_info_link = array('url' => ''); // Additional info link
      }

      // Add translation
      if($lang !== $emw_node->value()->language && !empty($step_1[$lang])) {
        $translation = array(
          'translate' => 0,
          'status' => 1,
          'language' => $lang, // here is the language you're translating to
          'source' => $emw_node->value()->language, // here is the source language
        );
        
        $translation_handler->setTranslation($translation, $emw_node->value());
        // Create url alias for translation
        //$op = isset($form_data['existing_node']) ? 'update' : 'insert';
        //pathauto_node_update_alias($emw_node->value(), $op, $values = array ( 'language' => $lang));
      }
      // Remove deleted translations
      if($val === 0 && isset($existing_translations->data[$lang])) {
        $translation_handler->removeTranslation($lang);
      }
    }
    if(empty($existing_translations->data) && isset($form_data['existing_node'])) {
      $translation_handler->setOriginalLanguage(LANGUAGE_NONE);
    }
  }
  // Step 2 data
  // Data stored on different steps for activity & event.
  $step_2 = $node_type === 'event' ? $form_data['step_2'] : $form_data['step_1'];
  if(!empty($step_2)) {
    // Use subcategory if present, use main category otherwise.
    $category = !empty($step_2['category']['field_subcategory']) 
    ? $step_2['category']['field_subcategory'] : $step_2['category']['field_category'];
    
    if(!empty($step_2['category']['field_category'])) {
      $category = array_merge($category, $step_2['category']['field_category']);
    }
    
    if(!empty($category)) {
      $category_array = array();
      foreach($category as $c) {
        $category_array[] = isset($c['tid']) ? $c['tid'] : $c;
      }
      if($node_type === 'event') {
        $emw_node->field_category = $category_array;
      } else {
        $emw_node->field_category_activity = $category_array;
      }
    }
    
    if($node_type === 'event') {
      // Target groups
      $target_groups = array();
      foreach($step_2['field_target_groups'] as $tid => $val) {
        if(!empty($val) && is_numeric($val)) {
          $target_groups[] = $tid;
        }
      }

      if(!empty($target_groups)) {
        $emw_node->field_target_groups = $target_groups;
      }
    }
    
  }
  
  // Step 3 data
  $step_3 = $form_data['step_3'];

  if(!empty($step_3)) {
    if($node_type === 'event') {
      // Event dates
      $emw_node->field_event_dates = array(NULL); // Delete existing paragraphs to prevent duplication
      
      $pids = array();
      foreach($step_3['event_date_location'] as $key => $event_item) {
        if(!is_array($event_item)) continue;
        
        if(!empty($event_item['pid']) && $type !== 'preview' 
          && $event_date_paragraph = paragraphs_item_load($event_item['pid'])) { // Load an existing paragraph item if one exists.
        } else {
          $event_date_paragraph = entity_create('paragraphs_item', array('field_name' => 'field_event_dates', 'bundle' => 'event_date'));
          $event_date_paragraph->is_new = TRUE;
        }
        $event_date_paragraph->setHostEntity('node', $node);
        $event_date_paragraph->save();
        
        $emw_paragraph = entity_metadata_wrapper('paragraphs_item', $event_date_paragraph);

        $date_from = strtotime(sprintf('%s %s', 
        $event_item['date']['start']['field_event_date_from'], 
        $event_item['date']['start']['field_event_time_from']));
        
        if($event_item['date']['end']['field_event_time_to'] === 'unset') { // Set default end time to 23:59 if it is unset
          $event_time_to = '23:55';
          $emw_paragraph->field_end_date_unset->set(TRUE);
        } else {
          $event_time_to = $event_item['date']['end']['field_event_time_to'];
          $emw_paragraph->field_end_date_unset->set(FALSE);
        }
        
        $date_to = strtotime(sprintf('%s %s', 
        $event_item['date']['end']['field_event_date_to'], $event_time_to));
        
        $emw_paragraph->field_event_date = array('value' => $date_from, 'value2' => $date_to);

        $location = array(
          'name' => $event_item['location']['field_event_location'], 
          'street' => $event_item['location']['field_event_address'], 
          'country' => 'ee',
        );
        $lid = location_save($location);

        if($lid) { // Location saved succesfully
          $emw_paragraph->field_location->set($location);
        }
        
        $emw_paragraph->save();
        $pids[] = $emw_paragraph->getIdentifier();
        
      }
      // Set the paragraph items
      //$emw_node->field_event_dates->set($pids);
      
      // Event price
      $ticket_type = $step_3['event_tickets']['price']['field_event_ticket_type'];
      
      if(!empty($ticket_type)) {
        $emw_node->field_event_ticket_type = $ticket_type;
        switch($ticket_type) {
          case 'fixed':
            $price = floatval(str_replace(',', '.', $step_3['event_tickets']['price']['field_event_ticket_price']));
            $emw_node->field_event_ticket_price = !empty($price) ? $price : NULL;
            break;
          case 'between':
            $p_from = isset($step_3['event_tickets']['price']['field_event_ticket_price_from']) 
              && $step_3['event_tickets']['price']['field_event_ticket_price_from'] !== "" ? TRUE : FALSE;
            $price_from = $p_from ? floatval(str_replace(',', '.', $step_3['event_tickets']['price']['field_event_ticket_price_from'])) : NULL;
            $emw_node->field_event_ticket_price_from = isset($price_from) ? $price_from : NULL;
            
            $price_to = floatval(str_replace(',', '.', $step_3['event_tickets']['price']['field_event_ticket_price_to']));
            $emw_node->field_event_ticket_price_to = !empty($price_to) ? $price_to : NULL;
            break;
        }
      }
      
      // Ticket availability
      $ticket_availability = $step_3['event_tickets']['availability']['field_event_ticket_availability'];
      
      if(!empty($ticket_availability)) {
        $emw_node->field_event_ticket_availability = $ticket_availability;
        switch($ticket_availability) {
          case 'link':
            $emw_node->field_event_ticket_link = array('url' => $step_3['event_tickets']['availability']['field_event_ticket_link']);
            break;
          case 'other':
            $emw_node->field_event_ticket_other = $step_3['event_tickets']['availability']['field_event_ticket_other'];
            break;
        }
      }
      
      // Ticket additional info
      if(!empty($step_3['event_tickets']['additional']['languages'])) {
        foreach($step_3['event_tickets']['additional']['languages'] as $lang => $additional) {
          if(!empty($additional)) {
            $emw_node->language($lang)->field_event_ticket_info = $additional;
          }
        }
      }
    } else {
      // Activity groups
      foreach($step_3['groups'] as $group) {
        
        // Remove group if set
        if(!empty($group['remove_group_confirmed']) && $group['remove_group_confirmed']) {
          if(!empty($group['group_id'])) {
            foreach($emw_node->field_activity_groups as $delta => $n_group) {
              if($n_group->getIdentifier() === $group['group_id']) {
                $emw_node->field_activity_groups->offsetUnset($delta);
              }
            }
          }
          continue;
        }
        
        // Is existing group
        if(!empty($group['group_id']) && $type !== 'preview') {
          $group_paragraph = paragraphs_item_load($group['group_id']);
          
        } else {
          $group_paragraph = entity_create('paragraphs_item', array('field_name' => 'field_activity_groups', 'bundle' => 'activity_group'));
          $group_paragraph->setHostEntity('node', $node);
        }
        
        $emw_paragraph = entity_metadata_wrapper('paragraphs_item', $group_paragraph);
        
        // Name
        $emw_paragraph->field_activity_group_name = $group['name'];
        $emw_paragraph->field_activity_group_name_en = !empty($group['name_t']['en']) ? $group['name_t']['en'] : '';
        $emw_paragraph->field_activity_group_name_ru = !empty($group['name_t']['ru']) ? $group['name_t']['ru'] : '';
        
        // Is full
        $emw_paragraph->field_activity_group_full = boolval($group['is_full']);
        if(!empty($group['schedule']['type'])) {
          $emw_paragraph->field_activity_group_type = $group['schedule']['type'];
        } else if($group['activity_type'] === 'single_group') {
          $emw_paragraph->field_activity_group_type = 'fixed';
        }
        
        // Price
        $emw_paragraph->field_activity_group_price_type = $group['price']['type'];
        if($group['price']['type'] === 'single_payment' && !empty($group['price']['single_payment'])) {
          $price = floatval(str_replace(',', '.', str_replace('.', '', $group['price']['single_payment'])));
          $emw_paragraph->field_activity_group_price = !empty($price) ? $price : NULL;
        } else if($group['price']['type'] === 'monthly_payment' && !empty($group['price']['monthly_payment'])) {
          $price = floatval(str_replace(',', '.', str_replace('.', '', $group['price']['monthly_payment'])));
          $emw_paragraph->field_activity_group_price = !empty($price) ? $price : NULL;
        }

        // Additional comment
        if(!empty($group['price']['additional_comment'])) {
          $emw_paragraph->field_additional_comment = $group['price']['additional_comment'];
        }
        
        // Age
        if(!empty($group['age_range']['from']) && is_numeric($group['age_range']['from'])) {
          $emw_paragraph->field_activity_group_age_from = (int) $group['age_range']['from'];
        }
        if(!empty($group['age_range']['to']) && is_numeric($group['age_range']['to'])) {
          $emw_paragraph->field_activity_group_age_to = (int) $group['age_range']['to'];
        }
        
        // Target groups
        $target_groups = array();
        foreach($group['age_groups'] as $key => $val) {
          if($key === $val) {
            $target_groups[] = $val;
          }
        }

        if(!empty($target_groups)) {
          $emw_paragraph->field_activity_group_demographic = $target_groups;
        }

        // Schedule
        if(!empty($group['schedule']['type']) && $group['schedule']['type'] === 'regular') {
          if(!empty($emw_paragraph->field_activity_groups_fixed->value())) {
            // Remove the other type of schedule
            $emw_paragraph->field_activity_groups_fixed = NULL;
          }
          // Set date period
          if(!empty($group['schedule']['regular']['period_from']) && $period_from = strtotime($group['schedule']['regular']['period_from'])
            && !empty($group['schedule']['regular']['period_to']) && $period_to = strtotime($group['schedule']['regular']['period_to'])
          ) {
            $period_from = strtotime($group['schedule']['regular']['period_from']);
            $period_to = strtotime($group['schedule']['regular']['period_to']);
            $emw_paragraph->field_activity_date_period->set(array('value' => $period_from, 'value2' => $period_to));
          }
          foreach($group['schedule']['regular']['weekdays'] as $weekday => $active) {
            
            $schedule_day = $group['schedule']['regular']['day'][$weekday];
            $schedule_id = $schedule_day['schedule_id'];
            if($weekday == $active && $weekday != 0) {
              
              // If the schedule paragraph already exists, change it instead.
              if(!empty($schedule_id) && ($paragraph = paragraphs_item_load($schedule_id))) {
                // Existing pargraph, loaded in condition
                $schedule_paragraph = $paragraph;
              } else {
                // New paragraph
                $schedule_paragraph = entity_create('paragraphs_item', array('field_name' => 'field_activity_groups_regular', 'bundle' => 'activity_group_regular_dates'));
                $schedule_paragraph->is_new = TRUE;
                
                $schedule_paragraph->setHostEntity('paragraphs_item', $emw_paragraph->value());
              }
              
              $emw_schedule_paragraph = entity_metadata_wrapper('paragraphs_item', $schedule_paragraph);
              
              // Day
              $emw_schedule_paragraph->field_activity_date_days = $weekday;
              
              // Time
              $time_from = strtotime($schedule_day['time_from']);
              $time_to = strtotime($schedule_day['time_to']);
              $emw_schedule_paragraph->field_activity_date_time = array('value' => $time_from, 'value2' => $time_to);
              
              // Location
              if($location = $emw_schedule_paragraph->field_location->value()) {
              } else {
                $location = array();
              }
              
              $location['name'] = $schedule_day['location'];
              $location['street'] = $schedule_day['address'];
              
              $lid = location_save($location);
            
              if($lid) {
                $emw_schedule_paragraph->field_location = array(
                  'lid' => $lid,
                );
              }

              $emw_schedule_paragraph->save();
            } else if(!empty($schedule_id)) { // Delete existing paragraph
              foreach($emw_paragraph->field_activity_groups_regular->getIterator() as $delta => $activity_day) {
                $p_weekday = $activity_day->field_activity_date_days->value();
                if($weekday == $p_weekday && $weekday != $active) {
                  $emw_paragraph->field_activity_groups_regular->offsetUnset($delta);
                }
              }
            }
          }
          
        } else if((!empty($group['schedule']['type']) && $group['schedule']['type'] === 'fixed') 
          || $group['activity_type'] === 'single_group') {
          if(!empty($emw_paragraph->field_activity_groups_regular->value())) {
            $emw_paragraph->field_activity_groups_regular = NULL;
          }
          
          $schedule_ids = array();
          if($group['activity_type'] === 'single_group') { // Single group

            // Single group activity
            $emw_paragraph->field_single_group_activity = TRUE;
            $s = $emw_paragraph->field_activity_groups_fixed->value();
            if(!empty($s) && ($paragraph = paragraphs_item_load(reset($s)->item_id))) {
              // Existing pargraph, loaded in condition
              $schedule_paragraph = $paragraph;
            } else {
              // New paragraph
              $schedule_paragraph = new ParagraphsItemEntity(
                array('field_name' => 'field_activity_groups_fixed', 
                'bundle' => 'activity_group_set_dates')
              );
              $schedule_paragraph->is_new = TRUE;
              $schedule_paragraph->setHostEntity('paragraphs_item', $emw_paragraph->value());
            }
            
            $emw_schedule_paragraph = entity_metadata_wrapper('paragraphs_item', $schedule_paragraph);
            
            // Date
            $time_from = strtotime($group['schedule']['date']['start']['field_event_date_from'] . ' 00:00');
            $time_to = strtotime($group['schedule']['date']['end']['field_event_date_to'] . ' 23:59');
            $emw_schedule_paragraph->field_event_date = array('value' => $time_from, 'value2' => $time_to);
            
            // Location
            if(!empty($group['schedule']['location'])) { // Make sure the address is actually set.
              $emw_schedule_paragraph->field_location = array(
                'name' => $group['schedule']['location']['field_event_location'], 
                'street' => $group['schedule']['location']['field_event_address'],
              );
            }
            
            $emw_schedule_paragraph->save();
            
          } else { // Multigroup
            foreach($group['schedule']['fixed'] as $schedule_day) {
              if(!is_array($schedule_day)) continue;
              $schedule_id = $schedule_day['schedule_id'];
              if(!empty($schedule_id) && ($paragraph = paragraphs_item_load($schedule_id))) {
                // Existing pargraph, loaded in condition
                $schedule_paragraph = $paragraph;
              } else {
                // New paragraph
                $schedule_paragraph = new ParagraphsItemEntity(
                  array('field_name' => 'field_activity_groups_fixed', 
                  'bundle' => 'activity_group_set_dates')
                );
                $schedule_paragraph->is_new = TRUE;
                $schedule_paragraph->setHostEntity('paragraphs_item', $emw_paragraph->value());
              }
              
              $emw_schedule_paragraph = entity_metadata_wrapper('paragraphs_item', $schedule_paragraph);
              
              // Not Single Group Activity
              $emw_paragraph->field_single_group_activity = FALSE;
              
              // Date
              $time_from = strtotime($schedule_day['time_from'] . ' ' . $schedule_day['date']);
              $time_to = strtotime($schedule_day['time_to'] . ' ' . $schedule_day['date']);
              $emw_schedule_paragraph->field_event_date = array('value' => $time_from, 'value2' => $time_to);
              
              // Location
              if(!empty($schedule_day['address'])) { // Make sure the address is actually set.
                $emw_schedule_paragraph->field_location = array(
                  'name' => $schedule_day['location'], 
                  'street' => $schedule_day['address'],
                );
              }
  
              $emw_schedule_paragraph->save();
              $schedule_ids[$emw_schedule_paragraph->getIdentifier()] = TRUE;
            }
            
            // Remove old/deleted schedule paragraphs.
            foreach($emw_paragraph->field_activity_groups_fixed->getIterator() as $delta => $schedule_wrapper) {
              $schedule_id = $schedule_wrapper->getIdentifier();
              if(!isset($schedule_ids[$schedule_id])) {
                $emw_paragraph->field_activity_groups_fixed->offsetUnset($delta);
              }
            }
          }
        }
        $emw_paragraph->save();
      }
    }
  }
  
  // Step 4 data
  $step_4 = $form_data['step_4'];
  if(!empty($step_4)) {
    // Video
    if(!empty($step_4['video_parsed'])) {
      foreach($step_4['video_parsed'] as $lang => $video) {
        if(!empty($video) && !empty($step_4['field_video'][$lang])) {
          // Check if video already exists.
          $video_object = ($existing_video = file_uri_to_object($video->uri)) ? file_save($existing_video) : file_save($video);
          $emw_node->language($lang)->field_video->set(array('fid' => $video_object->fid, 'display' => '1'));
        } else { // Unset deleted video
          $emw_node->language($lang)->field_video->set(NULL);
        }
      }
    }
    
    // Gallery
    $image_fids = array();
    $main_image = !empty($step_4['gallery']['image_preview']['main_image']) ? $step_4['gallery']['image_preview']['main_image'] : NULL;
    $emw_node->field_gallery = array();
    if(!empty($step_4['gallery']['image_preview']['images'])) {
      foreach($step_4['gallery']['image_preview']['images'] as $key => $image) {
        if($key === $main_image) continue; // Skip main image, we'll add it in its own field.
          $emw_node->field_gallery[] = array('fid' => $key);
        }
    }
    
    // Main image
    if(!empty($main_image)) {
      $emw_node->field_main_image = array('fid' => $main_image);
    }
  }
  
  // Step 5 data
  $step_5 = $form_data['step_5'];
  
  if(!empty($step_5)) {
    // Organizer data
    $emw_node->field_organizer_name = !empty($step_5['left']['field_organizer_name']) ? 
      $step_5['left']['field_organizer_name'] : '';
    $emw_node->field_organizer_www = !empty($step_5['left']['field_organizer_www']) ?
      array('url' => $step_5['left']['field_organizer_www']) : array('url' => '');
    $emw_node->field_organizer_address = !empty($step_5['left']['field_organizer_address']) ?
      $step_5['left']['field_organizer_address'] : '';
    $emw_node->field_organizer_contact = !empty($step_5['right']['field_organizer_contact']) ?
      $step_5['right']['field_organizer_contact'] : '';
    $emw_node->field_organizer_email = !empty($step_5['right']['field_organizer_email']) ?
      $step_5['right']['field_organizer_email'] : '';
    $emw_node->field_organizer_phone = !empty($step_5['right']['field_organizer_phone']) ?
      $step_5['right']['field_organizer_phone'] : '';
    
    if($node_type === 'activity') {
      // Activity registration options
      $allowed_registration_types = array();
      if(!empty($step_5['registration']['field_registration_types'])) {
        foreach($step_5['registration']['field_registration_types'] as $key => $registration_type) {
          if(!empty($registration_type)) {
            $allowed_registration_types[] = $key;
            if($key === 'internet') {
              $emw_node->{"field_registration_$key"} = array('url' => $step_5['registration']["field_registration_$key"]);
            } else {
              $emw_node->{"field_registration_$key"} = $step_5['registration']["field_registration_$key"];
            }
            
          } 
        }
        $emw_node->field_registration_types = $allowed_registration_types;
      }
    }
  }
  $node_type_string = node_type_get_name($node);
  $finish_message = NULL;
  switch($type) {
    case 'draft':
      $emw_node->field_ka_status = 'unfinished';
      $finish_message = ka_t('@type saved as draft!', array('@type' => $node_type_string));
      break;
    case 'publish':
      $emw_node->field_ka_status = 'needs_confirmation';
      $emw_node->field_publish_date = time(); // Reset the publish date if it has been set before
      $finish_message = ka_t('@type saved! @type will be published once it has been been confirmed by our moderators.', array('@type' => $node_type_string));
      break;
    case 'publish_later':
      $emw_node->field_ka_status = 'needs_confirmation';
      // Set the publish later date.
      if(!empty($form_data['publish_later'])) {
        $publish_date = strtotime($form_data['publish_later']['date'] . ' ' . $form_data['publish_later']['time']);
        if($publish_date) { // Date is valid.
          $emw_node->field_publish_date = $publish_date;
        }
      }
      $finish_message = ka_t('@type saved! @type will be published on @date once it has been been confirmed by our moderators.', 
      array('@type' => $node_type_string,
            '@date' => format_date($publish_date, 'detailed_date'),
      ));
      break;
    case 'api':
      $emw_node->field_ka_status = 'published';
      break;
  }

  if($type === 'preview') {
    $emw_node->field_ka_status = 'preview';
    // Set parent preview node
    
    if(isset($form_data['existing_node'])) {
      $node = (object) $form_data['existing_node'];
      $emw_node->field_preview_parent_node = $node->nid;
    }
    
    $emw_node->save();
    return $emw_node->value();
  } else {
    $emw_node->save();
    drupal_set_message($finish_message);
  }
  
  // Delete preview node
  if(!empty($form_data['preview_generated'])) {
    $nid = $form_data['preview_generated']['nid'];
    node_delete($nid);
  }
}

function ka_event_node_preview($node) {
  // Clone the node before previewing it to prevent the node itself from being
  // modified.
  $cloned_node = clone $node;
  if (TRUE/*node_access('create', $cloned_node) || node_access('update', $cloned_node)*/) {
    _field_invoke_multiple('load', 'node', array($cloned_node->nid => $cloned_node));
    // Load the user's name when needed.
    if (isset($cloned_node->name)) {
      // The use of isset() is mandatory in the context of user IDs, because
      // user ID 0 denotes the anonymous user.
      if ($user = user_load_by_name($cloned_node->name)) {
        $cloned_node->uid = $user->uid;
        $cloned_node->picture = $user->picture;
      }
      else {
        $cloned_node->uid = 0; // anonymous user
      }
    }
    elseif ($cloned_node->uid) {
      $user = user_load($cloned_node->uid);
      $cloned_node->name = $user->name;
      $cloned_node->picture = $user->picture;
    }

    $cloned_node->changed = REQUEST_TIME;
    $nodes = array($cloned_node->nid => $cloned_node);

    // Display a preview of the node.
    if (!form_get_errors()) {
      $cloned_node->in_preview = TRUE;
      $output = theme('node_preview', array('node' => $cloned_node));
      unset($cloned_node->in_preview);
    }
    drupal_set_title(t('Preview'), PASS_THROUGH);

    return $output;
  }
}
