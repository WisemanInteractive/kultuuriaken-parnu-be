<?php

function ka_admin_variable_group_info() {
  $groups['general'] = array(
    'title' => t('General variables'),
    'access' => 'translate interface',
    'path' => array('admin/variables/general'),
  );
  
  $groups['tooltips'] = array(
    'title' => t('Tooltips'),
    'access' => 'translate interface',
    'path' => array('admin/content/variables/tooltips'),
  );
  
  $groups['event_form'] = array(
    'title' => t('Event form'),
    'access' => 'administer site configuration',
    'path' => array('admin/variables/event_form'),
  );
  
  $groups['activity_form'] = array(
    'title' => t('Activity form'),
    'access' => 'administer site configuration',
    'path' => array('admin/variables/activity_form'),
  );
  return $groups;
}

function ka_admin_variable_info($options) {
  $variables['footer_address'] = array(
    'title' => t('Footer address'),
    'localize' => TRUE,
    'type' => 'text_format',
    'group' => 'general',
  );
  
  $variables['footer_contact_us'] = array(
    'title' => t('Contact us'),
    'type' => 'text_format',
    'group' => 'general',
  );
  
  $variables['join_newsletter_title'] = array(
    'title' => t('"Join newsletter" title'),
    'type' => 'text_format',
    'group' => 'general',
  );
  
  $variables['join_newsletter_body'] = array(
    'title' => t('"Join newsletter" body'),
    'type' => 'text_format',
    'group' => 'general',
  );
  
  $variables['facebook_link'] = array(
    'title' => t('Facebook link'),
    'type' => 'text',
    'group' => 'general',
  );
  
  /*
  $variables['video_url_tooltip'] = array(
    'title' => t('"Add video URL" tooltip'),
    'type' => 'text_format',
    'group' => 'tooltips',
  );
  
  $variables['gallery_image_tooltip'] = array(
    'title' => t('"Select how to add photos" tooltip'),
    'type' => 'text_format',
    'group' => 'tooltips',
  );
  */
  $variables['save_as_draft_popup_message'] = array(
    'title' => t('Save as draft popup text'),
    'localize' => TRUE,
    'type' => 'text_format',
    'group' => 'general',
  );
  
  $variables['anon_add_event_note'] = array(
    'title' => t('"Add new event" note'),
    'type' => 'text_format',
    /*'localize' => TRUE,*/
    'group' => 'general',
  );
  
  $variables['anon_add_activity_note'] = array(
    'title' => t('"Add new recreational activity" note'),
    'type' => 'text_format',
    /*'localize' => TRUE,*/
    'group' => 'general',
  );
  
  $variables['title_char_limit'] = array(
    'title' => t('Title character limit'),
    'type' => 'number',
    'group' => 'general',
    'default' => 50,
  );
  
  $variables['short_description_char_limit'] = array(
    'title' => t('Short description character limit'),
    'type' => 'number',
    'group' => 'general',
    'default' => 500,
  );
  
  $variables['main_text_char_limit'] = array(
    'title' => t('Main text character limit'),
    'type' => 'number',
    'group' => 'general',
    'default' => 1500,
  );
  
  $variables['activity_name_placeholder'] = array(
    'title' => t('Activity name placeholder text'),
    'type' => 'string',
    'group' => 'general',
  );
  
  // Form tooltips
  // Event dates
  $variables['add_date_tooltip'] = array(
    'title' => ka_t('Add date'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  // Group
  $variables['group_name_tooltip'] = array(
    'title' => ka_t('Group name'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  $variables['group_age_tooltip'] = array(
    'title' => ka_t('Target group'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  $variables['time_place_tooltip'] = array(
    'title' => ka_t('Time and place'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  $variables['price_tooltip'] = array(
    'title' => ka_t('Price'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  // Pictures and videos
  $variables['enter_video_url_tooltip'] = array(
    'title' => ka_t('Enter video URL'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  $variables['add_picture_tooltip'] = array(
    'title' => ka_t('Add picture'),
    'type' => 'text',
    'localize' => TRUE,
    'group' => 'tooltips',
  );
  
  
  return $variables;
}
