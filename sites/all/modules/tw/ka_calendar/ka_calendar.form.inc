<?php

function ka_calendar_forms($form_id, $args) {
  $forms = array();
  
  if (strpos($form_id, 'add_to_calendar_form_') === 0) {
    $forms[$form_id] = array('callback' => 'add_to_calendar_form');
  }
  
  return $forms;
}

function add_to_calendar_form($form, &$form_state, $nid, $event_id) {
  $form['add_to_calendar']= array(
    '#value' => ka_t('Add to calendar'),
    '#title' => 'down',
    '#type' => 'submit',
    'event_id' => array(
      '#type' => 'value',
      '#value' => $event_id,
    ),
    'nid' => array(
        '#type' => 'value',
        '#value' => $nid,
      ),
    '#attributes' => array(
        'style' => 'display:none;',
        'data-id' => 'add-to-calendar',
        'data-group-id' => $event_id,
      ),
  );
  return $form;
}

function add_to_calendar_form_validate($form, &$form_state) {
  
}

function add_to_calendar_form_submit($form, &$form_state) {
  drupal_set_message(ka_t('Added to calendar!'));
  $event_id = $form_state['values']['event_id'];
  $new_event_save = array('calendar' => $event_id);
  //dsm('2'. $event_id);
  $saved_calendar_string = !empty($_COOKIE['Drupal_visitor_calendar']) ? $_COOKIE['Drupal_visitor_calendar'] : NULL;

  if(!empty($saved_calendar_string)) {
    $saved_calendar = explode(",", $saved_calendar_string);
    $exists = FALSE;
    foreach($saved_calendar as $item) {
      if($item === $event_id) { // Already added
        $exists = TRUE;
        break;
      }
    }
    if(!$exists) {
      $saved_calendar[] = $event_id;
      $saved_calendar_string_new = implode(',', $saved_calendar);
      user_cookie_save(array('calendar' => $saved_calendar_string_new));
    }
  } else {
    user_cookie_save($new_event_save);
  }
}