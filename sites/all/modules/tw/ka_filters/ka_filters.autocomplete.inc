<?php

function ka_filters_autocomplete_json_callback($type) {
  global $language;
  $lang_code = $language->language;
  $query = drupal_get_query_parameters();
  if(!empty($query['search'])) {
    switch($type) {
      case 'main':
        $search_array_json = variable_get_value('general_autocomplete_'.$lang_code);
        break;
      case 'events':
        $search_array_json = variable_get_value('events_autocomplete_'.$lang_code);
        break;
      case 'activities':
        $search_array_json = variable_get_value('activities_autocomplete_'.$lang_code);
        break;
    }

    if(isset($search_array_json)) {
      $search_array = drupal_json_decode($search_array_json);

      $search_result = preg_grep('~' . $query['search'] . '~iu', $search_array);
      
      $output = array('list' => array());
      foreach($search_result as $loc) {
        $output['list'][] = (object) array('title' => $loc);
      }
      drupal_json_output($output);
    }
  }
}

function ka_fetch_autocomplete_values($node_type, $lang_code) {
  global $language;
  
  $node_array = ka_get_nodes(NULL, $node_type);
  
  $locations = array();
  $now = time();
  foreach($node_array as $node) {
    $emw_node = entity_metadata_wrapper('node', $node)->language($lang_code);
    
    // Check if a translation exists
    $translation_handler = entity_translation_get_handler('node', $node);
    $translations = $translation_handler->getTranslations();

    if($language->language !== $translations->original && !isset($translations->data[$language->language])) {
      continue;
    }
  
    //$locations[] = $emw_node->label();

    if($node_type === 'events') {
      // Event dates
      $event_dates = $emw_node->field_event_dates->getIterator();
      foreach($event_dates as $delta => $event) {
        
        $event_date = !empty($event->field_event_date->value()['value']) ? (int) $event->field_event_date->value()['value'] : 0;
        $event_date2 = !empty($event->field_event_date->value()['value2']) ? (int) $event->field_event_date->value()['value2'] : 0;
        if($event_date2 < $now) continue; // Skip past events
        $locations[] = $emw_node->title_field->value();
        // Location
        $location = $event->field_location->value();
        if(!empty($location['name'])) {
          $locations[] = trim($location['name']);
        }
      }
    } else if($node_type === 'activities') {
      $activity_groups = $emw_node->field_activity_groups->getIterator();
      
      foreach($activity_groups as $emw_activity_group) {
        $type = $emw_activity_group->field_activity_group_type->value();
        $group_times = $type === 'regular' ? $emw_activity_group->field_activity_groups_regular->getIterator() :
        $emw_activity_group->field_activity_groups_fixed->getIterator();
        foreach($group_times as $group_time) {
          if($group_time->field_name->value() === 'field_activity_groups_fixed'){
            $activity_date = $group_time->field_event_date->value()['value2'];
          }elseif($group_time->field_name->value() === 'field_activity_groups_regular'){
            $activity_date = $group_time->field_activity_date_time->value()['value2'];
          }
          if(empty($date = calculate_activity_dates($emw_activity_group, TRUE, 'next')) || (is_array($date) && $date[0] == false)) continue;
          
          if($lang_code !== 'et') {
            $locations[] = $emw_activity_group->{"field_activity_group_name_$lang_code"}->value();
          } else {
            $locations[] = $emw_activity_group->field_activity_group_name->value();
          }
          
          // Get the location for the specific group time.
          $location = $group_time->field_location->value();
          if(!empty($location['name'])) {
            $locations[] = trim($location['name']);
          }
        }
      }
    }
  }
  $locations_unique = array_unique($locations);
  
  return $locations_unique;
}

function ka_fetch_event_autocomplete_values($lang_code) {
  $columns = array(
  	'tf.title_field_value',
  	'l.name'
  );
  
  $output = array();
  
  foreach($columns as $c) {
  
    $query = db_query("
    	SELECT DISTINCT ".$c."
    	FROM field_data_title_field tf
    	
    	INNER JOIN node n 
    		ON n.nid = tf.entity_id
    	INNER JOIN field_data_field_event_dates p
    		ON p.entity_id = n.nid
    	INNER JOIN field_data_field_event_date fp
    		ON fp.entity_id = p.field_event_dates_value
    	INNER JOIN field_data_field_category fc 
    		ON fc.entity_id = n.nid
    	INNER JOIN field_data_field_ka_status fs
    		ON fs.entity_id = n.nid
    	INNER JOIN field_data_field_publish_date pd
    		ON pd.entity_id = n.nid
    	INNER JOIN entity_translation et
    		ON et.entity_id = n.nid
    	INNER JOIN field_data_field_location fl
    		ON fl.entity_id = p.field_event_dates_value
    	INNER JOIN location l
    		ON l.lid = fl.field_location_lid
    
    	WHERE 
    		n.status = 1
    		AND fp.bundle = 'event_date'
    		AND fs.field_ka_status_value = 'published'
    		AND pd.field_publish_date_value < :time
    		AND et.language = :lang
    		AND fp.field_event_date_value2 > :time
    		AND ".$c." != ''
    		AND tf.language = :lang
    
    ", array(':lang' => $lang_code, ':time' => time())
    );
  
    $output = array_merge($query->fetchCol(), $output);
  }
  
  return $output;
}