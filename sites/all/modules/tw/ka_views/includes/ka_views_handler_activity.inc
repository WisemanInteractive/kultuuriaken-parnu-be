<?php
/**
 * @file
 * Custom views handler definition.
 *
 * Place this code in
 * /sites/all/[custom_module_name]/includes/views_handler_my_custom_field.inc
 */
 
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class ka_views_handler_activity extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {
 
  }
 
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['attribute'] = array('default' => 'eesnimi');

    return $options;
  }
 
  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['attribute'] = array(
      '#type' => 'select',
      '#title' => t('Kuvatav atribuut'),
      '#options' => array(
        'eesnimi' => 'Eesnimi',
        'perekonnanimi' => 'Perekonnanimi',
        'arstikood' => 'Arstikood',
        'isikukood' => 'Isikukood',
        '1' => 'Atribuut 1',
        '2' => 'Atribuut 2',
        '3' => 'Atribuut 3',
        '4' => 'Atribuut 4',
        '5' => 'Atribuut 5',
       ),
      '#default_value' => $this->options['attribute'],
      '#description' => t('Vali mis atribuuti kuvada'),
    );
  }
 
  /**
   * Render callback handler.
   *
   * Return the markup that will appear in the rendered field.
   */
  function render($data) {
    //dpm($data);
    $selected_attribute = $this->options['attribute'];
    switch($selected_attribute) {
      case 'eesnimi':
        $participant = $GLOBALS['order_participants'][$data->order_id][0];
        $participant->eesnimi->value;
        return $participant->eesnimi->value;
      case 'perekonnanimi':
        $participant = $GLOBALS['order_participants'][$data->order_id][0];
        $participant->perekonnanimi->value;
        return $participant->perekonnanimi->value;
      case 'arstikood':
        $participant = $GLOBALS['order_participants'][$data->order_id][0];
        $participant->arstikood->value;
        return $participant->arstikood->value;
      case 'isikukood':
        $participant = array_shift($GLOBALS['order_participants'][$data->order_id]);
        $participant->isikukood->value;
        return $participant->isikukood->value;
      default:

        if(!empty($GLOBALS['order_attributes'][$data->order_id])) {
          $order_attributes = &$GLOBALS['order_attributes'][$data->order_id];
        }

        if(isset($order_attributes)) {
          foreach($order_attributes[0] as $key => $attributes) {
            if(!empty($attributes)) {
              $row_value = implode(array_shift($order_attributes[0]));
              break;
            }
          }
        }
        
        if(isset($order_attributes[0]) && empty($order_attributes[0]) && $selected_attribute === '5') {
          // All attributes used, go to the next order item.
          array_shift($order_attributes);
        }
        
        if(!empty($row_value)) return $row_value;
    }
  }
}