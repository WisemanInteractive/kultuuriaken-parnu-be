<?php

function ka_views_get_organizer_list($type){
  
  $result[''] =  '- Any -';
  $result += db_query("
  SELECT DISTINCT field_organizer_name_value
    FROM {field_data_field_organizer_name}
    JOIN {node} on field_data_field_organizer_name.entity_id = node.nid
    WHERE node.type = :type
    AND field_organizer_name_value <> ''
    ", array(':type' => $type)
  )->fetchAllKeyed(0, 0);
  
  return $result;
}