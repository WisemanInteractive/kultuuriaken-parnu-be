<?php

function ka_views_action_info() {
  $actions['ka_views_publish_action'] = array(
    'type' => 'node',
    'label' => ka_t('Publish'),
    'configurable' => FALSE,
    'triggers' => array('any'),
    'behavior' => array('changes_property'),
    'vbo_configurable' => FALSE,
    'pass rows' => TRUE,
  );
  
  $actions['ka_views_unpublish_action'] = array(
    'type' => 'node',
    'label' => ka_t('Unpublish'),
    'configurable' => FALSE,
    'triggers' => array('any'),
    'behavior' => array('changes_property'),
    'vbo_configurable' => FALSE,
    'pass rows' => TRUE,
  );

  return $actions;
}

function ka_views_publish_action($entity, $context) {
  $emw_node = entity_metadata_wrapper('node', $entity);
  $emw_node->field_ka_status = 'published';
  $emw_node->status = 1;
  $emw_node->save();
}

function ka_views_unpublish_action($entity, $context) {
  $emw_node = entity_metadata_wrapper('node', $entity);
  $emw_node->field_ka_status = 'unpublished';
  $emw_node->save();
}