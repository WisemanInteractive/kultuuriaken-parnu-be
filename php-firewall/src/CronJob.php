<?php

namespace WebARX;

use Httpful\Mime;
use Httpful\Request;
use WebARX\Support\Logs\ReadLogs;
use WebARX\Support\Logs\WriteLogs;
use WebARX\Support\Rules\ReadRules;
use WebARX\Support\Rules\WriteRules;

class CronJob
{

    /**
     * Pull firewall rules from the API and save in the local file
     * @param $token
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function pullFirewallRules($token)
    {
        $headers = array(
            'Authorization' => "Bearer " . $token,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $results = Request::post(WebARX::WEBARX_APIHOST . '/api/rules/json')
            ->addHeaders($headers)
            ->send();

        WriteRules::firewallRules(json_decode($results)->rules);

    }

    /**
     * Pull global whitelist from the API and save in the local file
     * @param $token
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function pullWhitelistRules($token)
    {
        $headers = array(
            'Authorization' => "Bearer " . $token,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        );

        $results = Request::post(WebARX::WEBARX_APIHOST . '/api/rules/whitelist')
            ->addHeaders($headers)
            ->send();

        WriteRules::whitelistRules(json_decode($results)->rules);

    }

    /**
     * Upload local logs to the API
     * @param $token
     * @return \Httpful\Response|string
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function uploadLocalLogs($token)
    {
        $this->clearExpiredBannedIps();

        $headers = array(
            'Authorization' => "Bearer " . $token,
            'Accept' => 'application/json',
        );

        if(strpos(file_get_contents(__DIR__ . '/../logs/local.php'), '<?php die; ?>') === false){
            return "logs are empty";
        }
        $logs = ReadLogs::firewall();


        if ($logs == null || count($logs) < 1) {
            print "logs are empty";
            die;
        }

        $results = Request::post(WebARX::WEBARX_APIHOST . '/api/logs/log')
            ->addheaders($headers)
            ->body(http_build_query(array(
                'logs' => json_encode($logs),
                'type' => 'firewall'
            )))
            ->sendsType(Mime::FORM)
            ->send();
        WriteLogs::firewall('');
        return $results;

    }

    /**
     * Clear expired IP bans
     */
    public function clearExpiredBannedIps()
    {
        $ipBans = ReadRules::ipBanList();

        $ipBans = json_decode(json_encode($ipBans), true);;

        if($ipBans == null || count($ipBans) < 1){
            return;
        }

        $dateTimezone = new \DateTimeZone('UTC');
        $dateTimeObject = new \DateTime('now', $dateTimezone);
        $dateTime = $dateTimeObject->format('Y-m-d H:i:s');

        $toBeRemoved = array();
        foreach($ipBans as $ipBan){

            if($dateTime > $ipBan['until']){
                $toBeRemoved[] = $ipBan;

            }

        }

        $finalIpBans = @array_diff_assoc($ipBans, $toBeRemoved);

        $ipBansToWrite = array();

        if(count($finalIpBans) > 0) {
            foreach ($finalIpBans as $key => $finalIpBan) {
                $ipBansToWrite[$key]['ip'] = $finalIpBan['ip'];
                $ipBansToWrite[$key]['until'] = $finalIpBan['until'];
            }
        }

        $ipBansToWrite = array_values($ipBansToWrite);
        $ipBansToWrite = json_encode($ipBansToWrite);

        WriteRules::ipBan($ipBansToWrite);
    }

}
