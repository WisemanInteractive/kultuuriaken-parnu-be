<?php

namespace WebARX;

use WebARX\Support\Rules\ReadRules;

class ProcessRequest
{

    /**
     * Check if requested request is safe or not
     *
     * @return int
     */
    public function isRequestSafe()
    {
        $rules = ReadRules::firewallRules();

        if(is_null($rules)){
            return;
        }

        $allRequestTypes = new CaptureRequest();
        $allRequestTypes = $allRequestTypes->capture();

        $types = array(
            'ALL' => $allRequestTypes,
            'POST' => $_POST,
            'GET' => $_GET,
            'SERVER' => $_SERVER,
            'COOKIE' => $_COOKIE,
            'FILES' => $_FILES
        );

        foreach ($rules as $type => $typeRules) {
            $requests = $types[$type];
            $rules = $typeRules['rules'];

            if ($type == "ALL") {
                foreach ($rules as $rule) {
                    preg_match($rule['match'], urldecode($requests), $matches);
                    if (!empty($matches)) {
                        $attackId = $rule['id'];
                        return $attackId;
                    }
                }
            }

            if($type != "ALL") {
                foreach ($requests as $request) {
                    foreach ($rules as $rule) {
                        if(is_array($request)){
                            if(null !== $this->processArrayRequest($rule, $request)){
                                return $this->processArrayRequest($rule, $request);
                            }
                        } else {
                            preg_match($rule['match'], urldecode($request), $matches);
                            if (!empty($matches)) {
                                $attackId = $rule['id'];
                                return $attackId;
                            }
                        }
                    }
                }
            }

        }

    }

    private function processArrayRequest($rule, $request)
    {
        foreach ($request as $key => $value)
        {
            if(is_array($value)){
                $this->processArrayRequest($rule, $value);
            } else {
                preg_match($rule['match'], urldecode($value), $matches);
                if (!empty($matches)) {
                    $attackId = $rule['id'];
                    return $attackId;
                }
            }
        }
        return null;
    }

    /**
     * Check if request is whitelisted (custom whitelist is checked first)
     *
     * @param WebARX $webARX
     * @return bool
     */
    public function isRequestWhitelisted(WebARX $webARX)
    {
        // Pull and merge custom and default whitelists
        $customWhitelist = '';
        if ($webARX->config['customWhitelist']) {
            $customWhitelist = ReadRules::customWhitelist();
        }
        $whiteListFile = ReadRules::whitelistRules();
        $whitelists = $customWhitelist . PHP_EOL . $whiteListFile;

        // Grab visitor's IP address
        $clientIP = $webARX->getClientIP();

        // Turn string whitelist to an array
        $arrayWhitelist = explode("\n", $whitelists);

        // Remove empty lines if any
        $lines = array_filter($arrayWhitelist);

        // Loop through all lines.
        foreach ($lines as $line) {
            $t = explode(":", $line);

            if (count($t) == 2) {
                $val = strtolower(trim($t[1]));
                switch (strtolower($t[0])) {
                    // IP address match.
                    case 'ip':
                        if ($clientIP == $val) {
                            return true;
                        }
                        break;
                    // Payload match.
                    case 'payload':
                        if (count($_POST) > 0 && strpos(strtolower(print_r($_POST, true)), $val) !== false) {
                            return true;
                        }

                        if (count($_GET) > 0 && strpos(strtolower(print_r($_GET, true)), $val) !== false) {
                            return true;
                        }
                        break;
                    // URL match.
                    case 'url':
                        
                        if(!array_key_exists('REQUEST_URI', $_SERVER)){
                            $_SERVER['REQUEST_URI'] = '/';
                        }

                        if (strpos(strtolower($_SERVER['REQUEST_URI']), $val) !== false) {
                            return true;
                        }
                        break;
                }
            }
        }

        return false;
    }

    /**
     * Display blocked request page
     *
     * @param $attackId
     * @return string
     */
    public function displayAccessForbidden($attackId)
    {
        if(isset($_SERVER["CONTENT_TYPE"])) {
            if ($_SERVER["CONTENT_TYPE"] === 'application/json') {
                header('Content-Type: application/json');
                http_response_code(422);
                echo json_encode(array(
                    'data' => array(
                        'status' => 'Access Denied',
                        'message' => 'This request has been blocked by WebARX Web Application Firewall. If you are a legitimate user, contact the administrator of the site with above error code if this message persists.',
                        'error_code' => '55' . $attackId
                    )
                ));
                die;
            }
        }
        return '        
        <html lang="en">
			<head>
				<meta charset="utf-8">
				<title>Access Denied</title>
				<meta name="robots" content="noindex, nofollow">
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
				<style type="text/css">
					.container{ margin-top: 50px; width: 768px; }
					.grey > div{font-size: 18px; font-weight: 400; text-shadow: 0px 1px 1px #fff; color: #726f6f;}
					h4{ font-size: 20px; }
					.card-content > a:last-child{ margin-top: 20px; display: inline-block; font-size: 12px; }
				</style>
			</head>
			<body>
				<div class="container">
					<div class="row">
						<div class="col s12">
							<div class="card">
								<div class="card-content grey lighten-4">
									<div style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis; padding-right:30px;">Access Denied - ' . htmlentities("http" . (($_SERVER["SERVER_PORT"] == 443) ? "s://" : "://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], ENT_QUOTES) . '
    
                                    </div>
								</div>
								<div class="card-content">
									<h4>Error Code 55' . $attackId . '</h4>
                                    <p>This request has been blocked by <a href="https://www.webarxsecurity.com" target="_blank">WebARX</a> Web Application Firewall .</p>
									<p>If you are a legitimate user, contact the administrator of the site with above error code if this message persists.</p>
									<a href="/">Return To Homepage</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</body>
		</html>
        ';
        die;

    }

}
