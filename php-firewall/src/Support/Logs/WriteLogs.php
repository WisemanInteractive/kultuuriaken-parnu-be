<?php

namespace WebARX\Support\Logs;

use WebARX\Support\Writer;

class WriteLogs extends Writer
{
    /**
     * Write content to firewall logs in JSON encoded object
     *
     * @param $content
     * @return bool|int
     */
    public static function firewall($content)
    {
        return self::writeToFile('logs', 'local', $content);
    }
}