<?php

namespace WebARX\Support\Logs;

use WebARX\Support\Reader;

class ReadLogs extends Reader
{
    /**
     * Reads and return JSON object of firewall logs
     *
     * @return mixed
     */
    public static function firewall()
    {
        $logs = self::readFile('logs', 'local');
        return json_decode($logs, true);
    }
}