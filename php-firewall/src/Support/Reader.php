<?php

namespace WebARX\Support;

defined('WDS') or define('WDS', DIRECTORY_SEPARATOR);

class Reader
{
    /**
     * Read file and return content of it
     *
     * @param $folder
     * @param $file
     * @return mixed
     */
    public static function readFile($folder, $file)
    {
        $content = file_get_contents(__DIR__ . WDS . '..' . WDS . '..' . WDS . $folder . WDS . $file . '.php');

        return str_replace('<?php die; ?>', '', $content);
    }
}
