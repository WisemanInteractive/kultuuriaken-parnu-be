<?php

namespace WebARX\Support\Cache;

use WebARX\Support\Reader;

class ReadCache extends Reader
{
    /**
     * Read API cache and return JSON object of it
     *
     * @return mixed
     */
    public static function apiCache()
    {
        $cache = unserialize(self::readFile('cache', 'ApiToken.cache'));

        return json_decode($cache);
    }
}