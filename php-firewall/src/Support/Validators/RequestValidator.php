<?php

namespace WebARX\Support\Validators;

class RequestValidator
{
    public function validate($request, array $parameters)
    {
        foreach ($parameters as $parameter){
            if(isset($request[$parameter]) === false){
                return false;
            }
        }

        return true;
    }
}
