<?php

namespace WebARX\Support\Rules;

use WebARX\Support\Reader;

class ReadRules extends Reader
{
    /**
     * Read and return JSON object of local firewall rules
     *
     * @return mixed
     */
    public static function firewallRules()
    {
        $rules = self::readFile('data', 'firewall');
        return json_decode($rules, true);
    }

    /**
     * Read and return whitelist rules
     *
     * @return mixed
     */
    public static function whitelistRules()
    {
        return self::readFile('data', 'whitelist');
    }

    /**
     * Read and return custom blacklist
     *
     * @return mixed
     */
    public static function customBlacklist()
    {
        return self::readFile('data', 'custom-blacklist');
    }

    /**
     * Read and return custom whitelist
     *
     * @return mixed
     */
    public static function customWhitelist()
    {
        return self::readFile('data', 'custom-whitelist');
    }

    /**
     * Read and return JSON object of IP ban list
     *
     * @return mixed
     */
    public static function ipBanList()
    {
        $list = self::readFile('data', 'ip-ban');
        return json_decode($list);
    }

}
