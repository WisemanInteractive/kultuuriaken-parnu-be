<?php

namespace WebARX\Support\Rules;

use WebARX\Support\Writer;

class WriteRules extends Writer
{
    /**
     * Write rules to custom blacklist
     *
     * @param $content
     * @return bool|int
     */
    public static function customBlacklist($content)
    {
        return self::writeToFile('data', 'custom-blacklist', $content);
    }

    /**
     * Write rules to custom whitelist
     *
     * @param $content
     * @return bool|int
     */
    public static function customWhitelist($content)
    {
        return self::writeToFile('data', 'custom-whitelist', $content);
    }

    /**
     * Write group IP bans to file
     *
     * @param $content
     * @return bool|int
     */
    public static function ipBan($content)
    {
        return self::writeToFile('data', 'ip-ban', $content);
    }

    /**
     * Write to firewall rules file, should be called only from cronjob
     *
     * @param $content
     * @return bool|int
     */
    public static function firewallRules($content)
    {
        return self::writeToFile('data', 'firewall', $content);
    }

    /**
     * Write to whitelist rules file, should be called only from cronjob
     *
     * @param $content
     * @return bool|int
     */
    public static function whitelistRules($content)
    {
        return self::writeToFile('data', 'whitelist', $content);
    }
}
