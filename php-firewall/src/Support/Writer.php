<?php

namespace WebARX\Support;

defined('WDS') or define('WDS', DIRECTORY_SEPARATOR);

class Writer
{
    /**
     * Write to file and return result in boolean
     *
     * @param $folder
     * @param $file
     * @param $content
     * @return bool|int
     */
    public static function writeToFile($folder, $file, $content)
    {
        return file_put_contents(__DIR__ . WDS . '..' . WDS .'..' . WDS . $folder . WDS . $file . '.php', '<?php die; ?>' . $content);
    }
}
