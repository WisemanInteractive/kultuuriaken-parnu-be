<?php

namespace WebARX;

use Httpful\Mime;
use Httpful\Request;
use WebARX\Support\Cache\ReadCache;
use WebARX\Support\Rules\ReadRules;
use WebARX\Support\Rules\WriteRules;

class WebARX
{
    public $licenseId;
    public $licenseSecret;
    const WEBARX_APIHOST = "https://api.webarxsecurity.com";
    public $config;

    /**
     * Instance of WebARX listener
     */
    public $listener;

    /**
     * Instance of IpBan class
     *
     * @var
     */
    public $ipBan;

    /**
     * Instance of ProcessRequest class
     *
     * @var
     */
    protected $processRequest;

    /**
     * Instance of ManageOptions class
     *
     * @var
     */
    protected $manageOptions;

    /**
     * WebARX constructor.
     */
    public function __construct(array $options)
    {
        $this->config = parse_ini_file(__DIR__ . '/../config.ini');
        $this->licenseId = $options['id'];
        $this->licenseSecret = $options['secret'];
        $this->listener = new Listener();
        $this->ipBan = new IpBan($this);
        $this->processRequest = new ProcessRequest();
        $this->manageOptions = new ManageOptions($this);
    }

    /**
     *  Start WebARX
     */
    public function start()
    {
        // Verify firewall installation
        if(isset($_GET['WEBARX_VERIFY_TOKEN'])) {
            $token = $_GET['WEBARX_VERIFY_TOKEN'];

            if($this->sha1ApiKey($token)){
                print "true"; die;
            }
        }

        // Check if API wants to save settings
        $this->manageOptions->saveSettings();

        // Check if API wants to grab settings
        $this->manageOptions->getSettings();

        // If there's no token for API
        $apiToken = ReadCache::apiCache();
        if(!isset($apiToken->access_token)){
            $this->getApiAuthKey(true);
        }

        // Exit if firewall is disabled
        if(!$this->config['firewallEngine']){
            return;
        }

        // Check if IP is blacklisted
        if($this->config['customIpBlacklist']) {
            if ($this->isIPBlacklisted($this->getClientIP())) {
                print $this->processRequest->displayAccessForbidden(22);
                die;
            }
        }

        // Check dynamic IP ban according to settings
        if($this->config['dynamic_ip_ban']) {
            $this->ipBan->checkIpBan();
        }

        // check if request is whitelisted
        if($this->processRequest->isRequestWhitelisted($this) == false){
            $isRequestSafe = $this->processRequest->isRequestSafe();
            if(empty($isRequestSafe) == false){
                $attackId = $this->processRequest->isRequestSafe();
                $logger = new Logger($this);
                $logger->logIncidentLocally($attackId);
                print $this->processRequest->displayAccessForbidden($attackId);
                die;
            }
        }

        // Is site intranet, can we trigger cron-jobs remotely?
        if($this->config['is_intranet'] !== '1')
        {
            $this->listener->updateFirewallRules($this);
            $this->listener->updateGlobalWhitelist($this);
            $this->listener->uploadLogs($this);
        }

    }

    /**
     * GET api token for communication with API from ID and secret
     * @param bool $refresh
     * @return mixed
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function getApiAuthKey($refresh = false)
    {
        $apiTokenFile = __DIR__ . '/../cache/ApiToken.cache.php';

        if(file_exists($apiTokenFile) && $refresh == false) {
            $token = ReadCache::apiCache();
            return $token->access_token;
        } else {

            $results = Request::post(self::WEBARX_APIHOST . '/oauth/token')
                ->body(http_build_query(array(
                    'client_id' => $this->licenseId,
                    'client_secret' => $this->licenseSecret,
                    'grant_type' => 'client_credentials'
                )))
                ->sendsType(Mime::FORM)
                ->send();

            $token = $results->raw_body;

            $tokenFile = fopen(__DIR__ . '/../cache/ApiToken.cache.php', "w") or die("Unable to open file!");
            fwrite($tokenFile, '<?php die; ?>' . serialize($token));
            fclose($tokenFile);

            return json_decode($token)->access_token;

        }

    }

    /**
     * Check IP blacklist.
     *
     * @param string $ip The IP address of the user.
     * @return boolean Whether or not the user is blacklisted.
     */
    public function isIPBlacklisted( $ip ) {
        $dynamicBlacklists = ReadRules::ipBanList();

        $dateTimezone = new \DateTimeZone('UTC');
        $datetime = new \DateTime('now', $dateTimezone);
        $currentDatetime = $datetime->format('Y-m-d H:i:s');

        if(!is_null($dynamicBlacklists)){
            foreach($dynamicBlacklists as $dynamicBlacklist){
                $dateTimezone = new \DateTimeZone('UTC');
                $datetime = new \DateTime($dynamicBlacklist->until, $dateTimezone);
                $blacklistDatetime = $datetime->format('Y-m-d H:i:s');
                if($dynamicBlacklist->ip == $this->getClientIP() && $blacklistDatetime > $currentDatetime){
                    return true;
                }
            }
        }

        $ipRules = ReadRules::customBlacklist();
        $ipRules = explode( "\n", $ipRules );
        $ipRules = array_filter($ipRules);
        if(empty($ipRules)){
            return false;
        }

        $blacklisted = false;
        foreach ($ipRules as $ipRule){
            if(strpos($ipRule, '*') !== false){
                $blacklisted = $this->ipBan->checkWildcardRule($ip, $ipRule);
            } elseif (strpos($ipRule, '-') !== false){
                $blacklisted = $this->ipBan->checkRangeRule($ip, $ipRule);
            } elseif(strpos($ipRule, '/') !== false){
                $blacklisted = $this->ipBan->checkSubnetMaskRule($ip, $ipRule);
            } elseif($ip == $ipRule){
                $blacklisted = true;
            }

            if($blacklisted == true){
                return true;
            }
        }

        return $blacklisted;
    }

    /**
     * Dynamic IP ban for selected amount of time if there is X blocked requests
     *
     * @param $ip
     */
    public function dynamicIpBan($ip)
    {
        $currentIpBans = ReadRules::ipBanList();
        $minutesBan = $this->config['block_ip_for_minutes'];

        $dateTimezone = new \DateTimeZone('UTC');
        $date = new \DateTime('now', $dateTimezone);
        $banUntil = $date->modify("+{$minutesBan} minutes")->format('Y-m-d H:i:s');

        $banIp = array(
            'ip' => $ip,
            'until' => $banUntil
        );

        $currentIpBans[] = $banIp;

        WriteRules::ipBan(json_encode($currentIpBans));
    }
    
    /**
     * Get visitor's IP
     *
     * @return mixed
     */
    public function getClientIP()
    {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        if(array_key_exists("REMOTE_ADDR", $_SERVER)){
            return $_SERVER['REMOTE_ADDR'];
        } else {
            return '127.0.0.1';
        }
    }

    /**
     * Returns authentication key for communication with the API
     *
     * @return string
     */
    public function keyForApi()
    {
        return md5($this->licenseId . $this->licenseSecret);
    }


    /**
     * Check authentication and pull new token if provided is correct
     * @param $token
     * @return bool
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function portalAuthentication($token)
    {
        $currentToken = ReadCache::apiCache();

        if($currentToken['access_token'] === $token){
            $this->getApiAuthKey(true);
            return true;
        }

        return false;
    }

    /**
     * Check if provided SHA1 token matches local one
     * @param $token
     * @return bool
     */
    public function sha1ApiKey($token)
    {
        $localToken = sha1($this->licenseId . $this->licenseSecret);

        if($token === $localToken){
            return true;
        } else {
            return false;
        }

    }

}
