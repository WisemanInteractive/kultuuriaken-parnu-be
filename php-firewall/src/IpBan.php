<?php

namespace WebARX;

use WebARX\Support\Logs\ReadLogs;
use WebARX\Support\Rules\ReadRules;
use WebARX\Support\Rules\WriteRules;

class IpBan
{
    protected $webARX;

    public function __construct(WebARX $webARX)
    {
        $this->webARX = $webARX;
    }

    /**
     * Check if visitor's IP address is banned, blacklisted or not
     */
    public function checkIpBan()
    {
        $logs = ReadLogs::firewall();

        $dateTimezone = new \DateTimeZone('UTC');
        $date = new \DateTime('now', $dateTimezone);
        $datetime = $date->modify("-5 minutes")->format('Y-m-d H:i:s');

        $counter = 0;

        if($logs === null){
            return;
        }

        foreach ($logs as $log) {
            $dateTimezone = new \DateTimeZone('UTC');
            $logDate = new \DateTime($log['log_date'], $dateTimezone);
            $logDatetime = $logDate->format('Y-m-d H:i:s');

            if ($log['ip'] == $this->webARX->getClientIP() && $logDatetime > $datetime) {
                $counter++;
            }
        }

        $blockAfter = $this->webARX->config['block_ip_after_attempts'];

        if ($counter >= $blockAfter) {
            $this->dynamicIpBan($this->webARX->getClientIP());
        }

    }

    /**
     * Dynamic IP ban for selected amount of time if there is X blocked requests
     *
     * @param $ip
     */
    public function dynamicIpBan($ip)
    {
        $currentIpBans = ReadRules::ipBanList();
        $minutesBan = $this->webARX->config['block_ip_for_minutes'];

        $dateTimezone = new \DateTimeZone('UTC');
        $date = new \DateTime('now', $dateTimezone);
        $banUntil = $date->modify("+{$minutesBan} minutes")->format('Y-m-d H:i:s');

        $banIp = array(
            'ip' => $ip,
            'until' => $banUntil
        );

        $currentIpBans[] = $banIp;

        WriteRules::ipBan(json_encode($currentIpBans));
    }

    /**
     * CIDR notation IP block check.
     *
     * @param string $ip The IP address of the user.
     * @param string $range The range to check.
     * @return boolean Whether or not the IP is in the range.
     */
    public function checkSubnetMaskRule($ip, $range)
    {
        list( $range, $netmask ) = explode( '/', $range, 2 );
        $range_decimal = ip2long( $range );
        $ip_decimal = ip2long( $ip );
        $wildcard_decimal = pow( 2, ( 32 - $netmask ) ) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
    }

    /**
     * Wildcard IP block check.
     *
     * @param string $ip The IP address of the user.
     * @param string $rule The wildcard range to check against.
     * @return boolean Whether or not the IP is in the wilcard range.
     */
    public function checkWildcardRule($ip, $rule)
    {
        $match = explode('*', $rule);
        $match = $match[0];
        return (substr($ip, 0, strlen($match)) == $match);
    }

    /**
     * IP range block check.
     *
     * @param string $ip The IP address of the user.
     * @param string $rule The range to check against.
     * @return boolean Whether or not the IP is in the range.
     */
    public function checkRangeRule($ip, $rule)
    {
        $firstIp = explode('-', $rule);
        $secondIp = explode('-', $rule);

        $startIp = ip2long($firstIp[0]);
        $endIp = ip2long($secondIp[1]);
        $requestIp = ip2long($ip);

        return ($requestIp >= $startIp && $requestIp <= $endIp);
    }
}
