<?php

namespace WebARX;

use WebARX\Support\Validators\RequestValidator;

class Listener extends RequestValidator
{
    /**
     * Update firewall rules from the API
     * @param WebARX $webARX
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function updateFirewallRules(WebARX $webARX)
    {
        if(!$this->validate($_GET, array(
            'WEBARX-UPDATE-FIREWALL-RULES',
        ))){ return; }

        if($webARX->sha1ApiKey($_GET['WEBARX-UPDATE-FIREWALL-RULES'])){
            $token = $webARX->getApiAuthKey(true);
            $cronJob = new CronJob();
            $cronJob->pullFirewallRules($token);
        }
    }

    /**
     * Pulling global whitelist from the API
     * @param WebARX $webARX
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function updateGlobalWhitelist(WebARX $webARX)
    {
        if(!$this->validate($_GET, array(
            'WEBARX-UPDATE-WHITELIST',
        ))){ return; }

        if($webARX->sha1ApiKey($_GET['WEBARX-UPDATE-WHITELIST'])){
            $token = $webARX->getApiAuthKey(true);
            $cronJob = new CronJob();
            $cronJob->pullWhitelistRules($token);
        }
    }

    /**
     * Send logs to the portal
     * @param WebARX $webARX
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function uploadLogs(WebARX $webARX)
    {
        if(!$this->validate($_GET, array(
            'WEBARX-UPLOAD-LOGS',
        ))){ return; }

        if($webARX->sha1ApiKey($_GET['WEBARX-UPLOAD-LOGS'])){
            $token = $webARX->getApiAuthKey(true);
            $cronJob = new CronJob();
            print $cronJob->uploadLocalLogs($token); die;
        }
    }
}
