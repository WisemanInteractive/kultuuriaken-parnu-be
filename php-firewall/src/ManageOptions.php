<?php

namespace WebARX;

use WebARX\Support\Rules\ReadRules;
use WebARX\Support\Rules\WriteRules;
use WebARX\Support\Validators\RequestValidator;

class ManageOptions extends RequestValidator
{
    protected $webARX;

    public function __construct(WebARX $webARX)
    {
        $this->webARX = $webARX;
    }

    /**
     * Handle incoming settings from portal to be saved in config file
     */
    public function saveSettings()
    {
        $this->saveWhitelist();
        $this->saveIpBlacklist();

        if(!$this->validate($_GET, array(
            'WEBARX_TOKEN',
            'WEBARX_SETTINGS'
        ))){ return; }

        $webARXToken = $_GET['WEBARX_TOKEN'];
        $webARXSettings = $_GET['WEBARX_SETTINGS'];

        if ($webARXToken
            && $_SERVER['REQUEST_METHOD'] == 'GET'
            && $this->webARX->sha1ApiKey($webARXToken)
        ) {
            $settings = json_decode($webARXSettings, true);

            foreach ($settings as $key => $setting) {
                $this->editConfigFile($key, $setting);
            }

            print "success"; die;

        }

        print "failed"; die;


    }

    /**
     * Handle incoming settings from portal to grab config file
     */
    public function getSettings()
    {
        if(!$this->validate($_GET, array(
            'WEBARX_GET_SETTINGS_TOKEN'
        ))){ return; }

        $webARXToken = $_GET['WEBARX_GET_SETTINGS_TOKEN'];

        if ($webARXToken
            && $_SERVER['REQUEST_METHOD'] == 'GET'
            && $this->webARX->keyForApi() == $webARXToken
        ) {
            $configFile = parse_ini_file(__DIR__ . '/../config.ini');
            $configFile['whitelists'] = ReadRules::customWhitelist();
            $configFile['customIpBlacklists'] = ReadRules::customBlacklist();

            print json_encode($configFile); die;

        }

        return "failed";

    }

    /**
     * Handle provided key and new value to be set in config file
     *
     * @param $configKey
     * @param $value
     */
    public function editConfigFile($configKey, $value)
    {
        $lines = file(__DIR__ . '/../config.ini');
        foreach ($lines as $key => $line) {
            if (strpos($line, $configKey) !== false) {
                $lines[$key] = $configKey . ' = ' . $value . PHP_EOL;
            }
        }

        $configValue = implode('', $lines);

        file_put_contents(__DIR__ . '/../config.ini', $configValue);

    }

    public function saveWhitelist()
    {
        if(!$this->validate($_GET, array(
            'WEBARX_SAVE_CUSTOM_WHITELIST',
            'WEBARX_TOKEN'
        ))){ return; }

        if($this->webARX->sha1ApiKey($_GET['WEBARX_TOKEN'])){
            WriteRules::customWhitelist($_GET['WEBARX_SAVE_CUSTOM_WHITELIST']);
        }
    }

    /**
     * Handle for saving incoming IP blacklist from Portal
     */
    public function saveIpBlacklist()
    {
        if(!$this->validate($_GET, array(
            'WEBARX_SAVE_CUSTOM_IPBLACKLIST',
            'WEBARX_TOKEN'
        ))){ return; }

        if($this->webARX->sha1ApiKey($_GET['WEBARX_TOKEN'])){
            WriteRules::customBlacklist($_GET['WEBARX_SAVE_CUSTOM_IPBLACKLIST']);
        }
    }
    
}
