<?php

namespace WebARX;

class CaptureRequest {

    /**
     * Returns all request methods and parameters
     * @return string
     */
    public function capture() {

        if(!array_key_exists('REQUEST_URI', $_SERVER)){
            $_SERVER['REQUEST_URI'] = '/';
        }

        $data = sprintf(
            "%s %s %s %s\n",
            $this->multi_implode($_POST, ''),
            $this->multi_implode($_GET, ''),
            $this->multi_implode($_FILES, ''),
            $_SERVER['REQUEST_URI']
        );

        return $data;
    }

    private function multi_implode($array, $glue) {
        $ret = '';

        foreach ($array as $item) {
            if (is_array($item)) {
                $ret .= $this->multi_implode($item, $glue) . $glue;
            } else {
                $ret .= $item . $glue;
            }
        }

        $ret = substr($ret, 0, 0-strlen($glue));

        return $ret;
    }

}
