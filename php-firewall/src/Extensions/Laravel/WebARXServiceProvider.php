<?php

namespace WebARX\Extensions\Laravel;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use WebARX\Extensions\Laravel\Middlewares\WebARXMiddleware;

class WebARXServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        //register middlewares
        $this->registerMiddleWare($router);
    }

    /**
     * register middleware.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    protected function registerMiddleWare(Router $router)
    {
        // Append middleware to the 'web' middlware group
        $router->pushMiddlewareToGroup('web', WebARXMiddleware::class);
    }
    
}