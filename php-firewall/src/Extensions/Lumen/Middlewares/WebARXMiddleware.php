<?php

namespace WebARX\Extensions\Lumen\Middlewares;

use Closure;
use WebARX\WebARX;

class WebARXMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $webARX = new WebARX(array(
            'id' => env('WEBARX_LICENSE_ID', ''),
            'secret' => env('WEBARX_LICENSE_SECRET', '')
        ));

        $webARX->start();

        return $next($request);
    }
}
