<?php

namespace WebARX\Extensions\Lumen;

use Illuminate\Support\ServiceProvider;
use WebARX\Extensions\Lumen\Middlewares\WebARXMiddleware;

class WebARXServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerMiddleWare();
    }

    /**
     * register middleware.
     * @return void
     */
    protected function registerMiddleWare()
    {
        // Append middleware to the 'web' middlware group
        $this->app->middleware(array(
            WebARXMiddleware::class
        ));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}