<?php

namespace WebARX\Extensions\Symfony;

use WebARX\WebARX;

class WebARXBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function __construct()
    {
        $this->webarxStart();
    }

    public function webarxStart()
    {
        if (php_sapi_name() === "cli") {
            return;
        }
        $webARX = new WebARX(array(
            'id' => getenv('WEBARX_LICENSE_ID'),
            'secret' => getenv('WEBARX_LICENSE_SECRET')
        ));

        $webARX->start();
    }
}
