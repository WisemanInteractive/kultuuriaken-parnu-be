<?php

namespace WebARX;

use WebARX\Support\Logs\ReadLogs;
use WebARX\Support\Logs\WriteLogs;

class Logger
{
    protected $webarx;

    public function __construct(WebARX $webarx)
    {
        $this->webarx = $webarx;
    }

    /**
     * Save attack incident locally in file
     * @param $attackId
     */
    public function logIncidentLocally($attackId)
    {
        $ip = $this->webarx->getClientIP();
        $request_uri = isset( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : '';
        $user_agent = isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $protocol = isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : '';
        $method = isset( $_SERVER['REQUEST_METHOD'] ) ? $_SERVER['REQUEST_METHOD'] : '';

        $incident = array(
            'ip' => $ip,
            'fid' => 55 . $attackId,
            'request_uri' => urlencode($request_uri),
            'user_agent' => $user_agent,
            'protocol' => $protocol,
            'method' => $method,
            'log_date' => date('Y-m-d H:i:s'),
            'post_data' => json_encode($_POST)
        );

        if(file_exists(__DIR__ . '/../logs/local.php')) {
            $logs = ReadLogs::firewall();
            if(is_null($logs)){
                $logs = array();
            }
            array_push($logs, $incident);
            WriteLogs::firewall(json_encode($logs));
        } else {
            $logs[] = $incident;
            WriteLogs::firewall(json_encode($logs));
        }

    }

}
