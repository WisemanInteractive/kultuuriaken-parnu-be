<?php

require_once __DIR__ . '/vendor/autoload.php';

$webARX = new WebARX\WebARX(array(
    'id' => '',
    'secret' => ''
));
$cronJob = new WebARX\CronJob();

$cronJob->clearExpiredBannedIps();

$token = $webARX->getApiAuthKey(true);
$cronJob->pullFirewallRules($token);

$cronJob->pullWhitelistRules($token);
$cronJob->uploadLocalLogs($token);
