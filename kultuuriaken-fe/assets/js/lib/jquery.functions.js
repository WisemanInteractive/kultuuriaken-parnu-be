(function(history){
	var pushState = history.pushState;
	history.pushState = function(state) {
		 if (typeof history.onpushstate == "function") {
			  history.onpushstate({state: state});
		 }
		 // ... whatever else you want to do
		 // maybe call onhashchange e.handler
		 return pushState.apply(history, arguments);
	};
})(window.history);

function hoverTouchUnstick() {
    // Check if the device supports touch events
    if('ontouchstart' in document.documentElement) {
        // Loop through each stylesheet
        for(var sheetI = document.styleSheets.length - 1; sheetI >= 0; sheetI--) {
            var sheet = document.styleSheets[sheetI];
            // Verify if cssRules exists in sheet
            if(sheet.cssRules) {
                // Loop through each rule in sheet
                for(var ruleI = sheet.cssRules.length - 1; ruleI >= 0; ruleI--) {
                    var rule = sheet.cssRules[ruleI];
                    // Verify rule has selector text
                    if(rule.selectorText) {
                        // Replace hover psuedo-class with active psuedo-class
                        rule.selectorText = rule.selectorText.replace(":hover", ":active");
                    }
                }
            }
        }
    }
};

hoverTouchUnstick();


$.fn.sortTable = function(){
   var main = $(this);

   var thead = main.find("thead");
   if( thead.size() == 0 ){ return false; }
   var th = thead.find("th");

   var headers = {};

   th.each(function(e, val){
      var obj = $(this);
      var sorter = obj.attr("data-sort") == "true" ? true : false;
      var tmpArray = {
         sorter: sorter
      }
      if( !sorter ){
         headers[e] = tmpArray;   
      }

   });

   //console.log(headers);

   main.tablesorter({
      headers: headers
   });
};

$.fn.priceField = function(){

   var obj = $(this);

   obj.bind("keyup", function(){
      var value = obj.val();

      value = value.match(/[\d^,]/g,'');
      if( value ){
         value = value.join("");
      }else{
         value = "";
      }
      obj.val(value);

   });

};

$.fn.SumoSearch = function(){

   /*
   var part1 = 0;
   var part2 = -5;

   var output = '';
   for( var i = 0; i < 500; i++ ){
      part2+= 5;

      if( part2 > 55 ){
         part1++;
         part2 = 0;
      }

      var tmpPart1 = part1;
      if( tmpPart1 < 10 ){
         tmpPart1 = "0"+tmpPart1;
      }

      var tmpPart2 = part2;
      if( tmpPart2 < 10 ){
         tmpPart2 = "0"+tmpPart2;
      }
      output+= '<option value="'+tmpPart1+':'+tmpPart2+'">'+tmpPart1+':'+tmpPart2+'</option>';

      if( part1 == 24 ){ i = 1000; }

   }

   console.log( output );
   */

   if( $(this).attr("data-open") ){
      $(this).SumoSelect({search: true, searchText: '00:00', up: true});
   }else{
      $(this).SumoSelect({search: true, searchText: '00:00'});   
   }
   

};

$.fn.accordion = function(){
   var main = $(this);
   var entries = main.find(".accordion-entry");

   entries.each(function(){
      var obj = $(this);
      var title = obj.find(".accordion-title:first");
      var content = obj.find(".accordion-content:first");

      title.bind("click", function(e){
         e.preventDefault();

         obj.toggleClass("active");
      });
   });
};

$.fn.filterAccordion = function(){
	var main = $(this);
	var mainRelatives = $(".col-9.sm-12:first");

   var triggers = main.find("[rel='trigger']");

   var showMore = main.find(".show-more");

   var reset = main.find(".reset");

   var inputs = main.find("input");

   var filterTrigger = $("#filter-trigger:first");
   var closeFilter = main.find("[rel='closeFilter']");

   var searchFields = main.parents('.inline:first').find("[name='title']");
   
   searchFields.bind("change", function(){
      searchFields.val( $(this).val() );
   });
   
   triggers.each(function(){
      var obj = $(this);

      if( obj.is(".form-item") ){
         obj = obj.find(".label-title:first");
      }

      obj.bind("click", function(e){
         e.preventDefault();
         var li = $(this).parents("li:first");

         if( li.is(".active") ){
            li.removeClass("active");
         }else{
            li.addClass("active");
         }
      });
   });

   main.find("button").bind("click", function(e){
      e.preventDefault();
      closeFilter.trigger("click");
   });
   showMore.each(function(){
      var obj = $(this);
      var button = obj.find("*");
      var moreLabel = button.attr("data-show_more");
      var lessLabel = button.attr("data-show_less");

      obj.bind("click", function(e){
         e.preventDefault();
         var li = $(this).parents("li:first");

         if( li.is(".show-hidden") ){
            li.removeClass("show-hidden");
            button.text(moreLabel).removeClass("after-minus").addClass("after-plus");
         }else{
            li.addClass("show-hidden");
            button.text(lessLabel).removeClass("after-plus").addClass("after-minus");
         }
      }).bind("show", function(){
         var li = $(this).parents("li:first");
         li.addClass("show-hidden");
         button.text(lessLabel).removeClass("after-plus").addClass("after-minus");

      });
   });

   main.find("li.hidden").each(function(){
      var obj = $(this);
      var checked = obj.find("input:checked");

      if( checked.size() > 0 ){
         var li = obj.parents("li:first");
         li.children("ul").children(".show-more").trigger("show");
      }

   });

   main.find("li").each(function(){
      var obj = $(this);
      var checked = obj.children("ul").find("input:checked");

      if( checked.size() > 0 ){
         //obj.addClass("active");
      }
   });


   inputs.bind("change", function(){
      resetStatus();
   });

   function resetStatus(){
      var status = 0;

      if( inputs.filter(":checked").size() > 0 ){
         status = 1;
      }

      inputs.filter("[type='text'], [type='hidden']").each(function(){
         if( $(this).val() !== "" ){
            status = 1;
            //console.log( $(this).val() );
         }
      });

      if( status == 1 ){
         reset.show();
      }else{
         reset.hide();
      }
      
   }

   resetStatus();

   reset.find(".link:first").bind("click", function(e){
      e.preventDefault();
      main.find("input[type='checkbox'], input[type='radio']").prop("checked", false);
      main.find("input[type='text'], input[type='hidden']").val("");
		main.find(".slider-range").trigger("reset");
		
		mainRelatives.find("input[type='checkbox'], input[type='radio']").prop("checked", false);
      mainRelatives.find("input[type='text'], input[type='hidden']").val("").removeAttr("urlVal");
		mainRelatives.find(".slider-range").trigger("reset");
		
      inputs.eq(0).trigger("change");

		resetStatus();
		$(window).trigger("inputs:reset");
   });

   filterTrigger.bind("click", function(e){
      e.preventDefault();
      main.addClass("active");
   });

   closeFilter.bind("click", function(e){
      e.preventDefault();
      main.removeClass("active");
   });


};

$.fn.slideshow = function(){
   var swiper = new Swiper($(this), {
      pagination: '.swiper-pagination',
      paginationClickable: true,
      loop:true,
      nextButton: ".swiper-button-next",
      prevButton: ".swiper-button-prev"
   }); 
};

$.fn.caroussel = function(){
   var main = $(this);
   var slides = main.find(".swiper-slide").size();
   var loop = false;
   
   if (slides > 4) {
      loop = true;
   };
   
   var swiper = new Swiper($(this), {
      pagination: '.swiper-pagination',
      slidesPerView: 4,
      paginationClickable: true,
      loop:loop,
      spaceBetween: 20,
      nextButton: ".swiper-button-next",
      prevButton: ".swiper-button-prev",
      breakpoints: {
         1024: {
            slidesPerView: 2,
            spaceBetween: 20
         },
         640: {
            slidesPerView: 1,
            spaceBetween: 40
         }
      }
   });
   
   $(window).bind("resize:caroussel", function(){
      swiper.onResize();
   });
};

$.fn.accessibility = function(){
   var main = $(this);
   var inputs = main.find("input[type='radio']");
   var form = main.find("form:first");
   var buttons = main.find("[rel='submit'], [rel='reset']");
   var html = $("html:first");
   
   buttons.bind("click", function(e){
      e.preventDefault();
      
      var obj = $(this);
      var rel = obj.attr("rel");
      
      switch(rel){
         case "submit": submitSettings(); break;
         case "reset": resetSettings(); break;
      }
   });
   
   function submitSettings(){
      var tmpArray = {};
      var availableClasses = {};
      
      inputs.each(function(){
         availableClasses[ $(this).attr("name") ] = true;   
      });
      
      inputs.filter(":checked").each(function(){
         var obj = $(this);
         var name = obj.attr("name");
         var value = obj.val();
         tmpArray[name] = value;
      });
      
      
      var currentClasses = html.attr("class");
      
      //Remove available classes from HTML tag
      for( var i in availableClasses ){
         var regexp = new RegExp("\\S*" + i + "\\S*");
         currentClasses = currentClasses.replace(regexp, "").trim();
      }
      
      //Add new classes to HTML tag
      for( var i in tmpArray ){
         currentClasses += " "+i+"-"+tmpArray[i];
      }
      
      html.attr("class", currentClasses);
      
      setTimeout(function(){
         $(window).trigger("resize");
      }, 100);
      
      
      //remove cookies
      
      for( var i in availableClasses ){
         eraseCookie(i);   
      }
      
      //Add cookies
      for( var i in tmpArray ){
         createCookie(i, tmpArray[i], 365);
      }
      
   }
   
   var cookies = ["textSize", "lineHeight", "contrast"];
   
   for( var i in cookies ){
      var cookie = cookies[i];
      
      if( readCookie( cookie ) ){
         $("html").addClass(cookie+"-"+readCookie(cookie) );
         inputs.filter("[name='"+cookie+"'][value='"+readCookie(cookie)+"']").prop("checked", true);
      }
   }
   
   function resetSettings(){
      form[0].reset();
      submitSettings();
   }
   
};

$.fn.datepickerRange = function(){
   var main = $(this);

   main.addClass("datepicker-range").daterangepicker({
      autoApply: true,
      autoUpdateInput: false,
      locale: locale['default'],
      opens: "left"
   }, function(chosen_date, chosen_date2) {
      //alert(chosen_date);
      main.val(chosen_date.format('DD.MM') + " - " + chosen_date2.format('DD.MM')).attr("urlVal", chosen_date.format('DD.MM.YYYY') + " - " + chosen_date2.format('DD.MM.YYYY')).trigger("change");
   });
   
   main.bind("reset-date", function(){
      main.val("").removeAttr("urlVal").trigger("change");
   });
   
   main.parent().find('.after-calendar').click(function(){
      main.click();
   });
   
   if( main.attr("data-relatives") ){
      
      var relativesName = main.attr("data-relatives");
      
      var relatives = main.parents(".content:first").find("[name='"+relativesName+"']");
      
      relatives.bind("change", function(){
         var dateString = relatives.filter(":checked").attr("data-date").split(" - ");
         
         main.data('daterangepicker').setStartDate( dateString[0] );
			main.data('daterangepicker').setEndDate( dateString[1] );
			var outputString = dateString.join(" - ");
			var visibleString = outputString.replace(/.2[0-9]{3}/g, "");
         main.attr("urlVal", outputString).val(visibleString);
         
         main.trigger("change");
         
         //main.setStartDate( relatives.filter(":checked").attr("data-date").split(" - ")[0] ).setEndDate( relatives.filter(":checked").attr("data-date").split(" - ")[1] ).trigger("change");
         relatives.removeClass("has-changed");
         
         relatives.filter(":checked").addClass("has-changed");
         
      }).each(function(){
         
         var obj = $(this);
         var parent = obj.parent();
         
         /*
         parent.bind("click", function(e){
            var value = obj.attr("data-date");
            if( value == main.val() ){
               relatives.prop("checked", false);
               e.preventDefault();
               main.trigger("reset-date");
            }
         });
         */
         
      })
      
      
      main.bind("change", function(){
         var value = main.val();

         var foundRelative = relatives.filter("[data-date='"+value+"']");
         relatives.prop("checked", false);
         if( foundRelative.size() > 0 ){
            foundRelative.prop("checked", true);
			}
			if( value == "" ){
				var now = new Date();
				var dateString = now.getDate()+"."+(now.getMonth()+1)+"."+now.getFullYear();
				main.data('daterangepicker').setStartDate( dateString );
				main.data('daterangepicker').setEndDate( dateString );
			}
      });
      
   }
};

$.fn.datepickerSingle = function(){
   var main = $(this);

   var drops = main.attr("data-open") ? main.attr("data-open") : "down";
   
   main.daterangepicker({
      autoUpdateInput: false,
      singleDatePicker: true,
      locale: locale['default'],
      "opens": "center",
      drops: drops
   }, function(chosen_date) {
     main.val(chosen_date.format('DD.MM.YYYY')).attr("urlVal", chosen_date.format('DD.MM.YYYY')).trigger("change");
   });
   
   
   if( main.attr("data-cancel") ){
      var name = main.attr("data-cancel");
      var relatives = main.parents(".inline:first").find("input[name='"+name+"']");

      main.bind("change", function(){
         relatives.prop("checked", false);   
      });
      
      
      
   }
   main.parent().find('.after-calendar').click(function() {
      main.click();
    });
};
$.fn.resetField = function(){
	var main = $(this);
	var field = main.parent().find("input");
	
	function check(){
		if( field.val() == "" ){
			main.removeClass("visible");
			var now = new Date();
			var dateString = now.getDate()+"."+(now.getMonth()+1)+"."+now.getFullYear();
			field.data('daterangepicker').setStartDate( dateString );
			field.data('daterangepicker').setEndDate( dateString );
		}else{
			main.addClass("visible");
		}
	};

	main.bind("click", function(e){
		e.preventDefault();
		field.val("").removeAttr("urlVal").trigger("change");
	});

	field.bind("change", function(e){
		setTimeout(function(){
			check();
		}, 0);
	});

	setTimeout(function(){
		check();
	}, 200);

	$(window).bind("inputs:reset", function(){
		check();
	});
}


$.fn.inputCancel = function(){
   var main = $(this);
   var cancelName = main.attr("data-cancels");
   var inputs = main.find("input");
   var parent = main.parents(".content:first");
   var relatives = parent.find("input[name='"+cancelName+"']");
   
   inputs.bind("change", function(){
      if( inputs.filter(":checked").size() > 0 ){
         relatives.prop("checked", false);
      }
   });
   
   relatives.bind("change", function(){
      if( relatives.filter(":checked").size() > 0 ){
         inputs.prop("checked", false);
      }
   });
   
}

$.fn.autocomplete = function(){

   var main = $(this);
   var parent = main.parents(".form-item:first");
   var url = main.attr("data-url");
   var debounce;
   var delay = 300;
   var dropdown;
   var minChar = 3;
   
   main.attr('autocomplete', 'off');

   main.bind("keyup", function(){
      setDebounce();	
   });

   main.bind("blur", function(){
      clearTimeout(debounce);
      debounce = setTimeout(function(){
         closeDropdown();
      }, 200);
   });

   function setDebounce(){
      if( main.val().length < minChar ){
         if( dropdown ){
            dropdown.remove();
         }
         clearTimeout( debounce );
         return false;
      }
      clearTimeout( debounce );
      debounce = setTimeout(function(){
         getResults();
      }, delay);
   }

   function getResults(){
      var value = main.val();

      $.ajax({
         url: url+"?search="+value,
         dataType: "json",
         method: "GET",
         cache: false,
         success: function( response ){
            compileResponse( response.list );
         }
      })

   }

   function compileResponse( response ){

      parent.addClass("activated");

      if( dropdown ){
         dropdown.remove();
      }

      main.after( dropdown = $('<div class="autocomplete-dropdown"></div>') );

      var output= '<div class="autocomplete-scroll"><ul>';

      for( var i in response ){
         output+='<li><div data-value="'+response[i].title+'">'+response[i].title+'</div></li>';
      }

      output+='</ul></div>';
      dropdown.html( output );

      bindEvents();

   }

   function bindEvents(){

      dropdown.find("[data-value]").bind("click", function(e){
         var obj = $(this);
         var value = obj.attr("data-value");
         main.val(value);
         //closeDropdown();
         
         if (main.parents("form").size() > 0) {
            main.parents("form").find(".btn").trigger("click");
         }
      });
   }

   function closeDropdown(){
      
      parent.removeClass("activated");
      if( dropdown ){
         dropdown.remove();
      }
   }

};

$.fn.tabs = function(){
   var main = $(this);
   var anchors = main.find(".toolbar:first").find("a");
   var tabs = main.find(".tab");

   var activeHash = false;
   var activeTab = false;


   anchors.bind("click", function(e){
      e.preventDefault();
      var obj = $(this);
      var href = obj.attr("href");
      history.replaceState(false, false, href);
      $(window).trigger("hashchange");
   });

   $(window).bind("hashchange", function(e){
      e.preventDefault();

      var tmpHash = window.location.hash.split("#")[1];
      if( tmpHash == "" || !tmpHash ){
         tmpHash = tabs.eq(0).attr("rel");
      }

      activeHash = tmpHash;

      openTab();

   }).trigger("hashchange");

   function openTab(){
      if( activeTab ){ activeTab.hide(); }

      var tmpTab = tabs.filter("[rel='"+activeHash+"']");

      anchors.removeClass("active").filter("[href*='"+activeHash+"']").addClass("active");
      tmpTab.show();

      activeTab = tmpTab;

   }
};

$.fn.customTitle = function(){
   $(this).each(function(){

      var obj = $(this);
      var title = obj.attr("title");
      
      if( title !== "" ){
         var sourceObj = false;

         if( title.charAt(0) == "#" ){
            sourceObj = $(title+":first");
         }

         title = title.charAt(0) == "#" ? $(title+":first").html() : title;


         var debounce = false;
         var debounceTime = 0;

         var showDebounce = false;
         var showDebounceTime = 150;

         var titleObj = false;

         obj.removeAttr("title");

         var rel = obj.attr("rel");

         var openEvent = "mouseenter";
         var closeEvent = "mouseleave";

         if( rel == "click" ){
            openEvent = "click";
            closeEvent = "click";
         }


         if( openEvent == "mouseenter" ){
            obj.bind(openEvent, function(e){
               e.preventDefault();

               if( titleObj ){
                  stopClosing();	
               }else{

                  clearTimeout( showDebounce );

                  showDebounce = setTimeout(function(){
                     openTitle();
                  }, showDebounceTime);

               }
            });

            obj.bind(closeEvent, function(e){
               e.preventDefault();
               clearTimeout( showDebounce );

               if( titleObj ){
                  startClosing();	
               }

            });
         }else{
            obj.bind(openEvent, function(e){
               e.preventDefault();

               if( titleObj ){
                  startClosing();	
               }else{
                  clearTimeout( showDebounce );
                  openTitle();
               }
            });

         }


         function openTitle(){

            var addonClass = obj.attr("data-addonClass") ? obj.attr("data-addonClass") : false;

            $("body").append(titleObj = $('<div class="customTitle '+addonClass+'"><div class="customTitle-inner">'+title+'</div></div>') );

            if( sourceObj ){
               sourceObj.find("input,select,textarea").each(function(){
                  var elem = $(this);
                  var name = elem.attr("name");
                  var value = elem.val();
                  titleObj.find("[name='"+name+"']").val(value);
               });
            }

            titleObj.bindWPM();

            var  left = obj.offset().left + (obj.outerWidth()/2) - (titleObj.outerWidth()/2);


            if( addonClass == "dropdown-menu"){
               left = obj.offset().left + obj.outerWidth() - (18.75 * parseFloat( $("body").css("fontSize") ) );
            }


            obj.addClass("active");

            titleObj.css("left", left);

            var height = titleObj.outerHeight();
            var width = titleObj.outerWidth();
            var top = obj.offset().top + obj.outerHeight();
            var scrollTop = $(window).scrollTop();

            var orientation = 0;

            if( top - scrollTop + height + 15 >= $(window).height() ){
               top = obj.offset().top - obj.outerHeight() - height;
               titleObj.append('<div class="arrow-down"></div>');
               orientation = 1;
            }else{
               titleObj.prepend('<div class="arrow-up"></div>');
            }

            var arrowLeft = obj.offset().left - titleObj.offset().left + (obj.outerWidth() /2);
            /*
            var arrowLeft = obj.outerWidth() /2;

            if( arrowLeft + 30 > width ){
               arrowLeft = width / 2;
            }
            */

            //arrowLeft = 0;
            if( addonClass == "dropdown-menu"){
               titleObj.children("[class*='arrow-']").css({
                  left: parseFloat( $("body").css("fontSize") ) * 15
               });  
            }else{
               titleObj.children("[class*='arrow-']").css({
                  left: arrowLeft
               });  
            }

            titleObj.css({
               left: left,
               top: top
            });


            if( orientation == 0 ){
               titleObj.addClass("position-down");
            }else{
               titleObj.addClass("position-up");
            };

            setTimeout(function(){
               titleObj.addClass("animate");
            }, 50);

            if( obj.attr("rel") == "interact" && openEvent !== "click" ){
               debounceTime = 300;
               titleObj.bind("mouseenter", function(){
                  stopClosing();
               });

               titleObj.bind("mouseleave", function(){
                  startClosing();
               });
            }

            if( openEvent == "click" ){
               var form = obj.parents("form:first");

               titleObj.find("input,textarea").bind("change", function(){
                  var elem = $(this);
                  var name = elem.attr("name");
                  var value = elem.val();

                  form.find("[name='"+name+"']").val( value ).trigger("change");

               });

               titleObj.find("select").bind("change", function(){
                  var elem = $(this);
                  var name = elem.attr("name");
                  var value = elem.val();

                  form.find("[name='"+name+"']").val( value )[0].sumo.reload();

               });

               titleObj.find("button").bind("click", function(e){
                  e.preventDefault();

                  var elem = $(this);
                  var name = elem.attr("name");

                  form.find("[name='"+name+"']").click();

               });
            }

         }

         function startClosing(){
            clearTimeout( debounce );
            debounce = setTimeout(function(){
               closeTitle();
            }, debounceTime);
         }

         function stopClosing(){
            clearTimeout( debounce );
         }

         function closeTitle(){
            obj.removeClass("active");
            titleObj.removeClass("animate");
            setTimeout(function(){
               titleObj.remove();
               titleObj = false;	
            }, 250);

         }  
      }

   });
};

$.fn.containerClose = function(){
   var main = $(this);
   var closeBtn = main.find("[rel='close']");
   var triggered = false;

   closeBtn.bind("click", function(e){
      e.preventDefault();

      if( triggered ){ return false; }
      triggered = true;
      main.fadeOut(250, function(){
         main.remove();
      });
   });
};

$.fn.sliderRange = function(){
   var main = $(this);
   var knobs, range, bar;
   var inputs = main.parent().find("input");

   main.append(bar = $('<div class="bar"></div>') );
   bar.append(range = $('<div class="range"></div>') );
   bar.append('<div class="knob"><span></span></div><div class="knob"><span></span></div>');
   knobs = bar.find(".knob");

   var steps = main.attr("data-steps") ? main.attr("data-steps") : false;
   var unit = main.attr("data-unit") ? main.attr("data-unit") : "";

   if( steps ){
      steps = steps.split(",");
      var stepSize = 100 / steps.length;
   }else{
      var minValue = parseFloat(main.attr("data-min"));
      var maxValue = parseFloat(main.attr("data-max"));
   }


   main.bind("reset", function(){
      setPositon( knobs.eq(0), 0 );
      setPositon( knobs.eq(1), 9999 );
      inputs.val("");
   }).trigger("reset");

   knobs.bind("mousedown touchstart", function(e){
      e.preventDefault();
      var knob = $(this);

      var isTouch = e.type == "touchstart" ? true : false;

      var startX = isTouch ? e.originalEvent.touches[0].pageX : e.pageX;
      var offsetX = bar.offset().left;
      var knobWidth = knob.width();


      $(document).bind("mousemove touchmove", function(e){
         var moveX = isTouch ? e.originalEvent.touches[0].pageX : e.pageX;

         var endX = moveX - offsetX - (knobWidth/2);


         setPositon( knob, endX );

      });

      $(document).bind("mouseup touchend", function(){
         inputs.eq(0).trigger("change");
         $(document).unbind("mousemove mouseup touchend touchmove");
      });


   });

   inputs.bind("update", function(){
      var obj = $(this);

      //obj.addClass("has-changed");

      return false;

      var value = parseInt( obj.val() );
      var percentage = value / maxValue;
      var maxWidth = bar.width();
      var endX = (maxWidth*percentage) - (knobs.eq(0).width()/2);

      if( obj.attr("rel") == "min" ){
         setPositon(knobs.eq(0), endX, true);
      }else{
         setPositon(knobs.eq(1), endX, true);
      }

   });

   function setPositon(obj, pos, noTrigger){

      var objWidth = obj.width();

      var maxX = bar.width() - objWidth;

      var percentage = pos / maxX * 100;
      percentage = percentage.toFixed(2);

      if( steps ){
         var stepPos = (maxX / (steps.length-1) );
         pos = Math.floor((pos + (objWidth/2)) / stepPos) * stepPos;
      }

      if( pos < 0 ){ pos = 0; }
      else if( pos > maxX ){ pos = maxX; }

      var leftPercentage = (pos / bar.width()) * 100;

      obj.css({
         left: leftPercentage+"%"
      });

      getRange(noTrigger);
   }

   function getRange(noTrigger){
      var positions = new Array();
      var objWidth = knobs.eq(0).width();

      var maxX = bar.width() - objWidth;

      knobs.each(function(){
         var value = 0;

         var tmpArray = {};
         tmpArray.pos = $(this).position().left;


         if( steps ){
            var stepPos = (maxX / (steps.length-1) );
            var currentStep = Math.floor((tmpArray.pos + (objWidth/2)) / stepPos);
            value = steps[currentStep];
         }else{
            var percentage = (tmpArray.pos / maxX) * 100;
            var midValue = maxValue - minValue;
            percentage = midValue / 100 * percentage;
            percentage+= minValue;
            percentage = percentage.toFixed();
            value = percentage;
         }

         tmpArray.value = value;
         tmpArray.obj = $(this);

         positions.push(tmpArray);

         tmpArray.pos = ($(this).position().left / bar.width() ) *100;
         
      });


      var min = positions[0];
      var max = positions[1];
      if( min.pos > max.pos ){
         min = positions[1];
         max = positions[0];
      }

      range.css({
         left: min.pos+"%",
         width: (max.pos - min.pos)+"%"
      });

      min.obj.find("span").text(min.value + unit);
      max.obj.find("span").text(max.value + unit);

      //console.log(minValue+"-"+min.value);
      
      if( minValue == min.value ){
         inputs.filter("[rel='min']").val("").removeClass("has-changed");
      }else{
         inputs.filter("[rel='min']").val( min.value + unit).addClass("has-changed");   
      }
      
      if( maxValue == max.value ){
         inputs.filter("[rel='max']").val( max.value + unit).removeClass("has-changed");
      }else{
         inputs.filter("[rel='max']").val( max.value + unit).addClass("has-changed");
      }

   }

//   $( "#min" ).val(main).slider( "values", 0 );
//   $( "#max" ).val(main).slider( "values", 1 );                
};

$.fn.overlay = function(){
   var main = $(this);
   var href = main.attr("href");
   var html, overlay;
   var xhr = false;
   var visible = false;

   main.bind("click", function(e){
      e.preventDefault();
      openOverlay();
      getData();
   });

   if( main.attr("data-init") ){
      main.trigger("click");
   }

   function getData(newHref){

      var tmpHref = newHref ? newHref : href;
      xhr = $.ajax({
         dataType: "html",
         url: tmpHref,
         cache: false,
         success: function(response){
            html = response;
            appendContent();
         }
      });

   }

   function openOverlay(){

      var output = 	'<div class="overlay" data-close="true">';
         output+= 	'</div><!--/overlay-->';

      $("body").append(overlay = $(output) );
      $("html").addClass("overflow-hidden");

      overlay.fadeIn(250, function(){

      });


      overlay.bind("click", function(e){
         if( $(e.target).is("[data-close]") ){
            e.preventDefault();
            closeOverlay();
         }
      });

   }

   function appendContent(){
      visible = true;
      if( !visible ){
         setTimeout(function(){
            appendContent();
         }, 100);
         return false;
      }
      var output = '';
      output+=			'<div class="overlay-inline">';
         output+=			'<div class="close-x" data-close="true" ></div>';
         output+=			html;
      output+=			'</div>';

      overlay.html( output );

      setTimeout(function(){
         overlay.addClass("overlay-enter content-loaded");	
      }, 10);

      bindEvents();

   }

   function closeOverlay(){

      if( xhr ){
         xhr.abort();
         xhr = false;	
      }

      overlay.removeClass("overlay-enter");

      visible = false;
      html = "";

      setTimeout(function(){
         overlay.fadeOut(250, function(){
            $("html").removeClass("overflow-hidden");
            overlay.remove();
         });
      }, 250);
   }

   function bindEvents(){

      overlay.find("[target='overlay']").bind("click", function(e){
         e.preventDefault();
         overlay.removeClass("content-loaded");
         getData( $(this).attr("href") );
      });


      overlay.bindWPM();
   }

};

$.fn.cookiesNotification = function(){
   var main = $(this);
   var acceptBtn = main.find("[rel='accept']");
   var closeBtn = main.find("[rel='close']");
   
   if( !readCookie("cookiesNotification") ){
      main.css({
         display: "block",
         bottom: main.outerHeight() * (-1)
      });
      
      setTimeout(function(){
         main.animate({bottom:0}, {duration:500, queue:false});
      }, 1000);
   }
   
   
   
   function hideMain(){
      main.animate({bottom: main.outerHeight() * (-1)}, {duration:500, queue: false, complete: function(){
         main.remove();
      }})
   }
   
   closeBtn.bind("click", function(e){
      e.preventDefault();
      hideMain();
   });
   
   acceptBtn.bind("click", function(e){
      e.preventDefault();
      createCookie("cookiesNotification", "accepted", 365);
      hideMain();
   });
};

$.fn.googleAddress = function(){
   var main = $(this);
   var relatives = main.parent().find("[data-address]");
   var autocompleteObj;
   var autocomplete;
   var geocoder = new google.maps.Geocoder();
   
   main.parent().append( autocompleteObj = $("<div class='autocomplete-location'></div>") );

   var options = {
      types: ['geocode'],
      componentRestrictions: {country: "ee"}   
   };
   
   function init(){
      autocomplete = new google.maps.places.Autocomplete(main[0], options);
      autocomplete.addListener('place_changed', fillInAddress );
      google.maps.event.addDomListener(autocomplete, 'keydown', function(event) { 
         if (event.keyCode === 13) { 
            event.preventDefault(); 
         }
     }); 
   }
   
   function fillInAddress(){
      var address = main.val();
      var place = autocomplete.getPlace();

      relatives.val("");

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      console.log(place.address_components);
      
      relatives.each(function(){
         var obj = $(this);
         var key = obj.attr("data-address");
         
         var value = '';
         for( var i in place.address_components ){
            
            if( value !== '' ){ continue; }
            if( place.address_components[i].types[0] == key ){
               value = place.address_components[i].long_name;
            }
         }
         
         obj.val(value);
         
      });
      
      geocoder.geocode({ 'address': address }, function (results, status) {

         if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            
            relatives.filter("[data-address='lat']").val( latitude );
            relatives.filter("[data-address='lng']").val( longitude );

         }
     });

      return false;
      for (var i = 0; i < place.address_components.length; i++) {
         var addressType = place.address_components[i].types[0];
         
         
         if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            console.log(val);
            //document.getElementById(addressType).value = val;
         }
      }

   }

   init();
   
};

$.fn.characterCounter = function(){
   var main = $(this);
   var counter = main.find("[rel='counter']");
   var input = main.find("input,textarea");
   
   var maxCharacters = main.attr("data-maxCharacters") ? parseInt( main.attr("data-maxCharacters") ) : 100;
   
   function processValue(){
      var value = input.val();
      var length = value.length;
      var charactersRemaining = maxCharacters - length;
      
      if( charactersRemaining < 0 ){
         charactersRemaining = 0;
         value = value.substr(0, maxCharacters);
         input.val( value );
      }
      
      counter.text(charactersRemaining);
   }
   
   input.on("keypress keyup change mousedown", function(){
      processValue();
   });
   
   processValue();
   
};

$.fn.noResponsive = function(){
   var main = $(this);
   if( main.size() == 0 ){ return false; }
   main.parent("html").addClass("no-responsive-html");
   main.prev("head").find("meta[name=viewport]").attr("content", "width=1420px, user-scalable= 1.0");
}

$.fn.stickyTags = function(){
   var main = $(this);
   if( main.size() == 0 ){ return false; }
   
   var tagsObj = main.find(".toggler");
   
   tagsObj.bind("click", function(e){
      e.preventDefault();   
      if (main.is(".collapsed")){
         main.removeClass("collapsed");
      } else {
         main.addClass("collapsed");
      }
   });
}


$.fn.scrollToTop = function(){
   var main;
   
   var inline = $(".content:first").find(".inline:first");
   
   inline.append( main = $('<div class="scroll-to-top"></div>') );
   
   $(window).bind("resize scrollToTop", function(){
      var left = inline.width()/2;
      
      main.css({
         marginLeft: left
      });
   }).trigger("scrollToTop");
   
   $(window).bind("scroll", function(){
      
      if( $(window).scrollTop() > $(window).height() / 4 ){
         main.addClass("visible");
      }else{
         main.removeClass("visible");
      }
      if( inline.offset().top + inline.height() < $(window).scrollTop() + $(window).height() ){
         main.css({
            position: "absolute",
            bottom:20
         });
      }else{
         main.css({
            position: "fixed",
            bottom: 20
         });
      }
   });
   
   main.bind("click", function(e){
      e.preventDefault();
      $("html, body").animate({scrollTop:0}, {duration:500, queue:false});
   });
}


$.fn.bindWPM = function(){
   $wpm.bindObjects( $(this) );
}
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
};

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};

function eraseCookie(name) {
    createCookie(name,"",-1);
};

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};

!function(e){"use strict";e.fn.momomamanonomo=function(t){var n,i;return n=e.extend({},e.fn.momomamanonomo.defaults,t),i=[],n.eventProperties=e.extend({},t,n.eventProperties),this.keyup(function(e){var t=e.keyCode||e.which;n.code.length>i.push(t)||(n.code.length<i.length&&i.shift(),""+n.code==""+i&&n.cheat(e,n))}),this},e.fn.momomamanonomo.defaults={code:[38,38,40,40,37,39,37,39,66,65],eventName:"momomamanonomo",eventProperties:null,cheat:function(t,n){e(t.target).trigger(n.eventName,[n.eventProperties])}}}(jQuery);

$(function(){
   var hasFrame = false;

   $(window).momomamanonomo({
      cheat: function() {
         if( !hasFrame ){
            $("body").prepend("<div id='frame'></div>");
            hasFrame = true;
         }
      }
   });

   if( /MSIE 9/i.test(navigator.userAgent) ) {
      $(".oldBrowser-popup:first").show();
   }
   
   $("body.no-responsive").noResponsive();
   $("html").scrollToTop();
   
});
