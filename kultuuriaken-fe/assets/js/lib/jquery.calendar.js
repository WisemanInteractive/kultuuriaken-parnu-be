$.fn.calendar = function(){
   var current = new hCalendar( $(this) );
};

var hCalendar = function( main ){

   var _this = this;

   _this.options = {
      main: main,
      currentDate: new Date(),
      activeDate: new Date(),
      dataURL: main.attr("data-json"),
      templateURL: main.attr("data-template"),
      eventRemoveURL: main.attr("data-eventRemove"),
      template: ""
   }

   main.empty();

   _this.rel = {}

   moment.locale('et',{
       week : {
           dow : 1 // Monday is the first day of the week
       }
   });

   _this.initialize = function(){

      _this.getData();
   }


   _this.getData = function(){
      $.ajax({
         dataType: "json",
         url: _this.options.dataURL,
         success: function( response ){
            _this.data = response.list;
            _this.options.dayNames = response.dayNames;
            _this.options.monthNames = response.monthNames;
            _this.getTemplate();
         }
      })
   }

   _this.getTemplate = function(){
      $.ajax({
         dataType: "html",
         url: _this.options.templateURL,
         success: function( response ){

            _this.options.template = response;
            _this.updateCalendar();
         }
      })
   }

   _this.bindNavigation = function(){

      _this.rel.nextMonth = _this.options.main.find('[rel="nextMonth"]:first');
      _this.rel.previousMonth = _this.options.main.find('[rel="previousMonth"]:first');

      _this.rel.previousMonth.bind("click", function(e){
         e.preventDefault();
         _this.options.activeDate = moment( _this.options.activeDate ).subtract(1, "month").format();
         _this.updateCalendar();
      });

      _this.rel.nextMonth.bind("click", function(e){
         e.preventDefault();
         _this.options.activeDate = moment( _this.options.activeDate ).add(1, "month").format();
         _this.updateCalendar();
      });

   };

   _this.bindEvents = function(){


      var hasClick = _this.options.main.find(".has-click");

      hasClick.bind("click", function(e){
         e.preventDefault();

         var obj = $(this);

         if( obj.is(".overlay-active") ){
            return false;
         }

         hasClick.trigger("close");

         obj.addClass("overlay-active");

         var date = obj.attr("data-date");
         var dateString = obj.attr("data-dateString");

         var output = '<div class="calendar-overlay">';
            output+= '<span class="before-close" rel="close"></span>';
            output+= '<h3>'+dateString+'</h3>';

            for( var i in _this.data ){
               var current = _this.data[i];
               if( current.date == date ){
                  output+= '<div class="calendar-overlay_entry">';  
                     output+= '<div class="description">';
                        output+= '<span class="time">'+current.time+'</span>';
                        output+= '<span class="title">'+current.title+'</span>';
                        output+= '<span class="location">'+current.location+'</span>';
                     output+= '</div>';
                     output+= '<a class="after-close link secondary" data-remove="'+current.id+'">'+_this.options.main.attr("data-removeLabel")+'</a>';
                  output+= '</div><!--/calendar-overlay_entry-->';
               };
            }
         output+= '</div>';

         var calendarOverlay;
         
         obj.append( calendarOverlay = $(output) );
         
         calendarOverlay.find("[rel='close']").bind("click", function(e){
            e.preventDefault();
            calendarOverlay.remove();
            obj.removeClass("overlay-active");
            return false;
         });
         
         calendarOverlay.find("[data-remove]").bind("click", function(e){
            e.preventDefault();
            var obj = $(this);
            var id = parseInt(obj.attr("data-remove"));
            _this.removeEvent( id );
         });

      });

      hasClick.bind("close", function(){
         var obj = $(this);
         if( obj.is(".overlay-active") ){
            obj.find(".calendar-overlay").remove();
            obj.removeClass("overlay-active");
         }
      });
   };
   
   _this.removeEvent = function(id){
      $.ajax({
         url: _this.options.eventRemoveURL.replace("{id}", id),
         type: "get",
         dataType: "html",
         success: function(){
            deleteEntry();
         }
      });
      
      function deleteEntry(){
         i = _this.data.length;
         while (i--) {
            var current = _this.data[i];
            if( current.id == id ){
               
               _this.data.splice(i, 1);
            }
         };
         
//         for( var i in _this.data ){
//            var current = _this.data[i];
//            if( current.id == id ){
//               
//               _this.data.splice(i, 1);
//               break;
//            }
//         }
         
         _this.updateCalendar();
      }
      
   }

   _this.updateCalendar = function(){


      var year = moment( _this.options.activeDate ).year();
      var month = moment( _this.options.activeDate ).month();

      var currentMonth = _this.getArray();

      currentMonth = _this.combineData( currentMonth );

      var rows = _this.parseRows( currentMonth );

      swig.setDefaults({ cache: false, autoescape: false });

      var outputHTML = swig.render(_this.options.template, {locals:{
         data: rows,
         dayNames: _this.options.dayNames,
         activeMonth: _this.options.monthNames[month],
         activeYear: year
      }});

      _this.options.main.html( outputHTML );

      _this.bindNavigation();

      _this.bindEvents();

   }

   _this.combineData = function( currentMonth ){

      for( var i in currentMonth ){
         var currentDay = currentMonth[i];

         if( !currentDay.date ){
            continue;
         }else{

            for( var ii in _this.data ){
               var currentData = _this.data[ii];

               if( currentData.date == currentDay.date ){
                  if( !currentDay.list ){
                     currentDay.list = [];
                  }

                  currentDay.list.push( currentData );
               }
            }

         }
      }

      return currentMonth;

   };

   _this.parseRows = function( currentMonth ){
      var rows = {};

      var rowCounter = 0;
      var colCounter = 0;

      for( var i in currentMonth ){
         if( colCounter >= 7 ){ colCounter = 0; rowCounter++; }

         if( !rows[rowCounter] ){
            rows[rowCounter] = [];
         }

         rows[rowCounter].push( currentMonth[i]);

         colCounter++;
      }

      return rows;
   }

   _this.getToday = function(){
      var today;

      var currentDate = moment(_this.options.currentDate).format("DD.MM.YYYY");

      today = currentDate

      return today;
   }

   _this.getArray = function(){

      var year = moment( _this.options.activeDate ).year();
      var month = moment( _this.options.activeDate ).month();
      var monthClean = month;
      var totalDays = moment( _this.options.activeDate ).daysInMonth();
      var firstDay = new Date(year+"-"+(month+1)+"-"+"01");
      firstDay = moment(firstDay).weekday();

      month++;
      if( month < 10 ){
         month = "0"+month;
      }

      var rowCounter = 0;
      var colCounter = -1;


      var daysArray = [];

      var previousMonth = moment( _this.options.activeDate ).subtract(1, "month").format();
      var previousMonthDays = moment(previousMonth).daysInMonth();
      var previousMonthNumber = moment( previousMonth ).month();
      previousMonth = moment( previousMonth ).month();

      previousMonthNumber++;
      if( previousMonthNumber < 10 ){
         previousMonthNumber = "0"+previousMonthNumber;
      }

      //Get previous month
      for( var i = 0; i < firstDay; i++ ){
         var dayNumber = previousMonthDays - firstDay + i + 1;
         var dayNumberSimple = dayNumber;

         if( dayNumber < 10 ){
            dayNumber = "0"+dayNumber;
         }

         daysArray.push({
            number: dayNumberSimple,
            date: dayNumber+"."+previousMonthNumber+"."+year,
            dateString: dayNumber+". "+_this.options.monthNames[previousMonth]+" "+year
         });

         colCounter++;
      }


      var today = _this.getToday();

      //Get current month
      for( var i = 0; i < totalDays; i++ ){

         var dayNumber = i+1;
         var dayNumberSimple = dayNumber;
         if( dayNumber < 10 ){
            dayNumber = "0"+dayNumber;
         }

         var currentDate = dayNumber+"."+month+"."+year;

         var todayActive = currentDate == today ? true  : false;

         daysArray.push({
            number: dayNumberSimple,
            date: dayNumber+"."+month+"."+year,
            isToday: todayActive,
            dateString: dayNumber+". "+_this.options.monthNames[monthClean]+" "+year
         });

         if( colCounter >= 6 ){
            colCounter = 0;
         }else{
            colCounter++;   
         }

      }

      var nextMonth = moment( _this.options.activeDate ).add(1, "month").format();
      var nextMonthDays = moment(nextMonth).daysInMonth();
      var nextMonthCounter = 1;
      var nextMonthNumber = moment( nextMonth ).month();
      nextMonth = nextMonth;
      nextMonthNumber++;
      if( nextMonthNumber < 10 ){
         nextMonthNumber = "0"+nextMonthNumber;
      }

      //Get previous month
      for( var i = colCounter+1; i < 7; i++ ){

         var dayNumber = nextMonthCounter;
         var dayNumberSimple = dayNumber;

         if( dayNumber < 10 ){
            dayNumber = "0"+dayNumber;
         }

         daysArray.push({
            number: dayNumberSimple,
            date: dayNumber+"."+nextMonthNumber+"."+year,
            dateString: dayNumber+". "+_this.options.monthNames[parseInt(nextMonthNumber)-1]+" "+year
         });

         nextMonthCounter++;

         colCounter++;
      }

      return daysArray;
   }

   _this.initialize();

};