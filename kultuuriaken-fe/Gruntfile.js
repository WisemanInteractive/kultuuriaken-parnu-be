module.exports = function (grunt) {
	
	grunt.loadNpmTasks('grunt-includes');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	 grunt.loadNpmTasks('grunt-autoprefixer');
	 grunt.loadNpmTasks('grunt-contrib-connect');
	 grunt.loadNpmTasks('grunt-open');

	 var port = 8080; //Math.floor(Math.random() * (9000 - 8000 + 1)) + 8000;

	grunt.initConfig({
			
		connect: {
			server: {
				 options: {
						port: port
				 }
			}
	 },
	 
	 open : {
			dev: {
				 path: 'http://127.0.0.1:'+port
			}
	 },
      autoprefixer: {
         options:{
           browsers: ['opera >= 11', 'ff >= 3', 'chrome >= 4', 'ie >= 9'] 
         },
         default_css: {
            src: "assets/styles/default.css",
            dest: "assets/styles/default.css"
         },
      },
      
		concat: {
			components: {
				src: ['assets/js/lib/*.js'],
				dest: 'assets/js/main.js'
			}
		},
		
		uglify: {
			
			options: {
				mangle: false,
            banner: "/*                         \r\nMADE BY\r\n_               _    _       _   _   \r\n| |_ ___ ___ ___|_|  | |_ ___| |_| |_ \r\n|   | -_|   |  _| |  | '_| . | '_| '_|\r\n|_|_|___|_|_|_| |_|  |_,_|___|_,_|_,_|\r\n\r\Trinidad Wiseman\r\nhttp://www.twn.ee\r\n*/\r\n\r\n(function ($) {\r\n",
            footer: "\r\n}(jQuery));"
			},
			
			js: {
				src: ['assets/js/main.js'],
				dest: 'assets/js/main.js'
			}
			
		},

		less: {
			options: {
				compress: true
			},
			development: {
				files: {
					"assets/styles/default.css": "assets/styles/build.less"
				}
			}
			
		},
		
		includes: {
			files: {
				expand: "true",
				src: ['*.html'],
				dest: '',
				cwd: 'templates/',
				options: {
				silent: true
				}
			}
		},

		watch: {
			css: {
				files: ["assets/styles/*.less"],
				tasks: ['less', 'autoprefixer']
			},
			html: {
				files: ["templates/**/*.html"],
				tasks: ['includes']
			},
			js: {
				files: ['assets/js/lib/*.js'],
				tasks: ['concat', 'uglify'],
					options: {
					nospawn: true
				}
			}

		}
	});

	grunt.registerTask('default', ['includes', 'concat', 'uglify', 'less', 'autoprefixer', 'connect', 'open', 'watch']);
	
	grunt.registerTask('production', ['includes', 'concat', 'uglify', 'less']);

};